/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_AUDIO_ACTORAUDIO_H_
#define NOX_LOGIC_AUDIO_ACTORAUDIO_H_

#include <nox/logic/actor/Component.h>
#include <nox/app/audio/AudioBufferPositionProvider.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <map>

namespace nox {

namespace app { namespace audio
{

class AudioBuffer;

} }

namespace logic { namespace audio
{

/**
 * ActorAudio provides an interface for playing sounds.
 *
 * # JSON Description
 * ## Name
 * %ActorAudio
 *
 * ## Properties
 * - __applyTransform__:boolean - Whether or not to apply the transform of the Actor to all played sounds.
 *   	Does nothing unless a Transform-component is attached to the owning actor. Defaults to true.
 * - __applyVelocity__:boolean - Whether or not to apply the velocity of the Actor to all played sounds.
 *   	Does nothing unless an ActorPhysics-component is attached to the owning actor. Defaults to true.
 */
class ActorAudio: public actor::Component, public app::audio::AudioBufferPositionProvider
{
public:
	static const std::string NAME;

	ActorAudio();

	const actor::Component::IdType& getName() const override;

	virtual void initialize(const Json::Value& componentJsonObject) override;
	virtual void serialize(Json::Value& componentObject) override;

	void onComponentEvent(const std::shared_ptr<event::Event>& event);

	void setApplyTransform(bool applyTransform);
	void setApplyVelocity(bool applyVelocity);

	/**
	 * Play a sound file named 'fileName'.
	 *
	 * @param fileName The name of the sound file to play.
	 */
	void playSound(const std::string& fileName);

protected:
	virtual void updateAudioBuffer(app::audio::AudioBuffer* buffer) override;
	void applyTransformToBuffer(app::audio::AudioBuffer* buffer);
	void applyVelocityToBuffer(app::audio::AudioBuffer* buffer);

private:
	bool applyTransform;
	bool applyVelocity;
};

}
} }


#endif
