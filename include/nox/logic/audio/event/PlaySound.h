/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_AUDIO_PLAYSOUND_H_
#define NOX_LOGIC_AUDIO_PLAYSOUND_H_

#include <nox/logic/event/Event.h>
#include <nox/app/audio/AudioBufferPositionProvider.h>

namespace nox { namespace logic { namespace audio
{

/**
 * PlaySound can be raised from anywhere to play a sound. This interface for playing
 * sounds does not offer the same flexibility or depth of altering attributes as the
 * "direct" sound playing method does (i.e., playing directly from an audio::System).
 * However, in cases where simple positional or ambient sound need to be played, this
 * event offers a "fire-and-forget" type of interface.
 *
 * The PlaySound event interface for playing sounds does not support looping sounds,
 * as there is no reliable way to get a handle to the sound-Buffer initiated by the
 * Event. Looping tracks should be initiated by directly interfacing with audio::System.
 */
class PlaySound: public event::Event
{
public:
	static const event::Event::IdType ID;

	/**
	 * Play an ambient sound. The sound will contain no positional attributes in any regard.
	 */
	PlaySound(const std::string& fileName);

	/**
	 * Play a positional sound. The sound will follow the assigned BufferPositionProvider.
	 */
	PlaySound(const std::string& fileName, app::audio::AudioBufferPositionProvider* linkedTransform);

	const std::string& getFileName() const;
	app::audio::AudioBufferPositionProvider* getPositionProvider() const;

private:
	std::string fileName;
	app::audio::AudioBufferPositionProvider* positionProvider;
};

}
} }

#endif
