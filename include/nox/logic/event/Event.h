/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_EVENT_EVENT_H_
#define NOX_LOGIC_EVENT_EVENT_H_

#include <nox/common/compat.h>
#include <nox/logic/net/Packet.h>
#include <nox/logic/net/UserData.h>

#include <memory>
#include <string>
#include <istream>
#include <ostream>

namespace nox { namespace logic { namespace event
{

/**
 * An event in the application.
 */
class Event
{
public:
	using IdType = std::string;

	Event(IdType type);
	virtual ~Event();

	Event(Event&& other) NOX_NOEXCEPT;
	Event& operator=(Event&& other);

	Event(const Event& other) = default;
	Event& operator=(const Event& other) = default;

	/**
	 * Get the type of this event.
	 * @return The type of the event.
	 */
	inline const IdType& getType() const NOX_NOEXCEPT;

	inline bool isType(const IdType& type) const NOX_NOEXCEPT;

	bool serialize(nox::logic::net::Packet* packet) const;
	bool deSerialize(const nox::logic::net::Packet* packet);

	bool serialize(std::ostream& eventData) const;
	bool deSerialize(std::istream& eventData);

	const Event& operator>>(uint8_t& val) const;
	const Event& operator>>(bool& val) const;
	const Event& operator>>(uint32_t& val) const;
	const Event& operator>>(int32_t& val) const;
	const Event& operator>>(uint64_t &val) const;
	const Event& operator>>(int64_t& val) const;
	const Event& operator>>(float& val) const;
	const Event& operator>>(std::string &val) const;
	const Event& operator>>(std::vector<uint8_t> &val) const;
	const Event& operator>>(logic::net::UserData &val) const;

	const Event& operator<<(uint8_t val) const;
	const Event& operator<<(bool val) const;
	const Event& operator<<(uint32_t val) const;
	const Event& operator<<(int32_t val) const;
	const Event& operator<<(uint64_t val) const;
	const Event& operator<<(int64_t val) const;
	const Event& operator<<(float val) const;
	const Event& operator<<(std::string val) const;
	const Event& operator<<(std::vector<uint8_t> val) const;
	const Event& operator<<(logic::net::UserData val) const;

protected:
	virtual void doSerialize() const;
	virtual void doDeSerialize();

private:
	IdType type;

	mutable const nox::logic::net::Packet *serIpacket;
	mutable nox::logic::net::Packet *serOpacket;
	mutable std::istream *serIstream;
	mutable std::ostream *serOstream;
};


inline const Event::IdType& Event::getType() const NOX_NOEXCEPT
{
	return this->type;
}

inline bool Event::isType(const IdType& type) const NOX_NOEXCEPT
{
	return type == this->type;
}

} } }

#endif
