/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_EVENT_IBROADCASTER_H_
#define NOX_LOGIC_EVENT_IBROADCASTER_H_

#include "Event.h"

namespace nox { namespace logic { namespace event
{

class IEventListener;

class IEventBroadcaster
{
public:
	virtual ~IEventBroadcaster();

	/**
	 * Add listener to listen for specified event type.
	 * The listener will only receive events for the specified type.
	 * @param listener The listener that will listen.
	 * @param type The type of event to listen for.
	 */
	virtual void addListener(IEventListener* listener, const Event::IdType& type, const std::string& name) = 0;

	/**
	 * Remove a listener for a type.
	 * Removes the listener only from the specified type of event.
	 * @param listener The listener to be removed.
	 * @param type The type of event to remove the listener from.
	 */
	virtual void removeListener(IEventListener* listener, const Event::IdType& type) = 0;

	/**
	 * Queue an event to be triggered with next broadcastEvents call.
	 * @param event The event to be queued.
	 */
	virtual void queueEvent(std::shared_ptr<Event> event) = 0;

	/**
	 * Queue an event to be triggered with next broadcastEvents call.
	 * @param eventId The event to be queued.
	 */
	virtual void queueEvent(Event::IdType eventId) = 0;

	/**
	 * Trigger an event.
	 * This will immediately pass the event to its listeners.
	 * This should be avoided if possible (almost all times), use queueEvent in stead.
	 * @param event The event to be triggered.
	 */
	virtual void triggerEvent(const std::shared_ptr<Event>& event) = 0;

	/**
	 * Trigger an event.
	 * This will immediately pass the event to its listeners.
	 * This should be avoided if possible (almost all times), use queueEvent in stead.
	 * @param eventId The event to be triggered.
	 */
	virtual void triggerEvent(Event::IdType eventId) = 0;
};

}
} }

#endif
