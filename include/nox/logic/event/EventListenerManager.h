/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_EVENT_LISTENERMANAGER_H_
#define NOX_LOGIC_EVENT_LISTENERMANAGER_H_

#include <string>
#include <vector>
#include "Event.h"

namespace nox { namespace logic
{
namespace event
{

class IEventListener;
class IEventBroadcaster;

/**
 * Handles a listener and it's lifetime.
 * Automatically unregisters the listener when destroyed.
 *
 * The manager is setup to manage a listener listening to events from a broadcaster with setup().
 * It can then listen to events by calling addEventTypeToListenFor(), or stop listening to events
 * with removeEventTypeToListenFor().
 *
 * The manager will not start listening before startListening() is called. It can then stop listening
 * for all events with stopListening().
 *
 * @warning setup() must be called before starting to listen.
 */
class EventListenerManager
{
public:
	struct StartListening_t {};

	/**
	 * Create the manager with a name.
	 * The name is only used for debugging purposes and should be unique to the listener.
	 * @param name Name of listener.
	 */
	EventListenerManager(const std::string& name);

	~EventListenerManager();

	EventListenerManager(const EventListenerManager& other) = delete;
	EventListenerManager& operator=(const EventListenerManager& other) = delete;

	EventListenerManager(EventListenerManager&& other) NOX_NOEXCEPT;
	EventListenerManager& operator=(EventListenerManager&& other) NOX_NOEXCEPT;

	/**
	 * Setup the listener to listen from a broadcaster.
	 * This must be called before adding any events to listen for or starting listening.
	 *
	 * @param listener Listener that will listen to events.
	 * @param broadcaster Broadcaster that the listener will listen for events from.
	 */
	void setup(IEventListener* listener, IEventBroadcaster* broadcaster);

	/**
	 * Same as setup(IEventListener*, IEventBroadcaster*), but will immediately start listening.
	 *
	 * @param Tag indicating that it should start listening immediately.
	 */
	void setup(IEventListener* listener, IEventBroadcaster* broadcaster, StartListening_t);

	/**
	 * Add an event type to listen for.
	 * If the listener is currently listening, it will start listening for this type immediately,
	 * otherwise it will start listening when startListening() is called.
	 *
	 * @pre setup() must have been called.
	 */
	void addEventTypeToListenFor(const Event::IdType& type);

	/**
	 * Remove an event type from listening.
	 *
	 * @pre setup() must have been called.
	 */
	void removeEventTypeToListenFor(const Event::IdType& type);

	/**
	 * Start listening with the listener set up.
	 *
	 * @pre setup() must have been called.
	 */
	void startListening();

	/**
	 * Stop listening.
	 * Listening can be resumed with startListening(). No need to set it up.
	 *
	 * @pre setup() must have been called.
	 */
	void stopListening();

private:
	std::string name;
	bool listening;

	IEventBroadcaster* eventBroadcaster;
	IEventListener* listener;
	std::vector<Event::IdType> eventTypes;
};

}
} }

#endif
