/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_LOGIC_H_
#define NOX_LOGIC_LOGIC_H_

#include "ILogicContext.h"
#include <nox/common/types.h>
#include <nox/common/api.h>
#include <nox/common/compat.h>
#include <nox/app/ApplicationProcess.h>
#include <nox/app/log/Logger.h>
#include <nox/util/IniFile.h>
#include <vector>
#include <memory>

namespace nox { namespace logic
{

namespace event
{

class EventManager;

}

namespace physics
{

class PhysicsSimulation;

}

namespace world
{

class WorldManager;

}

namespace actor
{

class ActorIdentifier;

}

class View;

class NOX_API Logic: public app::ApplicationProcess, public ILogicContext
{
public:
	Logic();
	virtual ~Logic();

	app::IApplicationContext* getApplicationContext() const NOX_NOEXCEPT;

	void addView(std::unique_ptr<View> view);
	void setPhysics(std::unique_ptr<physics::PhysicsSimulation> physics);
	void setWorldManager(std::unique_ptr<world::WorldManager> worldHandler);

	void setClientNetworkManager(std::unique_ptr<logic::net::ClientNetworkManager> clientManager);
	void setServerNetworkManager(std::unique_ptr<logic::net::ServerNetworkManager> serverManager);

	logic::net::ClientNetworkManager* getClientNetworkManager() override;
	logic::net::ServerNetworkManager* getServerNetworkManager() override;

	void pause(bool pause) override;
	bool isPaused() const override;

	const View* findControllingView(const actor::ActorIdentifier& actorId) const override;

	event::IEventBroadcaster* getEventBroadcaster() override;
	physics::PhysicsSimulation* getPhysics() override;
	world::WorldManager* getWorldManager() override;
	app::resource::IResourceAccess* getResourceAccess() override;
	app::storage::IDataStorage* getDataStorage() override;
	app::log::Logger createLogger() override;

	util::IniFile& getSettingsFile() override;
	virtual std::string getApplicationId() const override;
	virtual std::string getApplicationVersionId() const override;

private:
	void onInit() override;
	void onUpdate(const Duration& deltaTime) override;
	void onSuccess() override;
	void onFail() override;
	void onAbort() override;
	void onUpdateFinished(const Duration& alpha) override;

	void destroy();
	void printSettingsIniErrors();

	bool paused;
	app::log::Logger log;
	mutable util::IniFile settingsFile;

	std::unique_ptr<event::EventManager> eventManager;
	std::vector<std::unique_ptr<View>> viewContainer;
	std::unique_ptr<physics::PhysicsSimulation> physics;
	std::unique_ptr<world::WorldManager> world;
	std::unique_ptr<logic::net::ClientNetworkManager> clientManager;
	std::unique_ptr<logic::net::ServerNetworkManager> serverManager;
};

} }

#endif
