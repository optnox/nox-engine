/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_PACKET_H_
#define NOX_LOGIC_NET_PACKET_H_

#include <nox/logic/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>
#include <glm/vec2.hpp>

#include <stdint.h>

namespace nox { namespace logic { namespace net {

/**
 * Packet is responsible for packing and reading data of various sizes, shapes
 * and colors into a tight binary format.
 *
 * Packet is minimally concerned with the contents of the data. Only the absolutely
 * essential overhead is included; game protocols are required to manually structure
 * the contents.
 *
 *
 * The binary format of a Packet
 * +--------+--------+---------+
 * | A (4B) |      B (8B)      |
 * +---------------------------+
 * |                           |
 * |          PAYLOAD          |
 * |                           |
 * +---------------------------+
 * A (4 bytes): The total size of the packet (minus A) in bytes.
 * B (8 bytes): The creation time of the packet in milliseconds. Do note that the
 *              creation time must be assigned externally, but it is always included.
 * P (x bytes): The payload of the packet. A binary serialization
 *              of all Packet::Fields.
 */
class Packet
{
public:
	/**
	 * Packet::Field contains a value of one of the supported types. The payload is
	 * an aggregate of all fields in a packet.
	 */
	class Field
	{
	public:
		// Wrapper type for glm::vec2. glm::vec2 is of non-POD type, and is
		// at best tricky to use in a union.
		struct WrapVec2
		{
			float x;
			float y;
		};

		/* Ensure that the used types sizes are as expected */
		static_assert(sizeof(uint32_t) == 4, "uint32_t must be 32 bit");
		static_assert(sizeof(int32_t) == 4, "int32_t must be 32 bit");
		static_assert(sizeof(uint64_t) == 8, "uint64_t must be 64 bit");
		static_assert(sizeof(int64_t) == 8, "int64_t must be 64 bit");

		// "ints" and "unsigned ints" may be commonly used by end-users. Ensure that
		// the default primitives are of the expected length.
		static_assert(sizeof(int32_t) == sizeof(int), "int is not 32 bits");
		static_assert(sizeof(uint32_t) == sizeof(unsigned int), "unsigned int is not 32 bits");

		typedef union
		{
			uint32_t u32;   // UINT32
			int32_t i32;    // INT32
			uint64_t u64;   // UINT64
			int64_t i64;    // INT64
			float f32;      // FLOAT32
			uint8_t b;      // BYTE
			char *str;      // STRING
			WrapVec2 vec2;  // VEC2

			struct
			{
				int32_t len;
				uint8_t* ptr;
			} array;			// ARRAY

			struct
			{
				// Packets are not de-serialized until they are requested.
				int32_t len;
				uint8_t* ptr;
			} packet;			// PACKET
		} FieldValue;

		// Ensure that FieldValue is a properly aligned structure.
		static_assert(sizeof(FieldValue) % 4 == 0, "FieldValue must be aligned by 4 bytes");

		Field(FieldType type);
		Field(const Field &other);
		~Field();

		/**
		 * Setter for primitive types: i32, u32, f32, b. The pointer is
		 * dereferenced to the type correlating to the active field type.
		 *
		 * @param val If non-null, the value to set.
		 * @return True on success, false on failure.
		 */
		bool set(void *val);

		bool set(glm::vec2 vec);

		/**
		 * Setter for Field-instances of type FieldType::STRING.
		 *
		 * @param strValue	The value to set.
		 * @return True if the type of the Field is STRING, false otherwise.
		 */
		bool set(std::string strValue);

		/**
		 * Setter for Field-instances of type FieldType::ARRAY and FieldType::PACKET.
		 *
		 * @param buffer The buffer.
		 * @param len The length.
		 * @return True if the type of field is ARRAY or PACKET and the assignment went
		 *   OK, false otherwise.
		 */
		bool set(const uint8_t* buffer, int len);

		bool set(const Packet* packet);
		bool set(const Packet& packet);

		const FieldValue& getValue() const;
		FieldType getType() const;

		/**
		 * Get the string value of a Field of type STRING.
		 *
		 * @return The std::string value of "value.str" if the type of the
		 *   field is STRING, an empty string otherwise.
		 */
		std::string getString() const;

		/**
		 * Get the uint8_t-array of a Field of type ARRAY. The array is pushed
		 * into a std::vector for easier usage.
		 *
		 * @return A std::vector with the contents of FieldType::array if
		 *   the type of the Field is FieldType::ARRAY, an empty
		 *   std::vector otherwise.
		 */
		std::vector<uint8_t> getArray() const;

		/**
		 * Get the glm::vec2 (wrapped in a WrapVec2) of a Field of type VEC2.
		 *
		 * @return  A glm::vec2 with the values of "value.vec2" if the type of
		 *   this field is VEC2, glm::vec2(0,0) otherwise.
		 */
		glm::vec2 getVec2() const;

	private:
		FieldType type;
		FieldValue value;
	};

public:
	Packet();
	~Packet();

	/**
	 * Serialize the Packet's fields into a binary array. The returned array
	 * must be explicitly deleted by the caller.
	 *
	 * @param[out] pktlen  The number of bytes returned is assigned to this variable.
	 *
	 * @return Byte-array if any fields could be serialized, NULL if the
	 *   Packet does not contain any serializable fields. The array
	 *   is in network byte order.
	 */
	uint8_t* serialize(int &pktlen) const;

	/**
	 * Deserialize a byte-buffer into the proper fields. The Packet-instance
	 * must have been initialized with it's correct fields beforehand.
	 *
	 * If the Packet contains any data, the data will be removed.
	 *
	 * @param buf The byte-buffer containing packed field data. The array
	 *   must be in network byte order.
	 * @param buflen The total length of the byte buffer.
	 * @return The number of bytes read from the buffer. 0 is returned if the
	 *   format of the data is invalid or upon other errors.
	 */
	int deserialize(const uint8_t* buf, size_t buflen);

	const Packet& operator>>(uint8_t &val) const;
	const Packet& operator>>(uint32_t &val) const;
	const Packet& operator>>(int32_t &val) const;
	const Packet& operator>>(uint64_t &val) const;
	const Packet& operator>>(int64_t &val) const;
	const Packet& operator>>(float &val) const;
	const Packet& operator>>(std::string &val) const;
	const Packet& operator>>(std::vector<uint8_t> &val) const;
	const Packet& operator>>(glm::vec2 &val) const;
	const Packet& operator>>(Packet &val) const;

	Packet& operator<<(uint8_t val);
	Packet& operator<<(uint32_t val);
	Packet& operator<<(int32_t val);
	Packet& operator<<(uint64_t val);
	Packet& operator<<(int64_t val);
	Packet& operator<<(float val);
	Packet& operator<<(std::string val);
	Packet& operator<<(std::vector<uint8_t> val);
	Packet& operator<<(glm::vec2 val);
	Packet& operator<<(const Packet& val);

	/**
	 * Append all fields contained within 'packet' onto 'this'. No other attributes
	 * bar 'this->fields' is altered.
	 */
	void appendFields(const Packet& packet);

	unsigned getFieldCount() const;
	const Packet::Field* getField(unsigned index) const;

	/**
	 * Reset the output stream to the first field. The input stream is
	 * unaffected by this method.
	 */
	void resetStream();

	unsigned getOutStreamPosition() const;

	// Seek to a specific index.
	bool setOutStreamPosition(unsigned position) const;

	/**
	 * Seek from the curent OutStreamPosition to a field with a specific value and
	 * FieldType. If the field at the current OutStreamIndex matches the criteria,
	 * nothing is done.
	 *
	 * @param fieldType The desired FieldType.
	 * @param value The value the field must have.
	 * @tparam T Type corresponding to the FieldType. Can not be FieldType::ARRAY.
	 *
	 * @return True if a Field of type "fieldType" with value "value" was found, false
	 *   if no such field exists or "T" does not match "fieldType". If true is returned,
	 *   the OutStreamPosition is pointing at the first field matching the requirements. If
	 *   false is returned - for any reason - the OutStreamPosition is unchanged.
	 */
	template<typename T>
	bool seekToNext(FieldType fieldType, const T& value) const;

	/**
	 * This value is only meaningful if the packet arrived from an external
	 * host.
	 */
	IPaddress getSourceAddress() const;

	void setSourceAddress(IPaddress ipaddr);

	void logContents() const;

	void setCreationTime(int64_t time) const;
	int64_t getCreationTime() const;

	void setEstimatedAge(const Duration& age);
	const Duration& getEstimatedAge() const;

private:
	std::vector<Packet::Field> fields;
	mutable unsigned outFieldIndex;
	IPaddress sourceIp;
	Duration estimatedAge;
	mutable int64_t creationTime;

	/**
	 * Ensure that a field (A) exists, and (B) holds the expected type.
	 *
	 * @throws std::runtime_error
	 *   Thrown when the fieldIndex is out of range, or the field at the
	 *   specified index is of a different type.
	 */
	const Packet::Field* verifyField(unsigned fieldIndex, FieldType expectedType) const;
};

template<typename T>
bool Packet::seekToNext(FieldType fieldType, const T& value) const
{
	const unsigned originalOutStream = this->outFieldIndex;

	if (fieldType == FieldType::ARRAY)
	{
		return false;
	}

	while (this->outFieldIndex < this->fields.size())
	{
		if (this->fields[this->outFieldIndex].getType() == fieldType)
		{
			try
			{
				T t;
				*this >> t;

				if (t == value)
				{
					// Streaming into a variable increments the outFieldIndex - revert
					// the increment.
					this->outFieldIndex--;
					return true;
				}
			}
			catch (std::runtime_error rte)
			{
				printf("Packet::seekToNext<T>(FieldType,constT&): %s\n", rte.what());
				this->outFieldIndex = originalOutStream;
				return false;
			}
		}
		else
		{
			this->outFieldIndex++;
		}
	}

	this->outFieldIndex = originalOutStream;
	return false;
}

} } }

#endif

