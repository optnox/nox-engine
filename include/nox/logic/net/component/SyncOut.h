/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SYNCOUT_H_
#define NOX_LOGIC_NET_SYNCOUT_H_

#include <nox/logic/actor/Component.h>

#include <set>

namespace nox { namespace logic { namespace net
{

class Packet;


class SyncOut: public logic::actor::Component
{
public:
	static const logic::actor::Component::IdType NAME;

	/**
	 * PACKET_HEADER is used in the transmitted packets to define the starting point of
	 * an actor-synchronization packet.
	 */
	static const std::string PACKET_HEADER;

	/**
	 * PACKET_FOOTER is used in the transmitted packets to define the ending point of
	 * an actor-synchronization packet.
	 */
	static const std::string PACKET_FOOTER;

	SyncOut();

	const IdType& getName() const override;

	void initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;

	/**
	 * Packs network data from all registered output components. All output
	 * components are asked to pack their data via Component::serialize(Packet&).
	 */
	void packNetworkData(logic::net::Packet &packet);

private:
	unsigned packetIndex;
};


} } }


#endif
