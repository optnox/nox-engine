/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SYNCIN_H_
#define NOX_LOGIC_NET_SYNCIN_H_

#include <nox/logic/actor/Component.h>

#include <set>

namespace nox { namespace logic { namespace net
{

class Packet;


/**
 * SyncIn received SyncOut packets from the remote SyncOut component and applies its sibling
 * components states transmitted across the network to its sibling components.
 *
 * SyncIn does not blindly apply component states. Only components which has been marked as
 * "synchronized" are applied. Unregistered component states are ignored.
 */
class SyncIn: public logic::actor::Component
{
public:
	static const logic::actor::Component::IdType NAME;

	SyncIn();

	const IdType& getName() const override;

	void initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;

	/**
	 * Unpack network data. Component states defined in the Packet are
	 * given to the sibling Components via Component::deSerialize(Packet&).
	 *
	 * The Packet must have read the packet's SyncId, meaning that the next
	 * element in the packet output stream is either a Component's IdType or
	 * SyncOut::PACKET_FOOTER.
	 */
	void unpackNetworkData(const logic::net::Packet &packet, unsigned packetIndex) const;

private:
	mutable unsigned lastPacketIndex;
};


} } }


#endif
