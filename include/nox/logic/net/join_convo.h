/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_COMMON_CONVO_H_
#define NOX_LOGIC_NET_COMMON_CONVO_H_

#include <nox/logic/net/Conversation.h>

#include <nox/logic/net/LocalClient.h>
#include <nox/logic/net/RemoteClient.h>

#include <nox/logic/net/ServerNetworkManager.h>
#include <nox/logic/net/ServerDelegate.h>

#include <nox/logic/net/Packet.h>
#include <nox/logic/net/protocol.h>


namespace nox { namespace logic { namespace net {

class ServerNetworkManager;
class NetworkManager;

class SocketBase;
class SocketTcp;
class SocketUdp;

namespace protocol {


/**
 * Conversation executed on the client side to connect to a remote server.
 * The conversation is initiated by the client.
 *
 * If the connection was successul, the LocalClient instance *MUST* be
 * retrieved, as it will not be deleted by the JoinServerConversation.
 *
 * If the server does not accept the connection, the passed SocketTcp instance
 * is deleted by the conversation.
 *
 * The server counterpart is AcceptClientConversation.
 */
class JoinServerConversation: public Conversation
{
public:
	static const std::string NAME;

	JoinServerConversation(NetworkManager* networkManager, nox::logic::ILogicContext *ctx, SocketTcp *tcp, UserData &user);
	virtual ~JoinServerConversation();

	const std::string& getName() const override;

	bool start() override final;
	bool update() override final;
	bool isDone() override final;

	// Was the connection successful?
	bool wasAccepted() const;

	unsigned getLobbySize() const;

	/**
	 * Retrieve the initiated UDP socket. This method only returns a non-nil
	 * value when the server accepted us.
	 */
	LocalClient* getLocalClient();

private:
	NetworkManager *netMgr;
	UserData userData;
	SocketTcp *tcpSocket;
	SocketUdp *udpSocket;
	LocalClient *localClient;
	unsigned lobbySize;

	bool init;
	bool sentTcp;
	bool sentUdp;

	bool accepted;
	bool done;
	bool error;

	bool sendTcpRequest();

	// Returns true if a TCP JOIN_RESPONSE was received, we were accepted, and the
	// UDP socket was created successfully. Other flags, such as "done" may be flagged
	// by this method to indicate to the handler that we failed.
	bool handleTcpResponse();

	bool sendUdpRequest();
	void handleUdpResponse();
};


/**
 * Conversation executed on the server side after a new host has connected to
 * the server.
 *
 * If the connection was successul, the SocketUdp instance *MUST* be
 * retrieved, as it will not be deleted by the AcceptClientConversation.
 *
 * If the server does not accept the connection, the passed SocketTcp instance
 * is deleted by the conversation.
 *
 * The client counterpart is JoinServerConversation.
 */
class AcceptClientConversation: public Conversation
{
public:
	static const std::string NAME;

	AcceptClientConversation(ServerNetworkManager *mgr, nox::logic::ILogicContext* ctx, SocketTcp *tcp);
	virtual ~AcceptClientConversation();

	const std::string& getName() const override;

	bool update() override final;
	bool isDone() override final;

	bool acceptedClient() const;
	RemoteClient* getRemoteClient();

private:
	ServerNetworkManager *serverMgr;
	SocketTcp *tcpSocket;
	SocketUdp *udpSocket;
	RemoteClient *remoteClient;
	UserData userData;

	bool recvTcpRequest;
	bool recvUdpRequest;

	bool done;
	bool accepted;

	bool handleTcpRequest();
	bool sendTcpResponse(bool response);
	bool handleUdpRequest();
	bool sendUdpResponse();
};

}
} } }

#endif
