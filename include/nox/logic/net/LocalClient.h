/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_LOCALCLIENT_H_
#define NOX_LOGIC_NET_LOCALCLIENT_H_


#include <nox/logic/net/CommunicationController.h>
#include <nox/logic/net/UserData.h>


namespace nox { namespace logic { namespace net
{

/**
 * LocalClient represents the connection FROM a client TO a server. All communication
 * related to all remote (and local) actors are passed through this object.
 */
class LocalClient : public CommunicationController
{
public:
	LocalClient(NetworkManager* netMgr, nox::logic::ILogicContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp);

	virtual bool update(const Duration& deltaTime) override;

protected:
	void onNetworkEvent(const Packet& packet);

private:
};


} } }

#endif
