/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SOCKETUDP_H_
#define NOX_LOGIC_NET_SOCKETUDP_H_

#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <string>
#include <vector>
#include <list>

namespace nox { namespace logic { namespace net {

/**
 * UDP is forbidden from sending packets larger than
 * UDP_MAX_PKT_LEN bytes in a single packet.
 */
#define UDP_MAX_PKT_LEN	 (4096)

/**
 * Socket using the UDP transport layer protocol
 *
 * Reading from UDP sockets requires the API to read into a UDPpacket
 * struct. The packet MUST be large enough to handle the incoming
 * packet - this could be problematic for large packets. The SocketUdp
 * object therefore allocates a UDPpacket with UDP_MAX_PKT_LEN upon
 * construction and uses this packet as a buffer for reading. If a
 * caller attempts to send more than UDP_MAX_PKT_LEN as a single
 * packet, the packet is divided into chunks of (UDP_MAX_PKT_LEN-1).
 */
class SocketUdp : public SocketBase {
public:
	/**
	 * Connect to a known remote host. As UDP is connectionless, it is
	 * extremely wise to specify a port on which you listen for activity.
	 *
	 * @param host	  The IP address of the remote host in host byte order
	 * @param port	  The port of the remote host in host byte order
	 * @param listPort  The UDP-port used for listening in host byte order.
	 *				  Use 0 to have it automatically assigned.
	 */
	SocketUdp(logic::ILogicContext *ctx, Uint32 host, Uint16 port, Uint16 listPort);

	/**
	 * Create a socket connected to "hostname:port", and listen on listPort.
	 */
	SocketUdp(logic::ILogicContext *ctx, std::string hostname, Uint16 port, Uint16 listPort);

	/**
	 * @param ipaddr	Connection details in network byte order
	 * @param listPort  The port to listen to in host byte order
	 */
	SocketUdp(logic::ILogicContext *ctx, IPaddress ipaddr, Uint16 listPort);

	~SocketUdp();

	virtual bool initSocket() final;
	virtual TLProtocol getTransportLayerProtocol() const final;

	/**
	 * Get the bound input-port in host-byte order.
	 */
	Uint16 getListenPort() const;

private:
	virtual SDLNet_GenericSocket getGenericSocket() const final;
	virtual uint8_t* getSocketData(int *bufferLen, IPaddress &source) final;
	virtual bool sendBuffer(const uint8_t* buffer, int bufferLen) const final;

	void cleanup() override;

	UDPsocket socket;
	Uint16 listenPort;
	UDPpacket *bufferPacket;
};

} } }

#endif

