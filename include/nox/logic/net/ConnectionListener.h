/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_CONNECTIONLISTENER_H_
#define NOX_LOGIC_NET_CONNECTIONLISTENER_H_

#include <nox/logic/net/SocketTcp.h>

#include <thread>
#include <mutex>

namespace nox { namespace logic { namespace net {


/**
 * Runs a background thread listening for new connections over TCP. The
 * ConnectionListener is running a "server socket", marking the final
 * frontier between a server and the internet.
 */
class ConnectionListener
{
public:
	typedef void (ConnectionListener::*NewConnectionCallback)(TCPsocket);
	typedef void (ConnectionListener::*ErrorCallback)(int);

	ConnectionListener(Uint16 listenPort);
	~ConnectionListener();

	/**
	 * Check if the ConnectionListener is actively listening for connections
	 * without error.
	 *
	 * @return True if everything is OK, false otherwise.
	 */
	bool isGood();

	/**
	 * Check if the ConnectionListener has received a new connection.
	 *
	 * @return True if there are waiting connections, and a call to
	 *   popNewConnection()
	 */
	bool hasNewConnection();

	/**
	 * Retrieve a new TCP socket, initialized with the oldest connection.
	 *
	 * @return A valid, connected SocetTCP if there are waiting
	 *   sockets, NULL otherwise.
	 */
	SocketTcp* popNewConnection(logic::ILogicContext *context);

	Uint16 getListenPort() const;

private:
	std::thread *thread;
	std::mutex mutex;
	Uint16 listenPort;

	/**
	 * Set by the ConnectionListener, read by the background worker thread.
	 * If it is ever true, the background thread will exit.
	 */
	bool abortThreads;

	/**
	 * Set by the background worker thread by using a pointer to setError(int).
	 */
	int threadError;

	std::vector<TCPsocket> newConnections;


	/**
	 * Called by hte background worker thread when an error has occurred.
	 */
	void setError(int error);

	/**
	 * Called by the background worker thread when a new connection is
	 * available.
	 */
	void addNewSocket(TCPsocket);
};


} } }

#endif
