/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_PROTOCOL_H_
#define NOX_LOGIC_NET_PROTOCOL_H_

#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <map>
#include <vector>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>


namespace nox { namespace logic { namespace net {

const Uint16 SERVER_BROADCAST_PORT_NO = 9090;
const Duration SERVER_BROADCAST_INTERV = Duration(std::chrono::seconds(2));

/**
 * Transport Layer Protocol global identification enumeration.
 */
enum class TLProtocol
{
	UDP,
	TCP,
};

/**
 * If there ever is any ambiguity of which direction an event, a message or a
 * packet is going, use this enum to explicitly state the direction.
 */
enum class NetworkDirection
{
	// The destination is a remote host.
	NET_OUT,

	// The destination is localhost.
	NET_IN,
};

enum class FieldType: uint8_t
{
	UNDEFINED,  // If ever encountered, an error has occurred!
	UINT32,     // Unsigned 32 bit integer
	INT32,      // Signed 32 bit integer
	UINT64,     // Unsigned 64 bit integer   (use uint64_t)
	INT64,      // Signed 64 bit integer     (use int64_t)
	FLOAT32,    // 32 bit float
	BYTE,       // 8 bit unsigned
	STRING,     // C string
	ARRAY,      // Binary array
	VEC2,       // glm::vec2

	/* Packet is a special case in which one Packet has been inserted into another Packet */
	PACKET,     // nox::logic::net::Packet
};


/**
 * Struct that defines certain attributes of a server, and how a the server
 * can be reached.
 */
struct ServerConnectionInfo
{
	std::string appId;
	std::string versionId;
	std::string serverName;
	unsigned connectedClients;
	unsigned maxClients;
	std::string hostname;
	Uint16 tcpPort;
};


namespace protocol
{

/**
 * All packets sent across the network must have an unsigned int as it's first
 * field - this is the packet identifier. If the ID is unknown, the packet must
 * be disregarded
 */
enum PacketId
{
	// The values of the PacketId enumerations should be created using your
	// favorite editors GUID-generator. The number must under no circumstances
	// consistently collide with any other 32 bit value transmitted across the
	// network.
	JOIN_REQUEST		= 0xe7e889de,
	JOIN_RESPONSE	   = 0x6cfe9c18,

	HEARTBEAT_SYN	   = 0xa0fd7379,
	HEARTBEAT_SYNACK	= 0x50354522,
	HEARTBEAT_ACK	   = 0xcc1ae05c,

	SERVER_BROADCAST	= 0x2e251827,

	EVENT_PACKET		= 0x65766e47,
	SYNC_PACKET		 = 0xe6e93e07,
};

enum DcReason
{
	UNKNOWN,

	// The client willyfully (or accidentally) closed the connection to
	// the server.
	CONNECTION_LOST,

	// The server forcefully removed the client. The client will be
	// unable to join for a defined number of seconds.
	BAN_TIMED,

	// The server forcefully removed the client. The client will not
	// be able to join without the admin of the server manually
	// removing him from the blacklist.
	BAN_INDEFINITE,
};

}


std::string fieldTypeToStr(FieldType type);

/**
 * Safe function for testing the string-length of a buffer that cannot be
 * trusted to contain a terminating null-character. The function checks
 * "maxlen" bytes for a null-character.
 *
 * @param str The untrusted string-buffer
 * @param maxlen The maximum allowed length of the string
 * @return If a null-character is found, the number of preceding
 *   bytes is returned. -1 is returned if the buffer does
 *   not contain a terminating null character.
 */
int volatileStrlen(const char *str, int maxlen);

/**
 * Convert the IP address to octal string notation. The port is ignored
 * by this function.
 *
 * The "host" memeber of ipaddr must be in network byte order - unless
 * you created the IPaddress instance yourself, this is almost always the
 * case.
 */
std::string octalIpAddress(const IPaddress &ipaddr);

/**
 * Converts an IP address on octal notation to a 32 bit Uint in network
 * or host byte order (depending on the value of convertToNetOrder).
 *
 * No warning should be required, but one is given anyway; if you even
 * remotely intend to put the returned value into an IPaddress-struct,
 * you *MUST* have first converted the returned value to network byte
 * order. 1.0.0.127 is rarely a valid host...
 *
 * Returns 0 if the format of the string is invalid.
 */
Uint32 octalToIp(std::string octal, bool convertToNetOrder=false);

int32_t readInt(const uint8_t* buf);
uint32_t readUint(const uint8_t* buf);
int64_t readLInt(const uint8_t* buf);
uint64_t readLUint(const uint8_t* buf);
float readFloat(const uint8_t* buf);

uint32_t networkDword(void *ptr);
uint64_t networkQword(void *ptr);


/**
 * Check if the system is little endian (least significant bytes fist)
 */
inline bool sysLittleEndian()
{
	int i = 1;
	uint8_t* b = (uint8_t*)&i;
	return (*b == 1);
}

/**
 * Check if the system is big endian (most significant bytes first)
 */
inline bool sysBigEndian()
{
	return !sysLittleEndian();
}

/**
 * Check if the system is using the network byte order (big endian)
 */
inline bool sysNetworkEndian()
{
	return sysBigEndian();
}


} } }

#endif

