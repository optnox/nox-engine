/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_IPACKETTRANSLATOR_H_
#define NOX_LOGIC_NET_IPACKETTRANSLATOR_H_

#include <nox/logic/event/Event.h>
#include <nox/logic/actor/Component.h>
#include <nox/logic/net/Packet.h>

namespace nox { namespace logic { namespace net {


/**
 * IPacketTranslator is an interface used to translate Packet objects into
 * Event objects. *HOW* the implementation decides to realize this interface is
 * up to the implementation, but the Event classes can directly deserialize from
 * a Packet.
 *
 * Using the method Event::deSerialize(const Packet*), and the assumption that
 * the first field of every Packet is an identifier, the implementation may
 * look like the following. The Packet interface was designed with this
 * specific implementation in mind, but any translation will obviously work.
 *
 * std::shared_ptr<Event> translatePacket(std::string eventId, const Packet *packet)
 * {
 *	  if (eventId == MyEvent::ID)
 *	  {
 *		  return new MyEvent(packet);
 *	  }
 *
 *	  return nullptr;
 * }
 */
class IPacketTranslator
{
public:
	/**
	 * The Packet is guaranteed to be of type nox::logic::net::protocol::PacketId::EVENT_PACKET. The packet's out-stream
	 * position is pointing to the start of the serialized event, meaning that the packet can be given directly to an
	 * events Event::deSerialize(const Packet*) method.
	 */
	virtual std::shared_ptr<logic::event::Event> translatePacket(std::string eventId, const Packet *packet) = 0;
};


} } }


#endif
