/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_PING_CONVO_H_
#define NOX_LOGIC_NET_PING_CONVO_H_

#include <nox/logic/net/Conversation.h>
#include <nox/logic/net/protocol.h>
#include <nox/util/Clock.h>

namespace nox { namespace logic { namespace net {

class SocketBase;
class NetworkManager;

namespace protocol
{

/**
 * Abstract, generic base class for server- and client-side heartbeat implementations.
 *
 * The round-trip time is sent as a 32-bit unsigned integer in micro-seconds, as nano-seconds will
 * result in integer overflow if the RTT is too high (> 4.29 seconds).
 */
class PingConversation: public Conversation
{
public:
	PingConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr);

	virtual bool update() override;
	bool isDone() override;
	bool wasSuccessful();

	int64_t getClockDifference() const;

protected:
	// Server side initiation packet
	bool sendHeartbeatSyn();

	// Client side response packet
	bool sendHeartbeatSynAck();

	// Server side termination packet
	bool sendHeartbeatAck();

	void setDone(bool done);
	void setSuccess(bool success);

	NetworkManager* getNetworkManager();

	void setClockDifference(int64_t milliSeconds);

private:
	NetworkManager* networkManager;
	bool done;
	bool success;
	util::Clock timeoutClock;
	int64_t clockDifference;
};

/**
 * Server-side heartbeat conversation. Initiated by sending a HEARTBEAT_SYN to the client and starting a timer.
 * When the client responds with a HEARTBEAT_SYNACK, the RTT of all connected clients is sent in HEARTBEAT_ACK.
 */
class PingInitateConversation: public PingConversation
{
public:
	static const std::string NAME;

	PingInitateConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr, ClientId remoteClient);

	const std::string& getName() const override;

	const Duration& getRoundTripTime();

	bool start() override;
	bool update() override;

private:
	util::Clock clock;
	ClientId remoteClientId;
};

/**
 * Client-side heartbeat conversation. The implementation should be initiated *after* receiving a HEARTBEAT_SYN
 * in a hierarchically superior object, as this conversation *begins* with sending a HEARTBEAT_SYNACK in the first
 * update()-call.
 */
class PingResponseConversation: public PingConversation
{
public:
	static const std::string NAME;

	PingResponseConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr);

	const std::string& getName() const override;

	bool update() override;

private:
	bool hasSentSynAck;
};


}
} } }

#endif
