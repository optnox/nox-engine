/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_BROADCASTRECEIVER_H_
#define NOX_LOGIC_NET_BROADCASTRECEIVER_H_

#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/protocol.h>

#include <nox/app/log/Logger.h>
#include <nox/logic/ILogicContext.h>

namespace nox { namespace logic { namespace net {


/**
 * BroadcastReceiver listens for broadcast messages sent by the complimentary class
 * BroadcastSender. Whenever a new server appers or an existing disappears, the
 * receiver raises a ServerBroadcastEvent.
 *
 * The BroadcastReceiver is only capable of listening on servers on the local network.
 * At most one BroadcastReceiver may be active *per computer*.
 */
class BroadcastReceiver
{
public:
	BroadcastReceiver(logic::ILogicContext *context);
	~BroadcastReceiver();

	void update(const Duration& deltaTime);

	std::vector<ServerConnectionInfo> getServerList() const;

private:
	struct ServerBroadcast
	{
		struct ServerConnectionInfo serverInfo;
		Duration age;
	};

	SocketUdp *socket;
	std::map<Uint32,ServerBroadcast*> servers;
	logic::ILogicContext *context;
	app::log::Logger logger;


	void ageBroadcasts(const Duration& deltaTime);
	void readNewBroadcasts();
	void removeOldBroadcasts();

	void raiseBroadcastEvent(ServerConnectionInfo info, bool online);
};


} } }

#endif

