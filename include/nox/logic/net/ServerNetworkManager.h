/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SERVERNETWORKMANAGER_H_
#define NOX_LOGIC_NET_SERVERNETWORKMANAGER_H_

#include <nox/logic/net/NetworkManager.h>
#include <nox/logic/net/IPacketTranslator.h>
#include <nox/logic/net/ConnectionListener.h>
#include <nox/logic/net/ServerDelegate.h>
#include <nox/logic/net/AccessList.h>


namespace nox { namespace logic { namespace net {

class RemoteClient;
class BroadcastSender;

namespace protocol
{
class AcceptClientConversation;
}


/**
 * The ServerNetworkManager (SNM) *is* a game server. There should only ever exist one SNM
 * in an application.
 */
class ServerNetworkManager: public NetworkManager
{
public:
	ServerNetworkManager(IPacketTranslator *translator, ServerDelegate *delegate);
	~ServerNetworkManager();

	bool initialize(nox::logic::ILogicContext *context) override;
	void destroy() override;
	void update(const Duration& deltaTime) override;

	/**
	 * Estimate the age of a packet by comparing the estimated difference between the senders
	 * and the local system clock.
	 */
	void estimatePacketAge(Packet& packet, ClientId sender) override final;

	/**
	 * Start the server!
	 * @param tcpListenPort The TCP port the server will listen for new connections at.
	 * @return True if all sockets were bound successfully and the server is ready to accept clients.
	 */
	bool startServer(Uint16 tcpListenPort);

	/**
	 * Enable or disable discovery broadcast. The server will broadcast it's
	 * existance every 2 seconds on the broadcast channels on the local network
	 * on port SERVER_BROADCAST_PORT_NO.
	 *
	 * The broadcast is enabled until manually disabled (i.e., full servers
	 * will broadcast unless explicitly told otherwise).
	 */
	void setEnableDiscoveryBroadcast(bool enable);

	/**
	 * Retrieve a list of all the connected clients.
	 */
	std::vector<RemoteClient*> getClients();

	/**
	 * Send a packet ONLY to a specified client.
	 */
	void sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to everyony BUT a specified client.
	 */
	void sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to every connected client.
	 */
	void sendToEveryone(const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Kick a client. The client will be able to immediately reconnect. If this is not desired,
	 * use ServerNetworkManager::banClient instead.
	 */
	void kickClient(ClientId id, std::string reason);

	/**
	 * Ban a client for a specified duration.
	 * @param duration The duration to ban the client for. Use a zero-value to
	 *   ban the client indefinitely.
	 */
	void banClient(ClientId id, const Duration &duration, std::string reason);

	bool shouldAcceptClient(IPaddress ipaddr, std::string userName);

protected:
	void sendSyncOutStateUpdates() override;

	void onClientConnected(UserData userData) override;
	void onClientDisconnected(ClientId clientId) override;

private:
	AccessList accessList;
	ServerDelegate *serverDelegate;

	bool doBroadcast;
	BroadcastSender *broadcastSender;

	ConnectionListener *serverSocket;
	std::vector<protocol::AcceptClientConversation*> acceptConvos;

	std::vector<RemoteClient*> clients;

	virtual void setLobbySize(unsigned size) override final;

	void handleNewConnections();
	void handleAcceptConvos();
	void updateRemoteClients(const Duration &deltaTime);

	void raiseDcEvent(ClientId id, protocol::DcReason dcReason, unsigned dur=0, std::string banReason="");
};

} } }


#endif
