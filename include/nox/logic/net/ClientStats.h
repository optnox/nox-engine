/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_CLIENTSTATS_H_
#define NOX_LOGIC_NET_CLIENTSTATS_H_


#include <nox/logic/net/UserData.h>


namespace nox { namespace logic { namespace net {


/**
 * ClientStats contains details about a client connected to the lobby.
 * This data is intended for use only by the game layer. This class is not
 * used in the engine for other purpopses than providing the game with
 * details on the connected clients.
 */
class ClientStats
{
public:
	explicit ClientStats(UserData ud);
	ClientStats(const ClientStats &other);

	/**
	 * Retrieve the UserData associated with this client.
	 */
	const UserData& getUserData() const;

	/**
	 * Retrieve the round-trip time.
	 */
	const Duration& getRoundTripTime() const;

	/**
	 * Set the round-trip time. Beware that changing this value "manually"
	 * may have significant side effects.
	 */
	void setRoundTripTime(Duration ms);

	/**
	 * Retrieve the clock difference between this host's local clock and the server's clock in milliseconds.
	 *
	 * The difference is ALWAYS from the perspective of the server. If the server's clock is at T10 when the
	 * client's clock is at T15, this method will *always* return 5 (or, the closest approximation to 5 we
	 * can achieve).
	 */
	long int getClockDifference() const;

	/**
	 * Set the clock difference between the client's and the server's system clock. The time must be from
	 * the perspective of the server (Tc - Ts).
	 * "Willy-nilly assignment" may have extreme side effects.
	 */
	void setClockDifference(int64_t msDifference);


private:
	UserData userData;
	Duration roundTripTime;

	bool initDifference;
	long double clockDifference;
};


} } }

#endif
