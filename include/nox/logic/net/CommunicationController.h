/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_COMMUNICATIONCONTROLLER_H_
#define NOX_LOGIC_NET_COMMUNICATIONCONTROLLER_H_

#include <nox/logic/net/SocketTcp.h>
#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/protocol.h>
#include <nox/logic/net/UserData.h>
#include <nox/logic/net/IPacketTranslator.h>
#include <nox/logic/net/DefaultPacketTranslator.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/EventListenerManager.h>
#include <nox/app/log/Logger.h>

#include <functional>

namespace nox {

namespace logic {
namespace actor {

class Actor;

}
}

namespace logic { namespace net {

class NetworkManager;
class Conversation;
class SyncOut;
class SyncIn;


// Subclasses of CommunicationController may register themselves for callbacks
// through the PacketHandler lambda-type.
using PacketHandler = std::function<void(const Packet&)>;


/**
 * CommunicationController contains general logic upon which actual protocols can
 * be realized.
 */
class CommunicationController: public logic::event::IEventListener
{
public:
	CommunicationController(NetworkManager* netMgr, nox::logic::ILogicContext *context, UserData ud, SocketTcp *tcp, SocketUdp *udp);
	virtual ~CommunicationController();

	virtual bool update(const Duration& deltaTime);

	virtual bool isGood();

	bool sendPacket(const Packet *packet, TLProtocol protocol);

	const UserData& getUserData() const;
	IPaddress getIpAddress(TLProtocol protocol);

	app::log::Logger& getLogger();

	void onEvent(const std::shared_ptr<logic::event::Event>& event) override;

	void setCustomPacketTranslator(IPacketTranslator* translator);

	/**
	 * Translate a packet into an event. If the default packet translator was able
	 * to translate the packet, "defaultTranslated" will be set to true.
	 *
	 * @param packet The packet that will be checked for event data.
	 * @param defaultTranslated Will be set to true if the default packet translator
	 *   was used to translate the packet, false if the custom translator was used or
	 *   the translation failed.
	 * @return The translated event contained within "packet".
	 */
	std::shared_ptr<logic::event::Event> translatePacket(const Packet& packet, bool &defaultTranslated);

	/**
	 * Retrieve the timestamp of the last SyncIn-state update packet retrieved. The
	 * time-stamp is the stamp assigned by the remote host, whose clock is very likely
	 * to differ from this system's clock.
	 */
	long int getLastSyncInPacketTimestamp();

	void packSyncOutUpdates(Packet& packet);

protected:
	void onSyncInUpdate(Packet& packet);

	bool disconnect(TLProtocol protocol);

	void handleIncomingPackets();

	virtual void onActorRemoved(logic::actor::Actor* actor);
	virtual void onActorCreated(logic::actor::Actor* actor);

	virtual void onConversationCompleted(Conversation *convo);
	virtual void onConversationFailed(Conversation *convo);

	/**
	 * Add a Conversation object to this CommunicationController. The Conversation
	 * will be updated until it flags itself as "done", at which point it will be
	 * deleted by the CommunicationController.
	 */
	void addConversation(Conversation* conversation);
	void updateConversations();

	nox::logic::ILogicContext* getContext();
	NetworkManager* getNetworkManager();

	SocketBase* getSocket(TLProtocol protocol);
	SocketUdp* getSocketUdp();
	SocketTcp* getSocketTcp();

	void addPacketHandler(protocol::PacketId, PacketHandler handler);

private:
	NetworkManager* networkManager;
	logic::ILogicContext *context;
	logic::event::EventListenerManager eventListener;

	SocketUdp *udpSocket;
	SocketTcp *tcpSocket;

	std::list<Conversation*> conversations;
	std::map<protocol::PacketId,PacketHandler> packetHandlers;

	UserData userData;
	app::log::Logger logger;

	IPacketTranslator *customPacketTranslator;
	DefaultPacketTranslator defaultPacketTranslator;

	std::map<SyncId,SyncOut*> syncOutComps;
	std::map<SyncId,SyncIn*> syncInComps;

	long int lastSyncInPacketTimestamp;
};


} } }

#endif
