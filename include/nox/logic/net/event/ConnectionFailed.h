/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_CONNECTIONFAILED_H_
#define NOX_LOGIC_NET_CONNECTIONFAILED_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace logic { namespace net
{

/**
 * ConnectionFailed is raised in the client application whenever the application disconnects
 * from the server, *FOR ANY REASON*. The event is also raised if the connection failed
 * for reasons such as the host being unreachable.
 */
class ConnectionFailed: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ConnectionFailed(std::string hostname, protocol::DcReason dc,
					 std::string reason = "", unsigned dur = 0);

	std::string getServerHostname() const;

	/**
	 * Return types and their meaning:
	 * UNKNOWN: The server is unreachable or the connection failed for some other reason
	 * CONNECTION_LOST: The client was connected, but the connection got interrupted.
	 * BAN_TIMED: The client was kicked or banned for a discrete time period.
	 * BAN_INDEFINITE: The client was banned indefinitely.
	 */
	protocol::DcReason getDisconnectType() const;

	/**
	 * Only valid if getDisconnectType() is BAN_TIMED or BAN_INDEFINITE.
	 */
	std::string getKickReason() const;

	/**
	 * Only valid if getDisconnectType() is BAN_TIMED.
	 */
	unsigned getBanDuration() const;

private:
	std::string serverHostname;
	protocol::DcReason dcType;
	std::string kickReason;
	uint32_t banDuration;
};

}
} }

#endif
