/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_CLIENTCONNECTED_H_
#define NOX_LOGIC_NET_CLIENTCONNECTED_H_

#include <nox/logic/event/Event.h>
#include <nox/logic/net/UserData.h>

namespace nox { namespace logic { namespace net
{

/**
 * ClientConnected is raised on client applications when a new client has connected
 * to the server the application is connected to.
 *
 * This event is also raised if the client application connects to a server which
 * had previously existing clients connected. One event is raised for each connected
 * client.
 */
class ClientConnected: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ClientConnected(UserData userData);
	ClientConnected(const Packet* packet);

	const UserData& getUserData() const;

	void doSerialize() const override;
	void doDeSerialize() override;

private:
	UserData userData;
};

}
} }

#endif
