/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_DESTROYSYNCHRONIZED_H_
#define NOX_LOGIC_NET_DESTROYSYNCHRONIZED_H_

#include <nox/logic/event/Event.h>


namespace nox { namespace logic { namespace net
{

/**
 * DestroySynchronized is raised by the network layer when the server has issued a
 * command to destroy a synchronized actor.
 *
 * This event is handled by the logic layer, and can be disregarded fully by the
 * game layer.
 */
class DestroySynchronized: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	DestroySynchronized(ClientId owner, SyncId id);
	DestroySynchronized(const Packet* packet);

	ClientId getOwningClientId() const;
	SyncId getSyncId() const;

	void doSerialize() const override;
	void doDeSerialize() override;

private:
	ClientId ownerClient;
	SyncId syncId;
};

}
} }

#endif
