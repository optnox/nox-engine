/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_NETWORKMANAGER_H_
#define NOX_LOGIC_NET_NETWORKMANAGER_H_

#include <nox/logic/net/IPacketTranslator.h>
#include <nox/logic/net/CommunicationController.h>
#include <nox/logic/net/ClientStats.h>

#include <nox/logic/ILogicContext.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/event/IEventBroadcaster.h>

namespace nox { namespace logic { namespace net {


/**
 * The NetworkManager is the prime executive in a network connection.
 */
class NetworkManager
{
public:
	/**
	 * Set the interval at which packets over the Component channel will be sent. This value should be
	 * assigned *ONCE* and equally between all possible variations of the developed application. For instance,
	 * the server and the clients should have the exact same interval. The default value is 50ms (20hz).
	 * The interval must be assigned before *ANY* actor is created.
	 */
	static void setComponentChannelPacketInterval(const Duration& duration);

	/**
	 * Retrieve the interval in which packets over the Component channel are being sent.
	 */
	static const Duration& getComponentChannelPacketInterval();


	explicit NetworkManager(IPacketTranslator *translator);

	virtual bool initialize(nox::logic::ILogicContext* context);
	virtual void destroy();
	virtual void update(const Duration& deltaTime);

	/**
	 * Estimate the age of the packet using the ClientStats' estimated clock difference. The packet
	 * must be considered "fresh-off-the-host", i.e., it just arrived from a remote host.
	 *
	 * @param packet The packet whose age must be estimated.
	 * @param sender The *original* sender of the packet - the packet may have been sent from the server,
	 *               but it most definitely did not originate there.
	 */
	virtual void estimatePacketAge(Packet& packet, ClientId sender) = 0;

	const std::vector<const ClientStats*> getConnectedClients() const;
	const ClientStats* getClientStats(ClientId client) const;
	bool isClientConnected(ClientId clientId) const;

	unsigned getClientsInLobbyCount() const;
	unsigned getLobbySize() const;

	nox::logic::ILogicContext* getContext() const;

	void updateRoundTripTime(ClientId client, const Duration& roundTripTime);
	void updateNetworkClockDifference(ClientId client, int64_t msDifference);

	/**
	 * Returns the time in milliseconds from an arbitrary point in time. This time should
	 * only be used to calculate the difference between the system clocks between the hosts
	 * of a lobby.
	 */
	int64_t getNetworkTimeMilliSeconds() const;

protected:
	virtual void sendSyncOutStateUpdates() = 0;

	void setCommunicationController(CommunicationController *comCont);

	/**
	 * Set the maximum number of players allowed in the lobby. This value can
	 * only be assigned once per NetworkManager.
	 */
	virtual void setLobbySize(unsigned size);

	app::log::Logger& getLogger();

	/**
	 * onClientConnected must be called by subclasses when a new client
	 * connects.
	 */
	virtual void onClientConnected(UserData ud);

	/**
	 * onClientDisconnected must be called by subclasses when a client
	 * disconnects, for *any* reason.
	 */
	virtual void onClientDisconnected(ClientId id);

	/**
	 * onEventPacketReceived broadcasts the event contained within the packet.
	 */
	void onEventPacketReceived(const Packet *packet);
	IPacketTranslator *packetTranslator;
private:
	static Duration componentChannelInterval;

	nox::logic::ILogicContext *context;
	app::log::Logger logger;
	CommunicationController *comControl;
	unsigned lobbySize;

	Duration syncOutTimer;

	// ClientStats are stored in a map for faster lookup when modifying the
	// contents. The cost of this comes when copying all the entires to a
	// vector in NetworkManager::getConnectedClients() - however, modifications
	// are more likely to occur frequently.
	std::map<ClientId,ClientStats*> clientStats;
};


} } }


#endif
