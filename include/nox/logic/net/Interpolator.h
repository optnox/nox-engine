/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_INTERPOLATOR
#define NOX_LOGIC_NET_INTERPOLATOR

#include <nox/common/types.h>
#include <nox/util/Timer.h>
#include <stdio.h>

namespace nox { namespace logic { namespace net
{

/**
 * The Interpolation Type defines how the 'destination' of the interpolation is
 * calculated, and what 'T getValue()' returns over time.
 */
enum class InterpolationType
{
	// The getValue() method returns '1*initial + 0*destination' initially, moving to '0*initial + 1*destination'
	// as time passes. After the timer has expired, '0*initial + 1*destination' is returned continuously.
	// 		getValue() = (1 - (elaped/duration)) * initial + (elapsed / duration) * destination
	SWAP,

	// The getValue() method will over the duration of the timer return the full value of 'destination'. After
	// the timer has expired, getValue returns static_cast<T>(0).
	// 		getValue() = initial + (deltaTime/duration) * destination
	ACCUMULATE,
};


template<typename T>
class Interpolator
{
public:
	Interpolator(InterpolationType type, const Duration& interpolationDuration);

	void setDestination(T initial, T destination);
	void setDestination(T initial, T destination, const Duration& interpolationDuration);

	T getValue(const Duration& deltaTime);
	T getInitial() const;
	T getDestination() const;

	bool isDone() const;

private:
	float getCompletionFactor() const;
	float getDeltaFactor(const Duration& deltaTime) const;

	T initial;
	T destination;

	InterpolationType type;
	util::Timer<Duration> timer;
	Duration duration;
};


template<typename T>
inline Interpolator<T>::Interpolator(InterpolationType type, const Duration& interpolationDuration):
	initial(T()),
	destination(T()),
	type(type),
	duration(interpolationDuration)
{
}

template<typename T>
inline void Interpolator<T>::setDestination(T initial, T destination)
{
	this->destination = destination;
	this->initial = initial;
	this->timer.setTimerLength(this->duration);
	this->timer.reset();
}

template<typename T>
inline void Interpolator<T>::setDestination(T initial, T destination, const Duration& interpolationDuration)
{
	this->duration = interpolationDuration;
	this->setDestination(initial, destination);
}

template<typename T>
inline T Interpolator<T>::getValue(const Duration& deltaTime)
{
	this->timer.spendTime(deltaTime);

	if (this->type == InterpolationType::SWAP)
	{
		const float factor = this->getCompletionFactor();
		return ((1.f - factor) * this->initial) + (factor * this->destination);
	}
	else if (this->type == InterpolationType::ACCUMULATE)
	{
		if (this->timer.timerReached())
		{
			return static_cast<T>(0);
		}

		const float factor = this->getDeltaFactor(deltaTime);
		return this->initial + factor * this->destination;
	}

	/* Never reached */
	return (T)0;
}

template<typename T>
inline T Interpolator<T>::getInitial() const
{
	return this->initial;
}

template<typename T>
inline T Interpolator<T>::getDestination() const
{
	return this->destination;
}

template<typename T>
inline bool Interpolator<T>::isDone() const
{
	return this->timer.isDone();
}

template<typename T>
inline float Interpolator<T>::getCompletionFactor() const
{
	const long double max = this->duration.count();
	const long double remaining = this->timer.getRemainingTime().count();

	long double factor = 1.0;

	if (max > 0)
	{
		factor -= (remaining / max);
	}

	return (float)factor;
}

template<typename T>
inline float Interpolator<T>::getDeltaFactor(const Duration& deltaTime) const
{
	const long double max = this->duration.count();
	const long double cur = deltaTime.count();

	long double factor = 1.f;

	if (max > 0)
	{
		factor = (cur / max);
	}

	return (float)factor;
}


}
} }


#endif
