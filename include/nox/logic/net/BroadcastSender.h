/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_BROADCASTSENDER_H_
#define NOX_LOGIC_NET_BROADCASTSENDER_H_

#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/Packet.h>

#include <nox/logic/ILogicContext.h>
#include <nox/app/log/Logger.h>

namespace nox { namespace logic { namespace net {

class NetworkManager;

/**
 * BroadcastSender dispatches a broadcast on a semi-regular interval. The packet
 * is broadcasted on the local network's broadcast channel (255.255.255.255) on
 * port SERVER_BROADCAST_PORT_NO.
 *
 * The packets sent contains the local hostname of the server, along with the
 * port on which clients may connect.
 */
class BroadcastSender
{
public:
	BroadcastSender(Uint16 tcpPort, logic::ILogicContext *context, NetworkManager *nv);
	~BroadcastSender();

	void setBroadcastInterval(const Duration &interval);

	void update(const Duration &deltaTime);

private:
	logic::ILogicContext *context;
	Duration interval;
	Duration accumulatedTime;
	Uint16 tcpPort;
	SocketUdp *socket;
	NetworkManager *netMgr;
	app::log::Logger logger;

	void sendBroadcast();
};

} } }

#endif
