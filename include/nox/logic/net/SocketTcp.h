/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SOCKETTCP_H_
#define NOX_LOGIC_NET_SOCKETTCP_H_

#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <string>
#include <vector>
#include <list>

namespace nox { namespace logic { namespace net {

/**
 * Socket using the TCP transport layer protocol.
 */
class SocketTcp: public SocketBase {
public:
	/**
	 * To initialize a server socket, pass 0 to "host" and a valid port no
	 * to "port". The socket will then listen on "port". To create a
	 * client socket, pass the remote host to  "host"", and the server port to
	 * "port".
	 * @param host	  The IP address to connect to in host in host byte order
	 * @param port	  The port to bind to in host byte order
	 */
	SocketTcp(logic::ILogicContext *ctx, Uint32 host, Uint16 port);
	SocketTcp(logic::ILogicContext *ctx, std::string hostname, Uint16 port);

	/**
	 * Constructor for when the connection has been initialized exeternally.
	 */
	SocketTcp(logic::ILogicContext *ctx, TCPsocket sock);

	/**
	 * @param ipaddr	Connection details in network byte order
	 */
	SocketTcp(logic::ILogicContext *ctx, IPaddress ipaddr);

	~SocketTcp();

	virtual bool initSocket() final;
	virtual TLProtocol getTransportLayerProtocol() const final;

private:
	virtual SDLNet_GenericSocket getGenericSocket() const final;
	virtual uint8_t* getSocketData(int *bufferLen, IPaddress &source) final;
	virtual bool sendBuffer(const uint8_t* buffer, int bufferLen) const final;

	void cleanup() override;

	TCPsocket socket;
};


} } }

#endif
