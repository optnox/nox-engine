/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_REMOTECLIENT_H_
#define NOX_LOGIC_NET_REMOTECLIENT_H_


#include <nox/logic/net/CommunicationController.h>
#include <nox/logic/net/UserData.h>

#include <nox/util/Clock.h>


namespace nox { namespace logic { namespace net {

class SyncIn;
class ServerDelegate;

/**
 * RemoteClient represents a connection FROM the server TO a client.
 */
class RemoteClient : public CommunicationController
{
public:
	RemoteClient(NetworkManager* netMgr, nox::logic::ILogicContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp);

	virtual bool update(const Duration& deltaTime) override;

	void setServerDelegate(ServerDelegate *delegate);

protected:
	virtual void onActorCreated(logic::actor::Actor* actor) override;
	void onNetworkEvent(const Packet& packet);

	void onConversationFailed(Conversation *convo);
	void onConversationCompleted(Conversation *convo);

private:
	static const int MAX_SUCCESSIVE_HEARTBEAT_FAILS = 5;
	static const Duration HEARTBEAT_MIN_WAIT;

	ServerDelegate *serverDelegate;

	int heartbeatSuccessiveFails;
	bool heartbeatRunning;
	util::Clock heartbeatTimer;
};


} } }

#endif
