/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_ICONVERSATION_H_
#define NOX_LOGIC_NET_ICONVERSATION_H_

#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/Packet.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/ILogicContext.h>

namespace nox { namespace logic { namespace net {

/**
 * Conversations are two or more coherent messages between two hosts flowing in
 * both directions. Conversation is an interface to abstract away the handling
 * of packets which belong strictly in a specific conversation.
 *
 * To implement a conversation, the two parts involved should use two different
 * implementations of Conversations. For example, the conversation in which a
 * client wishes to join a server: the client *starts* the conversation with a
 * JoinServerConversation-instance. The server responds in a
 * AcceptClientConversation.
 */
class Conversation
{
public:
	Conversation(SocketBase *socket, logic::ILogicContext *context);
	virtual ~Conversation() = default;

	/**
	 * Conversations should only be started from one of the engaged hosts.
	 */
	virtual bool start();

	/**
	 * Implementations do their work and waiting for packets in the update
	 * method.
	 */
	virtual bool update() = 0;

	/**
	 * The conversation should be fully discarded after the first occurrence
	 * of this method returning true.
	 */
	virtual bool isDone() = 0;

	/**
	 * Return the string-identifier which is unique to each Conversation-subtype.
	 * Implementations must guarantee that the returned value is unique to the class.
	 */
	virtual const std::string& getName() const = 0;

	app::log::Logger& getLogger();
	logic::ILogicContext* getContext();

protected:
	/**
	 * Find a packet in the SocketBase in which the first field of the packet
	 * holds the value "id".
	 */
	template<typename IdType>
	Packet* findPacket(IdType id);

	template<typename IdType>
	Packet* findPacket(SocketBase *socket, IdType id);

	void createLogger(std::string name);

	SocketBase *getSocket();

private:
	SocketBase *socket;
	logic::ILogicContext *context;

	// The Logger instance is lazily initiated.
	app::log::Logger logger;
	bool initLogger;
};


/**
 * Search in a Socket's incoming packet queue for a packet with a specific
 * first-field value. If a packet in which the first field is of type
 * "IdType" and it's value is "id", it is extracted from the socket's packet
 * queue and returned. The packet must be explicitly deleted by the
 * caller.
 */
template<typename IdType>
Packet* Conversation::findPacket(IdType id)
{
	return this->findPacket<IdType>(this->socket, id);
}

template<typename IdType>
Packet* Conversation::findPacket(SocketBase *searchSocket, IdType id)
{
	for (unsigned i = 0; i < searchSocket->getIncomingPacketQueueSize(); i++)
	{
		const Packet *peek = searchSocket->peekPacket(i);

		try
		{
			IdType type;
			*peek >> type;

			if (type == id)
			{
				Packet *pkt = searchSocket->getPacket(i);
				return pkt;
			}
		}
		catch (...)
		{
			continue;
		}
	}

	return nullptr;
}


} } }

#endif
