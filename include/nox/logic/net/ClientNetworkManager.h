/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_CLIENTNETWORKMANAGER_H_
#define NOX_LOGIC_NET_CLIENTNETWORKMANAGER_H_

#include <nox/logic/net/NetworkManager.h>
#include <nox/logic/net/LocalClient.h>
#include <nox/logic/net/IPacketTranslator.h>
#include <nox/logic/net/Conversation.h>

#include <nox/logic/event/Event.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/EventListenerManager.h>

#include <nox/logic/ILogicContext.h>


namespace nox { namespace logic { namespace net {

class BroadcastReceiver;

namespace protocol
{
class JoinServerConversation;
}


/**
 * ClientNetworkManager (CNM) is the highest-level object in the client related network hierarchy.
 * Only one CNM should exist per application. The CNM is responsible for connecting to a server,
 * notifying the application via events whenever a generic event of significance occurs, and in
 * general making sure that the active connection is running smoothly.
 */
class ClientNetworkManager: public NetworkManager, public logic::event::IEventListener
{
public:
	explicit ClientNetworkManager(IPacketTranslator *translator);
	~ClientNetworkManager();

	/**
	 * Connect to a server.
	 * @param hostname A resolable hostname ("1.2.3.4", "supergame.com", etc).
	 * @param tcpPort The TCP port the server (@hostname) is listening at.
	 * @param ud A UserData object containing data on the local user.
	 * @return True if the name was resolvable and the connection was initiated, false otherwise.
	 *   A returned "true" is *no* guarantee that the connection will succeed.
	 */
	bool connectToServer(std::string hostname, Uint16 tcpPort, UserData &ud);

	/**
	 * Connect to a server.
	 * @param ipaddr The pre-resolved IP-address of the server, including the TCP port
	 *   the server is listening at.
	 * @param ud A UserData object containing data on the local user.
	 * @return True if the name was resolvable and the connection was initiated, false otherwise.
	 *   A returned "true" is *no* guarantee that the connection will succeed.
	 */
	bool connectToServer(IPaddress ipaddr, UserData &ud);

	/**
	 * Check if the CNM is connected to a remote server.
	 */
	bool isConnected() const;

	/**
	 * Estimate the age of a packet. As the packet is nigh guaranteed to have arrived from the
	 * server, the 'sender' argument is ignored as the server re-stamps the packet with it's
	 * system clock.
	 */
	void estimatePacketAge(Packet& packet, ClientId sender) override final;

	/**
	 * Enable or disable listening for server broadcasts.
	 * @see BroadcastReceiver
	 */
	void setBroadcastListening(bool listen);

	bool initialize(nox::logic::ILogicContext *context) override;
	void destroy() override;
	void update(const Duration& deltaTime) override;

	/**
	 * Retrieve the LocalClient object representing the local application.
	 * @see LocalClient
	 */
	LocalClient* getClient();

	void onEvent(const std::shared_ptr<logic::event::Event>& event) override;

protected:
	void sendSyncOutStateUpdates();

private:
	protocol::JoinServerConversation *joinConvo;
	LocalClient *client;
	BroadcastReceiver *broadcastRecv;
	std::string serverHostname;
	bool listenToBroadcasts;

	logic::event::IEventBroadcaster *eventBroadcaster;
	logic::event::EventListenerManager eventListener;

	/**
	 * Flag set after successfully connecting to the server. If there exists a ServerNetworkManager within this
	 * process which contains a RemoteClient with the same user hash as ClientNetworkManager::client, this flag is set
	 * to true. When this flag is true, ClientNetworkManager::sendSyncOutStateUpdates() does nothing. This prevents
	 * the network layer from redundantly applying outdated component-states.
	 */
	bool loopbackConnected;

	void handleJoinConvo();

	virtual void onConnectionStarted();
	virtual void onConnectionSuccess();
	virtual void onConnectionFailed(protocol::DcReason dc, std::string reason="", unsigned dur=0);
};

} } }


#endif
