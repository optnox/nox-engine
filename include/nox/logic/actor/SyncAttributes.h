/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_NET_SYNCATTRIBUTES_H_
#define NOX_LOGIC_NET_SYNCATTRIBUTES_H_

#include <nox/logic/actor/Actor.h>
#include <nox/logic/net/protocol.h>

#include <set>

namespace nox { namespace logic { namespace actor
{

class ActorFactory;

class SyncAttributes
{
	friend class ActorFactory;
public:
	static const std::string DEFAULT_DESTROY_HANDLER;

	void addSyncComponent(const Component::IdType& idtype);
	void setDestroyHandlerName(std::string name);

	const std::set<Component::IdType>& getSyncComponents() const;
	bool getOwnedLocally() const;
	ClientId getOwnerClientId() const;
	SyncId getSyncId() const;
	std::string getDestroyHandlerName() const;

private:
	SyncAttributes(bool ownedLocally, ClientId owner, SyncId syncId);
	SyncAttributes(const SyncAttributes&) = delete;


	bool ownedLocally;
	ClientId ownerClient;
	SyncId syncId;
	std::string destroyHandler;

	std::set<Component::IdType> syncComponents;
};

}
} }

#endif
