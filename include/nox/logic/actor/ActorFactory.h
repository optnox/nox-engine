/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_FACTORY_H_
#define NOX_LOGIC_ACTOR_FACTORY_H_

#include <nox/app/log/Logger.h>
#include <nox/logic/net/event/CreateSynchronized.h>
#include <nox/logic/actor/Component.h>
#include <nox/logic/actor/ActorIdentifier.h>
#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/EventListenerManager.h>
#include <nox/util/GenericObjectFactory.h>
#include <nox/util/thread/ThreadSafeQueue.h>

#include <json/json.h>
#include <string>
#include <atomic>
#include <set>

namespace nox
{

namespace app
{

namespace resource
{

class Handle;
class IResourceAccess;

}

}

namespace logic
{

class ILogicContext;

namespace event
{

class Event;

}

namespace actor
{

class Actor;
class SyncAttributes;

/**
 * ActorFactory to create actors with their components from JSON files.
 *
 * The factory can create actors from actor definitions. A definition can be in the form of a pure JSON
 * object passed to the factory with createActor(const Json::Value&, std::string, std::vector<Actor*>&),
 * or one loaded from the ResourceCache using createActor(const std::string&, std::vector<Actor*>&), where the
 * definitions must be loaded with loadActorDefinitions beforehand.
 */
class ActorFactory: public event::IEventListener
{
public:
	ActorFactory(ILogicContext* logicContext);
	~ActorFactory();

	/**
	 * Load actor definitions from a directory in the resource cache.
	 * Definitions will be loaded recursively, and both actors and their children's definitions can be used.
	 * The definition name will be a combination of the package and the name. The package is the path to the actor,
	 * with directories separated by dots.
	 * @param actorDirectory Directory to load definitions from.
	 */
	void loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& actorDirectory);

	/**
	 * Create an empty actor.
	 */
	std::unique_ptr<Actor> createActor(const std::string& actorName);

	/**
	 * Create an actor from the specified JSON object.
	 * The calling function gets the ownership of the created actor.
	 *
	 * @param actorJsonObject The JSON object for the actor.
	 * @param definitionName The definition name to use for the actor created. Can be empty.
	 * @param[out] actorChildren All actors created as children recursively of the actor created.
	 * @return Unique pointer to the newly created actor, or nullptr if something went wrong.
	 */
	std::unique_ptr<Actor> createActor(const Json::Value& actorJsonObject, std::string definitionName, std::vector<Actor*>& actorChildren);

	/**
	 * Create an actor from the specified definition name.
	 * This definition must already be loaded by loadActorDefinitions().
	 * The calling function gets the ownership of the created actor.
	 *
	 * @param actorDefinitionName The name of the definition to create this actor from.
	 * @param[out] actorChildren All actors created as children recursively of the actor created.
	 * @return Unique pointer to the newly created actor, or nullptr if definition is not found or something else went wrong.
	 */
	std::unique_ptr<Actor> createActor(const std::string& actorDefinitionName, std::vector<Actor*>& actorChildren);

	/**
	 * Create a synchronized actor specified by a definition name. This is the only way to
	 * create a synchronized actor.
	 * The definition must already be loaded by loadActorDefinitions().
	 * The calling functions gets the ownership of the created actor.
	 *
	 * @param event The CreateSynchronized event raised.
	 * @param[out] actorChildren All actors created as children recursively of the actor created.
	 * @return Unique pointer to the newly created actor, or nullptr if
	 *		 definition is not found, no ClientId exists, or something else went wrong.
	 */
	std::unique_ptr<Actor> createActor(const logic::net::CreateSynchronized *event,
									   std::vector<Actor*>& children);

	/**
	 * Reset counter used for actor ID's.
	 * ID's from now on will start at 0.
	 */
	void resetCounter();

	/**
	 * Flush the storage of all actor definitions. They will have to be reloaded from the ResourceCache.
	 */
	void flushActorDefinitionCache();

	/**
	 * Registers a component for creation so that the factory can create that component from
	 * an actor definition.
	 *
	 * @tparam ComponentType The component class to register.
	 */
	template<class ComponentType>
	void registerComponent();

private:
	/**
	 * Data about an actor definition.
	 */
	struct ActorDefinition
	{
		//! Name of the definition.
		std::string name;

		//! Name of the package.
		std::string package;

		//! Actual definition.
		Json::Value json;

		//! Get the full definition name (pacakgename.name).
		std::string getFullName() const;
	};

	/**
	 * Create a component from the specified json object.
	 * The calling function gets the ownership of the created component.
	 *
	 * @param componentJsonObject The json object for the component
	 * @return Unique pointer to the newly created actor.
	 */
	std::unique_ptr<Component> createComponent(const std::string& name, const Json::Value& componentJsonObject);

	/**
	 * Extend a json object with the properties of another json object.
	 * All properties from the object extended from will recursively be added
	 * to the object extending to. New properties will overwrite already existing properties.
	 * @param extended The object that will be extended.
	 * @param extendWith The object to extend from.
	 */
	void extendJsonValue(Json::Value& extended, const Json::Value& extendWith);

	void extendActorComponents(Json::Value& componentArrayToExtend, const Json::Value& componentArrayExtendingFrom);

	std::vector<ActorDefinition> getActorDefinitionsFromResource(const std::shared_ptr<app::resource::Handle>& actorResourceHandle, const unsigned int rootDirPathLength);

	std::string extractNameFromDefinitionName(const std::string& definitionName) const;

	void onEvent(const std::shared_ptr<event::Event>& event);

	/** ActorFactory that maps form component name to a component intance. */
	util::GenericObjectFactory<Component::IdType, Component> componentActorFactory;

	std::unordered_map<std::string, ActorDefinition> actorDefinitions;

	std::atomic<ActorIdentifier::ValueType> nextActorId;
	ILogicContext* logicContext;
	app::log::Logger log;

	// The ActorFactory needs to know the ClientId of the local player, in order to properly
	// create Synchronized Actors with the correct attributes. The ActorFactory listens for
	// connectivity events (ConnectionSuccess, ConnectionFailed) to retrieve this info.
	ClientId localClientId;
	event::EventListenerManager listenerManager;

	// This is only set in the createActor() method creating synchronized actors,
	// and nulled out before returning. It is read from the inner createActor() method.
	//
	// The ways this field can be removed are:
	//  1. Refactor the inner createActor()
	//  2. Add private createActor(..., bool sync) methods which are called with "false" from
	//	 the existing outer createActor(...) methods and true from the createActor() which
	//	 creates synchronized actors.
	const logic::net::CreateSynchronized* createSyncEvent;
};

template<class ComponentType>
inline void ActorFactory::registerComponent()
{
	static_assert(std::is_base_of<Component, ComponentType>::value == true, "ComponentType is not derived from ActorComponent");

	this->componentActorFactory.addObjectCreation<ComponentType>(ComponentType::NAME);
}

}
}
}

#endif
