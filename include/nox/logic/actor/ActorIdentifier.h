/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_IDENTIFIER_H_
#define NOX_LOGIC_ACTOR_IDENTIFIER_H_

#include <cstdint>
#include <functional>
#include <ostream>

namespace nox { namespace logic { namespace actor
{

/**
 * Type used to identify Actors.
 */
class ActorIdentifier
{
public:
	using ValueType = std::uint32_t;

	struct hash
	{
		std::size_t operator()(const ActorIdentifier& id) const;
	};

	/**
	 * Create an invalid ActorIdentifier.
	 */
	ActorIdentifier();

	/**
	 * Create an identifier from a number.
	 *
	 * @warning This should only be used in special cases like when assigning
	 *          ID's to Actors.
	 * @warning Passing INVALID_ID_VALUE is the same as default constructing it, so rather
	 *          use the default constructor if that is what you want.
	 */
	explicit ActorIdentifier(const ValueType value);

	bool operator==(const ActorIdentifier& other) const;
	bool operator!=(const ActorIdentifier& other) const;

	bool isValid() const;

	/**
	 * Get the underlying value.
	 *
	 * @warning This is most likely not what you want. Use functions like isValid() and
	 *          the comparison operators.
	 */
	ValueType getValue() const;

	/**
	 * Get the first valid ActorIdentifier. Identifiers equal or grater than
	 * this will also be valid. Identifiers less than this will be invalid.
	 */
	static ActorIdentifier firstValid();

private:
	static const ValueType INVALID_ID_VALUE = 0;

	ValueType value;
};

/**
 * Overloaded ostream operator for ActorIdentifier.
 */
template <class CharT, class Traits>
std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, const ActorIdentifier& id);


template <class CharT, class Traits>
inline std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, const ActorIdentifier& id)
{
	return os << id.getValue();
}

} } }

namespace std {

template<>
struct hash<nox::logic::actor::ActorIdentifier>
{
	inline size_t operator()(const nox::logic::actor::ActorIdentifier& id) const
	{
		return hash<nox::logic::actor::ActorIdentifier::ValueType>()(id.getValue());
	}
};

}

#endif
