/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef NOX_LOGIC_PHYSICS_PARTICLEEMITTER_H_
#define NOX_LOGIC_PHYSICS_PARTICLEEMITTER_H_

#include <nox/common/types.h>
#include <nox/util/json_utils.h>
#include <nox/logic/physics/box2d/box2d_utils.h>
#include <nox/logic/ILogicContext.h>
#include <nox/util/RandomType.h>

#include <glm/vec2.hpp>
#include <Box2D/Box2D.h>

#include <string>
#include <vector>

namespace nox {

namespace logic {

namespace physics {


struct Particle
{
public:
	std::string name;
	util::RandomType<float> velocityScalar = util::RandomType<float>(1.0f, 0.0f);
	util::RandomType<float> r = util::RandomType<float>(1.0f, 0.0f);
	util::RandomType<float> g = util::RandomType<float>(1.0f, 0.0f);
	util::RandomType<float> b = util::RandomType<float>(1.0f, 0.0f);
	util::RandomType<float> a = util::RandomType<float>(1.0f, 0.0f);
	util::RandomType<float> lifetime = util::RandomType<float>(1.0f, 0.0f);
	b2ParticleFlag flags = b2_waterParticle;
};

// Emit particles from a circular region.
class ParticleEmitter {
public:
	ParticleEmitter(std::string name, ILogicContext* logicContext);
	~ParticleEmitter();

	void update(const Duration& deltaTime, float rotation = 0.0f, glm::vec2 position = glm::vec2(0.0f,0.0f));

	void enable();
	void disable();

	// Center of the emitter
	void setOffset(float x, float y);
	b2Vec2* getOffset();

	// Size of the circle which emits particles.
	void setSize(float x, float y);
	b2Vec2* getSize();

	// Starting velocity of emitted particles.
	void setVelocity(float x, float y, float xOffset, float yOffset);
	b2Vec2* getVelocity();

	// Emit rate in particles per second.
	void setEmitRate(util::RandomType<float> emitRate);
	float getEmitRate();

	// Particles in the emitter.
	bool addParticle(const Json::Value& jsonParticle);
	std::vector<Particle> getParticles();
	Particle getParticleByName(std::string particleName);

private:
	b2ParticleDef getParticleDef(Particle p);
	bool emit(glm::vec2 position, float rotation);

	//get a flag from a string
	b2ParticleFlag getFlagFromString(std::string sFlag);

	void updateRenderNodes();

	std::string name;

	float timeSinceParticle;

	b2Vec2* offset;
	b2Vec2* size;
	b2Vec2* velocity;
	b2Vec2* velocityOffset;
	util::RandomType<float> emitRate;
	ILogicContext* logicContext;

	b2ParticleSystem* particleSystem;

	bool enabled;

	std::vector<Particle> particles;
};

}
}
}

#endif
