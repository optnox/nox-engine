/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ACTORGRAVITATION
#define NOX_LOGIC_PHYSICS_ACTORGRAVITATION

#include <nox/logic/actor/Component.h>
#include "../GravitationalForce.h"

namespace nox { namespace logic { namespace physics
{

/**
 * Applies a gravitational force towards a position.
 *
 * The position of the gravitation center is decided by the position of the Transform.
 *
 *  # JSON Description
 *  - __mass__:float - Mass of the object having the gravitational force. Greatly affects the force strength.
 *  - __distanceLimit__:object - Limit both minimum and maximum distance of gravitational pull.
 *  	+ __min__:object - The minimum distance limit.
 *  		* __distance__:real - What distance is the minimum distance.
 *  		* __type__:string - What should happen when distance is less than minimum distance. "border" disables gravity, "clamp" clamps distance to the limit.
 *  	+ __max__:object - The maximum distance limit.
 *  		* __distance__:real - What distance is the maximum distance.
 *  		* __type__:string - What should happen when distance is larger than maximum distance. "border" disables gravity, "clamp" clamps distance to the limit.
 */
class ActorGravitation: public actor::Component
{
public:
	static const IdType NAME;

	const IdType& getName() const override;

	void initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;

private:

	float mass;
	GravitationalForce* force;
	GravitationalForce::DistanceLimit forceDistanceLimit;
};

} } }

#endif
