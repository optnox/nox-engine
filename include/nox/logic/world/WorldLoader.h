/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_LOADER_H_
#define NOX_LOGIC_WORLD_LOADER_H_

#include <nox/app/log/Logger.h>

#include <json/value.h>
#include <map>

namespace nox { namespace logic
{

class ILogicContext;
class View;

namespace actor
{

class Actor;

}

namespace world
{

class WorldManager;

/**
 * Load Actors and Views controlling them into the world.
 *
 * A world JSON object can contain one __actors__ array and one __views__ array.
 *
 * The __actors__ array contains a list of all Actors to be loaded. Each item is an object which contain
 * the Actor JSON definition.
 *
 * The __views__ array contains a list of all the Views in the world. Each item is an object with various properties
 * listed below. The View must have a __type__ property which can be either one of:
 * - "registered": A View that has been registered to the WorldLoader with registerControllingView(). The View is registered
 * on a controlIndex. For a registered View there is also a property __registeredIndex__. This index is the controlIndex
 * of the View to use.
 *
 * A View must also have a __controlledActor__ property which is an index to the __actors__ array. The Actor on the index
 * will be controlled by the View.
 *
 * # JSON Format
 * - __actors__:array[object] - All the Actors to load into the world. Each object is a standard
 * Actor JSON definition, including properties like name, extend and components.
 * - __views__:array[object] - All the Views to load that control the Actors.
 *	 + __type__:string - The type of the View. Must be "registered".
 *	 + __registeredIndex__:uint - Only for __type__="registered". Which View to control.
 *	 + __controlledActor__:uint - The Actor at the __controlledActor__ index in the __actors__ array that this View
 *	 will control.
 */
class WorldLoader
{
public:
	WorldLoader(ILogicContext* logicContext);

	/**
	 * Register a View on an index.
	 * The View registered will be used when a View of type "registered" is found in the world JSON.
	 * A "registered" View must also have a __registeredIndex__ which points to the controlIndex
	 * provided in this function.
	 *
	 * A normal use case is to provide the window as a View on index 0 here, and in the JSON
	 * give the "registered" View on __registeredIndex__ 0 the Actor that a user should control.
	 *
	 * @param controlIndex Index to register View on. Will overwrite previous. Use __registeredIndex__ to refer to this.
	 * @param view View to register on controlIndex.
	 */
	void registerControllingView(const std::size_t controlIndex, View* view);

	/**
	 * Load a world from a JSON object as described by the class documentation into the world::WorldManager provided.
	 *
	 * @param world World JSON to load.
	 * @param worldManager World manager to load it into.
	 * @return If loading succeeded or not. Loading can fail midways ending up with only parts of the world loaded.
	 */
	bool loadWorld(const Json::Value& world, world::WorldManager* worldManager);

private:
	ILogicContext* context;
	app::log::Logger log;

	std::map<std::size_t, View*> registeredControllingViews;
};

} } }

#endif
