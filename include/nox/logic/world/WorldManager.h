/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_MANAGER_H_
#define NOX_LOGIC_WORLD_MANAGER_H_

#include <nox/app/log/Logger.h>
#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/EventListenerManager.h>

#include <glm/vec2.hpp>
#include <nox/logic/actor/ActorFactory.h>
#include <nox/util/math/Box.h>

#include <thread>
#include <queue>

namespace nox { namespace logic
{

namespace actor
{

class Transform;

}

namespace event
{

class IEventBroadcaster;

}

namespace world
{

class WorldManager;

using SyncDestroyHandler = std::function<void(actor::Actor*,WorldManager*)>;


/**
 * Handles the world and all its Actors.
 */
class WorldManager: public event::IEventListener
{
public:
	//! Callback called when a world save has completed.
	using SaveCompleteCallback = std::function<void ()>;

	WorldManager(ILogicContext* context);
	virtual ~WorldManager();

	/**
	 * Let the WorldManager simulate over deltaTime duration.
	 *
	 * This involves updating all the actor::Actors.
	 * Subclasses can do additional work by overriding onUpdate().
	 */
	void update(const Duration& deltaTime);

	/**
	 * Completely reset the state.
	 *
	 * This will destroy all actor::Actors.
	 */
	void reset();

	/**
	 * Save the complete world state.
	 *
	 * The save process might run on a separate thread so pass a
	 * SaveCompleteCallback function to know when it is done.
	 *
	 * @param saveDirectory Directory to save the state to.
	 * @param completeCallback Function called when the save is complete.
	 */
	void saveEverything(const std::string& saveDirectory, SaveCompleteCallback completeCallback = SaveCompleteCallback());

	/**
	 * Load all Actor definitions from a resource directory.
	 *
	 * @param resourceAccess Access to the resources.
	 * @param definitionPath Path to the directory provided by the
	 *        resourceAccess.
	 */
	void loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& definitionPath);

	/**
	 * Registers an ActorComponent for creation so that the world can create that component from
	 * an actor definition.
	 *
	 * @tparam ActorComponent The component class to register.
	 */
	template<class ActorComponentType>
	void registerActorComponent();

	/**
	 * Create an empty actor. The actor will not have any attached Components. The created actor will be
	 * owned and managed by this WorldManager-instance.
	 *
	 * @param name The name of the actor.
	 * @return An initialized Actor named "name" without any Components.
	 */
	actor::Actor* createActor(const std::string& name);

	/*
	 * Add a destruction handler for synchronized actors. These allow you to override what happens when the
	 * server issues a command to destroy a synchronized actor.
	 *
	 * Do realize that whatever you do, the Actor will *not* be in a synchronized state when the lambda gets
	 * called.
	 *
	 * @param name	  The name of the handler, as referenced from the actor definitions. This name can not
	 *				  already be assigned to a custom handler.
	 * @param handler   The handling lambda. It will be called when an actor with this destroy-handler name
	 *				  is destroyed.
	 */
	void addSyncActorDestroyHandler(std::string name, SyncDestroyHandler handler);

	/**
	 * Create an Actor from a definition name.
	 *
	 * The definition name must be loaded with loadActorDefinitions().
	 *
	 * @param actorDefinitionName The definitionName of the Actor to create.
	 * @return The Actor created or nullptr if Actor could not be created.
	 */
	std::unique_ptr<actor::Actor> createActorFromDefinitionName(const std::string& actorDefinitionName);

	/**
	 * Create an Actor from a JSON source.
	 *
	 * An Actor created from source doesn't have a definition name so one has
	 * to be provided with the definitionName parameter..
	 *
	 * @param jsonSource The source to create the Actor from.
	 * @param definitionName DefinitionName to give the Actor.
	 * @return The Actor created or nullptr if Actor could not be created.
	 */
	std::unique_ptr<actor::Actor> createActorFromSource(const Json::Value& jsonSource, const std::string& definitionName);

	/*
	 * Let the WorldManager manage an Actor.
	 *
	 * This moves the ownership of the Actor from the user to the WorldManager.
	 * The WorldManager will control the Actor's state and lifetime.
	 *
	 * @param actor Actor to manage.
	 * @return Raw pointer to the Actor managed.
	 */
	actor::Actor* manageActor(std::unique_ptr<actor::Actor> actor);

	/**
	 * Remove an Actor from the WorldManager.
	 *
	 * This will completely destroy the Actor. Any references to it will be
	 * invalid and should not be used.
	 *
	 * @param actor Actor to remove.
	 */
	void removeActor(std::unique_ptr<actor::Actor> actor);

	/*
	 * Remove an Actor from the WorldManager.
	 *
	 * This will completely destroy the Actor. Any references to it will be
	 * invalid and should not be used.
	 *
	 * @param actorId ID of Actor to remove.
	 */
	void removeActor(const actor::ActorIdentifier& actorId);

	/**
	 * Find an actor from an id.
	 *
	 * @param id Id of the actor.
	 * @return Pointer to actor with id, or nullptr if non found.
	 */
	actor::Actor* findActor(const actor::ActorIdentifier& id) const;
	actor::Actor* findActor(SyncId syncId) const;

	/**
	 * Find all actors within a range of a point.
	 *
	 * @param position Center of range.
	 * @param range Range radius.
	 * @return Actors within range.
	 */
	std::vector<actor::Actor*> findActorsWithinRange(const glm::vec2& position, const float range) const;

	/**
	 * Find all actors within an axis aligned box.
	 *
	 * @param box Axis aligned box.
	 * @return Actors within box.
	 */
	std::vector<actor::Actor*> findActorsWithinAxisAlignedBox(const math::Box<glm::vec2>& box) const;

	void onEvent(const std::shared_ptr<event::Event>& event);

protected:
	ILogicContext* getLogicContext();

private:

	void handleQueuedTasks();
	void clearActors();
	void handleActorCreation(actor::Actor* actor);

	void initDefaultDestroyHandlers();

	void handleActorManagement(actor::Actor* actor);
	void handleActorRemoval(actor::Actor* actor);

	/**
	 * Called when the WorldManager updates its state ( update() ).
	 *
	 * Override this to add you own update functionality.
	 */
	virtual void onUpdate(const nox::Duration& deltaTime);

	/**
	 * Called when the WorldManager wants to save the world.
	 *
	 * Override this to implement saving of the world state.
	 *
	 * Call the complete callback if it is valid when the save has
	 * completed.
	 */
	virtual void saveWorld(SaveCompleteCallback completeCallback, const std::string& saveDirectory);

	/**
	 * Called when an Actor is about to be managed by the WorldManager.
	 */
	virtual void onActorManaged(actor::Actor* actor);

	/**
	 * Called when an Actor is about to be removed.
	 */
	virtual void onActorRemoved(actor::Actor* actor);

	/**
	 * Called when the WorldManager is reset.
	 *
	 * Subclasses should reset their state here.
	 */
	virtual void onReset();

	ILogicContext* context;
	event::IEventBroadcaster* eventBroadcaster;
	app::log::Logger log;

	std::vector<actor::Transform*> nonPhysicalActorTransforms;
	mutable std::mutex nonPhysicalActorTransformsMutex;

	std::thread saveThread;

	actor::ActorFactory actorFactory;
	std::vector<std::unique_ptr<actor::Actor>> actors;
	std::unordered_map<actor::ActorIdentifier, actor::Actor*> idToActorMap;
	std::unordered_map<SyncId, actor::ActorIdentifier> syncToIdMap;

	mutable std::mutex actorStorageMutex;
	mutable std::mutex actorIdMutex;
	bool actorUpdateLocked;

	std::queue<std::unique_ptr<actor::Actor>> actorCreations;
	thread::ThreadSafeQueue<actor::ActorIdentifier> actorRemovalQueue;

	std::unordered_map<std::string,SyncDestroyHandler> syncDestroyHandlers;

	event::EventListenerManager listenerManager;
};

template<class ActorComponentType>
inline void WorldManager::registerActorComponent()
{
	this->actorFactory.registerComponent<ActorComponentType>();
}

}
}
}

#endif
