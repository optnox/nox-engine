#include <random>

namespace nox {

namespace util {

template <typename Type, class DistributionType = std::uniform_real_distribution<Type>>
class RandomType
{
public:
	RandomType()
	{
		this->value = static_cast<Type>(0);
		this->offset = static_cast<Type>(0);
	}

	RandomType(Type value, Type offset):
		value(value),
		offset(offset)
	{
	}

	~RandomType()
	{
	}
	
	Type get()
	{

		std::random_device rd;
	       	auto gen = std::mt19937(rd());
		DistributionType dis = DistributionType(value-offset, value+offset);
		return static_cast<Type>(dis(gen));
	}

	//Make it automatically return x<=value+offset && x>=value-offset
	operator Type() const
	{

		std::random_device rd;
	       	auto gen = std::mt19937(rd());
		DistributionType dis = DistributionType(value-offset, value+offset);
		return static_cast<Type>(dis(gen));
	}

	Type getValue()
	{
		return value;
	}

	Type getOffset()
	{
		return offset;
	}

	void setValue(Type value)
	{
		this->value = value;
	}

	void setOffset(Type offset)
	{
		this->offset = offset;
	}

private:
	Type value;
	Type offset;

};

} }
