/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_UTIL_PERF_PERFORMANCEMONITOR_H_
#define NOX_APP_UTIL_PERF_PERFORMANCEMONITOR_H_

#include <nox/util/Clock.h>

#include <chrono>
#include <string>
#include <list>
#include <map>
#include <memory>


namespace nox { namespace util { namespace perf
{

enum AccumulationType
{
	FRAME,
	GLOBAL,
	AVG_FRAME,
};

enum DataType
{
	COUNT,
	TIME,
};


class DataSet
{
public:
	DataSet(std::string name, DataType dtype, AccumulationType atype, int currentFrame);

	std::string getName() const;
	AccumulationType getAccumulationType() const;
	DataType getDataType() const;

	void onNewFrame();

	bool startTimer();
	bool stopTimer();

	bool addCountValue(long double value);

	void writeToFile(std::ofstream& file);

private:
	std::string name;
	AccumulationType accType;
	DataType dataType;
	int startFrame;

	// For AccumulationType::FRAME and AVG_FRAME, the list will contain one entry per frame. The first
	// entry refers to frame number "startFrame". For GLOBAL, the list contains no entries,
	// as "curEntry" is used exclusively instead.
	std::list<long double> entries;
	long double curEntry;
	int curEntryCount;

	// Only valid when the DataType is DataType::TIME.
	Clock clock;
	bool clockRunning;
};


class PerformanceManager;

class PerformanceMonitor
{
public:
	friend class PerformanceManager;

	~PerformanceMonitor();

	bool addDataSet(std::string setName, DataType dtype, AccumulationType accType);

	bool add(std::string setName, long double value);

	bool startTimer(std::string setName);
	bool stopTimer(std::string setName);

	void recordEvent(std::string name);

	/**
	 * Writes all data and recorded events to the file at the specified path. If
	 * the file already exists, it *will* be overwritten!
	 *
	 * @return  True on success, false if the file could not be created or written to.
	 */
	bool dumpDataToFile(std::string file);

private:
	explicit PerformanceMonitor(std::string name);

	void onNewFrame();

	std::string name;
	mutable std::map<std::string,DataSet*> dataSets;
	mutable std::list<std::pair<std::string,int>> events;
};


/**
 * Pure static, singleton like class. Keeps track of all the existing (or at some-point existing)
 * PerformanceMonitor instances and their data sets.
 */
class PerformanceManager
{
public:
	/* PerformanceMonitor is given private access so they may hand over their data when they are destroyed.
	 */
	friend class PerformanceMonitor;

	/**
	 * Create a PerformanceMonitor-instance.
	 */
	static std::shared_ptr<PerformanceMonitor> createMonitor(std::string name);

	/**
	 * Should be called at the start of each iteration through the main loop.
	 */
	static void onFrameBegin();

	/**
	 * Dumps all recorded data to a set of files (defined by the PerformanceMonitor and their Data Set names)
	 * in a specified directory. Both active and dead monitors are dumped to disk. As this process can be
	 * time consuming, it should only be done during or after shutdown.
	 */
	static void dumpPerformanceData(std::string directory);

private:
	static int frameNo;

	static std::list<std::shared_ptr<PerformanceMonitor>> monitors;
};

}
} }

#endif
