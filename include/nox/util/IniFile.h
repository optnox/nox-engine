/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_INIFILE_H_
#define NOX_UTIL_INIFILE_H_

#include <string>
#include <map>
#include <fstream>
#include <vector>

namespace nox { namespace util {


class IniFile
{
public:
	class Section
	{
	public:
		Section(std::string sectionName);

		std::string getName();

		std::string getString(std::string key, std::string def) const;
		long getLong(std::string key, long def) const;
		double getDouble(std::string key, double def) const;
		bool getBool(std::string key, bool def) const;

		void setString(std::string key, std::string val) const;
		void setLong(std::string key, long val) const;
		void setDouble(std::string key, double val) const;
		void setBool(std::string key, bool val) const;

		void writeToFile(std::ofstream &file);

		const std::map<std::string,std::string>& getValues() const;

	private:
		std::string sectionName;
		mutable std::map<std::string,std::string> kvPairs;
	};

	IniFile();
	~IniFile();

	// Parse another file and merge the existing and new content. Returns true if
	// the given file contains at least one valid line.
	bool appendParse(std::string file);

	// Parse another file and replace all the existing contents. Returns true if
	// the given file contains at least one valid line.
	bool replaceParse(std::string file);

	// Write all content to the last file read from.
	bool writeToFile();

	// Write all content to an arbitrary file.
	bool writeToFile(std::string file);

	const Section* getSection(std::string name);

	// Retrieve and remove the oldest error. Call getError() in succession while
	// hasError() is true to void the error log of messages.
	std::string getError();
	bool hasError();

private:
	std::string lastFile;
	std::map<std::string,Section*> sections;
	std::vector<std::string> errors;
};


} }


#endif
