/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_AUDIOBUFFER_H_
#define NOX_APP_AUDIO_AUDIOBUFFER_H_

#include <nox/app/audio/AudioBufferPositionProvider.h>
#include <glm/glm.hpp>
#include <memory>

namespace nox { namespace app
{

namespace resource
{

class Handle;
class IResourceAccess;

}

namespace audio
{

/**
 * Buffer represents one sound. The buffer has multiple attributes which may be
 * altered to alter how the sound is played; position, velocity, pitch, etc. Do
 * note that not all attributes are supported by all audio frameworks - see the
 * relevant Buffer methods for availability.
 */
class AudioBuffer
{
	friend class AudioBufferPositionProvider;

public:
	AudioBuffer();
	virtual ~AudioBuffer();

	/**
	 * Initialize the buffer from a resource.
	 * An initialized buffer will be automatically cleaned up from the destructor.
	 * @param fileName The file name of the sound to be played.
	 * @param resourceAccess IResourceAccess instance which will be used to search for the file described by 'fileName'.
	 * @return If successfully initialized or not.
	 */
	virtual bool initialize(const std::string& fileName, resource::IResourceAccess* resourceAccess) = 0;

	void playAudio();
	void pauseAudio();
	void stopAudio();
	void resumeAudio();
	void enableLooping(const bool looping);

	bool isLooping() const;
	float getVolume() const;
	bool isPlaying();

	/**
	 * Set the volume of the buffer.
	 * Volume will be clamped to range [0, 1].
	 * Availability:  cAudio  OpenAL
	 */
	void setVolume(const float vol);

	/**
	 * Set the position of the audio source.
	 * Availability:  cAudio
	 */
	virtual void setPosition(const glm::vec2& position);
	virtual void setPosition(const glm::vec3& position);

	/**
	 * Set the minimum distance at which attenuation begins.
	 * Availability:  cAudio (range: [0..+inf])
	 */
	virtual void setMinimumDistance(float minDistance);

	/**
	 * Set the maximum distance at which attenuation ends (the furthest distance the sound is heard).
	 * Availability:  cAudio (range: [0..+inf])
	 */
	virtual void setMaximumDistance(float maxDistance);

	/**
	 * Set the pitch of the sound.
	 * Availability:  cAudio (range: [0..+inf] def: 1.0)
	 * @note The playback speed is also affected with cAudio.
	 */
	virtual void setPitch(float pitch);

	/**
	 * Set the strength at which the sound carries over distance.
	 * Availability:  cAudio (range: [0..+inf] def: 1.0)
	 */
	virtual void setStrength(float strength);

	/**
	 * Set the strength at which the doppler effect is applied.
	 * Availability:  cAudio (range: [0..+inf] def: 1.0)
	 */
	virtual void setDopplerStrength(float strength);

	/**
	 * Set the velocity of the sound. The velocity affects the doppler effect.
	 * Availability:  cAudio  OpenAL
	 */
	virtual void setVelocity(const glm::vec2& velocity);
	virtual void setVelocity(const glm::vec3& velocity);

private:
	void setBufferPositionProvider(AudioBufferPositionProvider* provider);

	virtual void handlePause() = 0;
	virtual void handleResume() = 0;
	virtual void handleVolumeChange(const float volume) = 0;
	virtual void handleLoopingChange(const bool looping) = 0;
	virtual void rewindPosition() = 0;
	virtual bool checkIfPlaying() const;

	bool playing;
	bool looping;
	float volume;
	AudioBufferPositionProvider* positionProvider;

};

}
} }

#endif
