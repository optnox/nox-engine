/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_AUDIOBUFFERPOSITIONPROVIDER_H_
#define NOX_APP_AUDIO_AUDIOBUFFERPOSITIONPROVIDER_H_

#include <vector>

namespace nox { namespace app { namespace audio
{

class AudioBuffer;

/**
 * A Buffer Position Provider is a link between an object in the game
 * (for instance, a Transform object) and a sound buffer, allowing the
 * movable object to synchronize it's state to the sound.
 */
class AudioBufferPositionProvider
{
public:
	virtual ~AudioBufferPositionProvider();

	/**
	 * Update all attached sound buffers. Implementations must manually call this
	 * method once (and only once) per tick.
	 */
	void updateAttachedBuffers();

	/**
	 * Attach an audio buffer to the Position Provider. The Buffer is notified that
	 * this object is the new provider.
	 *
	 * When the audioBuffer is finished playing, it is automatically removed.
	 */
	void attachSoundBuffer(AudioBuffer* audioBuffer);

	/**
	 * Detach a sound buffer from the Transform. If the audioBuffer is not
	 * finished, it will no longer be updated by this provider. The Buffer is
	 * notified that the provider has departed.
	 */
	void detachSoundBuffer(AudioBuffer* audioBuffer);

protected:
	virtual void updateAudioBuffer(AudioBuffer* buffer) = 0;

private:
	std::vector<AudioBuffer*> attachedBuffers;
};

}
} }

#endif
