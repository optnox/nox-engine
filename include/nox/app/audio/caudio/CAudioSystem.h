/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_CAUDIOSYSTEM_H_
#define NOX_APP_AUDIO_CAUDIOSYSTEM_H_

#include <nox/app/audio/AudioSystem.h>

#include <cAudio.h>


namespace nox { namespace app
{

namespace resource
{

class Handle;

}

namespace audio
{

class CAudioSystem: public AudioSystem
{
public:
	static std::vector<std::string> getPlaybackDeviceNames();
	static std::string getDefaultPlaybackDeviceName();

	/**
	 * The output device can be assigned by retrieving the list of available devices via
	 * CAudioSystem::getPlaybackDeviceNames(), and passing one of the elements to the ctor.
	 * If the passed playback device name is invalid (i.e., does not exist), the deafult is used.
	 */
	CAudioSystem(IApplicationContext *appContext, const std::string& playbackDeviceName = "");
	~CAudioSystem();

	bool initialize() override;
	void shutdown() override;

	std::string getCurrentPlaybackDeviceName() const;

	virtual void setListenerPosition(const glm::vec3& position) override;
	virtual void setListenerDirection(const glm::vec3& direction) override;
	virtual void setListenerUpVector(const glm::vec3& upVector) override;
	virtual void setListenerVelocity(const glm::vec3& velocity) override;

private:
	std::unique_ptr<AudioBuffer> createBuffer(PlayMode playMode) override;
	bool isDeviceValid(const std::string& deviceName);


	cAudio::IAudioManager* audioManager;
	std::string playbackDeviceName;
};

}
} }

#endif

