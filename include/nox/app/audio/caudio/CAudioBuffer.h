/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_CAUDIOBUFFER_H_
#define NOX_APP_AUDIO_CAUDIOBUFFER_H_

#include <nox/app/audio/AudioBuffer.h>
#include <nox/app/audio/AudioSystem.h>
#include <cAudio.h>

namespace nox { namespace app {

namespace resource
{

class SoundExtraData;

}

namespace audio
{

class CAudioBuffer: public AudioBuffer
{
public:
	CAudioBuffer(cAudio::IAudioManager* audioManager, PlayMode playMode);
	~CAudioBuffer();

	bool initialize(const std::string& fileName, resource::IResourceAccess* resourceAccess) override;

	virtual void setPosition(const glm::vec3& position) override;
	virtual void setMinimumDistance(float minDistance) override;
	virtual void setMaximumDistance(float maxDistance) override;
	virtual void setPitch(float pitch) override;
	virtual void setStrength(float strength) override;
	virtual void setDopplerStrength(float strength) override;
	virtual void setVelocity(const glm::vec3& velocity) override;

private:
	void handlePause() override;
	void handleResume() override;
	void handleVolumeChange(const float volume) override;
	void handleLoopingChange(const bool looping) override;
	void rewindPosition() override;
	bool checkIfPlaying() const override;

	cAudio::AudioFormats getCAudioFormat(const resource::SoundExtraData* soundData) const;

	PlayMode playMode;
	cAudio::IAudioManager* audioManager;
	cAudio::IAudioSource* audioSource;

	glm::vec3 soundPosition;
	float soundStrength;
};

}
} }

#endif
