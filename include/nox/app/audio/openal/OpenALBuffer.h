/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_OPENALBUFFER_H_
#define NOX_APP_AUDIO_OPENALBUFFER_H_

#include <nox/app/audio/AudioBuffer.h>

#include <al.h>
#include <alc.h>

namespace nox { namespace app
{

namespace audio
{

/**
 * Buffer for a sound with OpenAL.
 */
class OpenALBuffer : public AudioBuffer
{
public:
	OpenALBuffer();
	~OpenALBuffer();

	bool initialize(const std::string& fileName, resource::IResourceAccess* resourceAccess) override;

private:
	void handlePause() override;
	void handleResume() override;
	void handleVolumeChange(const float volume) override;
	void handleLoopingChange(const bool looping) override;
	void rewindPosition() override;
	bool checkIfPlaying() const override;

	ALuint buffer;
	ALuint source;
};

}
} }

#endif
