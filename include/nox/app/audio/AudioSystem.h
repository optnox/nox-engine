/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_AUDIOSYSTEM_H_
#define NOX_APP_AUDIO_AUDIOSYSTEM_H_

#include <nox/common/types.h>

#include <glm/glm.hpp>
#include <list>
#include <memory>

namespace nox { namespace app
{

class IApplicationContext;

namespace resource
{

class Handle;

}

namespace audio
{

/**
 * PlayMode defines how a sound is treated. Ambient sounds are always played
 * "in your face", and has no positional attributes. Background music, voice announcements,
 * and similar effects should be played as ambient sounds.
 *
 * Positional sounds are sounds which have a position (and optionally a velocity and other
 * spatial attributes).
 */
enum class PlayMode
{
	// Enable 3D playback wherever possible.
	// Availability:  cAudio
	POSITIONAL,

	// Do not enable 3D, play the sound at max volume at all times.
	// Availability:  cAudio  OpenAL
	AMBIENT,
};

class AudioBuffer;
class AudioListenerPositionProvider;

/**
 * Provide audio capabilities. There should never exist more than one System at any
 * given point in time.
 *
 * The listener is responsible for handling the position of the listener (i.e., where
 * the "ears" of the player are), direction and orientation. These attributes may not
 * be available in all NOX-supported sound frameworks. See specific method documentation
 * for availability - unless otherwise specified, all frameworks are supported.
 */
class AudioSystem
{
public:
	AudioSystem(IApplicationContext *appContext);
	virtual ~AudioSystem();

	void onUpdate(const Duration& deltaTime);

	/**
	 * Initialize the system so that it is ready to use.
	 */
	virtual bool initialize() = 0;
	virtual void shutdown();

	void stopAllSounds();
	void pauseAllSounds();
	void resumeAllSounds();


	/**
	 * Given a file name, play the corresponding sound with that name.
	 *
	 * @param soundFileName The name of the file to play.
	 * @return The playing buffer on success, nullptr on failure
	 */
	AudioBuffer* playSound(const std::string& fileName, PlayMode pmode = PlayMode::AMBIENT);

	/**
	 * Set the position of the listener in world space.
	 * Availability:  cAudio
	 */
	virtual void setListenerPosition(const glm::vec2& position);
	virtual void setListenerPosition(const glm::vec3& position);

	/**
	 * Set the direction the listener is looking.
	 * Availability:  cAudio
	 */
	virtual void setListenerDirection(const glm::vec3& direction);

	/**
	 * Set the up-vector of the listener.
	 * Availability:  cAudio
	 */
	virtual void setListenerUpVector(const glm::vec3& upVector);

	/**
	 * Set the velocity of the listener. Changing the velocity will apply
	 * the doppler effect on positional sound effects.
	 * Availability:  cAudio
	 */
	virtual void setListenerVelocity(const glm::vec2& velocity);
	virtual void setListenerVelocity(const glm::vec3& velocity);

	/**
	 * Set the AudioListenerPositionProvider. The AudioListenerPositionProvider will be
	 * prompted once every onUpdate()-call to set the spatial attributes of
	 * the listener.
	 */
	void setListenerPositionProvider(AudioListenerPositionProvider* provider);

	/**
	 * Detach the AudioListenerPositionProvider.
	 *
	 * @param provider If not-null, the System will check that the passed provider is
	 *   in fact the current provider. If null, the current provider will be detached.
	 */
	void detachListenerPositionProvider(AudioListenerPositionProvider* provider = nullptr);

protected:
	/**
	 * Initialize an audio buffer a filename.
	 * @param fileName Name of the file to start playing.
	 * @note Call releaseAudioBuffer to free the buffer after use.
	 * @return Pointer to audio buffer created, nullptr on failure.
	 */
	AudioBuffer* createAudioBuffer(const std::string& fileName, PlayMode playMode);

	/**
	 * Release an audio buffer.
	 * @pre Buffer must be created with createAudioBuffer.
	 * @param audioBuffer Pointer to the buffer to release.
	 */
	void releaseAudioBuffer(AudioBuffer* audioBuffer);

	IApplicationContext* getAppContext();

private:
	using AudioBufferList = std::list<std::unique_ptr<AudioBuffer>>;

	/**
	 * Create the Buffer instance to be initialized. Should be overridden as this is called
	 * from createAudioBuffer.
	 * @return Buffer created.
	 */
	virtual std::unique_ptr<AudioBuffer> createBuffer(PlayMode playMode) = 0;

	AudioBufferList buffers;
	IApplicationContext* appContext;
	AudioListenerPositionProvider* listenerProvider;
};

}
} }

#endif
