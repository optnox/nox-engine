/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_AUDIOLISTENERPOSITIONPROVIDER_H_
#define NOX_APP_AUDIO_AUDIOLISTENERPOSITIONPROVIDER_H_

#include <nox/app/audio/AudioSystem.h>

namespace nox { namespace app { namespace audio
{

/**
 * Interface for providing attributes to the audio listener. The audio::System
 * will itself query the provider once every tick to have it's attributes updated
 * via ListenerPositionProvider::updateListenerPosition(nox::app::audio::System*).
 */
class AudioListenerPositionProvider
{
	friend class AudioSystem;

public:
	AudioListenerPositionProvider();
	virtual ~AudioListenerPositionProvider();

	/**
	 * Called automatically by the audio::AudioSystem once every frame.
	 */
	virtual void updateListenerPosition(AudioSystem* audioSystem) = 0;

	/**
	 * Attach the ListenerPositionProvider to the audio::AudioSystem.
	 * The System will call updateListenerPosition() each frame.
	 */
	void attachToAudioSystem(AudioSystem* audioSystem);

private:
	/**
	 * The member is assigned by the audio::AudioSystem in
	 * audio::System::setListenerPositionProvider, and cleared when a new provider
	 * is attached or the System is destroyed.
	 */
	AudioSystem* audioSystem;
};

}
} }

#endif
