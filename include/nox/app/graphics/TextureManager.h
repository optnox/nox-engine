/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TEXTUREMANAGER_H_
#define NOX_APP_GRAPHICS_TEXTUREMANAGER_H_

#include "TextureQuad.h"
#include <nox/app/log/Logger.h>
#include <nox/app/resource/Handle.h>

#include <json/value.h>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <unordered_map>
#include <vector>

namespace nox { namespace app
{

namespace resource
{

class Descriptor;
class IResourceAccess;

}

namespace graphics
{

/**
 * Manages textures.
 * The job of this class is to load sprite information, store it,
 * write sprite images to OpenGL buffers, and provide data
 * for renderers.
 */
class TextureManager
{
public:
	TextureManager();
	TextureManager(const TextureManager&) = delete;
	TextureManager& operator = (const TextureManager&) = delete;

	void setLogger(log::Logger logger);

	/**
	 * Read sprite information from the json object provided.
	 * @param graphicsJson Json object to read from.
	 * @return true if it succeeded, false if not.
	 */
	bool readGraphicsFile(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess);

	/**
	 * Write all sprites atlases loaded to OpenGL texture buffers.
	 */
	void writeToTextureBuffers(resource::IResourceAccess* resourceAccess);

	bool hasTexture(const std::string& textureName) const;

	const TextureQuad& getTexture(const std::string& textureName) const;

	unsigned int getNumAtlases() const;

	GLuint getTextureBuffer(unsigned int atlasNumber) const;
	GLuint getTextureBuffer(const std::string& atlasName) const;

	GLuint getNormalMapBuffer(unsigned int atlasNumber) const;
	GLuint getNormalMapBuffer(const std::string& atlasName) const;

	GLuint getSpecularMapBuffer(unsigned int atlasNumber) const;
	GLuint getSpecularMapBuffer(const std::string& atlasName) const;

	glm::ivec2 getAtlasSize(const std::string& atlasName) const;

private:
	/**
	 * Information about a sprite atlas.
	 */
	struct TextureAtlas
	{
		std::string name;				//!< Name of the atlas.
		std::string imageExtension;		//!< Extension (e.g. .png) used for the atlas image.
		std::string dataExtension;		//!< Extension (e.g. .json) used for the atlas data.
		std::string normalMapFile;		//!< The normal map file specified.
		std::string specularMapFile;	//!< The specular map file specified.
		unsigned int width;
		unsigned int height;
		unsigned int id;				//!< Unique ID for the atlas managed by the SpriteManager.
		GLuint boundTextureBuffer;		//!< Texture buffer this atlas is bound to. 0 if not bound.
		GLuint boundNormalMapBuffer;	//!< Texture buffer the normal map is bound to. 0 if not bound.
		GLuint boundSpecularMapBuffer;	//!< Texture buffer the specular map is bound to. 0 if not bound.
		bool mipMap;
	};

	bool writeTextureAtlasToBuffer(TextureAtlas& atlas, resource::IResourceAccess* resourceAccess);
	bool writeNormalMapToBuffer(TextureAtlas& atlas, resource::IResourceAccess* resourceAccess);
	bool writeSpecularMapToBuffer(TextureAtlas& atlas, resource::IResourceAccess* resourceAccess);

	/**
	 * Write a image resource handle to a new OpenGL texture buffer.
	 * @param atlas The name of the image to write.
	 * @return The buffer name for the texture, 0 if no bound.
	 */
	GLuint writeToTextureBuffer(std::shared_ptr<resource::Handle> imageResource, const TextureAtlas& atlas) const;

	mutable log::Logger log;

	std::vector<TextureAtlas> textureAtlasFiles;
	std::unordered_map<std::string, TextureQuad> textureMap;

	unsigned int defaultTexelsPerMeter;

	TextureQuad defaultQuad;
};

}
} }

#endif
