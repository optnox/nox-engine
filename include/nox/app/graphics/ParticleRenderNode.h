#ifndef APP_GRAPHICS_PARTICLERENDERNODE_H_
#define APP_GRAPHICS_PARTICLERENDERNODE_H_

#include <nox/common/types.h>
#include <Box2D/Box2D.h>

#include <vector>


namespace nox { namespace app { namespace graphics
{

/**
 * POD intended to be transferred directly into video memory.
 */
struct ParticleRenderNode
{
	// Transform
	float x;
	float y;
	float rotation;
	float size;

	// Color
	float r;
	float g;
	float b;
	float a;

	float scale = 10;
};

struct ParticleRenderBatch
{
	std::vector<ParticleRenderNode> renderNodes;
	std::string texture;
};

}
} }

#endif
