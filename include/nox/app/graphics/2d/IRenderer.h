/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_IRENDERER_H_
#define NOX_APP_GRAPHICS_IRENDERER_H_

#include <memory>
#include <string>
#include <glm/vec2.hpp>
#include <nox/app/graphics/ParticleRenderNode.h>

namespace nox { namespace app
{

class IApplicationContext;

namespace resource
{

class Descriptor;
class IResourceAccess;

}

namespace graphics
{

class SceneGraphNode;
class Camera;
class TextureManager;
class GeometrySet;
class Light;
class TextureRenderer;
class SubRenderer;
class StenciledTiledTextureRenderer;
class StenciledTiledTextureGenerator;
class BackgroundGradient;

class IRenderer
{
public:
	virtual ~IRenderer();
	virtual bool init(IApplicationContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize) = 0;
	virtual void onRender() = 0;
	virtual void setRootSceneNode(const std::shared_ptr<SceneGraphNode>& rootNode) = 0;
	virtual void setCamera(const std::shared_ptr<Camera>& camera) = 0;
	virtual void addDebugGeometrySet(const std::shared_ptr<GeometrySet>& set) = 0;
	virtual void removeDebugGeometrySet(const std::shared_ptr<GeometrySet>& set) = 0;
	virtual const TextureManager& getTextureManager() const = 0;
	virtual void addLight(std::shared_ptr<Light> light) = 0;
	virtual void removeLight(std::shared_ptr<Light> light) = 0;
	virtual bool toggleDebugRendering() = 0;
	virtual bool isDebugRenderingEnabled() = 0;
	virtual void resizeWindow(const glm::uvec2& windowSize) = 0;
	virtual TextureRenderer& getTextureRenderer() = 0;
	virtual SubRenderer* addSubRenderer(std::unique_ptr<SubRenderer> renderer) = 0;
	virtual void removeSubRenderer(SubRenderer* renderer) = 0;
	virtual void addStenciledTiledTextureLayer(
			const std::string& layerName,
			std::unique_ptr<StenciledTiledTextureGenerator> generator,
			std::unique_ptr<StenciledTiledTextureRenderer> renderer,
			unsigned int renderLevel) = 0;
	virtual void removeTiledTextureLayer(const std::string& layerName) = 0;
	virtual void lightUpdate(const std::shared_ptr<Light>& light) = 0;
	virtual void setAmbientLightLevel(const float lightLevel) = 0;
	virtual void organizeRenderSteps() = 0;

	/**
	 * All render levels above or equal to the level will be affected by lighting. All render levels
	 * below will not.
	 *
	 * Default is 0.
	 */
	virtual void setLightStartRenderLevel(const unsigned int litRenderLevel) = 0;

	/**
	 * Set the background gradient to use.
	 *
	 * This gradient will be rendered behind every other element. By default there is
	 * no gradient.
	 *
	 * If nullptr is passed, the gradient will be removed.
	 */
	virtual void setBackgroundGradient(std::unique_ptr<BackgroundGradient> background) = 0;

	/**
	 * Load texture atlases from a resource.
	 *
	 * @param graphicsResourceDescriptor Resource describing atlases.
	 * @param resourceAccess Access to resources.
	 */
	virtual void loadTextureAtlases(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess) = 0;

	/**
	 * Set the atlas used for world rendering (all textures).
	 *
	 * The atlas must have been loaded in loadTextureAtlases
	 *
	 * @param atlasName Name of atlas loaded in loadTextureAtlases
	 */
	virtual void setWorldTextureAtlas(const std::string& atlasName) = 0;

	/**
	 * Set the positionBuffer for the particles.
	 * @param positions Pointer to array of particle render nodes.
	 * @param count The number of particles in the array.
	 */
	virtual void addParticleRenderBatch(std::shared_ptr<ParticleRenderBatch> batch) = 0;
};

}
} }

#endif
