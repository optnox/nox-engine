/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_LIGHT_H_
#define NOX_APP_GRAPHICS_LIGHT_H_

#include "GeometrySet.h"

#include <string>
#include <glm/glm.hpp>

namespace nox { namespace app
{
namespace graphics
{

class Triangle;

struct LightRenderTriangle
{
	glm::vec2 lowerPoint;
	glm::vec2 upperPoint;

	float lowerAngle;
	float upperAngle;

	Triangle* geoemtry;
};

/**
 * Representation of a light.
 */
class Light
{
public:
	enum class RenderHint
	{
		DYNAMIC,
		STATIC
	};

	Light();

	void setName(const std::string& name);
	const std::string& getName() const;

	void setOnScreen(const bool onScreen);
	bool isOnScreen() const;

	void setZHeight(float height);
	void setRange(float range);
	void setPosition(const glm::vec2& position);
	void setPositionOffset(const glm::vec2& offset);
	void setColor(const glm::vec4& color);
	void setAttenuation(const glm::vec3& attenuation);

	void setAlphaFallOff(const float alphaFallOff);
	void setConeAngleRadian(const float angleRadian);
	void setRadialLight(const bool radialLight);
	void setCastShadows(const bool castShadows);

	void setEnabled(bool enabled);
	void setCastDirection(float directionAngle);

	float getZHeight() const;
	float getRange() const;
	const glm::vec2& getPosition() const;
	const glm::vec2& getPositionOffset() const;
	const glm::vec4& getColor() const;
	const glm::vec3& getAttenuation() const;

	float getAlphaFallOff() const;
	float getConeAngleRadian() const;
	bool isRadialLight() const;
	bool doesCastShadows() const;

	bool isEnabled() const;
	float getCastDirection() const;

	void setGeometryUpdated();
	void setGeometryOutdated();
	bool needsGeometryUpdate() const;

	bool needsRenderUpdate() const;
	void setRenderingUpdated();

	void setNeedsShadowUpdate(bool needsUpdate);
	bool needsShadowUpdate() const;

	void setEffect(const bool state);
	bool isEffect() const;

	GeometrySet lightArea;
    GeometrySet shadowArea;
	std::vector<LightRenderTriangle> lightAreaTriangles;

	bool shouldRotateWithPlayer;
	float rotationOffset;

	RenderHint renderHint;

private:
	std::string name;

	glm::vec4 color;
	glm::vec2 position;
	glm::vec2 positionOffset;

	/**
	 * Must be assigned by the renderer each frame. If the light is *not* currently visible on
	 * the screen, the light should also not create shadows.
	 */
	bool onScreen;

	/**
	 * The alphaFalloff is used when rendering the light *without* taking into consideration
	 * the effect the light has on the surface material.
	 */
	float alphaFallOff;
	float coneAngleRadian;

	bool radialLight;
	bool castShadows;

	/**
	 * The attenuation vector is used when rendering the light's effect on the surface
	 * materials. The vector uses the "Constant-Linear-Quadratic Falloff" method, where
	 * X = constant term, Y = linear factor, Z = quadratic factor.
	 *
	 * This is a well-written article on the subject of CLQ-falloff:
	 * https://developer.valvesoftware.com/wiki/Constant-Linear-Quadratic_Falloff
	 */
	glm::vec3 attenuation;

	/**
	 * The z-height attribute is used as the position of the light along the Z-axis when
	 * calculating normal values. The limited usage of this attribute is why it is its
	 * own member instead of making Light::position a glm::vec3.
	 */
	float zHeight;

	float range;

	float castDirectionRadian;

	bool enabled;
	bool geometryChanged;
	bool geometryOutdated;

	/**
	 * The light may be in need of a shadow update for a long time. However, the shadows
	 * are not updated until *absolutely* necessary (i.e., when the light is on screen).
	 */
	bool needShadowUpdate;

	bool effect;
};

inline void Light::setName(const std::string &name)
{
	this->name = name;
}

inline const std::string& Light::getName() const
{
	return this->name;
}

inline void Light::setOnScreen(const bool onScreen)
{
	this->onScreen = onScreen;

	if (!this->onScreen)
	{
		// If the light is off the screen for a frame, we need to update the light's shadows
		// when it arrives back onto the screen.
		this->needShadowUpdate = true;
	}
}

inline bool Light::isOnScreen() const
{
	return this->onScreen;
}

inline void Light::setRange(float range)
{
	this->range = range;
}

inline void Light::setZHeight(float height)
{
	this->zHeight = height;
}

inline void Light::setPosition(const glm::vec2& position)
{
	this->position = position;
	this->needShadowUpdate = true;
}

inline void Light::setPositionOffset(const glm::vec2& offset)
{
	this->positionOffset = offset;
	this->needShadowUpdate = true;
}

inline void Light::setColor(const glm::vec4& color)
{
	this->color = color;
	this->geometryOutdated = true;
}

inline void Light::setAttenuation(const glm::vec3& attenuation)
{
	this->attenuation = attenuation;
}

inline void Light::setAlphaFallOff(const float alphaFalloff)
{
	this->alphaFallOff = alphaFalloff;
	this->geometryOutdated = true;
}

inline void Light::setConeAngleRadian(const float angleRadian)
{
	this->coneAngleRadian = angleRadian;
	this->geometryOutdated = true;
}

inline void Light::setRadialLight(const bool radialLight)
{
	this->radialLight = radialLight;
	this->geometryOutdated = true;
}

inline void Light::setCastShadows(const bool castShadows)
{
	this->castShadows = castShadows;
}

inline void Light::setEnabled(bool enabled)
{
	if (this->enabled != enabled)
	{
		this->geometryOutdated = true;
	}

	this->enabled = enabled;
}

inline void Light::setCastDirection(float directionAngle)
{
	this->castDirectionRadian = directionAngle;
}

inline void Light::setGeometryUpdated()
{
	this->geometryOutdated = false;
	this->geometryChanged = true;
}

inline void Light::setGeometryOutdated()
{
	this->geometryOutdated = true;
}

inline bool Light::needsGeometryUpdate() const
{
	return this->geometryOutdated;
}

inline bool Light::needsRenderUpdate() const
{
	return this->geometryChanged;
}

inline void Light::setRenderingUpdated()
{
	this->geometryChanged = false;
}

inline void Light::setNeedsShadowUpdate(bool needsUpdate)
{
	this->needShadowUpdate = needsUpdate;
}

inline bool Light::needsShadowUpdate() const
{
	return this->needShadowUpdate;
}

inline float Light::getZHeight() const
{
	return this->zHeight;
}

inline float Light::getRange() const
{
	return this->range;
}

inline const glm::vec2& Light::getPosition() const
{
	return this->position;
}

inline const glm::vec2& Light::getPositionOffset() const
{
	return this->positionOffset;
}

inline const glm::vec4& Light::getColor() const
{
	return this->color;
}

inline const glm::vec3& Light::getAttenuation() const
{
	return this->attenuation;
}

inline float Light::getAlphaFallOff() const
{
	return this->alphaFallOff;
}

inline float Light::getConeAngleRadian() const
{
	return this->coneAngleRadian;
}

inline bool Light::isRadialLight() const
{
	return this->radialLight;
}

inline bool Light::doesCastShadows() const
{
	return this->castShadows;
}

inline bool Light::isEnabled() const
{
	return this->enabled;
}

inline float Light::getCastDirection() const
{
	return this->castDirectionRadian;
}

inline void Light::setEffect(const bool state)
{
	this->effect = state;
}

inline bool Light::isEffect() const
{
	return this->effect;
}

}
} }

#endif
