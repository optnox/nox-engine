/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_PARTICLERENDERER_H_
#define NOX_APP_GRAPHICS_PARTICLERENDERER_H_

#include <nox/app/graphics/2d/Geometry.h>
#include <nox/app/graphics/ParticleRenderNode.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/app/graphics/2d/Camera.h>


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

#include <GL/glew.h>
#include <vector>
#include <memory>
#include <chrono>
#include <sstream>

namespace nox { namespace app {

namespace resource
{

class IResourceAccess;

}

namespace graphics {

class RenderData;
class TextureManager;

class ParticleRenderer
{
public:
	ParticleRenderer(resource::IResourceAccess* resourceAccess, TextureManager* textureManager);
	~ParticleRenderer() = default;

	bool init(RenderData& renderData, std::shared_ptr<Camera> camera);

	void onIo(RenderData& renderData);
	void onRender(RenderData& renderData, const glm::mat4& viewProjectionMatrix);

	void onDebugIo(RenderData& renderData);
	void onDebugRender(RenderData& renderData, const glm::mat4& viewProjectionMatrix);

	void addParticleRenderBatch(std::shared_ptr<ParticleRenderBatch> batch);
	void clearParticleRenderBatches();

	void setRenderScale(const float renderScale);

	void setCamera(std::shared_ptr<Camera> camera);

private:
	struct ParticleRenderStep
	{
		std::string texture;
		int startIndex;
		int count;
	};

	bool createDebugShader(RenderData& renderData);
	void setupDebugVAO(RenderData& renderData);

	bool createShader(RenderData& renderData);
	void setupVAO(RenderData& renderData);

	std::unique_ptr<std::istream> getFileIstream(const std::string& assetPath);
	void prepareRenderData();
	std::shared_ptr<Camera> camera;


	GLuint FBO;
	float renderScale;
	resource::IResourceAccess* resourceAccess;
	TextureManager* textureManager;

	std::map<std::string,std::vector<std::shared_ptr<ParticleRenderBatch>>> batchMap;
	std::vector<ParticleRenderNode> renderNodes;
	std::vector<ParticleRenderStep> renderSteps;

	// Debug
	GLuint debugVAO;
	GLuint debugVBO;

	GLuint debugProgram;
	GLint debugViewProjectionMatrixUniform;
	GLsizeiptr debugBufferSize;

	// Standard rendering
	GLuint VAO;
	GLuint VBO;

	GLuint program;
	GLint viewProjectionMatrixUniform;
	GLint texCoordUniform;
	GLint texSizeUniform;
	GLsizeiptr currentBufferSize;
};

} } }

#endif
