/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GEOMETRYSET_H_
#define NOX_APP_GRAPHICS_GEOMETRYSET_H_

#include "Geometry.h"

#include <memory>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <mutex>

namespace nox { namespace app { namespace graphics
{

/**
 * A set of geometries used for rendering.
 */
class GeometrySet
{
public:
	/**
	 * How the render data should be handled.
	 */
	enum class DataMode
	{
		DYNAMIC,	//!< Update vertex array each time a geometry is changed/deleted/created.
		REQUEST		//!< Update vertex array only when requested.
	};

	/**
	 * Construct a GeometrySet containing geometry of the specified type.
	 * No other geometry types can be added to this set.
	 * @param primitiveType Geometry primitive type to keep in this set.
	 */
	GeometrySet(GeometryPrimitive primitiveType, DataMode mode = DataMode::DYNAMIC);

	/**
	 * Create a triangle managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Triangle* createTriangle();

	/**
	 * Create a quad managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Quad* createQuad();

	/**
	 * Create a line managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Line* createLine();

	/**
	 * Create a polygon managed by this set.
	 * @param numVertices Number of vertices in the polygon.
	 * @return Pointer to the geometry created.
	 */
	Polygon* createPolygon(unsigned int numVertices);

	/**
	 * Create a loop of lines managed by this set.
	 *
	 * If it is closed, the number of lines drawn is equal to the number of vertices,
	 * otherwise it is one less (missing the one from end to begin).
	 *
	 * @param numVertices Number of vertices in the line loop.
	 * @param closed If a line should be drawn from the last vertex to the first.
	 * @return Pointer to the geometry created.
	 */
	LineLoop* createLineLoop(unsigned int numVertices, bool closed = true);

	/**
	 * Remove a geometry from the set.
	 * @param geometry Geometry to remove.
	 * @return true if found in set and removed. Otherwise false.
	 */
	bool removeGeometry(Geometry* geometry);

	/**
	 * Request that the render data should be updated.
	 * The size returned is the actual size of the data, and should be
	 * used rather than the size of the vertex vector.
	 * @return Size of the updated render data.
	 */
	unsigned int updateRenderData();

	/**
	 * Get the data mode of the geometry.
	 */
	DataMode getDataMode() const;

	/**
	 * Get the type of geometry primitive in this set.
	 */
	GeometryPrimitive getGeometryPrimitive() const;

	/**
	 * Get the vertex data in this set used for rendering.
	 * @return Vertex data.
	 */
	const std::vector<ObjectCoordinate>& getVertexData() const;

	/**
	 * Called when one of the geometries in this set changes its vertices.
	 * Update this set's representation of the vertices.
	 * @param geometry Geometry changed.
	 */
	void onGeometryChanged(Geometry* geometry);

	/**
	 * Set if the set is active or not.
	 * @param state Active or not.
	 */
	void setActive(bool state);

	/**
	 * Check if the set is active.
	 * The active state is an indication for the user of this instance
	 * if it should use it or not at the moment.
	 * E.g. the renderer renders it or don't.
	 */
	bool isActive() const;
	
	/**
	 * Clear all geometry within set.
	 */
	void clear();
	
private:
	void reserveRenderDataSpace(Geometry* geometry);
	void updateGeometryStartPositionMap();

	bool active;

	GeometryPrimitive geometryPrimitive;
	DataMode dataMode;

	std::vector<std::unique_ptr<Geometry>> geometryContainer;

	std::map<GeometryType, std::unordered_set<std::size_t>> vacantGeometry;
	std::unordered_map<Geometry*, bool> activeGeometry;
	std::unordered_map<Geometry*, size_t> containerPosition;

	std::vector<ObjectCoordinate> renderVertices;
	std::unordered_map<Geometry*, unsigned int> geometryRenderDataStart;
	unsigned int renderDataSize;
	std::mutex renderDataMutex;
};

} } }

#endif
