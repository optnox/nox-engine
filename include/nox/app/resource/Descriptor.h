/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_DESCRIPTOR_H_
#define NOX_APP_RESOURCE_DESCRIPTOR_H_

#include <nox/common/api.h>
#include <algorithm>
#include <string>

namespace nox { namespace app
{
namespace resource
{

/**
 * Representation of a resource at a path.
 */
class NOX_API Descriptor
{
public:
	Descriptor();
	Descriptor(const std::string& path);
	Descriptor(const char* path);

	const std::string& getPath() const;

	/**
	 * Retrieve the file extension of the file at the Descriptor's path.
	 * @return The extension of the file described by this object, empty string if the extension
	 *   does not exist.
	 */
	std::string getExtension() const;

private:
	std::string path;
};

inline const std::string& Descriptor::getPath() const
{
	return this->path;
}

inline std::string Descriptor::getExtension() const
{
	std::string file = this->path;

	// Find the last '\' or '/', cut off the string after this to get the file name.
	size_t lastSep = file.find_last_of("\\/");
	if (lastSep != std::string::npos)
	{
		file = file.substr(lastSep+1);
	}

	// Find the last '.'. If the last dot is the first character of the file name, this is a
	// hidden UNIX file with no extension (i.e., ".gitignore"). The file extension is also
	// undefined if the last dot is the last character of the string (i.e., "player.png.").
	size_t lastDot = file.find_last_of(".");
	if (lastDot == std::string::npos || lastDot == 0 || lastDot == file.length() - 1)
	{
		// No file extension available
		return "";
	}

	return file.substr(lastDot + 1);
}

}
} }

#endif
