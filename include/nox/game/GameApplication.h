#ifndef NOX_GAME_GAMEAPPLICATION_H_
#define NOX_GAME_GAMEAPPLICATION_H_

#include <nox/app/SdlApplication.h>

namespace nox { namespace game
{

class GameController;
class GameWindowView;

class GameApplication: public app::SdlApplication
{
public:
	GameApplication(GameController* gameController);

	void onUpdate(const Duration& deltaTime);

	virtual void onSdlEvent(const SDL_Event& event) override;

private:
	GameController* gameController;
};


} }

#endif
