#ifndef NOX_GAME_GAMECONTROLLER_H_
#define NOX_GAME_GAMECONTROLLER_H_

#include <nox/game/GameWindowView.h>
#include <nox/game/GameApplication.h>

#include <nox/app/log/Logger.h>
#include <nox/common/types.h>
#include <nox/common/api.h>
#include <nox/common/platform.h>

#include <nox/app/SdlApplication.h>
#include <nox/app/resource/cache/Cache.h>

#include <nox/logic/Logic.h>
#include <nox/logic/world/WorldManager.h>


namespace nox {

namespace app
{
namespace storage
{
class IDataStorage;
}
}

namespace game
{

class GameController
{
public:
	GameController();
	~GameController();

	virtual std::string getGameName() const;
	virtual std::string getOrganizationName() const;

	virtual std::unique_ptr<GameApplication> createApplication();
	virtual std::unique_ptr<logic::Logic> createLogic() const;
	virtual std::unique_ptr<app::resource::Cache> createResourceCache() const;
	virtual std::unique_ptr<app::resource::Provider> createResourceProviderForAssetDirectory(const std::string& dir) const;
	virtual std::unique_ptr<app::storage::IDataStorage> createDataStorage() const;
	virtual std::unique_ptr<GameWindowView> createGameView() const;

	virtual std::vector<std::string> getResourceCacheDirectories() const;
	virtual std::vector<std::string> getActorDefinitionSubdirectories() const;
	virtual std::vector<std::string> getWorldAtlasDefinitionFiles() const;
	virtual std::string getWorldAtlasName() const;

	virtual void registerActorComponents(nox::logic::world::WorldManager* worldManager) const;
	virtual bool onInit();
	virtual void onUpdate(const Duration& deltaTime);

	int execute(int argc, char **argv);
	bool initialize(int argc, char **argv);
	bool loadWorld(const std::string& worldFile);
	void update(const Duration& deltaTime);

	void setWorldAtlas(const std::string& worldAtlas);

	GameApplication* getApplication() const;
	logic::Logic* getLogic() const;
	GameWindowView* getGameView() const;

	app::graphics::Camera* getCamera() const;
	std::vector<std::string> getCommandLineArguments() const;
	app::graphics::IRenderer* getRenderer() const;

	logic::actor::Actor* createActor(const std::string& name = "Unnamed Actor");

	void onSdlEvent(const SDL_Event& sdlEvent);

private:
	std::unique_ptr<GameApplication> application;
	logic::Logic* logic;
	logic::world::WorldManager* worldManager;
	GameWindowView* gameView;
	mutable app::log::Logger log;
	bool initialized;

	void cleanup();
	bool initializeApplication(int argc, char **argv);
	bool initializeResourceCache();
	bool initializeDataStorage();
	bool initializeLogic();
	bool initializeWindow();
};


}

}


#endif
