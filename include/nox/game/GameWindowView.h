#ifndef NOX_GAME_GAMEWINDOWVIEW_H_
#define NOX_GAME_GAMEWINDOWVIEW_H_

#include <nox/window/RenderSdlWindowView.h>
#include <nox/app/IApplicationContext.h>
#include <nox/app/log/Logger.h>
#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/window/SdlKeyboardControlMapper.h>

namespace nox { namespace game
{

class GameWindowView: public window::RenderSdlWindowView
{
public:
	GameWindowView(app::IApplicationContext* appContext, const std::string& windowTitle);

	bool initialize(logic::ILogicContext* logicContext) override;

	void onKeyPress(const SDL_KeyboardEvent& event) override;
	void onKeyRelease(const SDL_KeyboardEvent& event) override;

	void onControlledActorChanged(logic::actor::Actor* controlledActor) override;

	void addAtlasDefinitionFile(const std::string& atlasDefitionFile);

	/**
	 * Overrides the current world texture atlas (if the renderer has been created), or assigns it
	 * after the renderer has been created.
	 * @param jsonDefFile The name of the world texture atlas to apply to the renderer. The texture atlas
	 *   must have been defined in a parsed atlas definition file (GameWindowView::addAtlasDefinitionFile).
	 */
	void setWorldTextureAtlas(const std::string& worldAtlasName);

	/**
	 * Set the method of control for the Actor controlled by this view. The Keyboard Control Mapper must already
	 * be initiated with a control scheme, or GameWindowView::loadControlDefinitions must be called at a later
	 * point in time.
	 *
	 * Note! If you have overridden GameWindowView::onKeyPress, View::onControlledActorChanged, or
	 * GameWindowView::onKeyRelease in your subclass, you must manually call both these methods for the control
	 * mapper to not misbehave.
	 */
	void setControlScheme(std::unique_ptr<window::SdlKeyboardControlMapper> keyMapper);

	/**
	 * Load a new control scheme definition file into the Keyboard Control Mapper assigned via
	 * GameWindowView::setControlSceme. If setControlScheme is re-called after loadControlDefinitions,
	 * the effects of this method is overwritten. If no Keyboard Control Mapper, or the provided
	 * file exists, this method has no effect and returns false.
	 *
	 * @return True if the file exists, a keyboard control mapper exists, and the scheme was successfully
	 * 	applied. False is returned if either step failed.
	 */
	bool loadControlDefinitions(const std::string& controlFile);

protected:
	virtual void onRendererCreated(app::graphics::IRenderer* renderer) override;

private:
	void loadTextureAtlas();

	app::log::Logger log;
	app::IApplicationContext* appContext;
	app::graphics::IRenderer* renderer;

	std::vector<std::string> atlasDefinitonFiles;
	std::string worldAtlas;

	std::unique_ptr<window::SdlKeyboardControlMapper> controlMapper;
	logic::event::IEventBroadcaster* eventBroadcaster;
};

} }

#endif
