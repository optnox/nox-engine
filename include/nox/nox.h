/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_NOX_H_
#define NOX_NOX_H_

#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/ActorIdentifier.h>
#include <nox/logic/actor/SyncAttributes.h>
#include <nox/logic/actor/Component.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/actor/event/ActorEvent.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/ActorFactory.h>
#include <nox/logic/graphics/actor/ActorSprite.h>
#include <nox/logic/graphics/actor/ActorGraphics.h>
#include <nox/logic/graphics/actor/ActorLight.h>
#include <nox/logic/graphics/event/DebugGeometryChange.h>
#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/physics/actor/ActorGravitation.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/IVisibilityMapper.h>
#include <nox/logic/physics/joint.h>
#include <nox/logic/physics/PhysicsSimulation.h>
#include <nox/logic/physics/GravitationalForce.h>
#include <nox/logic/physics/box2d/Box2DVisibilityMapper.h>
#include <nox/logic/physics/box2d/box2d_utils.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/physics_utils.h>
#include <nox/logic/ILogicContext.h>
#include <nox/logic/audio/actor/ActorAudio.h>
#include <nox/logic/audio/event/PlaySound.h>
#include <nox/logic/world/WorldManager.h>
#include <nox/logic/world/WorldLoader.h>
#include <nox/logic/world/WorldWriter.h>
#include <nox/logic/world/event/ActorRemoved.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/logic/event/EventListenerManager.h>
#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/BooleanEvent.h>
#include <nox/logic/event/EventManager.h>
#include <nox/logic/View.h>
#include <nox/logic/Logic.h>
#include <nox/logic/control/actor/Actor2dDirectionControl.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/control/actor/Actor2dRotationControl.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/net/NetworkManager.h>
#include <nox/logic/net/ping_convo.h>
#include <nox/logic/net/join_convo.h>
#include <nox/logic/net/protocol.h>
#include <nox/logic/net/ClientNetworkManager.h>
#include <nox/logic/net/IPacketTranslator.h>
#include <nox/logic/net/BroadcastReceiver.h>
#include <nox/logic/net/RemoteClient.h>
#include <nox/logic/net/ClientStats.h>
#include <nox/logic/net/ServerNetworkManager.h>
#include <nox/logic/net/SocketTcp.h>
#include <nox/logic/net/AccessList.h>
#include <nox/logic/net/DefaultPacketTranslator.h>
#include <nox/logic/net/Interpolator.h>
#include <nox/logic/net/UserData.h>
#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/ServerDelegate.h>
#include <nox/logic/net/event/ClientConnected.h>
#include <nox/logic/net/event/CreateSynchronized.h>
#include <nox/logic/net/event/DestroySynchronized.h>
#include <nox/logic/net/event/ConnectionStarted.h>
#include <nox/logic/net/event/ClientDisconnected.h>
#include <nox/logic/net/event/ConnectionFailed.h>
#include <nox/logic/net/event/NetworkOutEvent.h>
#include <nox/logic/net/event/ServerBroadcastEvent.h>
#include <nox/logic/net/event/ConnectionSuccess.h>
#include <nox/logic/net/BroadcastSender.h>
#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/component/SyncIn.h>
#include <nox/logic/net/Conversation.h>
#include <nox/logic/net/LocalClient.h>
#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/CommunicationController.h>
#include <nox/logic/net/ConnectionListener.h>
#include <nox/logic/net/Packet.h>
#include <nox/util/process/ProcessManager.h>
#include <nox/util/process/Implementation.h>
#include <nox/util/thread/ThreadSafeMap.h>
#include <nox/util/thread/ThreadBarrier.h>
#include <nox/util/thread/ThreadSafeQueue.h>
#include <nox/util/chrono_utils.h>
#include <nox/util/math/geometry.h>
#include <nox/util/math/Circle.h>
#include <nox/util/math/exponential.h>
#include <nox/util/math/vector_math.h>
#include <nox/util/math/interpolate.h>
#include <nox/util/math/Line.h>
#include <nox/util/math/Box.h>
#include <nox/util/math/angle.h>
#include <nox/util/math/equality.h>
#include <nox/util/Booker.h>
#include <nox/util/boost_utils.h>
#include <nox/util/performance.h>
#include <nox/util/string_utils.h>
#include <nox/util/json_utils.h>
#include <nox/util/Timer.h>
#include <nox/util/algorithm.h>
#include <nox/util/IniFile.h>
#include <nox/util/GenericObjectFactory.h>
#include <nox/util/Mask.h>
#include <nox/util/Buffer.h>
#include <nox/util/Clock.h>
#include <nox/nox.h>
#include <nox/app/graphics/SubRenderer.h>
#include <nox/app/graphics/TexturePackerTextureAtlasReader.h>
#include <nox/app/graphics/render_utils.h>
#include <nox/app/graphics/2d/Light.h>
#include <nox/app/graphics/2d/Geometry.h>
#include <nox/app/graphics/2d/StenciledTiledTextureGenerator.h>
#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/app/graphics/2d/TiledTextureGenerator.h>
#include <nox/app/graphics/2d/TiledTextureRenderer.h>
#include <nox/app/graphics/2d/WrappedSpriteRenderNode.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/app/graphics/2d/DebugRenderer.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/app/graphics/2d/LightRenderNode.h>
#include <nox/app/graphics/2d/GeometrySet.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/2d/BackgroundGradient.h>
#include <nox/app/graphics/2d/Camera.h>
#include <nox/app/graphics/2d/SpriteRenderNode.h>
#include <nox/app/graphics/2d/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/TextureAtlasReader.h>
#include <nox/app/graphics/TextureManager.h>
#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/TextureRenderer.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/storage/IDataStorage.h>
#include <nox/app/storage/DataStorageBoost.h>
#include <nox/app/ApplicationProcess.h>
#include <nox/app/log/OutputStream.h>
#include <nox/app/log/OutputManager.h>
#include <nox/app/log/Output.h>
#include <nox/app/log/Logger.h>
#include <nox/app/log/Message.h>
#include <nox/app/Application.h>
#include <nox/app/audio/AudioBuffer.h>
#include <nox/app/audio/AudioBufferPositionProvider.h>
#include <nox/app/audio/AudioSystem.h>
#include <nox/app/audio/AudioListenerPositionProvider.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/provider/Provider.h>
#include <nox/app/resource/Directory.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/loader/ILoader.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IHandleDestructionListener.h>
#include <nox/app/resource/cache/Cache.h>
#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/resource/data/IExtraData.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/IApplicationContext.h>
#include <nox/app/SdlApplication.h>
#include <nox/game/GameController.h>
#include <nox/game/GameApplication.h>
#include <nox/game/GameWindowView.h>
#include <nox/common/compat.h>
#include <nox/common/types.h>
#include <nox/common/api.h>
#include <nox/common/platform.h>
#include <nox/window/SdlKeyboardControlMapper.h>
#include <nox/window/AudioListenerCamera.h>
#include <nox/window/SdlWindowView.h>
#include <nox/window/RenderSdlWindowView.h>


#ifdef NOX_USING_OPENAL
#	include <nox/app/audio/openal/OpenALSystem.h>
#	include <nox/app/audio/openal/OpenALBuffer.h>
#endif

#ifdef NOX_USING_CAUDIO
#	include <nox/app/audio/caudio/CAudioSystem.h>
#	include <nox/app/audio/caudio/CAudioBuffer.h>
#endif

#endif
