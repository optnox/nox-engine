/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_WINDOW_SDLKEYBOARDCONTROLMAPPER_H_
#define NOX_WINDOW_SDLKEYBOARDCONTROLMAPPER_H_

#include <nox/app/resource/Handle.h>
#include <nox/logic/control/event/Action.h>

#include <SDL_keyboard.h>

#include <map>
#include <unordered_map>
#include <vector>

namespace nox { namespace window
{

/**
 * Generates logic::control::Action events based on the keyboard input.
 *
 * The mapping from keys to events is loaded from a JSON file with loadKeyboardLayout().
 * There are four types on control actions (all based on logic::control::Action, so see there for more info):
 * - Vector.
 * - Strength.
 * - Switch.
 * - Toggle.
 *
 * ## Key Strings
 * For mapping a key string to an actual SDL_Scancode, the SDL_GetScancodeFromName() is used.
 * So all key strings must match a string in SDL_GetScancodeFromName(). See http://wiki.libsdl.org/SDL_Scancode for all
 * the possible values.
 *
 * Since SDL_Scancode is used, the keys are independent of the keyboard layout. So mapping WASD to actions would result
 * in the same physical location on both a qwerty and dvorak keyboard. The qwerty layout is always the layout used for SDL_Scancode.
 *
 * ## Control Types
 *
 * Each control type has its own property in the JSON file:
 * - Vector: __vectorControls__
 * - Strength: __strengthControls__
 * - Switch: __switchControls__
 * - Toggle: __toggleControls__
 *
 * ### Vector Controls
 * The vector controls is a set of actions and directions belonging to these actions. For example there can be
 * an action "move" and four directions vec2(1, 0), vec2(0, 1), vec2(-1, 0) and vec2(0, -1) (there is no limit).
 * All directions have a key mapped to it. For example for "move" you could have D, W, A, S matching the directions
 * mentioned above. Combining several directions will average the vectors. For example holding W and D will trigger
 * a direction of vec2(0.7, 0.7). All directions are normalized.
 *
 * The format for the vector controls is as follows:
 * - __actions__:array[string] - A list of all the available actions. An action used in __buttons__ must be listed here.
 * - __buttons__:map[array[object]] - Keys mapping to arrays of direction actions.
 *	 + __action__:string - Name of the action that this key will trigger.
 *	 + __vector__:vec2 - The direction of the action that this key will trigger.
 *
 * ### Strength Controls
 * These are mapped as switch controls where pressing a key will result in strength 1, while releasing will result in strength 0.
 * See Switch Controls for more.
 *
 * ### Switch Controls
 * A switch control is either on or off. It is off when a key isn't pressed and on when a key is pressed. The JSON format is as follows:
 * - __actions__:array[object] - All the actions available.
 *	 + __name__:string - Name of the action that will be triggered.
 *	 + __keys__:array[string] - All the keys that will trigger this action.
 *
 * ### Toggle Controls
 * A toggle control has no state, it is just a plain signal. Only a key press will trigger a toggle action. The JSON format
 * is exactly the same as Switch Controls.
 */
class SdlKeyboardControlMapper
{
public:
	using ControlEventContainer = std::vector<std::shared_ptr<logic::control::Action>>;

	SdlKeyboardControlMapper();

	/**
	 * Load the layout from a resource.
	 * See the class documentation for the JSON format.
	 * @param controlLayoutResource Handle to the resource to load.
	 */
	void loadKeyboardLayout(const std::shared_ptr<app::resource::Handle>& controlLayoutResource);

	/**
	 * Set the Actor that this mapper will set as the Actor for each logic::control::Action event
	 * generated.
	 *
	 * @param actor Actor to set as controlled
	 */
	void setControlledActor(logic::actor::Actor* actor);

	/**
	 * Check if the mapper has a controlled Actor set.
	 *
	 * If not the logic::control::Action events mapped will have no effect as they specify no
	 * Actor to control.
	 */
	bool hasControlledActor() const;

	/**
	 * Map a key press to logic::control::Action events.
	 *
	 * @param keysym Key press to mapp.
	 * @return All logic::control::Action events generated.
	 */
	ControlEventContainer mapKeyPress(const SDL_Keysym& keysym);

	/**
	 * Map a key release to logic::control::Action events.
	 *
	 * @param keysym Key release to mapp.
	 * @return All logic::control::Action events generated.
	 */
	ControlEventContainer mapKeyRelease(const SDL_Keysym& keysym);

	/**
	 * Map all keys as released.
	 *
	 * Keys currently registered as pressed will be logically released and control::ACtion
	 * events will be generated for them.
	 *
	 * @return All logic::control::Action events generated.
	 */
	ControlEventContainer mapAllKeysReleased();

private:
	enum KeyState
	{
		PRESS,
		RELEASE
	};

	struct DirectionAction
	{
		std::string action;
		glm::vec2 direction;
	};

	/**
	 * Handles mapping a directional event, This is done by simulating a joystic with the keyboard.
	 * @param keysym The key that was pressed.
	 * @param[out] controlEvents any event that is created is put into this array refrence
	 */
	void mapMovementEvent(const SDL_Scancode& keysym, ControlEventContainer& controlEvents);

	/**
	 * A switch, this sends the current state of the key and
	 * uses that for the input
	 * @param keyEvent the key that was presses.
	 * @param controlName the name of the control it is mapped to
	 * @returns the control event
	 */
	std::shared_ptr<logic::control::Action> mapSwitchEvent(const SDL_Scancode& keyEvent, const std::string& controlName);

	/**
	 * Map a key event to an actual controller event.
	 * @param key Key to map.
	 * @return The controller event created.
	 */
	ControlEventContainer mapKeyEvent(const SDL_Scancode& keysym);

	std::map<SDL_Scancode, std::vector<DirectionAction>> directionMapping;
	std::unordered_map<std::string, std::map<SDL_Scancode, glm::vec2>> directionActionStates;

	std::map<SDL_Scancode, std::vector<std::string>> analogueMapping;
	std::map<SDL_Scancode, std::vector<std::string>> switchMapping;
	std::map<SDL_Scancode, std::vector<std::string>> toggleMapping;

	std::map<SDL_Scancode, bool> keyPressed;

	logic::actor::Actor* controlledActor;
};

} }

#endif
