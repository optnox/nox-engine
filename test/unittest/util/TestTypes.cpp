/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <random>

#include <gtest/gtest.h>

#include <nox/util/RandomType.h>

template <typename T>
class TestRealTypes: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
	}
};

TYPED_TEST_CASE_P(TestRealTypes);


TYPED_TEST_P(TestRealTypes, TestRealConstruction)
{
	nox::util::RandomType<TypeParam> rt = nox::util::RandomType<TypeParam>();
	
	ASSERT_EQ(rt.getValue(), static_cast<TypeParam>(0));
	ASSERT_EQ(rt.getOffset(), static_cast<TypeParam>(0));
	ASSERT_TRUE(rt == static_cast<TypeParam>(0));
	ASSERT_TRUE(rt.get() == static_cast<TypeParam>(0));
}

TYPED_TEST_P(TestRealTypes, TestRealBarriers)
{
	nox::util::RandomType<TypeParam> rt = nox::util::RandomType<TypeParam>(static_cast<TypeParam>(10), static_cast<TypeParam>(1));
	
	ASSERT_EQ(rt.getValue(), static_cast<TypeParam>(10));
	ASSERT_EQ(rt.getOffset(), static_cast<TypeParam>(1));

	TypeParam firstVal = rt;
	bool foundDifferent = false;

	for ( int i = 0; i < 10000; i++ )
	{
		TypeParam currentVal = rt;
		ASSERT_TRUE(currentVal >= static_cast<TypeParam>(9));
		ASSERT_TRUE(currentVal <= static_cast<TypeParam>(11));

		if (currentVal != firstVal)
		{
			foundDifferent = true;
		}
	}
	ASSERT_TRUE(foundDifferent);
}

REGISTER_TYPED_TEST_CASE_P(TestRealTypes, TestRealConstruction, TestRealBarriers);

typedef ::testing::Types< float, double, long double > RealTypes;

INSTANTIATE_TYPED_TEST_CASE_P(RealTypeTests, TestRealTypes, RealTypes);


template <typename T>
class TestIntegerTypes: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
	}
};

TYPED_TEST_CASE_P(TestIntegerTypes);


TYPED_TEST_P(TestIntegerTypes, TestIntegerConstruction)
{
	nox::util::RandomType<TypeParam, std::uniform_int_distribution<TypeParam>> rt = nox::util::RandomType<TypeParam, std::uniform_int_distribution<TypeParam>>();
	
	ASSERT_EQ(rt.getValue(), static_cast<TypeParam>(0));
	ASSERT_EQ(rt.getOffset(), static_cast<TypeParam>(0));
	ASSERT_TRUE(rt == static_cast<TypeParam>(0));
	ASSERT_TRUE(rt.get() == static_cast<TypeParam>(0));
}

TYPED_TEST_P(TestIntegerTypes, TestIntegerBarriers)
{
	nox::util::RandomType<TypeParam, std::uniform_int_distribution<TypeParam>> rt = nox::util::RandomType<TypeParam, std::uniform_int_distribution<TypeParam>>(static_cast<TypeParam>(10), static_cast<TypeParam>(1));
	
	ASSERT_EQ(rt.getValue(), static_cast<TypeParam>(10));
	ASSERT_EQ(rt.getOffset(), static_cast<TypeParam>(1));

	TypeParam firstVal = rt;
	bool foundDifferent = false;

	for ( int i = 0; i < 10000; i++ )
	{
		TypeParam currentVal = rt;
		ASSERT_TRUE(currentVal >= static_cast<TypeParam>(9));
		ASSERT_TRUE(currentVal <= static_cast<TypeParam>(11));

		if (currentVal != firstVal)
		{
			foundDifferent = true;
		}
	}
	ASSERT_TRUE(foundDifferent);
}

REGISTER_TYPED_TEST_CASE_P(TestIntegerTypes, TestIntegerConstruction, TestIntegerBarriers);

typedef ::testing::Types< signed char, signed short, signed int, signed long, signed long long, unsigned char, unsigned short, unsigned int, unsigned long, unsigned long long > IntegerTypes;

INSTANTIATE_TYPED_TEST_CASE_P(IntegerTypeTests, TestIntegerTypes, IntegerTypes);

