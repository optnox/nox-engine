/*
 * Copyright 2014 Suttung Digital AS.
 */

#include <gtest/gtest.h>

#include <memory>
#include <string>
#include <random>
#include <glm/gtc/constants.hpp>
#include <nox/util/math/angle.h>

TEST(TestMath, ClampAngle)
{
	const float angleStep = glm::pi<float>() / 100.0f;
	const float angleMin = -glm::pi<float>() * 100.0f;
	const float angleMax = -angleMin;
	float angle = angleMin;

	while (angle < angleMax)
	{
		const float clampedAngle = nox::math::clampAngle(angle);
		EXPECT_GT(clampedAngle, -glm::pi<float>());
		EXPECT_LE(clampedAngle, glm::pi<float>());

		angle += angleStep;
	}
}

TEST(TestMath, ClampAnglePositive)
{
	const float angleStep = glm::pi<float>() / 100.0f;
	const float angleMin = -glm::pi<float>() * 100.0f;
	const float angleMax = glm::pi<float>() * 100.0f;
	float angle = angleMin;

	while (angle < angleMax)
	{
		const float clampedAngle = nox::math::clampAnglePositive(angle);
		EXPECT_GE(clampedAngle, 0.0f);

		angle += angleStep;
	}
}

TEST(TestMath, AngleDifference)
{
	EXPECT_FLOAT_EQ(glm::half_pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::half_pi<float>()));
	EXPECT_FLOAT_EQ(-glm::half_pi<float>(), nox::math::calculateAngleDifference(glm::half_pi<float>(), 0.0f));

	EXPECT_FLOAT_EQ(-glm::pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::pi<float>()));
	EXPECT_FLOAT_EQ(-glm::pi<float>(), nox::math::calculateAngleDifference(glm::pi<float>(), 0.0f));

	EXPECT_FLOAT_EQ(-glm::half_pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::pi<float>() * 1.5f));
	EXPECT_FLOAT_EQ(glm::half_pi<float>(), nox::math::calculateAngleDifference(glm::pi<float>() * 1.5f, 0.0f));

	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(0.0f, glm::pi<float>() * 2.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 2.0f, 0.0f));

	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(0.0f, 0.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(10.0f, 10.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(999.0f, 999.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(983457.234f, 983457.234f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(-glm::pi<float>(), glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(-glm::pi<float>(), -glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), -glm::pi<float>()));
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), -glm::pi<float>() * 17.0f), 1.0e-5f);
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 3.0f, -glm::pi<float>() * 9.0f), 1.0e-5f);
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 2.0f, -glm::pi<float>() * 4.0f), 1.0e-5f);

	std::random_device randomDevice;
	std::minstd_rand randomEngine(randomDevice());
	std::uniform_real_distribution<float> angleDist(-glm::pi<float>() * 100.0f, glm::pi<float>() * 100.0f);
	const unsigned int numTests = 1000;

	for (unsigned int i = 0; i < numTests; i++)
	{
		const float angleA = angleDist(randomEngine);
		const float angleB = angleDist(randomEngine);

		const float difference = nox::math::calculateAngleDifference(angleA, angleB);

		EXPECT_GT(difference, -glm::pi<float>());
		EXPECT_LE(difference, glm::pi<float>());

		const float expectedDifference = nox::math::clampAngle(angleB - angleA);

		EXPECT_FLOAT_EQ(expectedDifference, difference);
	}
}

TEST(TestMath, AngleWithinCone)
{
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::half_pi<float>(), 0.0f, glm::pi<float>()));
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::pi<float>(), 0.0f, glm::pi<float>() * 2.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::pi<float>() * 1.5f, glm::pi<float>(), glm::pi<float>() * 2.0f));

	EXPECT_FALSE(nox::math::angleIsWithinCone(glm::pi<float>() * 1.5f, 0.0f, glm::pi<float>()));

	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.5f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.7f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.3f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_FALSE(nox::math::angleIsWithinCone(-glm::pi<float>(),  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_FALSE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.8f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
}
