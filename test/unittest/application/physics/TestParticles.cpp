/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <json/reader.h>

#include <string>
#include <nox/util/json_utils.h>
#include <nox/logic/physics/box2d/box2d_utils.h>
#include <nox/logic/physics/particle/ParticleEmitter.h>
#include <nox/logic/physics/particle/ParticleComponent.h>


const std::string uglyCode = "{\n\"ResorviorParticleSystem\":{\n    \"components\":\n    {\n            \"Emitters\": [\n                {\n                    \"name\": \"insertALovelyEmmitterName\",\n                    \"offset\": {\"x\":10.0, \"y\":25.0},\n                    \"size\": {\"x\":5.0, \"y\":5.0},\n                    \"emitRate\": 15.0,\n                    \"velocityDirection\": {\"x\":2.0, \"y\":1.0},\n                    \"particle\": [\n                        {\n                            \"name\": \"mrPink\",\n                            \"velocityScalar\": 5.0,\n                            \"color\": {\"r\": 1.0, \"g\": 0.0, \"b\": 1.0, \"a\": 0.7},\n                            \"lifetime\": 5.0\n                        },\n                        {\n                            \"name\": \"mrBrown\",\n                            \"velocityScalar\": 7.0,\n                            \"color\": {\"r\": 0.5, \"g\": 0.25, \"b\": 0.0, \"a\": 1.0},\n                            \"lifetime\": 2.0\n                        },\n                        {\n                            \"name\": \"mrBlonde\",\n                            \"velocityScalar\": 9.0,\n                            \"color\": {\"r\": 0.9, \"g\": 0.9, \"b\": 0.7, \"a\": 0.7},\n                            \"lifetime\": 9.0\n                        }\n                    ]\n                },\n                {\n                    \"name\": \"insertAnotherLovelyEmmitterName\",\n                    \"offset\": {\"x\":10.0, \"y\":-25.0},\n                    \"size\": {\"x\":5.0, \"y\":5.0},\n                    \"emitRate\": 15.0,\n                    \"velocityDirection\": {\"x\":1.0, \"y\":-2.0},\n                    \"particle\":[\n                        {\n                            \"name\": \"mrOrange\",\n                            \"velocityScalar\": 3.0,\n                            \"color\": {\"r\": 1.0, \"g\": 0.5, \"b\": 0.25, \"a\": 0.9},\n                            \"lifetime\": 7.0\n                        },\n                        {\n                            \"name\": \"mrBlue\",\n                            \"velocityScalar\": 2.0,\n                            \"color\": {\"r\": 0.0, \"g\": 0.0, \"b\": 1.0, \"a\": 1.0},\n                            \"lifetime\": 1.0\n                        }\n                    ]\n                }\n            ]\n        }\n    }\n}\0";

class TestParticles: public ::testing::Test
{
protected:
	virtual void SetUp()
	{

	}

	virtual void TearDown()
	{

	}
};

TEST_F(TestParticles, TestParse)
{
//	nox::logic::physics::ParticleComponent pCom;
//	Json::Value component;
//	Json::Reader reader;
//	ASSERT_TRUE(reader.parse(uglyCode, component));
//	ASSERT_TRUE(component["ResorviorParticleSystem"]["components"]["Emitters"] != 0);
//	ASSERT_TRUE(pCom.initialize(component["ResorviorParticleSystem"]["components"]));
}
