add_executable(test-application
	TestApplication.cpp
)

target_link_libraries(test-application
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(test-application PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)

add_test(
	NAME test-application
	COMMAND test-application
)

add_subdirectory(log)
add_subdirectory(physics)
