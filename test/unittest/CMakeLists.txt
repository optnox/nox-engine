enable_testing()

set(NOX_GTEST_LIBRARY gtest_main)

add_subdirectory(util)
add_subdirectory(application)
add_subdirectory(logic)

add_executable(test-all
	util/TestIni.cpp
	util/TestTypes.cpp
	logic/TestDefaultHexTranslator.cpp
	logic/TestLogicContext.cpp
	application/TestApplication.cpp
	application/physics/TestParticles.cpp
	application/log/TestLog.cpp
)

target_link_libraries(test-all
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

add_test(
	NAME test-all
	COMMAND test-all
)
