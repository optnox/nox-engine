/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>
#include <nox/logic/physics/physics_utils.h>

class TestDefaultHexTranslator: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
	}

};

TEST_F(TestDefaultHexTranslator, 0x)
{
	unsigned int direct = 0x123abc;

	unsigned int indirect = nox::logic::physics::defaultHexTranslator("0x123abc");
	ASSERT_EQ(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("0x123\tABC  ");
	ASSERT_EQ(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("0x123a\nBc");
	ASSERT_EQ(direct, indirect);
}

TEST_F(TestDefaultHexTranslator, poundSign)
{
	unsigned int direct = 0x123abc;

	unsigned int indirect = nox::logic::physics::defaultHexTranslator("#123abc");
	ASSERT_EQ(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("#123ABC  ");
	ASSERT_EQ(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("#123aBc");
	ASSERT_EQ(direct, indirect);
}

TEST_F(TestDefaultHexTranslator, wrongThings)
{
	unsigned int direct = 0x123abc;

	unsigned int indirect = nox::logic::physics::defaultHexTranslator("0xFFF");
	ASSERT_NE(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("asqqeg");
	ASSERT_NE(direct, indirect);

	indirect = nox::logic::physics::defaultHexTranslator("#1fffffffffffffffffffffffffffffffffffffffffff23aBc");
	ASSERT_NE(direct, indirect);
}
