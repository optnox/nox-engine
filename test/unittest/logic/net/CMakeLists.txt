add_executable(test-net
    TestNet.cpp
)

target_link_libraries(test-net
    ${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(test-net PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)

add_test(
	NAME test-net
	COMMAND test-net
)
