/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <memory>
#include <string>

#include <nox/logic/event/Event.h>
#include <nox/logic/net/event/NetworkOutEvent.h>
#include <nox/logic/event/BooleanEvent.h>

using namespace nox::logic::net;

TEST(NetworkOutEventTest, Create)
{
	auto boolEvent = new nox::logic::event::BooleanEvent("BooleanEvent", true);
	auto netEvent = new NetworkOutEvent(boolEvent);
	ASSERT_TRUE(((nox::logic::event::BooleanEvent*)(netEvent->getContainedEvent()))->isTrue());
	delete boolEvent;
	delete netEvent;
}

TEST(NetworkOutEventTest, GetEvent)
{
	auto boolEventTrue = new nox::logic::event::BooleanEvent("BooleanEvent", true);
	auto boolEventFalse = new nox::logic::event::BooleanEvent("BooleanEvent", false);
	auto netEventTrue = new NetworkOutEvent(boolEventTrue);
	auto netEventFalse = new NetworkOutEvent(boolEventFalse);

	ASSERT_TRUE(((nox::logic::event::BooleanEvent*)(netEventTrue->getContainedEvent()))->isTrue());
	ASSERT_FALSE(((nox::logic::event::BooleanEvent*)(netEventFalse->getContainedEvent()))->isTrue());
}

TEST(NetworkOutEventTest, DeleteEvent)
{
	for(int i = 0; i < 100; i++) {
		auto boolEvent = new nox::logic::event::BooleanEvent("BooleanEvent", true);
		auto netEvent = new NetworkOutEvent(boolEvent);
		delete netEvent;
		delete boolEvent;
		ASSERT_FALSE(netEvent == NULL);
	}
}
