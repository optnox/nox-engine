add_executable(test-net-event
    TestNetEvent.cpp
)

target_link_libraries(test-net-event
    ${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(test-net-event PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)

add_test(
	NAME test-net-event
	COMMAND test-net-event
)
