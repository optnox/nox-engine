add_executable(testbed-sdlwindow
	WindowView.h
	WindowView.cpp
	WindowApplication.h
	WindowApplication.cpp
	main.cpp
)

target_link_libraries(testbed-sdlwindow
	${NOX_LINK_TARGET}
)
