/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "WindowView.h"

#include <nox/app/IApplicationContext.h>
#include <glm/gtx/string_cast.hpp>

WindowView::WindowView(nox::app::IApplicationContext* applicationContext, const std::string& windowTitle):
	SdlWindowView(applicationContext, windowTitle, false)
{
	this->log = applicationContext->createLogger();
	this->log.setName("WindowView");
}

void WindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	this->log.info().format("Window resized to %s.", glm::to_string(size).c_str());
}

void WindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	if (event.keysym.sym == SDLK_f)
	{
		if (this->isFullscreen())
		{
			this->log.info().raw("Disabling fullscreen.");
			this->disableFullscreen();
		}
		else
		{
			this->log.info().raw("Enabling fullscreen.");
			this->enableFullscreen();
		}
	}
	else if (event.keysym.sym == SDLK_1)
	{
		this->log.info().raw("Setting fullscreen mode to DESKTOP.");
		this->setFullscreenMode(nox::window::SdlWindowView::Fullscreen::DESKTOP);
	}
	else if (event.keysym.sym == SDLK_2)
	{
		this->log.info().raw("Setting fullscreen mode to REAL.");
		this->setFullscreenMode(nox::window::SdlWindowView::Fullscreen::REAL);
	}
}
