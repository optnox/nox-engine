/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/window/AudioListenerCamera.h>

namespace nox { namespace window
{

AudioListenerCamera::AudioListenerCamera(const glm::uvec2& size):
	app::graphics::Camera(size)
{
}

void AudioListenerCamera::updateListenerPosition(app::audio::AudioSystem* system)
{
	system->setListenerPosition(this->getPosition());

	/* The default "0-angle-vector" is [1,0] - a vector pointing right. We therefore
	 * need to add half a PI to adjust for this, so the "0-angle-vector" points
	 * straight up ([0,1]).
	 */
	float rot = this->getRotation();
	rot += float(glm::pi<float>() / 2.0f);

	glm::vec3 up(std::cos(rot), std::sin(rot), 0.0f);
	system->setListenerUpVector(up);
}

}
}
