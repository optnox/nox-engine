/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/window/RenderSdlWindowView.h>

#include <nox/app/IApplicationContext.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/logic/ILogicContext.h>
#include <nox/logic/graphics/event/DebugGeometryChange.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>

#include <nox/app/audio/AudioSystem.h>
#include <nox/app/audio/AudioBuffer.h>
#include <nox/logic/audio/event/PlaySound.h>
#include <nox/logic/physics/particle/ParticleEvent.h>

#include <assert.h>

namespace nox
{
namespace window
{

RenderSdlWindowView::RenderSdlWindowView(app::IApplicationContext* applicationContext, const std::string& windowTitle):
	SdlWindowView(applicationContext, windowTitle, true),
	applicationContext(applicationContext),
	logicContext(nullptr),
	window(nullptr),
	camera(std::make_shared<AudioListenerCamera>(getWindowSize())),
	rootSceneNode(std::make_shared<app::graphics::TransformationNode>()),
	listener("RenderSdlWindowView")
{
	assert(applicationContext != nullptr);

	this->log = applicationContext->createLogger();
	this->log.setName("SdlRendererWindowView");

	this->listener.addEventTypeToListenFor(logic::graphics::DebugGeometryChange::ID);
	this->listener.addEventTypeToListenFor(logic::audio::PlaySound::ID);
	this->listener.addEventTypeToListenFor(logic::graphics::SceneNodeEdited::ID);
}

RenderSdlWindowView::~RenderSdlWindowView()
{
	this->listener.stopListening();
}

bool RenderSdlWindowView::initialize(logic::ILogicContext* context)
{
	this->logicContext = context;

	if (this->SdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), logic::event::EventListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(logic::graphics::DebugGeometryChange::ID);
	this->listener.addEventTypeToListenFor(logic::physics::ParticleEvent::ID);
	this->listener.startListening();

	return true;
}

void RenderSdlWindowView::render()
{
	assert(this->renderer != nullptr);

	this->renderer->onRender();
	SDL_GL_SwapWindow(this->window);
}

bool RenderSdlWindowView::onWindowCreated(SDL_Window* window)
{
	this->window = window;

	this->renderer = std::make_unique<app::graphics::OpenGlRenderer>();
	std::string shaderDirectory = "nox/shader/";

	if (this->renderer->init(this->applicationContext, shaderDirectory, this->getWindowSize()) == false)
	{
		this->log.fatal().raw("Failed to create OpenGL renderer.");
		return false;
	}
	else
	{
		this->log.verbose().raw("Created OpenGL renderer.");

		this->renderer->setCamera(this->camera);
		this->renderer->setRootSceneNode(this->rootSceneNode);

		this->onRendererCreated(this->renderer.get());

		this->renderer->organizeRenderSteps();
		return true;
	}
}

void RenderSdlWindowView::onSdlEvent(const SDL_Event& event)
{
	this->SdlWindowView::onSdlEvent(event);
}

void RenderSdlWindowView::onDestroy()
{
	this->listener.stopListening();
}

void RenderSdlWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	assert(this->renderer != nullptr);

	this->renderer->resizeWindow(size);
	this->camera->setSize(size);
}

void RenderSdlWindowView::onEvent(const std::shared_ptr<logic::event::Event>& event)
{
	if (event->isType(logic::graphics::SceneNodeEdited::ID))
	{
		auto nodeEvent = static_cast<logic::graphics::SceneNodeEdited*>(event.get());

		if (nodeEvent->getEditAction() == logic::graphics::SceneNodeEdited::Action::CREATE)
		{
			this->rootSceneNode->addChild(nodeEvent->getSceneNode());
		}
		else if (nodeEvent->getEditAction() == logic::graphics::SceneNodeEdited::Action::REMOVE)
		{
			this->rootSceneNode->removeChild(nodeEvent->getSceneNode());
		}
	}
	else if (event->isType(logic::graphics::DebugGeometryChange::ID))
	{
		auto geometryEvent = static_cast<logic::graphics::DebugGeometryChange*>(event.get());

		if (geometryEvent->wasAdded())
		{
			this->renderer->addDebugGeometrySet(geometryEvent->getGeometry());
		}
		else if (geometryEvent->wasRemoved())
		{
			this->renderer->removeDebugGeometrySet(geometryEvent->getGeometry());
		}
	}
	else if (event->isType(logic::audio::PlaySound::ID))
	{
		auto soundEvent = (logic::audio::PlaySound*)event.get();

		app::IApplicationContext* appContext = this->getApplicationContext();
		if (appContext)
		{
			app::audio::AudioSystem* audioSystem = appContext->getAudioSystem();
			if (!audioSystem)
			{
				this->log.error().format("Unable to play sound '%s': no AudioSystem",
						soundEvent->getFileName().c_str());
				return;
			}

			auto buffer = audioSystem->playSound(soundEvent->getFileName());
			if (buffer)
			{
				app::audio::AudioBufferPositionProvider* posProv = soundEvent->getPositionProvider();
				if (posProv)
				{
					posProv->attachSoundBuffer(buffer);
				}
			}
			else
			{
				this->log.error().format("Unable to play sound '%s': AudioSystem failed to play",
						soundEvent->getFileName().c_str());
			}
		}
	}
	else if (event->isType(logic::physics::ParticleEvent::ID))
	{
		//Particle system updated
		auto particleEvent = static_cast<logic::physics::ParticleEvent*>(event.get());

		this->renderer->addParticleRenderBatch(particleEvent->getParticleRenderBatch());
	}
}

AudioListenerCamera* RenderSdlWindowView::getCamera()
{
	return this->camera.get();
}

app::graphics::IRenderer* RenderSdlWindowView::getRenderer()
{
	return this->renderer.get();
}


logic::ILogicContext* RenderSdlWindowView::getLogicContext()
{
	return this->logicContext;
}

app::IApplicationContext* RenderSdlWindowView::getApplicationContext()
{
	return this->applicationContext;
}

}
}
