#include <nox/game/GameController.h>

#include <nox/app/storage/DataStorageBoost.h>
#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/data/JsonExtraData.h>

#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/world/WorldLoader.h>

#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/actor/ActorGravitation.h>
#include <nox/logic/graphics/actor/ActorSprite.h>
#include <nox/logic/graphics/actor/ActorLight.h>
#include <nox/logic/control/actor/Actor2dDirectionControl.h>
#include <nox/logic/control/actor/Actor2dRotationControl.h>
#include <nox/logic/audio/actor/ActorAudio.h>

#include <SDL2/SDL.h>


namespace nox { namespace game
{

GameController::GameController():
	application(nullptr),
	logic(nullptr),
	initialized(false)
{

}

GameController::~GameController()
{
	this->cleanup();
}


std::string GameController::getGameName() const
{
	return "NOX Game";
}

std::string GameController::getOrganizationName() const
{
	return "Unnamed Organization";
}

std::unique_ptr<GameApplication> GameController::createApplication()
{
	return std::make_unique<GameApplication>(this);
}

std::unique_ptr<logic::Logic> GameController::createLogic() const
{
	return std::make_unique<logic::Logic>();
}

std::unique_ptr<app::resource::Cache> GameController::createResourceCache() const
{
	const unsigned mbCacheSize = 512u;
	return std::make_unique<app::resource::LruCache>(mbCacheSize);
}

std::unique_ptr<app::resource::Provider> GameController::createResourceProviderForAssetDirectory(const std::string& dir) const
{
	return std::make_unique<nox::app::resource::BoostFilesystemProvider>(dir);
}

std::unique_ptr<app::storage::IDataStorage> GameController::createDataStorage() const
{
	auto storage = std::make_unique<app::storage::DataStorageBoost>();
	std::string dir = this->application->getStorageDirectoryPath(false);

	if (dir.empty())
	{
		return nullptr;
	}

	storage->initialize(dir);
	return std::move(storage);
}

std::unique_ptr<GameWindowView> GameController::createGameView() const
{
	return std::make_unique<GameWindowView>(this->application.get(), this->getGameName());
}


std::vector<std::string> GameController::getResourceCacheDirectories() const
{
	return std::vector<std::string>();
}

std::vector<std::string> GameController::getActorDefinitionSubdirectories() const
{
	return std::vector<std::string>();
}

std::vector<std::string> GameController::getWorldAtlasDefinitionFiles() const
{
	this->log.warning().raw("GameController::getWorldAtlasDefinitionFiles not overridden: no world atlas can be applied.");
	return std::vector<std::string>();
}

std::string GameController::getWorldAtlasName() const
{
	this->log.warning().raw("GameController::getWorldAtlasName not overridden: no world atlas will be applied.");
	return "";
}


void GameController::registerActorComponents(logic::world::WorldManager* /*worldManager*/) const
{
	/* Empty default implementation - core components are added in initializeLogic() */
}

bool GameController::onInit()
{
	return true;
}

void GameController::onUpdate(const Duration& /*deltaTime*/)
{
}


int GameController::execute(int argc, char **argv)
{
	if (!this->initialized && !this->initialize(argc, argv))
	{
		this->cleanup();
		return -1;
	}

	int retval = this->application->execute();
	this->application->shutdown();

	this->cleanup();
	return retval;
}

bool GameController::initialize(int argc, char **argv)
{
	if (this->initialized)
	{
		return false;
	}

	if (!this->initializeApplication(argc, argv))
	{
		return false;
	}

	if (!this->initializeResourceCache())
	{
		return false;
	}

	if (!this->initializeDataStorage())
	{
		return false;
	}

	if (!this->initializeLogic())
	{
		return false;
	}

	if (!this->initializeWindow())
	{
		return false;
	}

	this->logic->pause(false);

	this->initialized = true;

	if (!this->onInit())
	{
		return false;
	}

	return true;
}

bool GameController::loadWorld(const std::string& worldFile)
{
	if (!this->initialized)
	{
		return false;
	}

	app::resource::IResourceAccess* resAccess = this->application->getResourceAccess();
	app::resource::Descriptor worldDescriptor(worldFile);
	std::shared_ptr<app::resource::Handle> worldHandle = resAccess->getHandle(worldDescriptor);

	if (worldHandle == nullptr)
	{
		return false;
	}

	const app::resource::JsonExtraData* jsonData = worldHandle->getExtraData<app::resource::JsonExtraData>();

	logic::world::WorldLoader loader(this->logic);
	loader.registerControllingView(0, this->gameView);

	if (!loader.loadWorld(jsonData->getRootValue(), this->worldManager))
	{
		return false;
	}

	return true;
}

void GameController::update(const Duration& deltaTime)
{
	this->onUpdate(deltaTime);
	this->gameView->render();
}


GameApplication* GameController::getApplication() const
{
	return this->application.get();
}

logic::Logic* GameController::getLogic() const
{
	return this->logic;
}

GameWindowView* GameController::getGameView() const
{
	return this->gameView;
}

app::graphics::Camera* GameController::getCamera() const
{
	if (!this->gameView)
	{
		return nullptr;
	}

	return this->gameView->getCamera();
}

std::vector<std::string> GameController::getCommandLineArguments() const
{
	if (!this->application)
	{
		return std::vector<std::string>();
	}

	return this->application->getApplicationArguments();
}

app::graphics::IRenderer* GameController::getRenderer() const
{
	if (!this->gameView)
	{
		return nullptr;
	}

	return this->gameView->getRenderer();
}


logic::actor::Actor* GameController::createActor(const std::string& name)
{
	if (!this->initialized)
	{
		return nullptr;
	}

	logic::actor::Actor* actor = this->worldManager->createActor(name);
	return actor;
}


void GameController::onSdlEvent(const SDL_Event& sdlEvent)
{
	if (this->gameView != nullptr)
	{
		this->gameView->onSdlEvent(sdlEvent);
	}
}


void GameController::cleanup()
{
	this->application.release();

	if (this->logic != nullptr)
	{
		this->logic = nullptr;
	}

	this->initialized = false;
}

bool GameController::initializeApplication(int argc, char **argv)
{
	this->application = this->createApplication();
	if (!this->application->init(argc, argv))
	{
		return false;
	}

	this->log = this->application->createLogger();
	this->log.setName("GameController");

	if (!this->application->createDefaultAudioSystem())
	{
		return false;
	}

	return true;
}

bool GameController::initializeResourceCache()
{
	std::unique_ptr<app::resource::Cache> resourceCache = this->createResourceCache();
	if (resourceCache == nullptr)
	{
		return false;
	}

	/**
	 * Query for asset directories and add them to the ResourceCache.
	 */
	std::vector<std::string> assetDirs = this->getResourceCacheDirectories();
	for (std::string &dir : assetDirs)
	{
		std::unique_ptr<app::resource::Provider> provider = this->createResourceProviderForAssetDirectory(dir);
		if (provider != nullptr)
		{
			if (!resourceCache->addProvider(std::move(provider)))
			{
				this->log.fatal().format("Unable to add resource provider for asset directory '%s'", dir.c_str());
				return false;
			}
		}
	}

	/**
	 * Add the default resource loader classes.
	 */
	resourceCache->addLoader(std::make_unique<nox::app::resource::OggLoader>());
	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->application->createLogger()));

	this->application->setResourceCache(std::move(resourceCache));

	return true;
}

bool GameController::initializeDataStorage()
{
	std::unique_ptr<app::storage::IDataStorage> storage = nullptr;

	try
	{
		storage = this->createDataStorage();
	}
	catch (std::exception ex)
	{
		this->log.fatal().format("Exception thrown in createDataStorage(). Description: '%s'", ex.what());
		return false;
	}

	this->application->setDataStorage(std::move(storage));
	return true;
}

bool GameController::initializeLogic()
{
	std::unique_ptr<logic::Logic> uniqueLogic = this->createLogic();
	if (uniqueLogic == nullptr)
	{
		return false;
	}

	this->logic = uniqueLogic.get();
	this->application->addProcess(std::move(uniqueLogic));

	// Create and initialize the Box2D physics back-end.
	std::unique_ptr<logic::physics::Box2DSimulation> physics = std::make_unique<logic::physics::Box2DSimulation>(this->logic);
	physics->setLogger(this->application->createLogger());
	this->logic->setPhysics(std::move(physics));

	// Create and initialize the world manager. All engine provided Components should be added here.
	auto wmgr = std::make_unique<logic::world::WorldManager>(this->logic);
	this->worldManager = wmgr.get();
	this->logic->setWorldManager(std::move(wmgr));

	this->worldManager->registerActorComponent<nox::logic::actor::Transform>();
	this->worldManager->registerActorComponent<nox::logic::physics::ActorPhysics>();
	this->worldManager->registerActorComponent<nox::logic::physics::ActorGravitation>();
	this->worldManager->registerActorComponent<nox::logic::graphics::ActorSprite>();
	this->worldManager->registerActorComponent<nox::logic::graphics::ActorLight>();
	this->worldManager->registerActorComponent<nox::logic::control::Actor2dDirectionControl>();
	this->worldManager->registerActorComponent<nox::logic::control::Actor2dRotationControl>();
	this->worldManager->registerActorComponent<nox::logic::audio::ActorAudio>();

	// Prompt subclasses to add game-specific components
	this->registerActorComponents(this->worldManager);

	// Load actor definitions
	std::vector<std::string> actorSubdirs = this->getActorDefinitionSubdirectories();
	for (const std::string &dir : actorSubdirs)
	{
		this->worldManager->loadActorDefinitions(this->application->getResourceAccess(), dir);
	}

	return true;
}

bool GameController::initializeWindow()
{
	if (!this->application->initializeSdlSubsystem(SDL_INIT_VIDEO))
	{
		return false;
	}

	std::unique_ptr<GameWindowView> uniqueWin = this->createGameView();
	this->gameView = uniqueWin.get();
	this->logic->addView(std::move(uniqueWin));

	// Set the world atlas
	std::vector<std::string> atlasDefs = this->getWorldAtlasDefinitionFiles();
	for (const std::string& def : atlasDefs)
	{
		this->gameView->addAtlasDefinitionFile(def);
	}

	this->gameView->setWorldTextureAtlas(this->getWorldAtlasName());

	return true;
}

} }
