#include <nox/game/GameApplication.h>
#include <nox/game/GameController.h>

namespace nox { namespace game
{

GameApplication::GameApplication(GameController* gameController):
	SdlApplication(gameController->getGameName(), gameController->getOrganizationName()),
	gameController(gameController)
{
}

void GameApplication::onUpdate(const Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);
	gameController->update(deltaTime);
}

void GameApplication::onSdlEvent(const SDL_Event& event)
{
	if (this->gameController != nullptr)
	{
		this->gameController->onSdlEvent(event);
	}
}

} }
