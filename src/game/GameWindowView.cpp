#include <nox/game/GameWindowView.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/logic/ILogicContext.h>
#include <nox/app/resource/IResourceAccess.h>


namespace nox { namespace game
{

GameWindowView::GameWindowView(app::IApplicationContext* appContext, const std::string& windowTitle):
	RenderSdlWindowView(appContext, windowTitle),
	appContext(appContext),
	renderer(nullptr),
	controlMapper(nullptr),
	eventBroadcaster(nullptr)
{
	this->log = this->appContext->createLogger();
	this->log.setName("GameWindowView");
}


bool GameWindowView::initialize(logic::ILogicContext* logicContext)
{
	if (!this->RenderSdlWindowView::initialize(logicContext))
	{
		return false;
	}

	this->eventBroadcaster = logicContext->getEventBroadcaster();

	return true;
}



void GameWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	if (this->controlMapper == nullptr ||
		this->eventBroadcaster == nullptr ||
		this->controlMapper->hasControlledActor() == false)
	{
		return;
	}

	auto controlEvents = this->controlMapper->mapKeyPress(event.keysym);

	for (const auto& event : controlEvents)
	{
		this->eventBroadcaster->queueEvent(event);
	}
}

void GameWindowView::onKeyRelease(const SDL_KeyboardEvent& event)
{
	if (this->controlMapper == nullptr ||
		this->eventBroadcaster == nullptr ||
		this->controlMapper->hasControlledActor() == false)
	{
		return;
	}

	auto controlEvents = this->controlMapper->mapKeyRelease(event.keysym);

	for (const auto& event : controlEvents)
	{
		this->eventBroadcaster->queueEvent(event);
	}
}

void GameWindowView::onControlledActorChanged(nox::logic::actor::Actor* controlledActor)
{
	if (this->controlMapper != nullptr)
	{
		this->controlMapper->setControlledActor(controlledActor);
	}
}


void GameWindowView::addAtlasDefinitionFile(const std::string& atlasDefinitionFile)
{
	this->atlasDefinitonFiles.push_back(atlasDefinitionFile);
	this->loadTextureAtlas();
}

void GameWindowView::setWorldTextureAtlas(const std::string& worldAtlas)
{
	this->worldAtlas = worldAtlas;
	this->loadTextureAtlas();
}


void GameWindowView::setControlScheme(std::unique_ptr<window::SdlKeyboardControlMapper> keyMapper)
{
	this->controlMapper = std::move(keyMapper);
}

bool GameWindowView::loadControlDefinitions(const std::string& controlFile)
{
	if (this->controlMapper == nullptr)
	{
		return false;
	}

	app::resource::IResourceAccess* resourceAccess = this->getApplicationContext()->getResourceAccess();
	app::resource::Descriptor controlLayoutResourceDescriptor = app::resource::Descriptor{controlFile};
	std::shared_ptr<app::resource::Handle> controlLayoutResource = resourceAccess->getHandle(controlLayoutResourceDescriptor);

	if (controlLayoutResource == nullptr)
	{
		return false;
	}

	this->controlMapper->loadKeyboardLayout(controlLayoutResource);
	return true;
}


void GameWindowView::onRendererCreated(app::graphics::IRenderer* renderer)
{
	this->renderer = renderer;
	this->loadTextureAtlas();
}

void GameWindowView::loadTextureAtlas()
{
	if (this->renderer == nullptr)
	{
		return;
	}

	// Empty the queue of atlas definition files.
	for (const std::string &atlasDef : this->atlasDefinitonFiles)
	{
		app::resource::Descriptor atlasDesc(atlasDef);
		renderer->loadTextureAtlases(atlasDesc, this->appContext->getResourceAccess());
		this->log.verbose().format("Loaded atlas definition(s) from '%s'", atlasDef.c_str());
	}

	this->atlasDefinitonFiles.clear();

	// Assign the world atlas to the renderer.
	if (this->worldAtlas.empty())
	{
		return;
	}

	renderer->setWorldTextureAtlas(this->worldAtlas);
	this->log.verbose().format("World atlas changed: '%s'", this->worldAtlas.c_str());
}

} }
