/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/app/graphics/2d/LightRenderNode.h>

#include <cassert>

namespace nox { namespace app { namespace graphics
{

LightRenderNode::LightRenderNode()
{
}

void LightRenderNode::onNodeEnter(TextureRenderer& /*renderData*/, glm::mat4x4& /*modelMatrix*/)
{
	if (this->light->needsRenderUpdate())
	{
		this->getCurrentRenderer()->lightUpdate(this->light);
		this->light->setRenderingUpdated();
	}
}

void LightRenderNode::onAttachToRenderer(IRenderer* renderer)
{
	renderer->addLight(this->light);
}

void LightRenderNode::onDetachedFromRenderer()
{
	this->getCurrentRenderer()->removeLight(this->light);
}

void LightRenderNode::setLight(std::shared_ptr<Light>& light)
{
	this->light = light;
}

std::shared_ptr<Light>& LightRenderNode::getLight()
{
	return this->light;
}

} } }
