/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/ParticleRenderer.h>

#include <nox/app/log/Logger.h>
#include <nox/app/graphics/render_utils.h>
#include <nox/app/graphics/TextureManager.h>
#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>

#include <glm/gtc/type_ptr.hpp>

namespace nox { namespace app { namespace graphics {

ParticleRenderer::ParticleRenderer(resource::IResourceAccess* resourceAccess, TextureManager* texMgr):
	FBO(0),
	renderScale(1.0f),
	resourceAccess(resourceAccess),
	textureManager(texMgr),

	debugVAO(0),
	debugVBO(0),
	debugProgram(0),
	debugViewProjectionMatrixUniform(-1),
	debugBufferSize(0),

	VAO(0),
	VBO(0),
	program(0),
	viewProjectionMatrixUniform(-1),
	currentBufferSize(0)
{
}

bool ParticleRenderer::init(RenderData &renderData, std::shared_ptr<Camera> camera)
{
	this->camera = camera;

	this->setupDebugVAO(renderData);

	if (this->createDebugShader(renderData) == false)
	{
		return false;
	}

	this->setupVAO(renderData);

	if (this->createShader(renderData) == false)
	{
		return false;
	}

	renderData.enable(GL_VERTEX_PROGRAM_POINT_SIZE);

	return true;
}


void ParticleRenderer::onIo(RenderData& renderData)
{
	this->prepareRenderData();

	if (this->renderNodes.size() > 0)
	{
		for (int i = 0; i < this->renderNodes.size(); i++)
		{
			this->renderNodes[i].scale = this->camera.get()->getScale().x;
		}
		size_t bufSize = this->renderNodes.size() * sizeof(ParticleRenderNode);

		renderData.bindBuffer(GL_ARRAY_BUFFER, this->VBO);
		if (this->currentBufferSize < bufSize)
		{
			this->currentBufferSize = 2 * bufSize;
			glBufferData(GL_ARRAY_BUFFER, this->currentBufferSize, NULL, GL_STREAM_DRAW);
		}
		else if (bufSize < this->currentBufferSize / 4)
		{
			this->currentBufferSize = 2 * bufSize;
			glBufferData(GL_ARRAY_BUFFER, this->currentBufferSize, NULL, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(this->renderNodes.data(), bufSize);
	}
}

void ParticleRenderer::onRender(RenderData &renderData, const glm::mat4& viewProjectionMatrix)
{
	if (this->renderSteps.size() > 0)
	{
		renderData.bindVertexArray(VAO);
		renderData.bindShaderProgram(this->program);
		renderData.enable(GL_POINT_SPRITE);
		glUniformMatrix4fv(this->viewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

		for (const ParticleRenderStep& renderStep : this->renderSteps)
		{
			const TextureQuad& texQuad = this->textureManager->getTexture(renderStep.texture);
			const TextureQuad::RenderQuad& renderQuad = texQuad.getRenderQuad();

			glm::vec4 tl = renderQuad.topLeft.getTextureCoordinate();
			glm::vec4 br = renderQuad.bottomRight.getTextureCoordinate();
			glm::vec2 sz(br.x - tl.x, br.y - tl.y);

			glUniform2f(this->texCoordUniform, tl.x, tl.y);
			glUniform2f(this->texSizeUniform, sz.x, sz.y);

			glDrawArrays(GL_POINTS, (GLint)renderStep.startIndex, (GLint)renderStep.count);
		}

		renderData.disable(GL_POINT_SPRITE);
	}
}


void ParticleRenderer::onDebugIo(RenderData& renderData)
{
	this->prepareRenderData();

	if (this->renderNodes.size() > 0)
	{
		size_t bufSize = this->renderNodes.size() * sizeof(ParticleRenderNode);

		renderData.bindBuffer(GL_ARRAY_BUFFER, this->debugVBO);
		if (this->debugBufferSize < bufSize)
		{
			this->debugBufferSize = 2 * bufSize;
			glBufferData(GL_ARRAY_BUFFER, this->debugBufferSize, NULL, GL_STREAM_DRAW);
		}
		else if (bufSize < this->debugBufferSize / 4)
		{
			this->debugBufferSize = 2 * bufSize;
			glBufferData(GL_ARRAY_BUFFER, this->debugBufferSize, NULL, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(this->renderNodes.data(), bufSize);
	}
}

void ParticleRenderer::onDebugRender(RenderData &renderData, const glm::mat4& viewProjectionMatrix)
{
	if (this->renderSteps.size() > 0)
	{
		renderData.bindVertexArray(debugVAO);
		renderData.bindShaderProgram(this->debugProgram);
		glUniformMatrix4fv(this->debugViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));


		for (const ParticleRenderStep& renderStep : this->renderSteps)
		{
			glDrawArrays(GL_POINTS, (GLint)renderStep.startIndex, (GLint)renderStep.count);
		}
	}
}


void ParticleRenderer::addParticleRenderBatch(std::shared_ptr<ParticleRenderBatch> batch)
{
	this->batchMap[batch->texture].push_back(batch);
}

void ParticleRenderer::clearParticleRenderBatches()
{
	this->batchMap.clear();
	this->renderNodes.clear();
	this->renderSteps.clear();
}

void ParticleRenderer::setRenderScale(const float renderScale)
{
	this->renderScale = renderScale;
}

void ParticleRenderer::setCamera(std::shared_ptr<Camera> camera)
{
	this->camera = camera;
}


bool ParticleRenderer::createDebugShader(RenderData &renderData)
{
	const std::string SHADER_NAME = "nox/shader/debugParticle";

	std::unique_ptr<std::istream> vertFile, fragFile;
	vertFile = this->getFileIstream(SHADER_NAME + ".vert");
	fragFile = this->getFileIstream(SHADER_NAME + ".frag");

	if (vertFile && fragFile)
	{
		GLuint vertexShader = createShaderAutoVersion(*vertFile, GL_VERTEX_SHADER,
					renderData.getGlVersion()).getId();
		GLuint fragmentShader = createShaderAutoVersion(*fragFile, GL_FRAGMENT_SHADER,
					renderData.getGlVersion()).getId();
		GLuint shaderProgram = glCreateProgram();

		assert(shaderProgram != 0);
		assert(vertexShader != 0);
		assert(fragmentShader != 0);

		glBindAttribLocation(shaderProgram, 0, "vertex");
		glBindAttribLocation(shaderProgram, 1, "rotation");
		glBindAttribLocation(shaderProgram, 2, "size");
		glBindFragDataLocation(shaderProgram, 0, "outputFragColor");

		if (linkShaderProgram(shaderProgram, vertexShader, fragmentShader) == false)
		{
			return false;
		}


		renderData.bindShaderProgram(shaderProgram);
		this->debugProgram = shaderProgram;
		this->debugViewProjectionMatrixUniform = glGetUniformLocation(shaderProgram, "modelViewProjectionMatrix");
		assert(this->debugViewProjectionMatrixUniform >= 0);
	}

	return true;
}

void ParticleRenderer::setupDebugVAO(RenderData &renderData)
{
	glGenVertexArrays(1, &debugVAO);
	renderData.bindVertexArray(debugVAO);

	glGenBuffers(1, &debugVBO);
	renderData.bindBuffer(GL_ARRAY_BUFFER, debugVBO);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)0);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)8);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)12);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)16);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
}


bool ParticleRenderer::createShader(RenderData &renderData)
{
	const std::string SHADER_NAME = "nox/shader/textureParticle";

	std::unique_ptr<std::istream> vertFile, fragFile;
	vertFile = this->getFileIstream(SHADER_NAME + ".vert");
	fragFile = this->getFileIstream(SHADER_NAME + ".frag");

	if (vertFile && fragFile)
	{
		GLuint shaderProgram = glCreateProgram();
		GLuint vertexShader = createShaderAutoVersion(*vertFile, GL_VERTEX_SHADER,
					renderData.getGlVersion()).getId();
		GLuint fragmentShader = createShaderAutoVersion(*fragFile, GL_FRAGMENT_SHADER,
					renderData.getGlVersion()).getId();

		assert(shaderProgram != 0);
		assert(vertexShader != 0);
		assert(fragmentShader != 0);

		glBindAttribLocation(shaderProgram, 0, "vertex");
		glBindAttribLocation(shaderProgram, 1, "rotation");
		glBindAttribLocation(shaderProgram, 2, "size");
		glBindAttribLocation(shaderProgram, 3, "color");
		glBindAttribLocation(shaderProgram, 4, "scale");
		glBindFragDataLocation(shaderProgram, 0, "outputFragColor");

		if (linkShaderProgram(shaderProgram, vertexShader, fragmentShader) == false)
		{
			return false;
		}

		renderData.bindShaderProgram(shaderProgram);
		this->program = shaderProgram;

		this->viewProjectionMatrixUniform = glGetUniformLocation(shaderProgram, "modelViewProjectionMatrix");
		assert(this->viewProjectionMatrixUniform >= 0);

		this->texCoordUniform = glGetUniformLocation(shaderProgram, "textureCoord");
		assert(this->texCoordUniform >= 0);

		this->texSizeUniform = glGetUniformLocation(shaderProgram, "textureSize");
		assert(this->texSizeUniform >= 0);

		GLint texUniform = glGetUniformLocation(shaderProgram, "tex");
		assert(texUniform >= 0);
		glUniform1i(texUniform, 0);
	}
	else
	{
		return false;
	}

	return true;
}

void ParticleRenderer::setupVAO(RenderData &renderData)
{
	glGenVertexArrays(1, &VAO);
	renderData.bindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	renderData.bindBuffer(GL_ARRAY_BUFFER, VBO);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)0);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)8);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)12);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)16);
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleRenderNode), (GLvoid*)32);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
}

std::unique_ptr<std::istream> ParticleRenderer::getFileIstream(const std::string& assetPath)
{
	resource::Descriptor desc(assetPath);
	std::shared_ptr<resource::Handle> handle = this->resourceAccess->getHandle(desc);

	if (!handle)
	{
		return nullptr;
	}

	return std::make_unique<std::istringstream>(std::string(handle->getResourceBuffer(), handle->getResourceBufferSize()));
}

void ParticleRenderer::prepareRenderData()
{
	if (this->renderSteps.size() > 0)
	{
		return;
	}

	for (auto it = this->batchMap.begin(); it != this->batchMap.end(); it++)
	{
		std::vector<std::shared_ptr<ParticleRenderBatch>> vec = it->second;

		ParticleRenderStep renderStep;
		renderStep.texture = it->first;
		renderStep.startIndex = this->renderNodes.size();

		for (auto batch : vec)
		{
			this->renderNodes.insert(this->renderNodes.end(), batch->renderNodes.begin(), batch->renderNodes.end());
		}

		renderStep.count = this->renderNodes.size() - renderStep.startIndex;
		this->renderSteps.push_back(renderStep);
	}
}

} } }
