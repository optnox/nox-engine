/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/TransformationNode.h>

namespace nox { namespace app
{
namespace graphics
{

TransformationNode::TransformationNode()
{
}

TransformationNode::TransformationNode(const glm::mat4x4& matrix)
{
	this->transformationMatrix = matrix;
}

TransformationNode::~TransformationNode()
{
	
}

void TransformationNode::onNodeEnter(TextureRenderer& /*renderData*/, glm::mat4x4& modelMatrix)
{
	this->savedMatrix = modelMatrix;
	modelMatrix = modelMatrix * transformationMatrix;
}

void TransformationNode::onNodeLeave(TextureRenderer& /*renderData*/, glm::mat4x4& modelMatrix)
{
	modelMatrix = this->savedMatrix;
}

void TransformationNode::setTransform(const glm::mat4& matrix)
{
	this->transformationMatrix = matrix;
}

const glm::mat4& TransformationNode::getTransform() const
{
	return this->transformationMatrix;
}

}
} }
