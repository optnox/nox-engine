/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/GeometrySet.h>

#include <algorithm>

namespace nox { namespace app
{
namespace graphics
{

GeometrySet::GeometrySet(GeometryPrimitive primitiveType, DataMode mode):
	active(true),
	geometryPrimitive(primitiveType),
	dataMode(mode),
	renderDataSize(0)
{
}

Triangle* GeometrySet::createTriangle()
{
	const GeometryType type = GeometryType::TRIANGLE;

	assert(this->geometryPrimitive == GeometryPrimitive::TRIANGLE);

	Triangle* geometry;

	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->vacantGeometry[type].empty() == false)
	{
		auto vacancyIt = this->vacantGeometry[type].begin();
		size_t vacantPosition = *vacancyIt;
		this->vacantGeometry[type].erase(vacancyIt);

		geometry = static_cast<Triangle*>(this->geometryContainer[vacantPosition].get());
	}
	else
	{
		std::unique_ptr<Triangle> uniqueGeometry(new Triangle(this));
		geometry = uniqueGeometry.get();

		containerPosition[geometry] = this->geometryContainer.size();
		this->geometryContainer.push_back(std::move(uniqueGeometry));
	}

	this->reserveRenderDataSpace(geometry);
	activeGeometry[geometry] = true;

	return geometry;
}

Quad* GeometrySet::createQuad()
{
	const GeometryType type = GeometryType::QUAD;

	assert(this->geometryPrimitive == GeometryPrimitive::TRIANGLE);

	Quad* geometry;

	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->vacantGeometry[type].empty() == false)
	{
		auto vacancyIt = this->vacantGeometry[type].begin();
		size_t vacantPosition = *vacancyIt;
		this->vacantGeometry[type].erase(vacancyIt);

		geometry = static_cast<Quad*>(this->geometryContainer[vacantPosition].get());
	}
	else
	{
		std::unique_ptr<Quad> uniqueGeometry(new Quad(this));
		geometry = uniqueGeometry.get();

		this->containerPosition[geometry] = this->geometryContainer.size();
		this->geometryContainer.push_back(std::move(uniqueGeometry));
	}

	this->reserveRenderDataSpace(geometry);
	this->activeGeometry[geometry] = true;

	return geometry;
}

Line* GeometrySet::createLine()
{
	const GeometryType type = GeometryType::LINE;

	assert(this->geometryPrimitive == GeometryPrimitive::LINE);

	Line* geometry;

	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->vacantGeometry[type].empty() == false)
	{
		auto vacancyIt = this->vacantGeometry[type].begin();
		size_t vacantPosition = *vacancyIt;
		this->vacantGeometry[type].erase(vacancyIt);

		geometry = static_cast<Line*>(this->geometryContainer[vacantPosition].get());
	}
	else
	{
		std::unique_ptr<Line> uniqueGeometry(new Line(this));
		geometry = uniqueGeometry.get();

		this->containerPosition[geometry] = this->geometryContainer.size();
		this->geometryContainer.push_back(std::move(uniqueGeometry));
	}

	this->reserveRenderDataSpace(geometry);
	this->activeGeometry[geometry] = true;

	return geometry;
}

Polygon* GeometrySet::createPolygon(unsigned int numVertices)
{
	const GeometryType type = GeometryType::POLYGON;

	assert(this->geometryPrimitive == GeometryPrimitive::TRIANGLE);

	Polygon* geometry;

	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->vacantGeometry[type].empty() == false)
	{
		auto vacancyIt = this->vacantGeometry[type].begin();
		size_t vacantPosition = *vacancyIt;
		this->vacantGeometry[type].erase(vacancyIt);

		geometry = static_cast<Polygon*>(this->geometryContainer[vacantPosition].get());
	}
	else
	{
		std::unique_ptr<Polygon> uniqueGeometry(new Polygon(this));
		geometry = uniqueGeometry.get();

		this->containerPosition[geometry] = this->geometryContainer.size();
		this->geometryContainer.push_back(std::move(uniqueGeometry));
	}

	geometry->init(numVertices);

	this->reserveRenderDataSpace(geometry);
	this->activeGeometry[geometry] = true;

	return geometry;
}

LineLoop* GeometrySet::createLineLoop(unsigned int numVertices, bool closed)
{
	const GeometryType type = GeometryType::LINELOOP;

	assert(this->geometryPrimitive == GeometryPrimitive::LINE);

	LineLoop* geometry;

	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->vacantGeometry[type].empty() == false)
	{
		auto vacancyIt = this->vacantGeometry[type].begin();
		size_t vacantPosition = *vacancyIt;
		this->vacantGeometry[type].erase(vacancyIt);

		geometry = static_cast<LineLoop*>(this->geometryContainer[vacantPosition].get());
	}
	else
	{
		std::unique_ptr<LineLoop> uniqueGeometry(new LineLoop(this));
		geometry = uniqueGeometry.get();

		this->containerPosition[geometry] = this->geometryContainer.size();
		this->geometryContainer.push_back(std::move(uniqueGeometry));
	}

	geometry->init(numVertices, closed);

	this->reserveRenderDataSpace(geometry);
	this->activeGeometry[geometry] = true;

	return geometry;
}

void GeometrySet::onGeometryChanged(Geometry* geometry)
{
	if (this->dataMode == DataMode::DYNAMIC)
	{
		std::vector<ObjectCoordinate> updatedCoordinates = geometry->generateRenderableVertices();
		unsigned int startPos = this->geometryRenderDataStart[geometry];

		std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

		for (size_t i = 0; i < updatedCoordinates.size(); i++)
		{
			this->renderVertices[startPos + i] = updatedCoordinates[i];
		}
	}
	else
	{
//		dirtyGeometry.insert(geometry);
	}
}

bool GeometrySet::removeGeometry(Geometry* geometry)
{
	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	if (this->activeGeometry[geometry] == false)
	{
		return false;
	}

	if (this->containerPosition.find(geometry) == this->containerPosition.end())
	{
		return false;
	}

	size_t position = this->containerPosition[geometry];
	GeometryType type = geometry->getType();

	this->activeGeometry[geometry] = false;
	this->vacantGeometry[type].insert(position);

	if (this->dataMode == DataMode::DYNAMIC)
	{
		auto startRemoveIt = this->renderVertices.begin() + this->geometryRenderDataStart[geometry];
		auto endRemoveIt = startRemoveIt + geometry->getNumVertices();
		this->renderVertices.erase(startRemoveIt, endRemoveIt);
		this->updateGeometryStartPositionMap();
	}

	this->renderDataSize -= geometry->getNumVertices();

	return true;
}

void GeometrySet::reserveRenderDataSpace(Geometry* geometry)
{
	if (this->dataMode == DataMode::DYNAMIC)
	{
		unsigned int renderDataSize = (unsigned int)this->renderVertices.size() + geometry->getNumVertices();
		this->geometryRenderDataStart[geometry] = (unsigned int)this->renderVertices.size();
		this->renderVertices.resize(renderDataSize);
	}

	this->renderDataSize += geometry->getNumVertices();
}

GeometryPrimitive GeometrySet::getGeometryPrimitive() const
{
	return this->geometryPrimitive;
}

const std::vector<ObjectCoordinate>& GeometrySet::getVertexData() const
{
	return this->renderVertices;
}

void GeometrySet::setActive(bool state)
{
	this->active = state;
}

void GeometrySet::clear()
{
	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	for (auto& activePair : this->activeGeometry)
	{
		if (activePair.second == true)
		{
			this->vacantGeometry[activePair.first->getType()].insert(this->containerPosition[activePair.first]);
			activePair.second = false;
		}
	}

	if (this->dataMode == DataMode::DYNAMIC)
	{
		this->renderVertices.clear();
		this->geometryRenderDataStart.clear();
	}

	this->renderDataSize = 0;
}

bool GeometrySet::isActive() const
{
	return this->active;
}

void GeometrySet::updateGeometryStartPositionMap()
{
	this->geometryRenderDataStart.clear();

	unsigned int startPosition = 0;

	for (const auto& geometry : this->geometryContainer)
	{
		if (this->activeGeometry[geometry.get()])
		{
			this->geometryRenderDataStart[geometry.get()] = startPosition;
			startPosition += geometry->getNumVertices();
		}
	}
}

unsigned int GeometrySet::updateRenderData()
{
	std::lock_guard<std::mutex> dataLock(this->renderDataMutex);

	const unsigned int dataSize = this->renderDataSize;

	if (this->renderVertices.size() < dataSize)
	{
		this->renderVertices.resize(dataSize);
	}

	unsigned int currentPos = 0;

	for (const auto& geometry : this->geometryContainer)
	{
		if (this->activeGeometry[geometry.get()] == true)
		{
			std::vector<ObjectCoordinate> updatedCoordinates = geometry->generateRenderableVertices();

			for (size_t i = 0; i < updatedCoordinates.size(); i++)
			{
				this->renderVertices[currentPos] = updatedCoordinates[i];
				currentPos++;
			}
		}
	}

	return dataSize;
}

GeometrySet::DataMode GeometrySet::getDataMode() const
{
	return this->dataMode;
}

}
} }
