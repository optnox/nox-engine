/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/Camera.h>

#include <glm/gtc/matrix_transform.hpp>


namespace nox { namespace app

{
namespace graphics
{

Camera::Camera(const glm::uvec2& size):
	cameraSize(size),
	cameraRotation(0.0f)
{
	this->cameraPosition = glm::vec2(0.0f, 0.0f);
	this->cameraScale = glm::vec2(1.0f, 1.0f);

	this->projectionMatrix = glm::ortho(-(float)this->cameraSize.x / 2.0f, (float)this->cameraSize.x / 2.0f, -(float)this->cameraSize.y / 2.0f, (float)this->cameraSize.y / 2.0f, -1.0f, 1.0f);
}

const glm::mat4x4& Camera::getViewProjectionMatrix() const
{
	return this->viewProjectionMatrix;
}

const glm::mat4x4& Camera::getViewMatrix() const
{
	return this->viewMatrix;
}

void Camera::setPosition(const glm::vec2 &position)
{
	this->cameraPosition = position;
	this->updateCameraMatrix();
}

glm::vec2 Camera::getPosition() const
{
	return this->cameraPosition;
}


void Camera::setRotation(float rotation)
{
	this->cameraRotation = rotation;
	this->updateCameraMatrix();
}

float Camera::getRotation() const
{
	return this->cameraRotation;
}


void Camera::setScale(const glm::vec2& scale)
{
	this->cameraScale = scale;
	this->updateCameraMatrix();
}

glm::vec2 Camera::getScale() const
{
	return this->cameraScale;
}

void Camera::updateCameraMatrix()
{
	this->viewMatrix = glm::mat4x4(1);
	this->viewMatrix = glm::scale(this->viewMatrix, glm::vec3(cameraScale, 1.0f));
	this->viewMatrix = glm::rotate(this->viewMatrix, -this->cameraRotation, glm::vec3(0.0f, 0.0f, 1.0f));
	this->viewMatrix = glm::translate(this->viewMatrix, glm::vec3(-this->cameraPosition, 0.0f));
	this->viewProjectionMatrix = this->projectionMatrix * this->viewMatrix;
}

void Camera::setSize(const glm::uvec2& size)
{
	this->cameraSize = size;
	this->projectionMatrix = glm::ortho(-(float)this->cameraSize.x / 2.0f, (float)this->cameraSize.x / 2.0f, -(float)this->cameraSize.y / 2.0f, (float)this->cameraSize.y / 2.0f, -1.0f, 1.0f);
	this->updateCameraMatrix();
}

math::Box<glm::vec2> Camera::getBoundingAABB() const
{
	glm::vec2 cameraWorldSize(glm::vec2(this->cameraSize) / this->cameraScale);
	cameraWorldSize /= 2.0f;

	// Rotate each corner appropriately
	glm::vec2 sizeRot[4] = {
		glm::vec2(-cameraWorldSize.x, -cameraWorldSize.y),
		glm::vec2( cameraWorldSize.x, -cameraWorldSize.y),
		glm::vec2( cameraWorldSize.x,  cameraWorldSize.y),
		glm::vec2(-cameraWorldSize.x,  cameraWorldSize.y),
	};

	const float cosa = std::cos(this->cameraRotation);
	const float sina = std::sin(this->cameraRotation);

	for (int i=0; i<4; i++) {
		glm::vec2 tmp = sizeRot[i];
		sizeRot[i].x = tmp.x*cosa - tmp.y*sina;
		sizeRot[i].y = tmp.x*sina + tmp.y*cosa;
	}

	glm::vec2 corners[4] =
	{
		this->cameraPosition + sizeRot[0],
		this->cameraPosition + sizeRot[1],
		this->cameraPosition + sizeRot[2],
		this->cameraPosition + sizeRot[3],
	};

	glm::vec2 lowerBound = corners[0];
	glm::vec2 upperBound = corners[0];

	for (int i=0; i<4; i++)
	{
		if (corners[i].x < lowerBound.x)
		{
			lowerBound.x = corners[i].x;
		}

		if (corners[i].x > upperBound.x)
		{
			upperBound.x = corners[i].x;
		}

		if (corners[i].y < lowerBound.y)
		{
			lowerBound.y = corners[i].y;
		}

		if (corners[i].y > upperBound.y)
		{
			upperBound.y = corners[i].y;
		}
	}

	math::Box<glm::vec2> aabb;
	aabb.setLowerBound(lowerBound);
	aabb.setUpperBound(upperBound);
	return aabb;
}

glm::vec2 Camera::getSize() const
{
	glm::vec2 cameraWorldSize(glm::vec2(this->cameraSize) / this->cameraScale);
	return cameraWorldSize;
}

}
} }
