/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/TiledTextureRenderer.h>

#include <nox/app/graphics/opengl_utils.h>

#include <glm/gtc/type_ptr.hpp>
#include <algorithm>

namespace nox { namespace app
{
namespace graphics
{

TiledTextureRenderer::TiledTextureRenderer():
	terrainVBO(0),
	terrainVAO(0),
	indexBuffer(0),
	textureQuads(nullptr),
	quadDataChanged(false)
{
}

TiledTextureRenderer::~TiledTextureRenderer()
{
}

void TiledTextureRenderer::setup(RenderData& renderData)
{
	this->setupTerrainRendering(renderData);
}

void TiledTextureRenderer::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/)
{
	this->renderTexture(renderData);
}

void TiledTextureRenderer::setupTerrainRendering(RenderData& renderData)
{
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;
	const GLuint colorLocation = 2;
	const GLuint lightLuminanceLocation = 3;

	glGenVertexArrays(1, &this->terrainVAO);
	glGenBuffers(1, &this->indexBuffer);
	glGenBuffers(1, &this->terrainVBO);
	
	assert(this->terrainVAO > 0 &&
		   this->indexBuffer > 0 &&
		   this->terrainVBO > 0);

	renderData.bindVertexArray(this->terrainVAO);
	renderData.bindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->terrainVBO);
	
	TextureQuad::VertexAttribute::positionAttribPointer(vertexCoordLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureCoordLocation);
	TextureQuad::VertexAttribute::colorAttribPointer(colorLocation);
	TextureQuad::VertexAttribute::luminanceAttribPointer(lightLuminanceLocation);

	glEnableVertexAttribArray(vertexCoordLocation);
	glEnableVertexAttribArray(textureCoordLocation);
	glEnableVertexAttribArray(colorLocation);
	glEnableVertexAttribArray(lightLuminanceLocation);
}

void TiledTextureRenderer::renderTexture(RenderData& renderData)
{
	if (this->textureQuads != nullptr)
	{
		renderData.bindBuffer(GL_ARRAY_BUFFER, this->terrainVBO);
		renderData.bindVertexArray(this->terrainVAO);
		
		if (this->quadDataChanged == true)
		{
			resizeQuadIndexVector(this->indices, (unsigned int) this->textureQuads->size());

			glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(this->indices.size() * sizeof(GLuint)), this->indices.data(), GL_DYNAMIC_DRAW);
			
			glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(this->textureQuads->size() * sizeof(TextureQuad::RenderQuad)), this->textureQuads->data(), GL_DYNAMIC_DRAW);
			this->quadDataChanged = false;
		}
	
		glDrawElements(GL_TRIANGLES, (GLsizei)this->textureQuads->size() * INDICES_PER_QUAD, GL_UNSIGNED_INT, nullptr);

	}
}

void TiledTextureRenderer::updateTextureQuads(const std::vector<TextureQuad::RenderQuad>* quads)
{
	this->textureQuads = quads;
	this->quadDataChanged = true;
}

}
} }
