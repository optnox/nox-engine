/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/2d/Geometry.h>
#include <nox/app/graphics/2d/DebugRenderer.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>

namespace nox { namespace app
{
namespace graphics
{

DebugRenderer::DebugRenderer():
	debugVBO(0),
	debugVAO(0),
	debugShader(0),
	debugModelViewProjectionUniform(-1),
	debugBufferSize(0),
	numTriangleCoords(0),
	numLineCoords(0),
	renderTriangles(false),
	renderLines(false),
	totalDataSize(0)
{
	this->geometryOrder[GeometryPrimitive::TRIANGLE] = 0;
	this->geometryOrder[GeometryPrimitive::LINE] = 1;
}

DebugRenderer::~DebugRenderer()
{
}

void DebugRenderer::init(RenderData& renderData, ShaderInfo shader)
{
	glGenVertexArrays(1, &this->debugVAO);
	renderData.bindVertexArray(this->debugVAO);
	
	this->shaderInfo = shader;
	this->debugShader = shaderInfo.shaderProgram;
	
	this->debugModelViewProjectionUniform = this->shaderInfo.ViewProjectionMatrixUniform;

	glGenBuffers(1, &this->debugVBO);
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->debugVBO);
	
	
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
						  sizeof(ObjectCoordinate),
						  (void*)offsetof(ObjectCoordinate, geometryVertex));
	
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE,
						  sizeof(ObjectCoordinate),
						  (void*)offsetof(ObjectCoordinate, color));
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

void DebugRenderer::prepareData()
{
	this->coords.clear();

	this->numTriangleCoords = 0;
	this->numLineCoords = 0;

	this->renderTriangles = false;
	this->renderLines = false;

	for (const std::shared_ptr<GeometrySet>& geometrySet : this->geometrySets)
	{
		if (geometrySet->isActive() == true)
		{
			unsigned int renderDataSize = 0;

			const std::vector<ObjectCoordinate>& setCoordinates = geometrySet->getVertexData();
			GeometryPrimitive type = geometrySet->getGeometryPrimitive();

			if (geometrySet->getDataMode() == GeometrySet::DataMode::REQUEST)
			{
				renderDataSize = geometrySet->updateRenderData();
			}
			else
			{
				renderDataSize = (unsigned int)setCoordinates.size();
			}

			if (type == GeometryPrimitive::TRIANGLE)
			{
				renderTriangles = true;
				numTriangleCoords += renderDataSize;
			}
			else if (type == GeometryPrimitive::LINE)
			{
				renderLines = true;
				numLineCoords += renderDataSize;
			}

			this->coords.insert(this->coords.end(), setCoordinates.begin(), setCoordinates.begin() + renderDataSize);
		}
	}

	this->totalDataSize = static_cast<GLsizeiptr>(this->coords.size() * sizeof(ObjectCoordinate));
}


void DebugRenderer::onDebugIo(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->debugVBO);

	if (this->debugBufferSize < totalDataSize)
	{
		this->debugBufferSize = 2 * totalDataSize;
		glBufferData(GL_ARRAY_BUFFER, this->debugBufferSize, NULL, GL_STREAM_DRAW);
	}
	else if (totalDataSize < this->debugBufferSize / 4)
	{
		this->debugBufferSize = 2 * totalDataSize;
		glBufferData(GL_ARRAY_BUFFER, this->debugBufferSize, NULL, GL_STREAM_DRAW);
	}

	if (totalDataSize > 0)
	{
		handleAsyncDataCopy(this->coords.data(), totalDataSize);
	}

	renderData.bindShaderProgram(this->debugShader);

	glUniformMatrix4fv(this->debugModelViewProjectionUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));
}

void DebugRenderer::onDebugRender(RenderData& renderData)
{
	renderData.bindVertexArray(this->debugVAO);
	renderData.bindShaderProgram(this->debugShader);

	if (renderTriangles == true)
	{
		renderData.disable(GL_STENCIL_TEST);
		glDrawArrays(GL_TRIANGLES, (GLint) 0, (GLsizei) numTriangleCoords);
	}

	if (renderLines == true)
	{
		renderData.disable(GL_STENCIL_TEST);
		glDrawArrays(GL_LINES, (GLint) numTriangleCoords, (GLsizei) numLineCoords);
	}
	
}

void DebugRenderer::addGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	this->geometrySets.push_back(set);
	std::sort(
			this->geometrySets.begin(),
			this->geometrySets.end(),
			[this](const std::shared_ptr<GeometrySet>& a, const std::shared_ptr<GeometrySet>& b)
			{
				if (this->geometryOrder[a->getGeometryPrimitive()] < this->geometryOrder[b->getGeometryPrimitive()])
				{
					return true;
				}
				else
				{
					return false;
				}
			}
	);
}

void DebugRenderer::removeGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	auto newEndIt = std::remove(this->geometrySets.begin(), this->geometrySets.end(), set);
	this->geometrySets.erase(newEndIt);
}

}
} }
