/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl_utils.h>
#include <nox/util/string_utils.h>
#include <vector>
#include <cassert>
#include <cstring>

namespace nox { namespace app
{
namespace graphics
{

bool linkShaderProgram(const GLuint shaderProgram, const GLuint vertexShader, const GLuint fragmentShader)
{
	if (shaderProgram == 0) {
//		g_systemLogger->error("Creating shader program failed: Could not create gl shader program.\n");
		return false;
	}

	if (vertexShader == 0 || fragmentShader == 0)
	{
		return false;
	}

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	glLinkProgram(shaderProgram);

	if (checkLinkStatus(shaderProgram) == false)
	{
		return false;
	}

	if (vertexShader)
	{
		glDetachShader(shaderProgram, vertexShader);
		glDeleteShader(vertexShader);
	}
	if (fragmentShader)
	{
		glDetachShader(shaderProgram, fragmentShader);
		glDeleteShader(fragmentShader);
	}

	return true;
}

bool linkShaderProgram(const GLuint shaderProgram, const GlslShader& vertexShader, const GlslShader& fragmentShader)
{
	return linkShaderProgram(shaderProgram, vertexShader.getId(), fragmentShader.getId());
}

GlslShader createShader(const std::string& shaderContent, const GLenum shaderType)
{
	GlslShader shader;
	shader.create(shaderType);

	if (shader.isCreated() == true)
	{
		shader.compile(shaderContent);
	}

	return shader;
}

GlslShader createShader(std::istream& shaderFile, const GLenum shaderType)
{
	std::string shaderString;
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	return createShader(shaderString, shaderType);
}

std::string generateGlslVersionString(const std::string& requiredVersionString, const GlVersion glVersion)
{
	int requiredGlslVersion = std::stoi(requiredVersionString);
	assert(requiredGlslVersion > 110 && requiredGlslVersion < 500);

	std::string currentGlslVersionString("000\n");

	if (glVersion.getMajor() >= 4 || (glVersion.getMajor() == 3 && glVersion.getMinor() >= 3))
	{
		currentGlslVersionString[0] = *(util::toString(glVersion.getMajor()).c_str());
		currentGlslVersionString[1] = *(util::toString(glVersion.getMinor()).c_str());
	}
	else {
		currentGlslVersionString[0] = '1';
		if (glVersion.getMajor() == 3)
		{
			currentGlslVersionString[1] = *(util::toString(3 + glVersion.getMinor()).c_str());
		}
		else
		{
			currentGlslVersionString[1] = *(util::toString(1 + glVersion.getMinor()).c_str());
		}
	}

	int currentGlslVersion = std::stoi(currentGlslVersionString);

	if (currentGlslVersion < requiredGlslVersion)
	{
//		g_systemLogger->error("generateGlslVersionString: Current version (%i) lower than required version (%i).", currentGlslVersion, requiredGlslVersion);
		return "";
	}

	return "#version " + currentGlslVersionString;
}

GlslShader createShaderAutoVersion(std::istream& shaderFile, const GLenum shaderType, const GlVersion glVersion)
{
	std::string shaderString;
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	// TODO: This parsing could probably be made better.
	std::string::size_type specCommentPos = shaderString.find("//?");
	std::string::size_type lineEndPos = shaderString.find_first_of('\n', specCommentPos);
	assert(lineEndPos != std::string::npos);

	std::string firstLine = shaderString.substr(specCommentPos, lineEndPos - specCommentPos);
	std::string required = "required";
	std::string::size_type requiredVersionPos = firstLine.find(required);
	assert(requiredVersionPos != std::string::npos);

	std::string::size_type versionPos = requiredVersionPos + required.size() + 1;
	std::string commentVersionString = firstLine.substr(versionPos, firstLine.size() - versionPos);

	std::string glslVersionLine = generateGlslVersionString(commentVersionString, glVersion);
	if (glslVersionLine == "")
	{
		GlslShader();
	}

	shaderString.insert(0, glslVersionLine);

	return createShader(shaderString, shaderType);
}

bool checkLinkStatus(const GLuint program)
{
	auto status = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		GLint length;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);

		std::vector<char> log((unsigned int)length);
		glGetProgramInfoLog(program, length, &length, &log[0]);

		std::string errorMessage = "Shader program linking error: " + std::string(&log.front());

//		g_systemLogger->error(errorMessage + &log[0]);
		return false;
	}

	return true;
}

bool checkCompileStatus(const GLuint shaderId)
{
	GLint status;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		return false;
	}

	return true;
}

void fillQuadIndexVector(std::vector<GLuint>& indexVector, const unsigned int numQuads, const unsigned int startQuadNum)
{
	const unsigned int INDICES_PER_QUAD = 6;

	assert(startQuadNum * INDICES_PER_QUAD + numQuads * INDICES_PER_QUAD <= indexVector.size());

	unsigned int indexStart = 0;

	if (startQuadNum > 0)
	{
		indexStart = indexVector[startQuadNum * INDICES_PER_QUAD - 1] + 1;
	}

	for (unsigned int i = 0; i < numQuads; i++)
	{
		const unsigned int QUAD_INDEX_START = (startQuadNum * INDICES_PER_QUAD) + (i * INDICES_PER_QUAD);

		indexVector[QUAD_INDEX_START] = indexStart + 0;
		indexVector[QUAD_INDEX_START + 1] = indexStart + 1;
		indexVector[QUAD_INDEX_START + 2] = indexStart + 2;

		indexVector[QUAD_INDEX_START + 3] = indexStart + 0;
		indexVector[QUAD_INDEX_START + 4] = indexStart + 2;
		indexVector[QUAD_INDEX_START + 5] = indexStart + 3;

		indexStart += 4;
	}
}

void resizeQuadIndexVector(std::vector<GLuint>& indexVector, const unsigned int newSize)
{
	const unsigned int INDICES_PER_QUAD = 6;
	const int NUM_NEW_QUADS = (int)newSize - ((int)indexVector.size() / (int)INDICES_PER_QUAD);

	if (NUM_NEW_QUADS < 0)
	{
		unsigned int eraseStart = (unsigned int)indexVector.size() + ((unsigned int)NUM_NEW_QUADS * INDICES_PER_QUAD);
		auto eraseStartIt = indexVector.begin() + eraseStart;
		indexVector.erase(eraseStartIt, indexVector.end());
	}
	else if (NUM_NEW_QUADS > 0)
	{
		const unsigned int FIRST_QUAD_NUM = (unsigned int)indexVector.size() / INDICES_PER_QUAD;

		indexVector.resize(indexVector.size() + ((unsigned int)NUM_NEW_QUADS * INDICES_PER_QUAD));

		fillQuadIndexVector(indexVector, (unsigned int)NUM_NEW_QUADS, FIRST_QUAD_NUM);
	}
}


GLuint createTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLint internalFormat, const GLenum format, const GLint filtering)
{
	GLuint texture = 0;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, textureWidth, textureHeight, 0, format, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	return texture;
}

GLuint createMultisampleTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLsizei samples)
{
	GLuint texture = 0;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, texture);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA8, textureWidth, textureHeight, GL_FALSE);
	glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	return texture;
}

FrameBuffer createFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures, const bool hasDepthAndStencilBuffer)
{
	GLuint FBO = 0;
	GLuint colorRenderBuffer = 0;
	GLuint stencilRenderbuffer = 0;

	glGenFramebuffers(1,&FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,FBO);

	if (textures.size() == 1)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[0], 0);
	}
	else if (textures.size() > 1)
	{
		for (unsigned int i = 0; i < textures.size(); i++)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, COLOR_ATTACHMENTS[i], GL_TEXTURE_2D, textures[i], 0);
		}

		glDrawBuffers(static_cast<GLsizei>(textures.size()), COLOR_ATTACHMENTS);
	}
	else
	{
		glGenRenderbuffers(1, &colorRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
	}

	if (hasDepthAndStencilBuffer)
	{
		glGenRenderbuffers(1, &stencilRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, stencilRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, stencilRenderbuffer);
	}

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
//		g_systemLogger->error("Framebuffer object created was not complete.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER,0);

	return FrameBuffer(FBO, { colorRenderBuffer, stencilRenderbuffer });
}

FrameBuffer createMultisampleFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures, const GLsizei samples, const bool hasDepthAndStencilBuffer)
{
	GLuint FBO = 0;
	GLuint colorRenderBuffer = 0;
	GLuint stencilRenderbuffer = 0;

	glGenFramebuffers(1,&FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,FBO);



	if (textures.size() == 1)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textures[0], 0);
	}
	else if (textures.size() > 1)
	{
		for (unsigned int i = 0; i < textures.size(); i++)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, COLOR_ATTACHMENTS[i], GL_TEXTURE_2D_MULTISAMPLE, textures[i], 0);
		}

		glDrawBuffers(static_cast<GLsizei>(textures.size()), COLOR_ATTACHMENTS);
	}
	else
	{
		glGenRenderbuffers(1, &colorRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_RGBA8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
	}

	if (hasDepthAndStencilBuffer)
	{
		glGenRenderbuffers(1, &stencilRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, stencilRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, stencilRenderbuffer);
	}

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
//		g_systemLogger->error("Multisampled framebuffer object created was not complete.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER,0);

	return FrameBuffer(FBO, { colorRenderBuffer, stencilRenderbuffer });
}


void handleAsyncDataCopy(const void* data, const GLsizeiptr& dataSize, const GLbitfield invalidationFlag, const GLenum target)
{
	bool dataIsGood = false;
	const unsigned int maxTries = 2;
	unsigned int tries = 0;

	while (dataIsGood == false && tries < maxTries)
	{
		tries++;

		void* dataStore = glMapBufferRange(target, 0, dataSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT | invalidationFlag);

		memcpy(dataStore, data, static_cast<unsigned long>(dataSize));

		dataIsGood = glUnmapBuffer(target);
		if (dataIsGood == false)
		{
			if (tries < maxTries)
			{
//				g_systemLogger->debug("Renderer::onEffectMapRender: Data store needed to be reinitialized, %d tries out of %d", tries, maxTries);
			}
			else
			{
//				g_systemLogger->warning("opengl_utils:handleAsyncDataCopy: Uploading data might have failed, %d tries out of %d", tries, maxTries);
			}
		}
	}
}

std::string getShaderCompileLog(const GLuint shaderId)
{
		GLint length;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

		std::vector<GLchar> log(static_cast<std::vector<GLchar>::size_type>(length), ' ');
		glGetShaderInfoLog(shaderId, length, &length, log.data());

		return std::string(log.data());
}

void handleAsyncDataCopy(const void* firstData, const GLsizeiptr& firstDataSize, const void* secondData, const GLsizeiptr& secondDataSize, const GLbitfield invalidationFlag, const GLenum target)
{
	bool dataIsGood = false;
	const unsigned int maxTries = 2;
	unsigned int tries = 0;

	while (dataIsGood == false && tries < maxTries)
	{
		tries++;

		void* dataStore = glMapBufferRange(target, 0, firstDataSize + secondDataSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT | invalidationFlag);

		void* secondDataStore = (char*)dataStore + firstDataSize;

		memcpy(dataStore, firstData, static_cast<unsigned long>(firstDataSize));
		memcpy(secondDataStore, secondData, static_cast<unsigned long>(secondDataSize));

		dataIsGood = glUnmapBuffer(target);
		if (dataIsGood == false)
		{
			if (tries < maxTries)
			{
//				g_systemLogger->debug("Renderer::onEffectMapRender: Data store needed to be reinitialized, %d tries out of %d", tries, maxTries);
			}
			else
			{
//				g_systemLogger->warning("opengl_utils:handleAsyncDataCopy: Uploading data might have failed, %d tries out of %d", tries, maxTries);
			}
		}
	}

}

void deleteFramebufferIfValid(GLuint& name)
{
	if (name != 0)
	{
		glDeleteFramebuffers(1, &name);
		name = 0;
	}
}

void deleteTextureIfValid(GLuint& name)
{
	if (name != 0)
	{
		glDeleteTextures(1, &name);
		name = 0;
	}
}

void GlslShader::create(const GLenum type)
{
	this->id = glCreateShader(type);
}

bool GlslShader::compile(const std::string& source)
{
	return this->compile(source.c_str());
}

bool GlslShader::compile(const char* source)
{
	glShaderSource(this->id, 1, &source, nullptr);

	glCompileShader(this->id);

	if (checkCompileStatus(this->id) == false)
	{
		this->compiled = false;
		return false;
	}
	else
	{
		this->compiled = true;
		return true;
	}
}

void GlslShader::destroy()
{
	if (this->id != 0)
	{
		glDeleteShader(this->id);
		this->id = 0;
		this->compiled = false;
	}
}

bool GlslShader::isCreated() const
{
	return this->id != 0;
}

bool GlslShader::isCompiled() const
{
	return this->compiled;
}

GLuint GlslShader::getId() const
{
	return this->id;
}

std::string GlslShader::getCompileLog() const
{
	return getShaderCompileLog(this->id);
}

FrameBuffer::FrameBuffer() = default;

FrameBuffer::FrameBuffer(const GLuint name, const std::vector<GLuint>& renderBuffers):
	bufferName(name),
	renderBuffernNames(renderBuffers)
{
}

FrameBuffer::FrameBuffer(FrameBuffer&& other):
	bufferName(other.bufferName),
	renderBuffernNames(std::move(other.renderBuffernNames))
{
	other.bufferName = 0;
}

FrameBuffer& FrameBuffer::operator=(FrameBuffer&& other)
{
	this->bufferName = other.bufferName;
	this->renderBuffernNames = std::move(other.renderBuffernNames);

	other.bufferName = 0;

	return *this;
}

FrameBuffer::~FrameBuffer()
{
	if (this->bufferName != 0)
	{
		glDeleteRenderbuffers(static_cast<GLsizei>(this->renderBuffernNames.size()), this->renderBuffernNames.data());

		glDeleteRenderbuffers(1, &this->bufferName);
	}
}

GLuint FrameBuffer::getId() const
{
	return this->bufferName;
}

void FrameBuffer::bind(const GLenum target)
{
	glBindFramebuffer(target, this->bufferName);
}

GlslShader createShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType, const GlVersion glVersion, std::string& error)
{
	const auto shaderHandle = resourceCache->getHandle(resource::Descriptor(shaderPath));

	if (!shaderHandle)
	{
		error = "Could not open shader resource \"" + shaderPath + "\"";
		return GlslShader();
	}

	std::istringstream shaderStream(std::string(shaderHandle->getResourceBuffer(), shaderHandle->getResourceBufferSize()));

	auto shader = createShaderAutoVersion(shaderStream, shaderType, glVersion);

	return shader;
}

GlslVertexAndFragmentShader createVertexAndFragmentShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GlVersion glVersion, std::string& error)
{
	GlslVertexAndFragmentShader shader;

	std::string vertexError;
	shader.vertex = createShaderAutoVersion(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER, glVersion, vertexError);

	if (shader.vertex.isCreated() == false)
	{
		error = vertexError;
	}
	else if (shader.vertex.isCompiled() == false)
	{
		error = shader.vertex.getCompileLog();
	}
	else
	{
		std::string fragError;
		shader.fragment = createShaderAutoVersion(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER, glVersion, fragError);

		if (shader.fragment.isCreated() == false)
		{
			error = fragError;
		}
		else if (shader.fragment.isCompiled() == false)
		{
			error = shader.fragment.getCompileLog();
		}
	}

	return shader;
}

bool GlslVertexAndFragmentShader::isValid() const
{
	return (this->vertex.isCompiled() && this->fragment.isCompiled());
}

GlVersion::GlVersion(const std::uint8_t major, const std::uint8_t minor):
	glMajor(major),
	glMinor(minor)
{
}

std::uint8_t GlVersion::getMajor() const
{
	return this->glMajor;
}

std::uint8_t GlVersion::getMinor() const
{
	return this->glMinor;
}

}
}
}
