/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TextureAtlasReader.h>

namespace nox { namespace app
{
namespace graphics
{

TextureAtlasReader::~TextureAtlasReader()
{
}

unsigned int TextureAtlasReader::getAtlasWidth() const
{
	return this->atlasWidth;
}

unsigned int TextureAtlasReader::getAtlasHeight() const
{
	return this->atlasHeight;
}

const std::unordered_map<std::string, TextureQuad>& TextureAtlasReader::getTextureMap() const
{
	return this->spriteMap;
}

void TextureAtlasReader::reset()
{
	this->atlasHeight = 0;
	this->atlasWidth = 0;
	this->spriteMap.clear();
}

const std::string& TextureAtlasReader::getErrorString() const
{
	return this->errorString;
}

void TextureAtlasReader::setErrorString(const std::string& error)
{
	this->errorString = error;
}

}
} }
