/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/caudio/CAudioBuffer.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/data/SoundExtraData.h>


namespace nox { namespace app { namespace audio
{

CAudioBuffer::CAudioBuffer(cAudio::IAudioManager* audioManager, PlayMode playMode):
	playMode(playMode),
	audioManager(audioManager),
	audioSource(nullptr),
	soundPosition(0.0f, 0.0f, 0.0f),
	soundStrength(1.0f)
{
}

CAudioBuffer::~CAudioBuffer()
{
	if (this->audioSource)
	{
		this->audioSource->drop();
	}
}

bool CAudioBuffer::initialize(const std::string& fileName, resource::IResourceAccess* resourceAccess)
{
	resource::Descriptor descriptor{fileName};
	std::shared_ptr<resource::Handle> handle = resourceAccess->getHandle(descriptor);
	const resource::SoundExtraData* soundData = handle->getExtraData<resource::SoundExtraData>();

	cAudio::AudioFormats audioFormat = this->getCAudioFormat(soundData);
	if (audioFormat == cAudio::AudioFormats::EAF_INVALID_FORMAT)
	{
		return false;
	}

	this->audioSource = this->audioManager->createFromRaw(
			"",
			handle->getResourceBuffer(),
			handle->getResourceBufferSize(),
			soundData->getFormat().samplesPerSec,
			audioFormat);
	if (!this->audioSource)
	{
		return false;
	}

	this->audioSource->grab();
	this->audioSource->setVolume(1.0f);
	return true;
}

void CAudioBuffer::setPosition(const glm::vec3& position)
{
	this->soundPosition = position;

	if (this->audioSource)
	{
		this->audioSource->setPosition(cAudio::cVector3(position.x, position.y, position.z));
	}
}

void CAudioBuffer::setMinimumDistance(float minDistance)
{
	if (this->audioSource)
	{
		this->audioSource->setMinDistance(minDistance);
	}
}

void CAudioBuffer::setMaximumDistance(float maxDistance)
{
	if (this->audioSource)
	{
		this->audioSource->setMaxAttenuationDistance(maxDistance);
	}
}

void CAudioBuffer::setPitch(float pitch)
{
	if (this->audioSource)
	{
		this->audioSource->setPitch(pitch);
	}
}

void CAudioBuffer::setStrength(float strength)
{
	this->soundStrength = strength;

	if (this->audioSource)
	{
		this->audioSource->setStrength(strength);
	}
}

void CAudioBuffer::setDopplerStrength(float dopplerStrength)
{
	if (this->audioSource)
	{
		this->audioSource->setDopplerStrength(dopplerStrength);
	}
}

void CAudioBuffer::setVelocity(const glm::vec3& vel)
{
	if (this->audioSource)
	{
		this->audioSource->setVelocity(cAudio::cVector3(vel.x, vel.y, vel.z));
	}
}

void CAudioBuffer::handlePause()
{
	if (this->audioSource)
	{
		this->audioSource->pause();
	}
}

void CAudioBuffer::handleResume()
{
	if (this->audioSource)
	{
		if (this->playMode == PlayMode::POSITIONAL)
		{
			cAudio::cVector3 pos(this->soundPosition.x, this->soundPosition.y, this->soundPosition.z);
			this->audioSource->play3d(pos, this->soundStrength, true);
		}
		else
		{
			this->audioSource->play();
		}
	}
}

void CAudioBuffer::handleVolumeChange(const float volume)
{
	if (this->audioSource)
	{
		this->audioSource->setVolume(volume);
	}
}

void CAudioBuffer::handleLoopingChange(const bool looping)
{
	if (this->audioSource)
	{
		this->audioSource->loop(looping);
	}
}

void CAudioBuffer::rewindPosition()
{
	if (this->audioSource)
	{
		this->audioSource->seek(0.0f, false);
	}
}

bool CAudioBuffer::checkIfPlaying() const
{
	if (this->audioSource)
	{
		return this->audioSource->isPlaying();
	}

	return false;
}

cAudio::AudioFormats CAudioBuffer::getCAudioFormat(const resource::SoundExtraData* soundData) const
{
	unsigned int channels = soundData->getFormat().nChannels;
	unsigned int bitrate = soundData->getFormat().bitsPerSample;

	if (bitrate == 8)
	{
		if (channels == 1)
		{
			return cAudio::AudioFormats::EAF_8BIT_MONO;
		}
		else if (channels == 2)
		{
			return cAudio::AudioFormats::EAF_8BIT_STEREO;
		}
	}
	else if (bitrate == 16)
	{
		if (channels == 1)
		{
			return cAudio::AudioFormats::EAF_16BIT_MONO;
		}
		else if (channels == 2)
		{
			return cAudio::AudioFormats::EAF_16BIT_STEREO;
		}
	}

	return cAudio::AudioFormats::EAF_INVALID_FORMAT;
}

}
} }
