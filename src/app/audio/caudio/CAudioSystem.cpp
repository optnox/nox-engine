/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/caudio/CAudioSystem.h>
#include <nox/app/audio/caudio/CAudioBuffer.h>


namespace nox { namespace app { namespace audio
{

std::vector<std::string> CAudioSystem::getPlaybackDeviceNames()
{
	std::vector<std::string> retVec;
	cAudio::IAudioDeviceList* deviceList = cAudio::createAudioDeviceList(cAudio::IDeviceType::DT_PLAYBACK);

	for (unsigned i=0; i<deviceList->getDeviceCount(); i++)
	{
		retVec.push_back(deviceList->getDeviceName(i));
	}

	CAUDIO_DELETE deviceList;

	return retVec;
}

std::string CAudioSystem::getDefaultPlaybackDeviceName()
{
	cAudio::IAudioDeviceList* deviceList = cAudio::createAudioDeviceList(cAudio::IDeviceType::DT_PLAYBACK);

	std::string ret = deviceList->getDefaultDeviceName();

	CAUDIO_DELETE deviceList;

	return ret;
}


CAudioSystem::CAudioSystem(IApplicationContext *appContext, const std::string& playbackDeviceName):
	AudioSystem(appContext),
	audioManager(nullptr),
	playbackDeviceName(playbackDeviceName)
{

}

CAudioSystem::~CAudioSystem()
{
	this->shutdown();
}

bool CAudioSystem::initialize()
{
	if (this->audioManager)
	{
		return false;
	}

	this->audioManager = cAudio::createAudioManager(false);

	if (!this->isDeviceValid(this->playbackDeviceName))
	{
		this->playbackDeviceName = this->getDefaultPlaybackDeviceName();
	}

	if (!this->audioManager->initialize(this->playbackDeviceName.c_str()))
	{
		cAudio::destroyAudioManager(this->audioManager);
		return false;
	}

	return true;
}

void CAudioSystem::shutdown()
{
	this->AudioSystem::shutdown();

	if (this->audioManager)
	{
		cAudio::destroyAudioManager(this->audioManager);
		this->audioManager = nullptr;
	}
}

std::string CAudioSystem::getCurrentPlaybackDeviceName() const
{
	return this->playbackDeviceName;
}

void CAudioSystem::setListenerPosition(const glm::vec3& pos)
{
	if (this->audioManager)
	{
		this->audioManager->getListener()->setPosition(cAudio::cVector3(pos.x, pos.y, pos.z));
	}
}

void CAudioSystem::setListenerDirection(const glm::vec3& dir)
{
	if (this->audioManager)
	{
		this->audioManager->getListener()->setDirection(cAudio::cVector3(dir.x, dir.y, dir.z));
	}
}

void CAudioSystem::setListenerUpVector(const glm::vec3& up)
{
	if (this->audioManager)
	{
		this->audioManager->getListener()->setUpVector(cAudio::cVector3(up.x, up.y, up.z));
	}
}

void CAudioSystem::setListenerVelocity(const glm::vec3& vel)
{
	if (this->audioManager)
	{
		this->audioManager->getListener()->setVelocity(cAudio::cVector3(vel.x, vel.y, vel.z));
	}
}


std::unique_ptr<AudioBuffer> CAudioSystem::createBuffer(PlayMode playMode)
{
	if (!this->audioManager)
	{
		return nullptr;
	}

	return std::make_unique<CAudioBuffer>(this->audioManager, playMode);
}

bool CAudioSystem::isDeviceValid(const std::string& deviceName)
{
	auto devices = CAudioSystem::getPlaybackDeviceNames();

	for (const std::string &dev : devices)
	{
		if (dev == deviceName)
		{
			return true;
		}
	}

	return false;
}

}
} }

