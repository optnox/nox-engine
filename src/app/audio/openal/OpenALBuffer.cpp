/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/openal/OpenALBuffer.h>
#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/IResourceAccess.h>

namespace nox { namespace app
{

namespace audio
{

OpenALBuffer::OpenALBuffer():
	buffer(0),
	source(0)
{
}


OpenALBuffer::~OpenALBuffer()
{
	if (this->source != 0 && this->buffer != 0)
	{
		alSourceUnqueueBuffers(this->source, 1, &this->buffer);
	}

	if (this->source != 0)
	{
		alDeleteSources(1, &this->source);
		this->source = 0;
	}

	if (this->buffer != 0)
	{
		alDeleteBuffers(1, &this->buffer);
		this->buffer = 0;
	}
}

bool OpenALBuffer::initialize(const std::string& fileName, resource::IResourceAccess* resourceAccess)
{
	resource::Descriptor descriptor{fileName};
	auto handle = resourceAccess->getHandle(descriptor);

	if (handle == nullptr)
	{
		return false;
	}

	alGenSources(1, &this->source);

	alGenBuffers(1, &this->buffer);
	const auto soundExtra = handle->getExtraData<resource::SoundExtraData>();

	ALenum format;

	if (soundExtra->getFormat().nChannels == 1)
	{
		format = AL_FORMAT_MONO16;
	}
	else
	{
		format = AL_FORMAT_STEREO16;
	}

	const ALsizei bufferSize = static_cast<ALsizei>(handle->getResourceBufferSize());
	const ALsizei frequency = static_cast<ALsizei>(soundExtra->getFormat().samplesPerSec);

	alBufferData(this->buffer, format, handle->getResourceBuffer(), bufferSize, frequency);

	alSourceQueueBuffers(this->source, 1, &this->buffer);

	return true;
}

void OpenALBuffer::handlePause()
{
	alSourcePause(this->source);
}

void OpenALBuffer::handleResume()
{
	alSourcePlay(this->source);
}

void OpenALBuffer::handleVolumeChange(const float volume)
{
	alSourcef(this->source, AL_GAIN, volume);
}

void OpenALBuffer::rewindPosition()
{
	alSourceRewind(this->source);
}

void OpenALBuffer::handleLoopingChange(const bool looping)
{
	ALint enableLooping = AL_FALSE;

	if (looping == true)
	{
		enableLooping = AL_TRUE;
	}

	alSourcei(this->source, AL_LOOPING, enableLooping);
}

bool OpenALBuffer::checkIfPlaying() const
{
	ALenum state;
	alGetSourcei(this->source, AL_SOURCE_STATE, &state);

	return state == AL_PLAYING;
}

}
} }
