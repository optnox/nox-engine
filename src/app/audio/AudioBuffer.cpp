/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/AudioBuffer.h>
#include <glm/common.hpp>
#include <nox/app/resource/Handle.h>

namespace nox { namespace app
{

namespace audio
{

AudioBuffer::AudioBuffer():
	playing(false),
	looping(false),
	volume(1.0f),
	positionProvider(nullptr)
{

}

AudioBuffer::~AudioBuffer()
{
	if (this->positionProvider)
	{
		this->positionProvider->detachSoundBuffer(this);
	}
}

bool AudioBuffer::isLooping() const
{
	return this->looping;
}

float AudioBuffer::getVolume() const
{
	return this->volume;
}

bool AudioBuffer::isPlaying()
{
	if (this->playing == true && this->checkIfPlaying() == false)
	{
		this->playing = false;
	}

	return this->playing;
}

void AudioBuffer::setVolume(const float vol)
{
	this->volume = glm::clamp(vol, 0.0f, 1.0f);
	this->handleVolumeChange(vol);
}

void AudioBuffer::setPosition(const glm::vec2& position)
{
	this->setPosition(glm::vec3(position, 0.0f));
}

void AudioBuffer::setPosition(const glm::vec3& /*position*/)
{
}

void AudioBuffer::setMinimumDistance(float /*minDistance*/)
{
}

void AudioBuffer::setMaximumDistance(float /*maxDistance*/)
{
}

void AudioBuffer::setPitch(float /*pitch*/)
{
}

void AudioBuffer::setStrength(float /*strength*/)
{
}

void AudioBuffer::setDopplerStrength(float /*dopplerStrength*/)
{
}

void AudioBuffer::setVelocity(const glm::vec2& vel)
{
	this->setVelocity(glm::vec3(vel, 0.0f));
}

void AudioBuffer::setVelocity(const glm::vec3& /*vel*/)
{
}

void AudioBuffer::pauseAudio()
{
	if (this->playing == false)
	{
		this->handlePause();
		this->playing = true;
	}
}

void AudioBuffer::stopAudio()
{
	if (this->playing == false)
	{
		this->handlePause();
		this->playing = true;
	}

	this->rewindPosition();
}

void AudioBuffer::resumeAudio()
{
	if (this->playing == true)
	{
		this->handleResume();
		this->playing = false;
	}
}

void AudioBuffer::playAudio()
{
	if (this->playing == false)
	{
		this->rewindPosition();
		this->handleResume();
		this->playing = true;
	}
}

void AudioBuffer::enableLooping(const bool looping)
{
	if (looping != this->looping)
	{
		this->handleLoopingChange(looping);
		this->looping = looping;
	}
}

bool AudioBuffer::checkIfPlaying() const
{
	return this->playing;
}

void AudioBuffer::setBufferPositionProvider(AudioBufferPositionProvider* provider)
{
	if (provider == nullptr)
	{
		if (this->positionProvider != nullptr)
		{
			this->positionProvider->detachSoundBuffer(this);
		}
	}

	this->positionProvider = provider;

	if (provider != nullptr)
	{
		provider->attachSoundBuffer(this);
	}
}

}
} }
