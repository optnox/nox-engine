/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/AudioBuffer.h>
#include <nox/app/audio/AudioSystem.h>
#include <nox/app/audio/AudioListenerPositionProvider.h>

#include <nox/app/IApplicationContext.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/IResourceAccess.h>

#include <cassert>
#include <vector>

namespace nox { namespace app
{

namespace audio
{

AudioSystem::AudioSystem(IApplicationContext *appContext):
	appContext(appContext),
	listenerProvider(nullptr)
{
}


AudioSystem::~AudioSystem()
{
	if (this->listenerProvider)
	{
		this->listenerProvider->audioSystem = nullptr;
	}
}

void AudioSystem::onUpdate(const Duration& /*deltaTime*/)
{
	std::vector<AudioBuffer*> finishedBuffers;

	for (auto& buffer : this->buffers)
	{
		if (!buffer->isPlaying())
		{
			finishedBuffers.push_back(buffer.get());
		}
	}

	for (auto buffer : finishedBuffers)
	{
		this->releaseAudioBuffer(buffer);
	}

	// Update the listener position via the provider
	if (this->listenerProvider)
	{
		this->listenerProvider->updateListenerPosition(this);
	}
}

void AudioSystem::stopAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->stopAudio();
	}
}

void AudioSystem::pauseAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->pauseAudio();
	}
}

void AudioSystem::resumeAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->resumeAudio();
	}
}

void AudioSystem::shutdown()
{
	while (this->buffers.empty() == false)
	{
		this->buffers.front()->stopAudio();
		this->buffers.pop_front();
	}
}

AudioBuffer* AudioSystem::playSound(const std::string& fileName, PlayMode playMode)
{
	AudioBuffer *audioBuffer = this->createAudioBuffer(fileName, playMode);
	if (!audioBuffer)
	{
		return nullptr;
	}

	audioBuffer->playAudio();

	return audioBuffer;
}

void AudioSystem::setListenerPosition(const glm::vec2& position)
{
	this->setListenerPosition(glm::vec3(position, 0.0f));
}

void AudioSystem::setListenerPosition(const glm::vec3& /*position*/)
{
	// Empty default implementation
}

void AudioSystem::setListenerDirection(const glm::vec3& /*direction*/)
{
	// Empty default implementation
}

void AudioSystem::setListenerUpVector(const glm::vec3& /*upVector*/)
{
	// Empty default implementation
}

void AudioSystem::setListenerVelocity(const glm::vec2& velocity)
{
	this->setListenerVelocity(glm::vec3(velocity, 0.0f));
}

void AudioSystem::setListenerVelocity(const glm::vec3& /*velocity*/)
{
	// Empty default implementation
}

void AudioSystem::setListenerPositionProvider(AudioListenerPositionProvider* provider)
{
	this->detachListenerPositionProvider();

	this->listenerProvider = provider;
	this->listenerProvider->audioSystem = this;
}

void AudioSystem::detachListenerPositionProvider(AudioListenerPositionProvider* provider)
{
	if (this->listenerProvider && (!provider || provider == this->listenerProvider))
	{
		this->listenerProvider->audioSystem = nullptr;
		this->listenerProvider = nullptr;
	}
}

AudioBuffer* AudioSystem::createAudioBuffer(const std::string& fileName, PlayMode playMode)
{
	std::unique_ptr<AudioBuffer> buffer = this->createBuffer(playMode);
	if (!buffer->initialize(fileName, this->appContext->getResourceAccess()))
	{
		buffer.release();
		return nullptr;
	}

	AudioBuffer* bufferPointer = buffer.get();
	this->buffers.push_front(std::move(buffer));

	return bufferPointer;
}

void AudioSystem::releaseAudioBuffer(AudioBuffer* audioBuffer)
{
	assert(audioBuffer != nullptr);

	audioBuffer->stopAudio();

	this->buffers.remove_if(
			[audioBuffer](const std::unique_ptr<AudioBuffer>& listBuffer)
			{
				return (listBuffer.get() == audioBuffer);
			}
	);
}

IApplicationContext* AudioSystem::getAppContext()
{
	return this->appContext;
}

}
} }
