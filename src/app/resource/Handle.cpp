/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/data/IExtraData.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IHandleDestructionListener.h>

namespace nox { namespace app
{
namespace resource
{

Handle::Handle(const Descriptor& resourceDescriptor, char* dataBuffer, const unsigned int bufferSize, std::unique_ptr<IExtraData> extraData):
	resourceDescriptor(resourceDescriptor),
	resourceBuffer(dataBuffer),
	resourceBufferSize(bufferSize),
	extraData(std::move(extraData)),
	destructionListener(nullptr)
{
}


Handle::~Handle()
{
	if (this->resourceBuffer)
	{
		delete [] this->resourceBuffer;
		this->resourceBuffer = nullptr;
	}

	if (this->destructionListener != nullptr)
	{
		destructionListener->onResourceHandleDestroyed(this->resourceBufferSize);
	}
}

void Handle::setDestructionListener(IHandleDestructionListener* listener)
{
	this->destructionListener = listener;
}

unsigned int Handle::getResourceBufferSize() const
{
	return this->resourceBufferSize;
}

const char* Handle::getResourceBuffer() const
{
	return this->resourceBuffer;
}

const Descriptor& Handle::getResourceDescriptor() const
{
	return this->resourceDescriptor;
}

}
} }
