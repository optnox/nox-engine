/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/data/IExtraData.h>
#include <nox/app/resource/Handle.h>

#include "../loader/DefaultLoader.h"

#include <nox/app/resource/provider/Provider.h>
#include <nox/util/string_utils.h>

#include <cstring>
#include <cassert>
#include <queue>

namespace nox { namespace app
{
namespace resource
{

LruCache::LruCache(const unsigned int sizeInMb):
	cacheSize(sizeInMb * 1024 * 1024),
	allocatedMemory(0)
{
	this->resourceLoaders.push_back(std::unique_ptr<ILoader>(new DefualtLoader()));
}

LruCache::~LruCache()
{
	while (!this->leastRecentlyUsed.empty())
	{
		this->freeOneResource();
	}
}

void LruCache::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("LruCache");
}

void LruCache::addLoader(std::unique_ptr<ILoader> loader)
{
	this->resourceLoaders.push_front(std::move(loader));
}

std::vector<Descriptor> LruCache::getResourcesRecursivelyInDirectory(const std::string& directoryPath) const
{
	std::vector<Descriptor> resources;

	auto providerIt = this->providerList.begin();

	while (providerIt != this->providerList.end() && resources.empty() == true)
	{
		auto& provider = *providerIt;

		const Directory& topDirectory = provider->getTopDirectory();
		const std::vector<std::string> directoryTokens = util::splitString<std::vector>(directoryPath, '/');

		const Directory* directory = &topDirectory;
		auto tokenIt = directoryTokens.begin();
		bool foundChildDir = true;

		while (tokenIt != directoryTokens.end() && foundChildDir == true)
		{
			const std::vector<Directory>& subdirectories = directory->getSubdirectories();

			foundChildDir = false;
			auto childDirIt = subdirectories.begin();

			while (foundChildDir == false && childDirIt != subdirectories.end())
			{
				if (childDirIt->getName() == *tokenIt)
				{
					foundChildDir = true;
					directory = &(*childDirIt);
					tokenIt++;
				}

				childDirIt++;
			}
		}

		const bool validDirectory = (tokenIt == directoryTokens.end());

		if (validDirectory == true)
		{
			std::queue<const Directory*> directoryQueue;
			directoryQueue.push(directory);

			while (directoryQueue.empty() == false)
			{
				const Directory* currentDirectory = directoryQueue.front();

				for (const Descriptor& resource : currentDirectory->getResources())
				{
					this->log.debug().format("Found resource \"%s\" recursively in directory \"%s\".", resource.getPath().c_str(), directoryPath.c_str());

					resources.push_back(resource);
				}

				for (const Directory& childDirectory : currentDirectory->getSubdirectories())
				{
					directoryQueue.push(&childDirectory);
				}

				directoryQueue.pop();
			}
		}

		++providerIt;
	}

	return resources;
}

std::shared_ptr<Handle> LruCache::getHandle(const Descriptor& resource)
{
	std::lock_guard<std::mutex> guard(this->handleMutex);

	std::shared_ptr<Handle> handle(this->findCachedResource(resource));

	if (handle == nullptr)
	{
		handle = this->loadResource(resource);
	}
	else
	{
		this->onCachedHandleAccessed(handle);
	}

	return handle;
}

std::shared_ptr<Handle> LruCache::loadResource(const Descriptor& resource)
{
	ILoader* loader = nullptr;
	std::shared_ptr<Handle> handle;

	bool wildcardFound = false;
	auto it = this->resourceLoaders.begin();

	while (wildcardFound == false && it != this->resourceLoaders.end())
	{
		ILoader* testLoader = it->get();
		if (util::matchWildcard(testLoader->getPattern(), resource.getPath().c_str()))
		{
			loader = testLoader;
			wildcardFound = true;
		}
		else
		{
			it++;
		}
	}

	if (!loader)
	{
		assert(loader && "Default resource loader not found");
		this->log.error().raw("Default resource loader not found.");
		return std::shared_ptr<Handle>();
	}

	util::Buffer<char> rawBuffer;

	auto providerIt = this->providerList.begin();

	while (providerIt != this->providerList.end() && rawBuffer.size <= 0)
	{
		rawBuffer.size = (*providerIt)->getRawResourceSize(resource);

		if (rawBuffer.size <= 0)
		{
			++providerIt;
		}
	}

	if (rawBuffer.size == 0)
	{
		this->log.error().format("Got a raw size of 0 bytes for \"%s\"", resource.getPath().c_str());
		return std::shared_ptr<Handle>();
	}

	auto& provider = *providerIt;

	rawBuffer.data =  loader->useRawFile() ? this->allocate(rawBuffer.size) : new char[rawBuffer.size];

	if (rawBuffer.data == nullptr)
	{
		this->log.error().format("Could not allocate space for \"%s\"", resource.getPath().c_str());
		return std::shared_ptr<Handle>();
	}

	provider->getRawResource(resource, rawBuffer.data);

	util::Buffer<char> buffer;

	if (loader->useRawFile() == true)
	{
		buffer = rawBuffer;
		handle = std::shared_ptr<Handle>(new Handle(resource, buffer.data, buffer.size, nullptr));
	}
	else
	{
		buffer.size = loader->getLoadedResourceSize(rawBuffer.data, rawBuffer.size);

		if (buffer.size == 0)
		{
			this->log.error().format("Got a loaded size of 0 bytes for \"%s\"", resource.getPath().c_str());
			return std::shared_ptr<Handle>();
		}

		buffer.data = this->allocate(buffer.size);

		if (buffer.data == nullptr)
		{
			this->log.error().format("Could not allocate space for \"%s\"", resource.getPath().c_str());
			return std::shared_ptr<Handle>();
		}

		std::unique_ptr<IExtraData> extraData(nullptr);
		bool handleLoaded = loader->loadResource(rawBuffer, buffer, extraData);

		handle = std::shared_ptr<Handle>(new Handle(resource, buffer.data, buffer.size, std::move(extraData)));

		if (rawBuffer.data != nullptr)
		{
			delete [] rawBuffer.data;
		}

		if (handleLoaded == false)
		{
			this->log.error().format("Failed loading resource \"%s\"", resource.getPath().c_str());
			return std::shared_ptr<Handle>();
		}
	}

	if (handle == nullptr)
	{
		handle->setDestructionListener(this);

		this->leastRecentlyUsed.push_front(handle);
		this->resourceMap[resource.getPath()] = handle;
	}

	assert(loader && "Default resource loader not found");
	return handle;

}

char* LruCache::allocate(const unsigned int size)
{
	if (this->makeRoom(size) == false)
	{
		return nullptr;
	}

	char *memory = new char[size];
	if (memory)
	{
		this->allocatedMemory += size;
	}

	return memory;
}

bool LruCache::makeRoom(const unsigned int size)
{
	if (size > this->cacheSize)
	{
		this->log.error().raw("No more space.");
		return false;
	}

	while (size > (this->cacheSize - this->allocatedMemory))
	{
		if (this->leastRecentlyUsed.empty())
		{
			return false;
		}

		this->freeOneResource();
	}

	return true;
}

void LruCache::freeOneResource()
{
	ResourceHandleList::iterator gonner = this->leastRecentlyUsed.end();
	gonner--;

	std::shared_ptr<Handle> handle = *gonner;

	this->leastRecentlyUsed.pop_back();
	this->resourceMap.erase(handle->getResourceDescriptor().getPath());
}

void LruCache::onCachedHandleAccessed(std::shared_ptr<Handle> handle)
{
	this->leastRecentlyUsed.remove(handle);
	this->leastRecentlyUsed.push_front(handle);
}

std::shared_ptr<Handle> LruCache::findCachedResource(const Descriptor& resource)
{
	const ResourceHandleMap::iterator it = resourceMap.find(resource.getPath());
	if (it == this->resourceMap.end())
	{
		return std::shared_ptr<Handle>();
	}

	return it->second;
}

void LruCache::dropHandle(std::shared_ptr<Handle> handle)
{
	this->leastRecentlyUsed.remove(handle);
	this->resourceMap.erase(handle->getResourceDescriptor().getPath());
}

int LruCache::preload(const std::string& pattern, std::function<void (unsigned int, bool&)> progressCallback)
{
	int loaded = 0;

	for (auto& provider : this->providerList)
	{
		unsigned int numResources = provider->getNumResources();
		bool cancel = false;
		for (unsigned int i = 0; i < numResources; ++i)
		{
			Descriptor resource(provider->getResourcePath(i));

			if (util::matchWildcard(pattern.c_str(), resource.getPath().c_str()))
			{
				std::shared_ptr<Handle> handel = this->getHandle(resource);
				++loaded;
			}

			if (progressCallback)
			{
				progressCallback(i * 100 / numResources, cancel);
			}
		}
	}

	return loaded;
}

void LruCache::flush(void)
{
	while (!this->leastRecentlyUsed.empty())
	{
		std::shared_ptr<Handle> handle = leastRecentlyUsed.front();
		dropHandle(handle);
		this->leastRecentlyUsed.pop_front();
	}
}

void LruCache::onResourceHandleDestroyed(const unsigned int size)
{
	this->allocatedMemory -= size;
}

bool LruCache::addProvider(std::unique_ptr<Provider> provider)
{
	if (provider != nullptr && provider->open() == true)
	{
		this->providerList.push_back(std::move(provider));
		return true;
	}
	else
	{
		return false;
	}
}

}
} }
