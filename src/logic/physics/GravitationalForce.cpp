/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/GravitationalForce.h>
#include <nox/util/math/vector_math.h>

namespace nox { namespace logic { namespace physics
{

GravitationalForce::GravitationalForce():
	calculationType(CalculationType::NEWTON)
{
}

glm::vec2 GravitationalForce::calculateForceOnBody(const Body& otherBody, const float deltaTime) const
{
	if (this->calculationType == CalculationType::NEWTON)
	{
		const glm::vec2 distanceVector = this->body.position - otherBody.position;
		float distance = glm::length(distanceVector);

		if (distance > 0.0f && this->distanceLimit.insideBorder(distance) == true)
		{
			distance = this->distanceLimit.limitDistance(distance);

			const float distanceSquared = distance * distance;
			const float force = math::gravitationalConstant<float>() * ((this->body.mass * otherBody.mass) / distanceSquared);

			return glm::normalize(distanceVector) * force * deltaTime;
		}
	}
	else if (this->calculationType == CalculationType::CUSTOM && this->calculationFunction != nullptr)
	{
		const glm::vec2 distanceVector = this->body.position - otherBody.position;
		float distance = glm::length(distanceVector);

		if (distance > 0.0f && this->distanceLimit.insideBorder(distance) == true)
		{
			distance = this->distanceLimit.limitDistance(distance);

			const float force = this->calculationFunction(this->body, otherBody, distance);
			const glm::vec2 distanceVector = this->body.position - otherBody.position;

			return glm::normalize(distanceVector) * force * deltaTime;
		}
	}

	return glm::vec2();
}

void GravitationalForce::setPosition(const glm::vec2& position)
{
	this->body.position = position;
}

void GravitationalForce::setMass(const float mass)
{
	this->body.mass = mass;
}

void GravitationalForce::setForceCalculationType(const CalculationType& type)
{
	this->calculationType = type;
}

void GravitationalForce::setCustomForceCalculationFunction(const CalculationFunction& function)
{
	this->calculationFunction = function;
}

float GravitationalForce::getMass() const
{
	return this->body.mass;
}

const glm::vec2& GravitationalForce::getPosition() const
{
	return this->body.position;
}

GravitationalForce::DistanceLimit::DistanceLimit():
	limitMin(false),
	minDistance(0.0f),
	minLimitType(Type::CLAMP),
	limitMax(false),
	maxDistance(1.0f),
	maxLimitType(Type::CLAMP)
{
}

float GravitationalForce::DistanceLimit::limitDistance(const float distance) const
{
	if (this->limitMin == true && distance < this->minDistance)
	{
		return this->minDistance;
	}
	else if (this->limitMax == true && distance > this->maxDistance)
	{
		return this->maxDistance;
	}
	else
	{
		return distance;
	}
}

bool GravitationalForce::DistanceLimit::insideBorder(const float distance) const
{
	if (distance < this->minDistance && this->minLimitType == Type::BORDER)
	{
		return false;
	}
	else if (distance > this->maxDistance && this->maxLimitType == Type::BORDER)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void GravitationalForce::DistanceLimit::setMinLimitOff()
{
	this->limitMin = false;
}

void GravitationalForce::DistanceLimit::setMinLimitOn(const float distance, const Type limitType)
{
	this->limitMin = true;
	this->minDistance = distance;
	this->minLimitType = limitType;
}

void GravitationalForce::DistanceLimit::setMaxLimitOff()
{
	this->limitMax = false;
}

void GravitationalForce::DistanceLimit::setMaxLimitOn(const float distance, const Type limitType)
{
	this->limitMax = true;
	this->maxDistance = distance;
	this->maxLimitType = limitType;
}

void GravitationalForce::setDistanceLimit(const DistanceLimit& limit)
{
	this->distanceLimit = limit;
}

} } }
