#include <nox/logic/physics/particle/ParticleComponent.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/util/math/vector_math.h>
#include <cstdio>

namespace nox {

namespace logic {

namespace physics {

const ParticleComponent::IdType ParticleComponent::NAME = "Particle";

const ParticleComponent::IdType& ParticleComponent::getName() const
{
	return ParticleComponent::NAME;
}

void ParticleComponent::initialize(const Json::Value& componentJsonObject)
{
	bool passed = false;
	const Json::Value& jsonEmitters = componentJsonObject["Emitters"];

	this->position = glm::vec2(0.0f, 0.0f);
	this->rotation = 0.0f;

	for (int i = 0; i < jsonEmitters.size(); ++i)
	{
		passed += this->addEmitter(jsonEmitters[i]);
	}
}

void ParticleComponent::onCreate()
{
}

void ParticleComponent::onDestroy()
{
}

void ParticleComponent::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event.get()->isType(actor::TransformChange::ID))
	{
		this->position = ((actor::TransformChange*)event.get())->getPosition();
		this->rotation = ((actor::TransformChange*)event.get())->getRotation();
	}
}

void ParticleComponent::onUpdate(const Duration& deltaTime)
{
	for (ParticleEmitter* emitter : emitters)
	{
		emitter->update(deltaTime, this->rotation, this->position);
	}
}

void ParticleComponent::onActivate()
{
}

void ParticleComponent::onDeactivate()
{
}

std::vector<ParticleEmitter*> ParticleComponent::getEmitters()
{
	return emitters;
}

void ParticleComponent::serialize(nox::logic::net::Packet& packet)
{
}

void ParticleComponent::deSerialize(const nox::logic::net::Packet& packet)
{
}

void ParticleComponent::serialize(Json::Value& componentObject)
{
}

bool ParticleComponent::addEmitter(const Json::Value& componentJsonObject)
{
	ParticleEmitter* tmpEmitter;
	auto emitterName = componentJsonObject["name"];
	if (emitterName == 0)
	{
		//this->log.error().raw("All emitters must have names!");
		return false;
	}
	tmpEmitter = new ParticleEmitter(emitterName.asString(), this->getLogicContext());

	if (componentJsonObject["offset"] != 0)
	{
		tmpEmitter->setOffset(	componentJsonObject["offset"].get("x", 0.0f).asFloat(),
					componentJsonObject["offset"].get("y", 0.0f).asFloat());
	}
	else
	{
		tmpEmitter->setOffset(0.0f, 0.0f);
	}

	if (componentJsonObject["size"] != 0)
	{
		tmpEmitter->setSize(	componentJsonObject["size"].get("x", 1.0f).asFloat(),
					componentJsonObject["size"].get("y", 1.0f).asFloat());
	}
	else
	{
		tmpEmitter->setSize(1.0f, 1.0f);
	}

	tmpEmitter->setEmitRate(util::RandomType<float>(
				componentJsonObject.get("emitRate", 1.0f).asFloat(),
				componentJsonObject.get("emitRateOffset", 0.0f).asFloat() ));

	if (componentJsonObject["velocityDirection"] != 0)
	{
		tmpEmitter->setVelocity(componentJsonObject["velocityDirection"].get("x", 0.0f).asFloat(),
					componentJsonObject["velocityDirection"].get("y", 1.0f).asFloat(),
					componentJsonObject["velocityDirection"].get("xOffset", 0.0f).asFloat(),
					componentJsonObject["velocityDirection"].get("yOffset", 0.0f).asFloat());
	}
	else
	{
		tmpEmitter->setVelocity(0.0f, 1.0f, 0.0f, 0.0f);
	}

	// Add a particle
	bool validParticle = false;
	auto jsonParticle = componentJsonObject["particle"];
	for (int i = 0; i < jsonParticle.size(); ++i)
	{
		validParticle += tmpEmitter->addParticle(jsonParticle[i]);
	}

	if(!validParticle)
	{
		//this->log.error().raw("All emitters must have at leas one properly defined particle");
		delete tmpEmitter;
		return false;
	}

	this->emitters.push_back(tmpEmitter);
	return true;
}

}
}
}
