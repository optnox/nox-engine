#include <nox/logic/physics/particle/ParticleEmitter.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/particle/ParticleEvent.h>
#include <nox/util/math/vector_math.h>

#include <Box2D/Box2D.h>

#include <cstdlib>
#include <cstdio>
namespace nox {

namespace logic {

namespace physics {

ParticleEmitter::ParticleEmitter(std::string name, ILogicContext* logicContext):
		name(name),
		timeSinceParticle(0),
		logicContext(logicContext),
		enabled(true)
{
	this->particleSystem = ((Box2DSimulation*)logicContext->getPhysics())->getParticleSystem();
}

ParticleEmitter::~ParticleEmitter()
{
}

void ParticleEmitter::update(const Duration& deltaTime,float rotation, glm::vec2 position)
{
	timeSinceParticle += deltaTime.count();
	while (timeSinceParticle > 1000000000/this->emitRate)
	{
		this->emit(position, rotation);
		this->timeSinceParticle -= 1000000000/this->emitRate;
	}
}

void ParticleEmitter::enable()
{
	this->enabled = true;
}

void ParticleEmitter::disable()
{
	this->enabled = false;
}

void ParticleEmitter::setOffset(float x, float y)
{
	this->offset = new b2Vec2(x, y);
}

b2Vec2* ParticleEmitter::getOffset()
{
	return this->offset;
}

void ParticleEmitter::setSize(float x, float y)
{
	this->size = new b2Vec2(x, y);
}

b2Vec2* ParticleEmitter::getSize()
{
	return this->size;
}

void ParticleEmitter::setVelocity(float x, float y, float xOffset, float yOffset)
{
	this->velocity = new b2Vec2(x, y);
	this->velocityOffset = new b2Vec2(xOffset, yOffset);
}

b2Vec2* ParticleEmitter::getVelocity()
{
	return this->velocity;
}

void ParticleEmitter::setEmitRate(util::RandomType<float> emitRate)
{
	this->emitRate = emitRate;
}

float ParticleEmitter::getEmitRate()
{
	return this->emitRate;
}


bool ParticleEmitter::addParticle(const Json::Value& jsonParticle)
{
	Particle tmpParticle;
	auto name = jsonParticle["name"];
	if (name == 0)
	{
		return false;
	}

	tmpParticle.name = name.asString();
	tmpParticle.velocityScalar.setValue( jsonParticle.get("velocityScalar", 1.0f).asFloat() );
	tmpParticle.velocityScalar.setOffset( jsonParticle.get("velocityScalarOffset", 0.0f).asFloat() );
	if (jsonParticle["color"] != 0)
	{
		tmpParticle.r.setValue( jsonParticle["color"].get("r", 1.0f).asFloat() );
		tmpParticle.g.setValue( jsonParticle["color"].get("g", 1.0f).asFloat() );
		tmpParticle.b.setValue( jsonParticle["color"].get("b", 1.0f).asFloat() );
		tmpParticle.a.setValue( jsonParticle["color"].get("a", 1.0f).asFloat() );
		tmpParticle.r.setOffset( jsonParticle["color"].get("rOffset", 0.0f).asFloat() );
		tmpParticle.g.setOffset( jsonParticle["color"].get("gOffset", 0.0f).asFloat() );
		tmpParticle.b.setOffset( jsonParticle["color"].get("bOffset", 0.0f).asFloat() );
		tmpParticle.a.setOffset( jsonParticle["color"].get("aOffset", 0.0f).asFloat() );
	}

	tmpParticle.lifetime.setValue( jsonParticle.get("lifetime", 5.0f).asFloat() );
	tmpParticle.lifetime.setOffset( jsonParticle.get("lifetimeOffset", 0.0f).asFloat() );

	for (const Json::Value& jsonFlag : jsonParticle["flags"])
	{
		tmpParticle.flags = (b2ParticleFlag)(tmpParticle.flags | getFlagFromString(jsonFlag.asString()));
	}

	this->particles.push_back(tmpParticle);

	return true;
}

std::vector<Particle> ParticleEmitter::getParticles()
{
	return this->particles;
}

Particle ParticleEmitter::getParticleByName(std::string particleName)
{
	for (Particle p : particles)
	{
		if (p.name == particleName)
		{
			return p;
		}
	}
	Particle p;
	return p;
}

b2ParticleDef ParticleEmitter::getParticleDef(Particle p)
{
	b2ParticleDef pd;

	pd.flags = p.flags;
	pd.color = b2ParticleColor(p.r * 255, p.g * 255, p.b * 255, p.a * 255);
	pd.position = *offset;

	util::RandomType<float> x(pd.position.x, size->x);
	util::RandomType<float> y(pd.position.y, size->y);

	pd.position.x = x;
	pd.position.y = y;

	b2Vec2 tmpVelocity(util::RandomType<float>(this->velocity->x, this->velocityOffset->x).get(),
			   util::RandomType<float>(this->velocity->y, this->velocityOffset->y).get());

	pd.velocity = tmpVelocity * p.velocityScalar;
	pd.lifetime = p.lifetime;

	return pd;
}

bool ParticleEmitter::emit(glm::vec2 position, float rotation)
{
	if (enabled && particles.size())
	{
		b2ParticleDef pd = getParticleDef(particles[rand()%particles.size()]);
		pd.position.x += position.x;
		pd.position.y += position.y;


		pd.position = math::convertVec2<b2Vec2>( math::rotateVectorAroundPoint(
					math::convertVec2<glm::vec2>(pd.position), position, rotation ) );

		pd.velocity = math::convertVec2<b2Vec2>( math::rotateVectorAroundPoint(
					math::convertVec2<glm::vec2>(pd.velocity), glm::vec2(0.0f, 0.0f), rotation ) );


		particleSystem->CreateParticle(pd);

		return true;
	}
	return false;
}

b2ParticleFlag ParticleEmitter::getFlagFromString(std::string sFlag)
{
	if (sFlag == "water") return b2_waterParticle;
	if (sFlag == "zombie") return b2_zombieParticle;
	if (sFlag == "wall") return b2_wallParticle;
	if (sFlag == "spring") return b2_springParticle;
	if (sFlag == "elastic") return b2_elasticParticle;
	if (sFlag == "viscous") return b2_viscousParticle;
	if (sFlag == "powder") return b2_powderParticle;
	if (sFlag == "tensile") return b2_tensileParticle;
	if (sFlag == "colorMixing") return b2_colorMixingParticle;
	if (sFlag == "staticPressure") return b2_staticPressureParticle;
	if (sFlag == "repulsive") return b2_repulsiveParticle;
	return b2_waterParticle;

}

}
}
}
