/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/Box2DVisibilityMapper.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>


namespace nox { namespace logic { namespace physics
{

Box2DVisibilityMapper::Box2DVisibilityMapper(Box2DSimulation *sim):
	simulation(sim)
{

}

std::vector<std::array<glm::vec2,3>> Box2DVisibilityMapper::getInvisibilityRegions(glm::vec2 pos, float radius)
{
	std::vector<std::array<glm::vec2,3>> triangles;

	math::Box<glm::vec2> aabb;
	aabb.setLowerBound(glm::vec2(pos.x - radius, pos.y - radius));
	aabb.setUpperBound(glm::vec2(pos.x + radius, pos.y + radius));

	auto fixtures = this->simulation->getFixturesInAabb(aabb);

	for (const b2Fixture *fix : fixtures)
	{
		const b2Shape *shape = fix->GetShape();

		b2Shape::Type type = shape->GetType();
		if (type == b2Shape::e_polygon)
		{
			b2PolygonShape *polyShape = (b2PolygonShape*)shape;

			auto verts = this->getShapeVertices(polyShape, fix);
			const size_t vertCount = verts.size();

			std::vector<std::pair<glm::vec2,glm::vec2>> edges;
			edges.reserve(vertCount/2 + 1);

			for (size_t i=0; i<vertCount; i++)
			{
				const size_t indexLow = i;
				const size_t indexHigh = (i+1 >= verts.size() ? 0 : i + 1);

				glm::vec2 vertA = verts[indexLow];
				glm::vec2 vertB = verts[indexHigh];

				// The point should be on the LEFT side of the line, as the
				// polygon is wound CCW.
				// http://stackoverflow.com/a/3461533
				float cross = (vertB.x - vertA.x)*(pos.y - vertA.y) - (vertB.y - vertA.y)*(pos.x - vertA.x);
				if (cross <= 0.0f)
				{
					std::pair<glm::vec2,glm::vec2> pair(vertA, vertB);
					edges.push_back(pair);
				}
			}

			// Project all shadow casting edges
			for (auto pair : edges)
			{
				glm::vec2 diff;
				float length = 0.0f;

				glm::vec2 vertA = pair.first;
				glm::vec2 vertB = pair.second;
				glm::vec2 vertC;
				glm::vec2 vertD;

				// Project A to C
				diff = vertA - pos;
				length = (float)diff.length();
				diff.x /= length;
				diff.y /= length;
				vertC = (radius * diff * 100.0f) + vertA;

				// Project B to D
				diff = vertB - pos;
				length = (float)diff.length();
				diff.x /= length;
				diff.y /= length;
				vertD = (radius * diff * 100.0f) + vertB;

				// Create two triangles, (A,B,C) and (B,C,D)
				std::array<glm::vec2,3> tri;

				tri[0] = glm::vec2(vertA.x, vertA.y);
				tri[1] = glm::vec2(vertB.x, vertB.y);
				tri[2] = glm::vec2(vertC.x, vertC.y);
				triangles.push_back(tri);

				tri[0] = glm::vec2(vertB.x, vertB.y);
				tri[1] = glm::vec2(vertC.x, vertC.y);
				tri[2] = glm::vec2(vertD.x, vertD.y);
				triangles.push_back(tri);
			}
		}
		else
		{
			// FIXME
			// Other fixture types - circles, etc, should be handled as well.
		}
	}

	return triangles;
}

std::vector<glm::vec2> Box2DVisibilityMapper::getShapeVertices(const b2PolygonShape* polyShape, const b2Fixture* fixture)
{
	const int vertCount = polyShape->GetVertexCount();
	const float rotation = fixture->GetBody()->GetAngle();
	const b2Vec2& bodyPos = fixture->GetBody()->GetPosition();

	// Build a list containing all the verts of the shape
	std::vector<glm::vec2> verts;
	verts.reserve((size_t)vertCount);

	for (int i=0; i<vertCount; i++)
	{
		b2Vec2 vert = polyShape->GetVertex(i);

		b2Vec2 rotated = vert;
		rotated.x = vert.x * std::cos(rotation) - vert.y * std::sin(rotation);
		rotated.y = vert.x * std::sin(rotation) + vert.y * std::cos(rotation);
		rotated += bodyPos;

		verts.push_back(glm::vec2(rotated.x, rotated.y));
	}

	return verts;

}

}
} }
