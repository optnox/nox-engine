/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/control/actor/Actor2dDirectionControl.h>

#include <nox/logic/actor/Actor.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <glm/gtx/rotate_vector.hpp>
#include <cassert>

namespace nox { namespace logic { namespace control
{

const Actor2dDirectionControl::IdType Actor2dDirectionControl::NAME = "2dDirectionControl";

const Actor2dDirectionControl::IdType& Actor2dDirectionControl::getName() const
{
	return NAME;
}

void Actor2dDirectionControl::initialize(const Json::Value& componentJson)
{
	this->ActorControl::initialize(componentJson);

	this->movementForce = componentJson.get("movementForce", 1.0f).asFloat();
	this->relativeToRotation = componentJson.get("relativeToRotation", false).asBool();
}

void Actor2dDirectionControl::serialize(Json::Value& componentJson)
{
	componentJson["movementForce"] = this->movementForce;
}

void Actor2dDirectionControl::onCreate()
{
	this->ActorControl::onCreate();

	this->actorTransform = this->getOwner()->findComponent<actor::Transform>();
	assert(this->actorTransform != nullptr);

	this->actorPhysics = this->getOwner()->findComponent<physics::ActorPhysics>();
	assert(this->actorPhysics != nullptr);
}

bool Actor2dDirectionControl::handleControl(const std::shared_ptr<Action>& controlEvent)
{
	if (controlEvent->getControlName() == "move")
	{
		this->controlInput = controlEvent->getControlVector();

		if (this->relativeToRotation == true)
		{
			const auto rotation = this->actorTransform->getRotation();

			this->currentMovementDirection = glm::rotate(this->controlInput, rotation);
		}
		else
		{
			this->currentMovementDirection = this->controlInput;
		}

		return true;
	}

	return false;
}

void Actor2dDirectionControl::onUpdate(const nox::Duration& deltaTime)
{
	this->ActorControl::onUpdate(deltaTime);

	if (this->currentMovementDirection.x != 0.0f || this->currentMovementDirection.y != 0.0f)
	{
		this->actorPhysics->applyForce(this->currentMovementDirection * this->movementForce);
	}
}

void Actor2dDirectionControl::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (this->isOwnedRemotely())
	{
		return;
	}

	if (this->relativeToRotation == true && event->isType(actor::TransformChange::ID))
	{
		const auto transformEvent = static_cast<actor::TransformChange*>(event.get());
		const auto rotation = transformEvent->getRotation();

		this->currentMovementDirection = glm::rotate(this->controlInput, rotation);
	}
}

void Actor2dDirectionControl::serialize(logic::net::Packet& packet)
{
	packet << this->currentMovementDirection;
}

void Actor2dDirectionControl::deSerialize(const logic::net::Packet& packet)
{
	packet >> this->currentMovementDirection;
}

} } }
