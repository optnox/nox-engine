/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/control/actor/ActorControl.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/control/event/Action.h>

namespace nox { namespace logic { namespace control
{

ActorControl::ActorControl():
	listener("ActorControl")
{
}

ActorControl::~ActorControl() = default;

void ActorControl::initialize(const Json::Value& /*componentJson*/)
{
	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(Action::ID);
}

void ActorControl::onActivate()
{
	this->listener.startListening();
}

void ActorControl::onDeactivate()
{
	this->listener.stopListening();
}

void ActorControl::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(Action::ID))
	{
		this->handleControl(std::static_pointer_cast<Action>(event));
	}
}

void ActorControl::onEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(Action::ID))
	{
		auto controlEvent = std::static_pointer_cast<Action>(event);

		if (controlEvent->getActorId() == this->getOwner()->getId())
		{
			this->handleControl(controlEvent);
		}
	}
}

} } }
