/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/audio/actor/ActorAudio.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/ILogicContext.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/logic/audio/event/PlaySound.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/actor/ActorPhysics.h>

#include <nox/app/audio/AudioBuffer.h>

namespace nox { namespace logic { namespace audio
{

const std::string ActorAudio::NAME = "ActorAudio";


ActorAudio::ActorAudio():
	applyTransform(true),
	applyVelocity(true)
{
}


const actor::Component::IdType& ActorAudio::getName() const
{
	return ActorAudio::NAME;
}


void ActorAudio::initialize(const Json::Value& componentJsonObject)
{
	this->applyTransform = componentJsonObject.get("applyTransform", true).asBool();
	this->applyVelocity = componentJsonObject.get("applyVelocity", true).asBool();
}

void ActorAudio::serialize(Json::Value& componentObject)
{
	componentObject["applyTransform"] = this->applyTransform;
	componentObject["applyVelocity"] = this->applyVelocity;
}

void ActorAudio::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	// Only update the buffers if there is a transform change.
	if (event.get()->isType(actor::TransformChange::ID))
	{
		this->updateAttachedBuffers();
	}
}

void ActorAudio::setApplyTransform(bool applyTransform)
{
	this->applyTransform = applyTransform;
}

void ActorAudio::setApplyVelocity(bool applyVelocity)
{
	this->applyVelocity = applyVelocity;
}


void ActorAudio::playSound(const std::string& fileName)
{
	if (!this->getOwner())
	{
		return;
	}

	ILogicContext* context = this->getOwner()->getLogicContext();
	if (!context)
	{
		return;
	}

	std::shared_ptr<audio::PlaySound> event = std::make_shared<audio::PlaySound>(fileName, this);
	context->getEventBroadcaster()->queueEvent(event);
}


void ActorAudio::updateAudioBuffer(app::audio::AudioBuffer* buffer)
{
	if (!this->getOwner())
	{
		return;
	}

	this->applyTransformToBuffer(buffer);
	this->applyVelocityToBuffer(buffer);
}

void ActorAudio::applyTransformToBuffer(app::audio::AudioBuffer* buffer)
{
	actor::Transform* transform = nullptr;

	if (this->applyTransform && (transform = this->getOwner()->findComponent<actor::Transform>()) != nullptr)
	{
		buffer->setPosition(transform->getPosition());
	}
}

void ActorAudio::applyVelocityToBuffer(app::audio::AudioBuffer* buffer)
{
	physics::ActorPhysics *physics = nullptr;

	if (this->applyVelocity && (physics = this->getOwner()->findComponent<physics::ActorPhysics>()) != nullptr)
	{
		buffer->setVelocity(physics->getVelocity());
	}
}


}
} }
