/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/WorldManager.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/View.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/PhysicsSimulation.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/SyncAttributes.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>
#include <nox/logic/net/event/CreateSynchronized.h>
#include <nox/logic/net/event/DestroySynchronized.h>
#include <nox/util/algorithm.h>

#include <list>
#include <cassert>

namespace nox { namespace logic { namespace world
{

WorldManager::WorldManager(ILogicContext* context):
	context(context),
	eventBroadcaster(context->getEventBroadcaster()),
	log(context->createLogger()),
	actorFactory(this->context),
	actorUpdateLocked(false),
	listenerManager("world::WorldManager")
{
	assert(this->context != nullptr);
	assert(this->eventBroadcaster != nullptr);

	this->log.setName("WorldWorldManager");

	this->listenerManager.setup(this, this->context->getEventBroadcaster());
	this->listenerManager.addEventTypeToListenFor(logic::net::CreateSynchronized::ID);
	this->listenerManager.addEventTypeToListenFor(logic::net::DestroySynchronized::ID);
	this->listenerManager.startListening();

	this->initDefaultDestroyHandlers();
}

WorldManager::~WorldManager()
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}
}

void WorldManager::update(const Duration& deltaTime)
{
	this->actorUpdateLocked = true;

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);

	for (auto& actor : this->actors)
	{
		if (actor->isActive() == true)
		{
			actor->update(deltaTime);
		}
	}

	actorLock.unlock();

	this->actorUpdateLocked = false;

	this->onUpdate(deltaTime);
}

void WorldManager::reset()
{
	std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
	this->nonPhysicalActorTransforms.clear();

	this->handleQueuedTasks();

	while (this->actorCreations.empty() == false)
	{
		this->actorCreations.pop();
	}

	this->actorRemovalQueue.clear();

	this->clearActors();
	this->actorFactory.resetCounter();
}

void WorldManager::saveEverything(const std::string& saveDirectory, SaveCompleteCallback completeCallback)
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}

	this->saveThread = std::thread(&WorldManager::saveWorld, this, completeCallback, saveDirectory);
}

void WorldManager::saveWorld(SaveCompleteCallback /*completeCallback*/, const std::string& /*saveDirectory*/)
{
}

void WorldManager::loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& definitionPath)
{
	this->actorFactory.loadActorDefinitions(resourceAccess, definitionPath);
}

actor::Actor* WorldManager::findActor(const actor::ActorIdentifier& id) const
{
	std::lock_guard<std::mutex> idMapLock(this->actorIdMutex);

	auto actorItr = this->idToActorMap.find(id);

	if (actorItr != this->idToActorMap.end())
	{
		return actorItr->second;
	}

	return nullptr;
}

actor::Actor* WorldManager::findActor(SyncId syncId) const
{
	if (this->syncToIdMap.count(syncId) == 0)
	{
		return nullptr;
	}

	const actor::ActorIdentifier& actorId = this->syncToIdMap.at(syncId);
	return this->findActor(actorId);
}

std::vector<actor::Actor*> WorldManager::findActorsWithinRange(const glm::vec2& position, const float range) const
{
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	const math::Box<glm::vec2> rangeAabb(position - glm::vec2(range / 2.0f), position + glm::vec2(range / 2.0f));

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(rangeAabb, physics::allBodyStates());
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(rangeAabb, physics::allBodyStates()));

	for (actor::Actor* actor : physicsActorsWithin)
	{
		auto actorTransform = actor->findComponent<actor::Transform>();
		assert(actorTransform != nullptr);

		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	return actorsInRange;
}

std::vector<actor::Actor*> WorldManager::findActorsWithinAxisAlignedBox(const math::Box<glm::vec2>& box) const
{
	std::unordered_map<SyncId, actor::ActorIdentifier> syncToIdMap;
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (box.intersects(actorTransform->getPosition()))
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(box, physics::allBodyStates());
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(box, physics::allBodyStates()));

	actorsInRange.insert(actorsInRange.end(), physicsActorsWithin.begin(), physicsActorsWithin.end());

	return actorsInRange;
}

void WorldManager::onEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(logic::net::CreateSynchronized::ID))
	{
		std::vector<actor::Actor*> childActors;

		auto createEvent = (const logic::net::CreateSynchronized*)event.get();
		auto actor = this->actorFactory.createActor(createEvent, childActors);

		if (actor != nullptr)
		{
			this->log.debug().format("Created sync actor from def '%s' (SyncId = %u, Owner = %u)",
					createEvent->getActorName().c_str(),
					createEvent->getSyncId(),
					createEvent->getOwningClientId());
		}
		else
		{
			this->log.error().format("Failed to create sync actor from def '%s' (SyncId = %u, Owner = %u)",
					createEvent->getActorName().c_str(),
					createEvent->getSyncId(),
					createEvent->getOwningClientId());
			return;
		}

		this->handleActorCreation(actor.get());
		this->manageActor(std::move(actor));
	}
	else if (event->isType(logic::net::DestroySynchronized::ID))
	{
		auto destroyEvent = (const logic::net::DestroySynchronized*)event.get();

		if (this->syncToIdMap.count(destroyEvent->getSyncId()) == 0)
		{
			this->log.error().format("The server issued a destroy command on Synchronized "
					"Actor with SyncID %u, owning ClientId %u, but it does not exist.",
					destroyEvent->getSyncId(), destroyEvent->getOwningClientId());
		}
		else
		{
			this->log.debug().format("Removing Synchronized Actor %u, owned by %u",
					destroyEvent->getSyncId(), destroyEvent->getOwningClientId());


			actor::Actor *actor = this->findActor(destroyEvent->getSyncId());
			std::string handler = actor->getDestroyHandlerName();

			// Ensure that the handler exists. Use default if it doesn't.
			if (this->syncDestroyHandlers.count(handler) == 0)
			{
				handler = actor::SyncAttributes::DEFAULT_DESTROY_HANDLER;
			}

			actor::g_actorDesyncSafety = true;

			// The actor must *always* be desynchronized.
			actor->desynchronize();
			this->syncDestroyHandlers[handler](actor, this);

			actor::g_actorDesyncSafety = false;
		}
	}
}

void WorldManager::handleActorManagement(actor::Actor* actor)
{
	auto actorTransform = actor->findComponent<actor::Transform>();

	if (actorTransform != nullptr)
	{
		auto actorPhysics = actor->findComponent<physics::ActorPhysics>();

		if (actorPhysics == nullptr)
		{
			std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
			this->nonPhysicalActorTransforms.push_back(actorTransform);
		}
	}

	this->onActorManaged(actor);
}

void WorldManager::handleActorRemoval(actor::Actor* actor)
{
	const actor::ActorIdentifier& actorId = actor->getId();

	std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	auto actorTransformIt = std::find_if(
			this->nonPhysicalActorTransforms.begin(),
			this->nonPhysicalActorTransforms.end(),
			[actorId](actor::Transform* transform)
			{
				return actorId == transform->getOwner()->getId();
			}
	);

	if (actorTransformIt != this->nonPhysicalActorTransforms.end())
	{
		this->nonPhysicalActorTransforms.erase(actorTransformIt);
	}

	this->onActorRemoved(actor);
}

void WorldManager::onActorManaged(actor::Actor* actor)
{
	if (actor->hasParent() == false)
	{
		actor->create();
		actor->activate();
	}
}

void WorldManager::onActorRemoved(actor::Actor* /*actor*/)
{
}

actor::Actor* WorldManager::createActor(const std::string& name)
{
	std::unique_ptr<actor::Actor> actor = this->actorFactory.createActor(name);
	this->handleActorCreation(actor.get());

	return this->manageActor(std::move(actor));
}

void WorldManager::addSyncActorDestroyHandler(std::string name, SyncDestroyHandler handler)
{
	if (this->syncDestroyHandlers.count(name) == 0)
	{
		this->syncDestroyHandlers[name] = handler;
	}
	else
	{
		this->log.warning().format("Attempted to overwrite Destroy Handler '%s'. "
				"The existing handler will be used.", name.c_str());
	}
}

std::unique_ptr<actor::Actor> WorldManager::createActorFromDefinitionName(const std::string& actorDefinitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(actorDefinitionName, childActors);

	this->handleActorCreation(actor.get());

	return actor;
}

std::unique_ptr<actor::Actor> WorldManager::createActorFromSource(const Json::Value& jsonSource, const std::string& definitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(jsonSource, definitionName, childActors);

	this->handleActorCreation(actor.get());

	return actor;
}

actor::Actor* WorldManager::manageActor(std::unique_ptr<actor::Actor> actor)
{
	actor::Actor* actorReference = actor.get();

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);
	this->actors.push_back(std::move(actor));
	actorLock.unlock();

	this->handleActorManagement(actorReference);

	this->log.verbose().format("Actor %s with id %d managed.", actorReference->getName().c_str(), actorReference->getId().getValue());

	return actorReference;
}

void WorldManager::removeActor(std::unique_ptr<actor::Actor> actor)
{
	if (actor->isSynchronized())
	{
		// Abort execution right here and now. The game will be in a broken state which
		// under very few circumstances should function, and under no circumstance
		// should be desired.
		this->log.fatal().format("Attempted to remove synchronized actor. Do not remove synchronize "
				"actors manually - only do so on the server's command. "
				"Name: %s  Definition name: %s  ActorId: %u  SyncId: %u  OwnerId: %u",
				actor->getName().c_str(), actor->getDefinitionName().c_str(),
				actor->getId().getValue(), actor->getSyncId(), actor->getOwnerClientId());
		abort();
	}

	// Erase entries of the Actor
	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);
	this->idToActorMap.erase(actor->getId());
	this->syncToIdMap.erase(actor->getSyncId());
	idMapLock.unlock();

	// Deactivate and destroy the Actor
	actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
	actor->deactivate();
	actor->destroy();

	// Detach and gather all children of this Actor
	std::vector<std::unique_ptr<actor::Actor>> childActors = actor->detachAllChildActors();
	std::list<std::unique_ptr<actor::Actor>> detachedChildActorQueue;
	std::move(childActors.begin(), childActors.end(), std::back_inserter(detachedChildActorQueue));

	std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
	removeEvent->setActor(std::move(actor));
	this->eventBroadcaster->queueEvent(removeEvent);

	while (detachedChildActorQueue.empty() == false)
	{
		std::unique_ptr<actor::Actor>& childActor = detachedChildActorQueue.front();

		std::vector<std::unique_ptr<actor::Actor>> childOfChildActors = childActor->detachAllChildActors();
		std::move(childOfChildActors.begin(), childOfChildActors.end(), std::back_inserter(detachedChildActorQueue));

		idMapLock.lock();
		this->idToActorMap.erase(childActor->getId());
		idMapLock.unlock();

		childActor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		childActor->deactivate();
		childActor->destroy();

		std::shared_ptr<ActorRemoved> childRemoveEvent = std::make_shared<ActorRemoved>();
		childRemoveEvent->setActor(std::move(childActor));
		this->eventBroadcaster->queueEvent(childRemoveEvent);

		detachedChildActorQueue.pop_front();
	}

	this->handleActorRemoval(removeEvent->getActor());
}

void WorldManager::removeActor(const actor::ActorIdentifier& actorId)
{
	if (this->actorUpdateLocked == true)
	{
		this->actorRemovalQueue.push(actorId);
	}
	else
	{
		std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);

		auto actorItr = std::find_if(
				this->actors.begin(),
				this->actors.end(),
				[&actorId](const std::unique_ptr<actor::Actor> &actor)
				{
					return actorId == actor->getId();
				}
		);

		if (actorItr != this->actors.end())
		{
			this->removeActor(std::move(*actorItr));

			this->actors.erase(actorItr);
			actorLock.unlock();
		}
	}
}

void WorldManager::clearActors()
{
	for (auto& actor : this->actors)
	{
		actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		actor->deactivate();

		actor::g_actorDesyncSafety = true;
		actor->desynchronize();
		actor::g_actorDesyncSafety = false;

		actor->destroy();


		std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
		removeEvent->setActor(std::move(actor));

		this->eventBroadcaster->queueEvent(removeEvent);

	}

	this->actors.clear();
	this->idToActorMap.clear();
	this->syncToIdMap.clear();
}

void WorldManager::handleQueuedTasks()
{
	while (this->actorCreations.empty() == false)
	{
		std::unique_ptr<actor::Actor> actor = std::move(this->actorCreations.front());
		this->manageActor(std::move(actor));

		this->actorCreations.pop();
	}

	while (this->actorRemovalQueue.isEmpty() == false)
	{
		actor::ActorIdentifier id = this->actorRemovalQueue.pop(actor::ActorIdentifier());

		if (id != actor::ActorIdentifier())
		{
			this->removeActor(id);
		}
	}
}

void WorldManager::handleActorCreation(actor::Actor* actor)
{
	assert(actor != nullptr);

	actor::ActorIdentifier actorId = actor->getId();

	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);

	this->idToActorMap[actorId] = actor;

	if (actor->isSynchronized())
	{
		this->syncToIdMap[actor->getSyncId()] = actorId;
	}

	const std::vector<actor::Actor::Child> children = actor->findChildrenRecursively();

	for (const actor::Actor::Child& child : children)
	{
		this->idToActorMap[child.actor->getId()] = child.actor;
	}

	idMapLock.unlock();

	auto actorCreatedEvent = std::make_shared<ActorCreated>(actor);

	this->eventBroadcaster->queueEvent(actorCreatedEvent);

	for (const actor::Actor::Child& child : children)
	{
		auto childActorCreatedEvent = std::make_shared<ActorCreated>(child.actor);

		this->eventBroadcaster->queueEvent(childActorCreatedEvent);
	}

	this->log.verbose().format("Created actor \"%s\" with id %d from definition \"%s\".", actor->getName().c_str(), actor->getId().getValue(), actor->getDefinitionName().c_str());
}

void WorldManager::initDefaultDestroyHandlers()
{
	this->addSyncActorDestroyHandler(actor::SyncAttributes::DEFAULT_DESTROY_HANDLER,
	[](actor::Actor* actor, WorldManager* mgr) -> void
	{
		actor->desynchronize();
		mgr->removeActor(actor->getId());
	});

	this->addSyncActorDestroyHandler("desync",
	[](actor::Actor* /*actor*/, WorldManager* /*mgr*/) -> void
	{
		// The actor was already given to us in a desynronized state, so
		// we don't really need to do anything. This handler only exist to
		// avoid falling back to the default, which removes the actor.
	});
}

ILogicContext* WorldManager::getLogicContext()
{
	return this->context;
}

void WorldManager::onUpdate(const nox::Duration& /*deltaTime*/)
{
}

void nox::logic::world::WorldManager::onReset()
{
}

} } }
