/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/WorldWriter.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/View.h>
#include <nox/logic/actor/Actor.h>

#include <json/writer.h>
#include <json/reader.h>

namespace nox { namespace logic { namespace world
{

WorldWriter::WorldWriter(const ILogicContext* logicContext):
	logicContext(logicContext)
{
}

void WorldWriter::write(std::ostream& output) const
{
	Json::Value documentRoot;
	this->write(documentRoot);

	Json::StyledStreamWriter writer;

	writer.write(output, documentRoot);
}

void WorldWriter::write(Json::Value& worldJson) const
{
	Json::Value& actorJsonArray = worldJson["actors"];
	Json::Value& viewJsonArray = worldJson["views"];

	Json::ArrayIndex actorIndex = 0;

	for (const auto& actor : this->actorVector)
	{
		Json::Value& actorJson = actorJsonArray[actorIndex];
		actor->serialize(actorJson, true);

		const View* controllingView = this->logicContext->findControllingView(actor->getId());

		if (controllingView != nullptr)
		{
			Json::Value viewJson;
			viewJson["type"] = controllingView->getTypeString();
			viewJson["controlledActorIndex"] = actorIndex;
			controllingView->serialize(viewJson["viewData"]);

			viewJsonArray.append(viewJson);
		}

		actorIndex++;
	}
}

void WorldWriter::clear()
{
	this->actorVector.clear();
}

void WorldWriter::removeDuplicates(Json::Value& removeFrom, const Json::Value& duplicates) const
{
	Json::Value::Members originalMembers = removeFrom.getMemberNames();

	for (std::string memberName : originalMembers)
	{
		Json::Value duplicate = duplicates.get(memberName, Json::nullValue);

		if (duplicate == Json::objectValue)
		{
			this->removeDuplicates(removeFrom[memberName], duplicate);
		}
		else if (duplicate != Json::nullValue)
		{
			if (removeFrom[memberName] == duplicate)
			{
				removeFrom.removeMember(memberName);
			}
		}
	}
}

void WorldWriter::addActor(const actor::Actor* actor)
{
	this->actorVector.push_back(actor);
}

void WorldWriter::addActors(std::vector<const actor::Actor*> actors)
{
	this->actorVector.insert(this->actorVector.end(), actors.begin(), actors.end());
}

} } }
