/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/WorldLoader.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/View.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/WorldManager.h>

#include <cassert>

namespace nox { namespace logic { namespace world
{

WorldLoader::WorldLoader(ILogicContext* logicContext):
	context(logicContext)
{
	this->log = this->context->createLogger();
	this->log.setName("WorldLoader");
}

void WorldLoader::registerControllingView(const std::size_t controlIndex, View* view)
{
	assert(view != nullptr);
	this->registeredControllingViews[controlIndex] = view;
}

bool WorldLoader::loadWorld(const Json::Value& worldJson, world::WorldManager* worldManager)
{
	const Json::Value& actorArrayValue = worldJson["actors"];

	std::vector<std::unique_ptr<actor::Actor>> createdActors;
	createdActors.reserve(actorArrayValue.size());

	for (const Json::Value& actorValue : actorArrayValue)
	{
		auto actor = worldManager->createActorFromSource(actorValue, "");

		if (actor != nullptr)
		{
			createdActors.push_back(std::move(actor));
		}
		else
		{
			this->log.error().format("Failed loading world: Failed creating Actor.");
			return false;
		}
	}

	const Json::Value& viewArrayValue = worldJson["views"];

	for (const Json::Value& viewValue : viewArrayValue)
	{
		View* view = nullptr;

		const auto typeString = viewValue.get("type", "").asString();

		if (typeString == "registered")
		{
			this->log.debug().format("Loading View from world: View is of type \"registered\".");

			const auto& registeredViewIndexValue = viewValue["registeredIndex"];

			if (registeredViewIndexValue.isNull())
			{
				this->log.error().format("Failed loading View from world: View is of type \"registered\", but it has no \"registeredIndex\".");
				return false;
			}
			else if (registeredViewIndexValue.isUInt64() == false && registeredViewIndexValue.isUInt() == false)
			{
				this->log.error().format("Failed loading View from world: View is of type \"registered\", but its \"registeredIndex\" is not and unsigned integer.");
				return false;
			}
			else
			{
				const std::size_t registerIndex = static_cast<std::size_t>(registeredViewIndexValue.asLargestUInt());

				this->log.debug().format("Loading View from world: View has registered index %u.", registerIndex);

				auto viewIt = this->registeredControllingViews.find(registerIndex);

				if (viewIt == this->registeredControllingViews.end())
				{
					this->log.error().format("Failed loading View from world: View is of type \"registered\", but its \"registeredIndex\" was not found.");
					return false;
				}
				else
				{
					this->log.debug().format("Loading View from world: Found View on registered index %u.", registerIndex);

					view = viewIt->second;
				}
			}
		}
		else
		{
			this->log.error().format("Failed loading View from world: View type \"%s\" not supported.", typeString.c_str());
			return false;
		}

		if (view != nullptr)
		{
			this->log.debug().format("Loading View from world: View was found or created.");

			const auto& controlledActorValue = viewValue["controlledActor"];

			if (controlledActorValue.isUInt() || controlledActorValue.isUInt64())
			{
				const auto controlledActorIndex = static_cast<decltype(createdActors.size())>(controlledActorValue.asLargestUInt());

				this->log.debug().format("Loading View from world: View is controlling Actor index %u.", controlledActorIndex);

				if (controlledActorIndex < createdActors.size())
				{
					auto actor = createdActors[controlledActorIndex].get();

					view->setControlledActor(actor);

					this->log.verbose().format("Successfully loaded View from world: View set to control Actor \"%s\"", actor->getName().c_str());
				}
				else
				{
					this->log.error().format("Failed loading View from world: \"controlledActor\" index is out of bounds.");
					return false;
				}
			}
			else if (controlledActorValue.isNull())
			{
				this->log.error().format("Failed loading View from world: No \"controlledActor\".");
				return false;
			}
			else
			{
				this->log.error().format("Failed loading View from world: \"controlledActor\" was not and unsigned int.");
				return false;
			}
		}
		else
		{
			this->log.error().format("Failed loading View from world: Could not find or create View.");
			return false;
		}
	}

	for (auto& actor : createdActors)
	{
		worldManager->manageActor(std::move(actor));
	}

	return true;
}

} } }
