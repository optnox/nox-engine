/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/BroadcastReceiver.h>
#include <nox/logic/net/event/ServerBroadcastEvent.h>

#include <nox/logic/event/IEventBroadcaster.h>


namespace nox { namespace logic { namespace net {


BroadcastReceiver::BroadcastReceiver(logic::ILogicContext *ctx):
	socket(nullptr),
	context(ctx),
	logger(ctx->createLogger())
{
	this->logger.setName("BroadcastReceiver");

	SocketUdp *udp = new SocketUdp(context, "", 0, SERVER_BROADCAST_PORT_NO);

	if (!udp->initSocket() || !udp->isGood())
	{
		throw std::runtime_error("Unable to listen to server broadcasts");
	}

	this->socket = udp;
}

BroadcastReceiver::~BroadcastReceiver()
{
	if (this->socket)
	{
		delete this->socket;
	}

	for (auto it = this->servers.begin(); it != this->servers.end(); it++)
	{
		delete it->second;
	}
}


void BroadcastReceiver::update(const Duration& deltaTime)
{
	if (!this->socket || !this->socket->isGood())
	{
		return;
	}

	this->ageBroadcasts(deltaTime);
	this->readNewBroadcasts();
	this->removeOldBroadcasts();
}


std::vector<ServerConnectionInfo> BroadcastReceiver::getServerList() const
{
	std::vector<ServerConnectionInfo> vec;

	for (auto it = this->servers.begin(); it != this->servers.end(); it++)
	{
		ServerConnectionInfo sci = it->second->serverInfo;
		vec.push_back(sci);
	}

	return vec;
}


void BroadcastReceiver::ageBroadcasts(const Duration& deltaTime)
{
	for (auto it = this->servers.begin(); it != this->servers.end(); it++)
	{
		it->second->age += deltaTime;
	}
}

void BroadcastReceiver::readNewBroadcasts()
{
	// Well this is a fucking mess.

	this->socket->parseTrafficData();	// If return = false?

	Packet *packet = nullptr;
	while ((packet = this->socket->popPacket()))
	{
		try
		{
			unsigned pktId;
			unsigned tcpPort;

			*packet >> pktId;
			if (pktId == (unsigned)protocol::SERVER_BROADCAST)
			{
				// Retrieve the sender of the broadcast.
				IPaddress ipaddr = packet->getSourceAddress();

				ServerBroadcast *server;
				bool fresh = false;

				if (this->servers.count(ipaddr.host))
				{
					server = this->servers[ipaddr.host];
				}
				else
				{
					server = new ServerBroadcast;
					servers[ipaddr.host] = server;
					fresh = true;
				}

				*packet >> server->serverInfo.appId
						>> server->serverInfo.versionId
						>> server->serverInfo.serverName
						>> server->serverInfo.connectedClients
						>> server->serverInfo.maxClients
						>> tcpPort;
				server->serverInfo.tcpPort = (Uint16)tcpPort;

				// Abort if the server is incompatible (mismatch in appId &/ versionId)
				if (server->serverInfo.appId != this->context->getApplicationId() ||
					server->serverInfo.versionId != this->context->getApplicationVersionId())
				{
					delete server;
					this->servers.erase(ipaddr.host);
					continue;
				}

				// Convert the source IP to a string on octal IP format.
				// This is because SDLNet_ResolveIP may give funny results
				// when resolving ones own local IP address.
				server->serverInfo.hostname = octalIpAddress(ipaddr);
				server->age = Duration(0);

				if (fresh)
				{
					this->raiseBroadcastEvent(server->serverInfo, true);
				}
			}
		}
		catch (std::runtime_error rte)
		{
			this->logger.error().format("Exception when reading packet: %s\n", rte.what());
		}

		delete packet;
		packet = nullptr;
	}
}

void BroadcastReceiver::removeOldBroadcasts()
{
	auto it = this->servers.begin();
	while (it != this->servers.end())
	{
		// Wait for three broadcast intervals before assuming the server has
		// ceased existing.
		if (it->second->age >= (SERVER_BROADCAST_INTERV * 3))
		{
			this->raiseBroadcastEvent(it->second->serverInfo, false);

			delete it->second;
			it = this->servers.erase(it);
		}
		else
		{
			it++;
		}
	}
}


void BroadcastReceiver::raiseBroadcastEvent(ServerConnectionInfo info, bool online)
{
	logic::event::IEventBroadcaster *broadcaster = this->context->getEventBroadcaster();

	auto event = std::make_shared<ServerBroadcastEvent>(online, info);
	broadcaster->queueEvent(event);
}


} } }
