/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/protocol.h>
#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/SocketUdp.h>

#include <nox/app/log/Logger.h>

#include <SDL2/SDL_net.h>

#include <stdlib.h>
#include <assert.h>
#include <sstream>
#include <string.h>


namespace nox { namespace logic { namespace net {


std::string fieldTypeToStr(FieldType type)
{
	switch (type)
	{
		case FieldType::UINT32:
			return "UINT32";
		case FieldType::INT32:
			return "INT32";
		case FieldType::UINT64:
			return "UINT64";
		case FieldType::INT64:
			return "INT64";
		case FieldType::FLOAT32:
			return "FLOAT32";
		case FieldType::BYTE:
			return "BYTE";
		case FieldType::STRING:
			return "STRING";
		case FieldType::ARRAY:
			return "ARRAY";
		case FieldType::VEC2:
			return "VEC2";
		case FieldType::PACKET:
			return "PACKET";
		case FieldType::UNDEFINED:
			return "UNDEFINED";
		default:
			return "UNDEFINED";
	}
}

int volatileStrlen(const char *str, int maxlen)
{
	int i = 0;

	while (*str && i < maxlen)
	{
		i++;
		str++;
	}

	if (i == maxlen)
	{
		return -1;
	}

	return i;
}

std::string octalIpAddress(const IPaddress &ipaddr)
{
	// Convert the ipaddr from NO to HO.
	Uint32 ip = SDLNet_Read32(&ipaddr.host);

	std::stringstream ss;
	ss << ((ip >> 24) & 0xFF) << ".";
	ss << ((ip >> 16) & 0xFF) << ".";
	ss << ((ip >> 8 ) & 0xFF) << ".";
	ss << ((ip	  ) & 0xFF);
	return ss.str();
}

Uint32 octalToIp(std::string octal, bool convToNO)
{
	Uint32 ip = 0;

	int sections = 0;
	const char *delim = ".";
	char *token = nullptr;
	char str[80];
	strcpy(str, octal.c_str());

	token = strtok(str, delim);
	while (token != nullptr)
	{
		if (!*token)
		{
			return 0;
		}

		int val = atoi(token);
		if (val < 0 || val > 255)
		{
			return 0;
		}

		ip += (uint32_t)val << uint32_t((3-sections) * 8);

		sections++;
		if (sections > 4)
		{
			return 0;
		}

		token = strtok(nullptr, delim);
	}

	if (sections != 4)
	{
		return 0;
	}

	if (!convToNO)
	{
		return ip;
	}

	// Convert the IP from HO to NO.
	Uint32 noIp = 0;
	SDLNet_Write32(ip, &noIp);

	return noIp;
}

int readInt(const uint8_t* buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	int i = 0;
	assert(sizeof(i) == sizeof(ho));

	// Copy memory directory to guarantee that the sign is not implicitly changed
	memcpy(&i, &ho, sizeof(int));
	return i;
}

uint32_t readUint(const uint8_t* buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	return ho;
}

int64_t readLInt(const uint8_t* buf)
{
	Uint64 no = *((Uint64*) buf);
	Uint64 ho = networkQword(&no);

	int64_t i = 0;

	// Copy memory directory to guarantee that the sign is not implicitly changed
	memcpy(&i, &ho, sizeof(int64_t));
	return i;
}

uint64_t readLUint(const uint8_t* buf)
{
	Uint64 no = *((Uint64*) buf);
	Uint64 ho = networkQword(&no);

	return ho;
}

float readFloat(const uint8_t* buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	float f = 0.0f;

	assert(sizeof f == sizeof ho);

	memcpy(&f, &ho, sizeof(f));
	return f;
}

uint32_t networkDword(void *ptr)
{
	Uint32 uint = 0;
	Uint32 out = 0;
	memcpy(&uint, ptr, sizeof(Uint32));
	SDLNet_Write32(uint, &out);

	return out;
}

uint64_t networkQword(void *ptr)
{
	if (!sysNetworkEndian())
	{
		// The first 4 bytes are the least significant. Convert each half into
		// two network-ordered DWORDs.
		Uint32 low = networkDword(ptr);
		Uint32 high = networkDword((uint32_t*)((size_t)ptr + 4));

		// Swap the order of the two halves.
		uint64_t qw = ((uint64_t)low << 32) | high;
		return qw;
	}

	return *(uint64_t*)ptr;
}


} } }
