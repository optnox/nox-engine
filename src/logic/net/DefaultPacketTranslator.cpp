/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/DefaultPacketTranslator.h>

#include <nox/logic/net/event/ClientConnected.h>
#include <nox/logic/net/event/ClientDisconnected.h>
#include <nox/logic/net/event/CreateSynchronized.h>
#include <nox/logic/net/event/DestroySynchronized.h>

namespace nox { namespace logic { namespace net
{

DefaultPacketTranslator::DefaultPacketTranslator()
{
	/**
	 * Add a lambda to this->eventCreatorMap, returning a new instance of an event,
	 * given an event::ID.
	 */
	#define MAP_EVENT(_event_class)									 \
		this->eventCreatorMap[_event_class::ID] =					   \
		[](const Packet* pkt)										   \
		{															   \
			_event_class *raw = new _event_class(pkt);				  \
			std::shared_ptr<_event_class> shared(raw);				  \
			return shared;											  \
		}

	MAP_EVENT(ClientConnected);
	MAP_EVENT(ClientDisconnected);
	MAP_EVENT(CreateSynchronized);
	MAP_EVENT(DestroySynchronized);

	#undef MAP_EVENT
}

std::shared_ptr<event::Event> DefaultPacketTranslator::translatePacket(std::string eventId, const Packet *packet)
{
	if (this->eventCreatorMap.count(eventId))
	{
		return this->eventCreatorMap[eventId](packet);
	}

	return nullptr;
}

}
} }
