/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/ServerNetworkManager.h>
#include <nox/logic/net/ClientNetworkManager.h>
#include <nox/logic/net/join_convo.h>
#include <nox/logic/net/RemoteClient.h>
#include <nox/logic/net/BroadcastSender.h>
#include <nox/logic/net/event/ClientConnected.h>
#include <nox/logic/net/event/ClientDisconnected.h>

#include <assert.h>


namespace nox { namespace logic { namespace net {


ServerNetworkManager::ServerNetworkManager(IPacketTranslator *translator,
									 ServerDelegate *delegate):
	NetworkManager(translator),
	accessList("access.ini", AccessFilter::FILTER_IP),
	serverDelegate(delegate),
	doBroadcast(false),
	broadcastSender(nullptr),
	serverSocket(nullptr)
{
	const unsigned lobbySize = delegate->getLobbySize();
	if (lobbySize == 0)
	{
		throw std::runtime_error("ServerNetworkManager initiated with a lobby "
				"size of 0. The ServerDelegate implementation must return "
				"a non-zero value in '::getLobbySize() const'.");
	}

	this->setLobbySize(delegate->getLobbySize());
}

ServerNetworkManager::~ServerNetworkManager()
{
	if (this->broadcastSender)
	{
		delete this->broadcastSender;
	}
}


bool ServerNetworkManager::initialize(nox::logic::ILogicContext *context)
{
	if (!NetworkManager::initialize(context))
	{
		return false;
	}

	this->getLogger().setName("ServerNetworkManager");
	this->serverDelegate->setNetworkManager(this);

	// If setEnableDiscoveryBroadcast has been called before initialize,
	// re-call it so that the broadcasting actually starts.
	if (this->doBroadcast && !this->broadcastSender)
	{
		this->setEnableDiscoveryBroadcast(true);
	}


	return true;
}

void ServerNetworkManager::destroy()
{
	NetworkManager::destroy();
}

void ServerNetworkManager::update(const Duration& deltaTime)
{
	NetworkManager::update(deltaTime);

	handleNewConnections();
	handleAcceptConvos();
	updateRemoteClients(deltaTime);

	if (this->broadcastSender)
	{
		this->broadcastSender->update(deltaTime);
	}

	if (this->serverDelegate != nullptr)
	{
		this->serverDelegate->onUpdate(deltaTime);
	}
}


void ServerNetworkManager::estimatePacketAge(Packet& packet, ClientId sender)
{
	const int64_t creationInHostTime = packet.getCreationTime();
	const int64_t hostClockDifference = this->getClientStats(sender)->getClockDifference();
	const int64_t creationInServerTime = creationInHostTime - hostClockDifference;

	int64_t msAge = this->getNetworkTimeMilliSeconds() - creationInServerTime;
	if (msAge < 0)
	{
		msAge = 0;
	}

	Duration age = Duration(std::chrono::milliseconds(msAge));

	packet.setEstimatedAge(age);
}


bool ServerNetworkManager::startServer(Uint16 tcpListenPort)
{
	if (this->serverSocket)
	{
		return false;
	}

	this->serverSocket = new ConnectionListener(tcpListenPort);

	if (!this->serverSocket->isGood())
	{
		delete this->serverSocket;
		this->serverSocket = nullptr;
		return false;
	}

	return true;
}


void ServerNetworkManager::setEnableDiscoveryBroadcast(bool enable)
{
	// Set the "doBroadcast" flag. If initialize() has not yet been called,
	// this method will be called after it has.
	this->doBroadcast = enable;

	if (!this->serverSocket)
	{
		this->getLogger().warning().raw("Attempted to change discover broadcast "
										"setting on instance without a valid "
										"ConnectionListener");
		return;
	}

	if (enable && !this->broadcastSender && this->getContext())
	{
		Uint16 tcpPort  = this->serverSocket->getListenPort();
		this->broadcastSender = new BroadcastSender(tcpPort, this->getContext(), this);
	}
	else if (!enable && this->broadcastSender)
	{
		delete this->broadcastSender;
		this->broadcastSender = nullptr;
	}
}

std::vector<RemoteClient*> ServerNetworkManager::getClients()
{
	return clients;
}


void ServerNetworkManager::sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() == client)
		{
			rc->sendPacket(&packet, proto);
			return;
		}
	}
}

void ServerNetworkManager::sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() != client)
		{
			rc->sendPacket(&packet, proto);
		}
	}
}

void ServerNetworkManager::sendToEveryone(const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		rc->sendPacket(&packet, proto);
	}
}


void ServerNetworkManager::kickClient(ClientId id, std::string reason)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		if ((*it)->getUserData().getClientId() == id)
		{
			this->onClientDisconnected(id);
			raiseDcEvent(id, protocol::DcReason::BAN_TIMED, 0, reason);

			delete *it;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void ServerNetworkManager::banClient(ClientId id, const Duration &dur, std::string reason)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		if ((*it)->getUserData().getClientId() == id)
		{
			this->onClientDisconnected(id);
			raiseDcEvent(id, protocol::DcReason::BAN_TIMED, 0, reason);

			IPaddress ipaddr = (*it)->getIpAddress(TLProtocol::TCP);
			std::string uname = (*it)->getUserData().getUserName();

			if (dur.count() == 0)
			{
				// Ban indefinitely
				this->accessList.setAccess(ipaddr, uname, AccessType::ALWAYS_DENY);
			}
			else
			{
				// Ban for a period
				this->accessList.banWithDuration(ipaddr, uname, dur);
			}

			delete *it;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

bool ServerNetworkManager::shouldAcceptClient(IPaddress ipaddr, std::string userName)
{
	if (this->getClientsInLobbyCount() >= this->getLobbySize())
	{
		return false;
	}

	switch (this->accessList.getAccessType(ipaddr, userName))
	{
		case AccessType::ALWAYS_ALLOW:
			return true;
		case AccessType::ALWAYS_DENY:
			return false;
		default:
			break;
	}

	/* Never reached */
	return true;
}

void ServerNetworkManager::sendSyncOutStateUpdates()
{
	// *IF* this process is also running a client, we must avoid sending it's own updates
	// back to it. Find the hash of the potential client, and ensure we never send updates to
	// a RemoteClient with the same hash.
	std::string localClientHash = "undef";
	ClientNetworkManager* clientNetMgr = this->getContext()->getClientNetworkManager();
	if (clientNetMgr != nullptr)
	{
		LocalClient *localClient = clientNetMgr->getClient();
		if (localClient != nullptr)
		{
			localClientHash = localClient->getUserData().getUserHash();
		}
	}

	// Build a map of every RemoteClient and their SyncOut-state-bundle.
	std::map<RemoteClient*,Packet> clientStates;
	for (RemoteClient *rc : this->clients)
	{
		Packet packet;
		packet.setCreationTime(this->getNetworkTimeMilliSeconds());

		rc->packSyncOutUpdates(packet);
		clientStates[rc] = packet;
	}

	// Bundle "everyone else" into a packet and send it to each RC
	for (RemoteClient *rc : this->clients)
	{
		const std::string& rcHash = rc->getUserData().getUserHash();
		if (rcHash == localClientHash)
		{
			// Ignore this RemoteClient, as it's counterpart is running within this process.
			continue;
		}

		Packet packet;
		packet << (uint32_t)protocol::PacketId::SYNC_PACKET;

		int stateCount = 0;

		for (auto pair : clientStates)
		{
			if (pair.first != rc)
			{
				packet << pair.second;
				stateCount++;
			}
		}

		if (stateCount != 0)
		{
			rc->sendPacket(&packet, TLProtocol::UDP);
		}
	}
}

void ServerNetworkManager::onClientConnected(UserData userData)
{
	NetworkManager::onClientConnected(userData);
	this->serverDelegate->synchronizeExistingActors(userData.getClientId());
	this->serverDelegate->onClientJoined(this->getClientStats(userData.getClientId()));

	// Send notification to the existing lobby
	{
		ClientConnected conEvent(userData);

		Packet pkt;
		pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
		conEvent.serialize(&pkt);

		this->sendToEveryoneElse(userData.getClientId(), pkt);
	}

	// Send notifications about the existing lobby to the new arrival
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() != userData.getClientId())
		{
			ClientConnected conEvent(rc->getUserData());

			Packet pkt;
			pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
			conEvent.serialize(&pkt);

			this->sendExeclusivelyTo(userData.getClientId(), pkt);
		}
	}
}

void ServerNetworkManager::onClientDisconnected(ClientId clientId)
{
	this->serverDelegate->onClientLeft(this->getClientStats(clientId));
	NetworkManager::onClientDisconnected(clientId);
}


void ServerNetworkManager::setLobbySize(unsigned size)
{
	if (this->getLobbySize() != 0)
	{
		this->getLogger().warning().raw("ServerNetworkManager::setLobbySize called more than once");
	}

	NetworkManager::setLobbySize(size);
}


void ServerNetworkManager::handleNewConnections()
{
	while (this->serverSocket && this->serverSocket->hasNewConnection())
	{
		SocketTcp *tcp = this->serverSocket->popNewConnection(this->getContext());

		protocol::AcceptClientConversation *convo =
			new protocol::AcceptClientConversation(this, this->getContext(), tcp);
		this->acceptConvos.push_back(convo);
	}
}

void ServerNetworkManager::handleAcceptConvos()
{
	auto iter = this->acceptConvos.begin();
	while (iter != this->acceptConvos.end())
	{
		if (!(*iter)->update())
		{
			delete *iter;
			iter = this->acceptConvos.erase(iter);
			continue;
		}

		if ((*iter)->isDone())
		{
			this->getLogger().info().raw("AcceptConvo done");

			if ((*iter)->acceptedClient())
			{
				RemoteClient *client = (*iter)->getRemoteClient();
				assert(client != nullptr);
				client->setCustomPacketTranslator(packetTranslator);
				client->setServerDelegate(this->serverDelegate);
				clients.push_back(client);

				this->onClientConnected(client->getUserData());

				this->getLogger().info().format("Accepted client %s with ID %u",
						client->getUserData().getUserName().c_str(),
						client->getUserData().getClientId());
			}
			else
			{
				this->getLogger().info().raw("Rejected client");
			}

			delete *iter;
			iter = this->acceptConvos.erase(iter);
			continue;
		}

		iter++;
	}
}

void ServerNetworkManager::updateRemoteClients(const Duration &deltaTime)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		RemoteClient *client = *it;

		if (!client->update(deltaTime) || !client->isGood())
		{
			this->onClientDisconnected(client->getUserData().getClientId());
			this->raiseDcEvent(client->getUserData().getClientId(), protocol::CONNECTION_LOST);

			delete client;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void ServerNetworkManager::raiseDcEvent(ClientId id, protocol::DcReason dcReason,
									 unsigned dur, std::string banReason)
{
	ClientDisconnected dcEvt(id, dcReason, dur, banReason);

	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	dcEvt.serialize(&pkt);

	this->sendToEveryoneElse(id, pkt);
}

} } }
