/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/component/SyncIn.h>
#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/Packet.h>
#include <nox/logic/actor/Actor.h>


namespace nox { namespace logic { namespace net
{

const logic::actor::Component::IdType SyncIn::NAME = "SyncIn";


SyncIn::SyncIn():
	lastPacketIndex(0)
{
}

const logic::actor::Component::IdType& SyncIn::getName() const
{
	return SyncIn::NAME;
}

void SyncIn::initialize(const Json::Value& /*componentJsonObject*/)
{
}

void SyncIn::serialize(Json::Value& /*componentObject*/)
{
}

void SyncIn::unpackNetworkData(const Packet& packet, unsigned packetIndex) const
{
	if (packetIndex < this->lastPacketIndex)
	{
		return;
	}

	this->lastPacketIndex = packetIndex;

	auto syncComponents = this->getOwner()->getSynchronizedComponents();

	std::string header;
	packet >> header;

	while (header != SyncOut::PACKET_FOOTER)
	{
		int numFields = 0;
		packet >> numFields;

		unsigned position = packet.getOutStreamPosition();

		// Skip this Component's state if it is not attached to the Actor, or
		// it is not listed in the synchronized components set.
		Component *component = this->getOwner()->findComponent(header);
		if (component == nullptr || syncComponents.find(header) == syncComponents.end())
		{
			if (!packet.setOutStreamPosition(position + (unsigned)numFields))
			{
				// This is bad. The Component header lied about its length, or
				// data has been corrupted. None of the remaining data can be
				// trusted.
				return;
			}
		}
		else
		{
			component->deSerialize(packet);

			// In case the Components deSerialize() didn't retrieve all fields,
			// set the out stream index to what it *should* have read. This step
			// is redundant in nearly all cases, but still an important safety measure.
			if (!packet.setOutStreamPosition(position + (unsigned)numFields))
			{
				// As when the Component is NULL (branch above this one), this indicates
				// a more severe problem. None of the data can be trusted.
				return;
			}
		}

		packet >> header;
	}
}


} } }
