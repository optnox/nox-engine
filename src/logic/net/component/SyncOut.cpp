/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/Packet.h>
#include <nox/logic/actor/Actor.h>


namespace nox { namespace logic { namespace net
{

const std::string SyncOut::PACKET_HEADER = "SyncOut";
const std::string SyncOut::PACKET_FOOTER = "/SyncOut";

const logic::actor::Component::IdType SyncOut::NAME = "SyncOut";

SyncOut::SyncOut():
	packetIndex(0)
{

}

const SyncOut::IdType& SyncOut::getName() const
{
	return SyncOut::NAME;
}

void SyncOut::initialize(const Json::Value& /*componentJson*/)
{
}

void SyncOut::serialize(Json::Value& /*componentObject*/)
{
}

void SyncOut::packNetworkData(Packet &packet)
{
	auto syncComponents = this->getOwner()->getSynchronizedComponents();
	this->packetIndex++;

	packet << PACKET_HEADER;			// Sync header
	packet << this->packetIndex;		// Sequence number
	packet << getOwner()->getSyncId();  // SyncId

	for (auto &id : syncComponents)
	{
		Component *comp = this->getOwner()->findComponent(id);
		if (comp != nullptr)
		{
			// Serialize into a temporary packet. This is done so we can check if
			// the Component actually put any values in - if it did, we need to
			// add a Component-header.
			Packet tmp;
			comp->serialize(tmp);
			if (tmp.getFieldCount() != 0)
			{
				// We need to store the ID so we know where to put the data, and
				// the number of fields to handle errors when deserializing
				packet << id;
				packet << (int)tmp.getFieldCount();
				packet.appendFields(tmp);
			}
		}
	}

	packet << PACKET_FOOTER;
}


}
} }
