/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/ping_convo.h>
#include <nox/logic/net/SocketBase.h>
#include <nox/logic/net/NetworkManager.h>

#include <nox/logic/ILogicContext.h>


namespace nox { namespace logic { namespace net { namespace protocol
{

static const Duration g_timeout(std::chrono::seconds(2));

/*
==================
PingConversation
==================
*/
PingConversation::PingConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr):
	Conversation(socket, context),
	networkManager(netMgr),
	done(false),
	success(false),
	clockDifference(0)
{

}

bool PingConversation::update()
{
	if (this->isDone())
	{
		return false;
	}

	if (this->timeoutClock.getElapsedTime() > g_timeout)
	{
		this->getLogger().error().raw("Heartbeat timed out");
		this->setDone(true);
		this->setSuccess(false);
		return false;
	}

	return true;
}

bool PingConversation::isDone()
{
	return this->done;
}

bool PingConversation::wasSuccessful()
{
	return this->success;
}

int64_t PingConversation::getClockDifference() const
{
	return this->clockDifference;
}

bool PingConversation::sendHeartbeatSyn()
{
	Packet packet;
	packet << static_cast<uint32_t>(PacketId::HEARTBEAT_SYN);

	return this->getSocket()->sendPacket(&packet);
}

bool PingConversation::sendHeartbeatSynAck()
{
	const int64_t netClock = this->getNetworkManager()->getNetworkTimeMilliSeconds();

	Packet packet;
	packet << static_cast<uint32_t>(PacketId::HEARTBEAT_SYNACK);
	packet << netClock;

	return this->getSocket()->sendPacket(&packet);
}

bool PingConversation::sendHeartbeatAck()
{
	Packet packet;
	packet << static_cast<uint32_t>(PacketId::HEARTBEAT_ACK);

	const std::vector<const ClientStats*>& clientStats = this->getNetworkManager()->getConnectedClients();
	for (const ClientStats *cs : clientStats)
	{
		const auto microDuration = std::chrono::duration_cast<std::chrono::microseconds>(cs->getRoundTripTime());
		const uint32_t rttMicro = (uint32_t)microDuration.count();

		const int64_t clockDiff = cs->getClockDifference();

		packet << cs->getUserData().getClientId();
		packet << rttMicro;
		packet << clockDiff;
	}

	return this->getSocket()->sendPacket(&packet);
}

void PingConversation::setDone(bool done)
{
	this->done = done;
}

void PingConversation::setSuccess(bool success)
{
	this->success = success;
}

NetworkManager* PingConversation::getNetworkManager()
{
	return this->networkManager;
}

void PingConversation::setClockDifference(int64_t milliSeconds)
{
	this->clockDifference = milliSeconds;
}



/*
==================
PingInitiateConversation
==================
*/
const std::string PingInitateConversation::NAME = "PingInitateConvo";

PingInitateConversation::PingInitateConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr, ClientId remoteClient):
	PingConversation(socket, context, netMgr),
	remoteClientId(remoteClient)
{
	this->createLogger("PingInitiateConversation");
}

const std::string& PingInitateConversation::getName() const
{
	return PingInitateConversation::NAME;
}

bool PingInitateConversation::start()
{
	if (!this->sendHeartbeatSyn())
	{
		this->getLogger().error().raw("Failed to send HEARTBEAK_SYN");
		return false;
	}

	this->clock.start();
	return true;
}

bool PingInitateConversation::update()
{
	if (!this->PingConversation::update())
	{
		return false;
	}

	Packet* packet = this->findPacket<uint32_t>(static_cast<uint32_t>(PacketId::HEARTBEAT_SYNACK));
	if (packet != nullptr)
	{
		packet->setOutStreamPosition(1);

		int64_t netClock = 0;
		*packet >> netClock;

		const int64_t msClockDiff = netClock - this->getNetworkManager()->getNetworkTimeMilliSeconds();
		const int64_t elapsedMillis = std::chrono::duration_cast<std::chrono::milliseconds>(this->clock.getElapsedTime() / 2).count();
		const int64_t rttAdjustedDiff = msClockDiff + elapsedMillis;

		this->getNetworkManager()->updateRoundTripTime(this->remoteClientId, this->clock.getElapsedTime());
		this->getNetworkManager()->updateNetworkClockDifference(this->remoteClientId, rttAdjustedDiff);

		this->setSuccess(this->sendHeartbeatAck());

		this->setDone(true);

		delete packet;
	}

	return true;
}


/*
==================
PingResponseConversation
==================
*/
const std::string PingResponseConversation::NAME = "PingResponseConvo";

PingResponseConversation::PingResponseConversation(SocketBase* socket, logic::ILogicContext* context, NetworkManager* netMgr):
	PingConversation(socket, context, netMgr),
	hasSentSynAck(false)
{
	this->createLogger("PingResponseConversation");
}

const std::string& PingResponseConversation::getName() const
{
	return PingResponseConversation::NAME;
}

bool PingResponseConversation::update()
{
	if (!this->PingConversation::update())
	{
		return false;
	}

	if (!this->hasSentSynAck)
	{
		this->hasSentSynAck = true;

		if (!this->sendHeartbeatSynAck())
		{
			this->getLogger().error().raw("Unable to send HEARTBEAT_SYNACK");
			return false;
		}
	}

	Packet* packet = this->findPacket<uint32_t>(static_cast<uint32_t>(PacketId::HEARTBEAT_ACK));
	if (packet != nullptr)
	{
		packet->setOutStreamPosition(1);

		const int rttCount = (int)(packet->getFieldCount() - 1) / 3;
		for (int i=0; i<rttCount; i++)
		{
			ClientId clientId = 0;
			uint32_t rttMicroSec = 0;
			int64_t hostClockDiff = 0;

			*packet >> clientId >> rttMicroSec >> hostClockDiff;

			this->getNetworkManager()->updateNetworkClockDifference(clientId, hostClockDiff);

			Duration rttDuration = Duration(std::chrono::microseconds(rttMicroSec));
			this->getNetworkManager()->updateRoundTripTime(clientId, rttDuration);
		}

		this->setDone(true);
		this->setSuccess(true);

		delete packet;
	}

	return true;
}



}
} } }
