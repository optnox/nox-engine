/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/ServerDelegate.h>
#include <nox/logic/net/ServerNetworkManager.h>
#include <nox/logic/net/RemoteClient.h>

#include <nox/logic/net/event/CreateSynchronized.h>
#include <nox/logic/net/event/DestroySynchronized.h>

#include <nox/logic/net/RemoteClient.h>

#include <nox/logic/net/event/ClientConnected.h>
#include <nox/logic/net/event/ClientDisconnected.h>
#include <nox/logic/net/protocol.h>

namespace nox { namespace logic { namespace net
{

// Using a static, global counter to guarantee uniqueness. This value holds the SyncId of
// the *next* synchronized actor, and is intended to be read, assigned, and incremented.
static SyncId g_syncId = 1337;


void ServerDelegate::setNetworkManager(ServerNetworkManager *netMgr)
{
	assert(netMgr != nullptr);
	this->networkManager = netMgr;

	this->context = this->networkManager->getContext();

	this->log = this->context->createLogger();
	this->log.setName("ServerDelegate");
}

void ServerDelegate::onUpdate(const nox::Duration& deltaTime)
{
	// Empty implementation - this method is not required.
}

void ServerDelegate::synchronizeExistingActors(ClientId clientId)
{
	for (auto pair : this->actorMap)
	{
		auto set = pair.second;

		for (auto syncId : set)
		{
			std::string defName = this->actorDefinitionMap[syncId];

			CreateSynchronized event(defName, pair.first, syncId);
			Packet pkt;
			pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
			event.serialize(&pkt);

			this->sendExeclusivelyTo(clientId, pkt);
		}
	}
}

SyncId ServerDelegate::createSynchronizedActor(std::string actor, ClientId owner)
{
	SyncId currentSyncId = g_syncId;
	g_syncId++;

	if (!this->networkManager->isClientConnected(owner))
	{
		this->log.error().format("Attempted to create synchronized actor of type '%s' with "
				"non-existing owner of ID '%u'", actor.c_str(), owner);
		return 0;
	}

	this->log.debug().format("Creating actor of type '%s' withSyncId %u, owned by client %u",
			actor.c_str(), currentSyncId, owner);


	// Notify the lobby
	CreateSynchronized createEvent(actor, owner, currentSyncId);

	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	createEvent.serialize(&pkt);

	this->sendToEveryone(pkt, TLProtocol::TCP);

	// Map the SyncId of the Actor to the owner
	this->actorMap[owner].insert(currentSyncId);
	this->actorDefinitionMap[currentSyncId] = actor;
	return currentSyncId;
}

bool ServerDelegate::destroySynchronizedActor(SyncId syncId)
{
	for (auto it = this->actorMap.begin(); it != this->actorMap.end(); it++)
	{
		std::set<SyncId> &set = it->second;
		if (set.find(syncId) != set.end())
		{
			this->raiseDestroySynchronizedActorEvent(it->first, syncId);
			set.erase(syncId);
			this->actorDefinitionMap.erase(syncId);
			return true;
		}
	}

	return false;
}

int ServerDelegate::destroyAllSynchronizedActors(ClientId owner)
{
	const int count = (int)this->actorMap[owner].size();

	for (SyncId syncId : this->actorMap[owner])
	{
		this->raiseDestroySynchronizedActorEvent(owner, syncId);
		this->actorDefinitionMap.erase(syncId);
	}

	this->actorMap[owner].clear();

	return count;
}


void ServerDelegate::sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendExeclusivelyTo(client, packet, proto);
}

void ServerDelegate::sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendToEveryoneElse(client, packet, proto);
}

void ServerDelegate::sendToEveryone(const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendToEveryone(packet, proto);
}


app::log::Logger& ServerDelegate::getLog()
{
	return this->log;
}

logic::ILogicContext* ServerDelegate::getContext()
{
	return this->context;
}

void ServerDelegate::broadcastExclusivelyTo(ClientId client, const std::shared_ptr<event::Event> event, TLProtocol proto)
{
	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	event->serialize(&pkt);

	this->sendExeclusivelyTo(client, pkt, proto);
}

void ServerDelegate::broadcastToEveryoneElse(ClientId client, const std::shared_ptr<event::Event> event, TLProtocol proto)
{
	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	event->serialize(&pkt);

	this->sendToEveryoneElse(client, pkt, proto);
}

void ServerDelegate::broadcastToEveryone(const std::shared_ptr<event::Event> event, TLProtocol proto)
{
	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	event->serialize(&pkt);

	this->sendToEveryone(pkt, proto);
}

void ServerDelegate::raiseDestroySynchronizedActorEvent(ClientId owner, SyncId syncId)
{
	DestroySynchronized destroy(owner, syncId);

	Packet pkt;
	pkt << (uint32_t)protocol::PacketId::EVENT_PACKET;
	destroy.serialize(&pkt);

	this->sendToEveryone(pkt, TLProtocol::TCP);
}

}
} }
