/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/join_convo.h>
#include <nox/logic/net/ServerNetworkManager.h>

#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/SocketTcp.h>

#include <assert.h>


namespace nox { namespace logic { namespace net { namespace protocol
{

static ClientId g_clientId = 1;


/*
==================
JoinServerConversation
==================
*/
const std::string JoinServerConversation::NAME = "JoinServerConvo";

JoinServerConversation::JoinServerConversation(NetworkManager *netMgr, nox::logic::ILogicContext *ctx, SocketTcp *tcp, UserData &user):
	Conversation(tcp, ctx),
	netMgr(netMgr),
	userData(user),
	tcpSocket(tcp),
	udpSocket(nullptr),
	localClient(nullptr),
	lobbySize(0U),
	init(false),
	sentTcp(false),
	sentUdp(false),
	accepted(false),
	done(false)
{
	this->createLogger("JoinServerConvo");
}

JoinServerConversation::~JoinServerConversation()
{
	if (!this->accepted && this->tcpSocket)
	{
		delete this->tcpSocket;
		assert(this->localClient == nullptr);
	}

	if (!this->accepted && this->udpSocket)
	{
		delete this->udpSocket;
		assert(this->localClient == nullptr);
	}
}


const std::string& JoinServerConversation::getName() const
{
	return JoinServerConversation::NAME;
}


bool JoinServerConversation::start()
{
	if (this->init)
	{
		return false;
	}

	// Create a new UDP socket, bind to a random listening port.
	this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIPAddress(), 0);

	this->getLogger().info().format("UDP listening at %u", udpSocket->getListenPort());

	if (!this->udpSocket->initSocket())
	{
		this->done = true;
		return false;
	}

	if (!this->udpSocket->isGood())
	{
		this->done = true;
		return false;
	}

	if (!this->sendTcpRequest())
	{
		this->done = true;
		return false;
	}

	this->init = true;
	return true;
}

bool JoinServerConversation::update()
{
	if (this->done)
		return false;

	if (!this->tcpSocket->parseTrafficData())
	{
		this->getLogger().error().raw("Bad TCP socket state");
		this->done = true;
		this->accepted = false;
		return false;
	}

	if (this->sentTcp && !this->sentUdp)
	{
		if (this->handleTcpResponse())
		{
			// We have been accepted. Now we need to ensure that
			// the UDP sockets are functional.
			if (!this->sendUdpRequest())
			{
				this->done = true;
				this->accepted = false;
				return false;
			}
		}
	}

	if (this->sentUdp)
	{
		if (!this->udpSocket->parseTrafficData())
		{
			this->getLogger().error().raw("Bad UDP socket state");
			this->done = true;
			this->accepted = false;
			return false;
		}

		this->handleUdpResponse();
	}

	return true;
}

bool JoinServerConversation::isDone()
{
	return this->done;
}


bool JoinServerConversation::wasAccepted() const
{
	return this->accepted;
}

unsigned JoinServerConversation::getLobbySize() const
{
	return this->lobbySize;
}

LocalClient* JoinServerConversation::getLocalClient()
{
	if (this->localClient == nullptr && this->accepted)
	{
		this->localClient = new LocalClient(this->netMgr, this->getContext(), this->userData, this->tcpSocket, this->udpSocket);
	}

	return this->localClient;
}

bool JoinServerConversation::sendTcpRequest()
{
	Packet pkt;

	pkt << (uint32_t)PacketId::JOIN_REQUEST;
	pkt << (uint32_t)this->udpSocket->getListenPort();
	pkt << this->getContext()->getApplicationId();
	pkt << this->getContext()->getApplicationVersionId();
	this->userData.serialize(&pkt);

	if (!this->tcpSocket->sendPacket(&pkt))
	{
		this->getLogger().error().raw("Failed to send TCP JOIN_REQUEST");
		return false;
	}

	this->sentTcp = true;

	this->getLogger().info().raw("Sent TCP JOIN_REQUEST");
	return true;
}

bool JoinServerConversation::handleTcpResponse()
{
	uint32_t id = (uint32_t)PacketId::JOIN_RESPONSE;
	Packet *pkt = this->findPacket<uint32_t>(this->tcpSocket, id);

	if (pkt != nullptr)
	{
		uint32_t id = 0;
		uint8_t accept = 0;
		ClientId clientId = 0;
		uint32_t udpPort = 0;

		*pkt >> id >> accept >> clientId >> udpPort >> this->lobbySize;
		delete pkt;

		this->accepted = accept;

		this->getLogger().info().format("Received JOIN_RESPONSE. "
										"Accepted=%i LobbySize=%u",
										accept, this->lobbySize);

		if (this->accepted)
		{
			this->userData.setClientId(clientId);

			// Create a new SocketUdp now that we know the port numbers of
			// both ourselves and the server.
			uint16_t inPort = this->udpSocket->getListenPort();
			uint16_t outPort = (uint16_t)udpPort;
			delete this->udpSocket;

			this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIP(),
											outPort, inPort);

			if (!this->udpSocket->initSocket() || !this->udpSocket->isGood())
			{
				// If the UDP socket initialization failed, abort everything.
				// The manager of this conversation will clean up the TCP
				// socket, which will notify the server of our departure.
				this->done = true;
				this->accepted = false;

				this->getLogger().error().format("Second UDP socket "
												"initialization failed.");
			}
			else
			{
				this->userData.setClientId(clientId);
				return true;
			}
		}
	}

	return false;
}

bool JoinServerConversation::sendUdpRequest()
{
	Packet pkt;

	pkt << (uint32_t)PacketId::JOIN_REQUEST;

	if (!this->udpSocket->sendPacket(&pkt))
	{
		this->getLogger().error().raw("Failed to send UDP JOIN_REQUEST");
	}

	this->sentUdp = true;
	this->getLogger().info().raw("Sent UDP JOIN_REQUEST");
	return true;
}

void JoinServerConversation::handleUdpResponse()
{
	uint32_t id = (uint32_t)PacketId::JOIN_RESPONSE;
	Packet *pkt = this->findPacket<uint32_t>(this->udpSocket, id);

	if (pkt)
	{
		delete pkt;

		// Coolio! All done!
		this->done = true;
	}
}



/*
==================
AcceptClientConversation
==================
*/
const std::string AcceptClientConversation::NAME = "AcceptClientConvo";

AcceptClientConversation::AcceptClientConversation(ServerNetworkManager *mgr,
												   nox::logic::ILogicContext *ctx,
												   SocketTcp *tcp):
	Conversation(tcp, ctx),
	serverMgr(mgr),
	tcpSocket(tcp),
	udpSocket(nullptr),
	remoteClient(nullptr),
	recvTcpRequest(false),
	recvUdpRequest(false),
	done(false),
	accepted(false)
{
	this->createLogger("AcceptClientConvo");
}

AcceptClientConversation::~AcceptClientConversation()
{
	if (!this->accepted && this->tcpSocket)
	{
		delete this->tcpSocket;
		assert(this->remoteClient == nullptr);
	}

	if (!this->accepted && this->udpSocket)
	{
		delete this->udpSocket;
		assert(this->remoteClient == nullptr);
	}
}

const std::string& AcceptClientConversation::getName() const
{
	return AcceptClientConversation::NAME;
}

bool AcceptClientConversation::update()
{
	if (this->done)
		return false;

	if (!this->tcpSocket->parseTrafficData())
	{
		this->getLogger().error().raw("Bad state in TCP socket");
		this->done = true;
		this->accepted = false;
		return false;
	}

	if (!this->recvTcpRequest)
	{
		if (!this->handleTcpRequest())
		{
			this->done = true;
			this->accepted = false;
			return false;
		}
	}
	else if (!this->recvUdpRequest)
	{
		if (!this->udpSocket)
		{
			this->getLogger().error().raw("Internal inconsistency: UDP socket is null");
			return false;
		}

		if (!this->udpSocket->parseTrafficData())
		{
			this->getLogger().error().raw("Bad state in UDP socket");
			this->done = true;
			this->accepted = false;
			return false;
		}

		if (!this->handleUdpRequest())
		{
			this->done = true;
			this->accepted = false;
			return false;
		}
	}


	return true;
}

bool AcceptClientConversation::isDone()
{
	return this->done;
}

bool AcceptClientConversation::acceptedClient() const
{
	return this->accepted;
}

RemoteClient* AcceptClientConversation::getRemoteClient()
{
	if (this->accepted && this->remoteClient == nullptr)
	{
		this->remoteClient = new RemoteClient(this->serverMgr, this->getContext(), this->userData, this->tcpSocket, this->udpSocket);
	}

	return this->remoteClient;
}

bool AcceptClientConversation::handleTcpRequest()
{
	uint32_t uId = (uint32_t)PacketId::JOIN_REQUEST;
	Packet *pkt = findPacket<uint32_t>(this->tcpSocket, uId);

	if (pkt)
	{
		this->recvTcpRequest = true;

		uint32_t udpPort;
		bool admission = true;
		std::string appId;
		std::string versionId;

		*pkt >> uId
			 >> udpPort
			 >> appId
			 >> versionId;
		if (!this->userData.deSerialize(pkt))
		{
			this->getLogger().error().raw("Unable to deserialize UserData");
			admission = false;
		}

		if (admission == true)
		{
			// Deny the client if the app-ID or version-ID are mismatches
			if (appId != this->getContext()->getApplicationId() ||
				versionId != this->getContext()->getApplicationVersionId())
			{
				admission = false;
			}
		}

		if (admission == true)
		{
			IPaddress addr = this->tcpSocket->getIPAddress();
			admission = this->serverMgr->shouldAcceptClient(addr, this->userData.getUserName());
		}

		this->getLogger().info().format("Receieved TCP JOIN_REQUEST from %s (%s)",
										this->userData.getUserName().c_str(),
										(admission ? "accepted" : "rejected"));
		if (admission)
		{
			uint16_t outPort = (uint16_t)udpPort;
			uint16_t inPort = 0;  // Let the OS decide a free port

			this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIP(),
											outPort, inPort);
			if (!this->udpSocket->initSocket() || !this->udpSocket->isGood())
			{
				admission = false;
			}
		}

		return this->sendTcpResponse(admission);
	}

	return true;
}

bool AcceptClientConversation::sendTcpResponse(bool response)
{
	Packet pkt;

	ClientId clientId = (g_clientId++);
	this->userData.setClientId(clientId);

	const uint32_t lobbySize = this->serverMgr->getLobbySize();


	pkt << (uint32_t)PacketId::JOIN_RESPONSE;
	pkt << (uint8_t)response;
	pkt << clientId;
	pkt << (response ? (uint32_t)this->udpSocket->getListenPort() : 0U);
	pkt << lobbySize;

	if (!this->tcpSocket->sendPacket(&pkt))
	{
		this->accepted = false;
		this->getLogger().error().raw("Failed to send TCP JOIN_RESPONSE");
		return false;
	}

	this->getLogger().info().raw("Sent TCP JOIN_RESPONSE");

	this->accepted = response;
	return response;
}

bool AcceptClientConversation::handleUdpRequest()
{
	uint32_t id = (uint32_t)PacketId::JOIN_REQUEST;
	Packet *pkt = this->findPacket<uint32_t>(this->udpSocket, id);

	if (pkt)
	{
		this->getLogger().info().raw("Received UDP JOIN_REQUEST");
		this->recvUdpRequest = true;
		delete pkt;
		return this->sendUdpResponse();
	}

	return true;
}

bool AcceptClientConversation::sendUdpResponse()
{
	Packet pkt;

	pkt << (uint32_t)PacketId::JOIN_RESPONSE;

	if (!this->udpSocket->sendPacket(&pkt))
	{
		this->accepted = false;
		this->getLogger().error().raw("Failed to send UDP JOIN_RESPONSE");
		return false;
	}

	this->getLogger().info().raw("Sent UDP JOIN_RESPONSE");
	this->done = true;
	return true;
}


}
} } }
