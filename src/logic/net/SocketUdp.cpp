/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/SocketUdp.h>
#include <nox/logic/net/Packet.h>

#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>


namespace nox { namespace logic { namespace net {


SocketUdp::SocketUdp(logic::ILogicContext *ctx, Uint32 remHost, Uint16 remPort, Uint16 listPort)
	:   SocketBase(ctx, remHost, remPort),
		socket(NULL) ,
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUdp::SocketUdp(logic::ILogicContext *ctx, std::string hostname, Uint16 port, Uint16 listPort)
	:   SocketBase(ctx, hostname, port),
		socket(NULL),
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUdp::SocketUdp(logic::ILogicContext *ctx, IPaddress ipaddr, Uint16 listPort)
	:   SocketBase(ctx, ipaddr),
		socket(NULL),
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUdp::~SocketUdp()
{
	cleanup();
	SDLNet_FreePacket(this->bufferPacket);
}

bool SocketUdp::initSocket()
{
	IPaddress *sockip = NULL;

	if (isInitialized())
	{
		return true;
	}

	this->socket = SDLNet_UDP_Open(this->listenPort);
	if (!this->socket)
	{
		this->getLogger().error().format("SocketUdp: Failed to bind UDP port %hu (%s)",
										 this->listenPort, SDLNet_GetError());
		cleanup();
		return false;
	}

	// Bind the remote host on channel 0 if this is not a server socket
	if (!isServerSocket())
	{
		IPaddress ipaddr = getIPAddress();
		int chan = SDLNet_UDP_Bind(this->socket, 0, &ipaddr);
		if (chan == -1)
		{
			this->getLogger().error().format("SocketUdp: Unable to bind UDP "
											 "socket on channel 0: %s",
											 SDLNet_GetError());
			cleanup();
			return false;
		}
	}

	// Get the port bound on the local machine
	sockip = SDLNet_UDP_GetPeerAddress(this->socket, -1);
	Uint16 boundPort = SDLNet_Read16(&sockip->port);
	if (this->listenPort && boundPort != this->listenPort)
	{
		this->getLogger().warning().format("SocketUdp: Explicitly requested "
										   "port %hu, bound on %hu",
										   this->listenPort, boundPort);
	}

	this->listenPort = boundPort;

	this->getLogger().debug().format("Created UDP socket with remote host "
									 "%s:%hu, listen on %hu",
									 getOctalIP().c_str(), getPort(), this->listenPort);

	if (!onInitSuccess())
	{
		cleanup();
		return false;
	}

	return true;
}

TLProtocol SocketUdp::getTransportLayerProtocol() const
{
	return TLProtocol::UDP;
}

Uint16 SocketUdp::getListenPort() const
{
	return this->listenPort;
}

SDLNet_GenericSocket SocketUdp::getGenericSocket() const
{
	return (SDLNet_GenericSocket) this->socket;
}

uint8_t* SocketUdp::getSocketData(int *bufferLen, IPaddress &source)
{
	*bufferLen = 0;
	uint8_t* buf = NULL;
	int status = 0;

	status = SDLNet_UDP_Recv(this->socket, this->bufferPacket);
	if (status == 1)
	{
		source = this->bufferPacket->address;

		if (this->bufferPacket->len >= this->bufferPacket->maxlen)
		{
			this->getLogger().error().format("Received packet exceeding "
				 "UDP_MAX_PKT_LEN (%u). Errors are very likely to occur!",
				 UDP_MAX_PKT_LEN);
		}

		buf = new uint8_t[this->bufferPacket->len];
		memcpy(buf, this->bufferPacket->data, (size_t)this->bufferPacket->len);
		*bufferLen = this->bufferPacket->len;
	}
	else if (status == 0)
	{
		// No data received
		return NULL;
	}
	else if (status == -1)
	{
		// Errors occurred when reading
		this->getLogger().error().format("Error in SDLNet_UDP_Recv: %s",
						 				SDLNet_GetError());
		return NULL;
	}

	return buf;
}

bool SocketUdp::sendBuffer(const uint8_t* buffer, int bufferLen) const
{
	/* Ignore the packet if it exceeds UDP_MAX_PKT_LEN */
	if (bufferLen >= UDP_MAX_PKT_LEN)
	{
		this->getLogger().fatal().format("Attempted to send %d bytes in a "
			"single packet. UDP is forbidden from sending packets "
			"exceeding %d bytes.", bufferLen, UDP_MAX_PKT_LEN);
		return false;
	}

	uint8_t*  tmp = new uint8_t[bufferLen];
	memcpy(tmp, buffer, (size_t)bufferLen);

	UDPpacket pkt;
	pkt.data = tmp;
	pkt.len = bufferLen;
	pkt.address = getIPAddress();
	pkt.channel = 0;

	if (SDLNet_UDP_Send(this->socket, pkt.channel, &pkt) == 0)
	{
		this->getLogger().error().format("Failed to send UDP packet: %s",
				SDLNet_GetError());
		delete [] tmp;
		return false;
	}

	delete [] tmp;

	return true;
}

void SocketUdp::cleanup()
{
	if (this->socket)
	{
		SDLNet_UDP_Close(this->socket);
		this->socket = NULL;
	}
}

} } }
