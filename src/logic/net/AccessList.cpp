/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/AccessList.h>
#include <nox/logic/net/protocol.h>
#include <algorithm>
#include <ctime>


namespace nox { namespace logic { namespace net {


AccessList::AccessList(std::string file, AccessFilter filter):
	AccessList(nullptr, file, filter)
{
}

AccessList::AccessList(logic::ILogicContext* context, std::string file, AccessFilter filter):
	fileName(file),
	filterType(filter),
	defaultAccess(AccessType::ALWAYS_ALLOW)
{
	if (context != nullptr)
	{
		this->log = context->createLogger();
		this->log.setName("AccessList");
	}

	this->loadList();
}

AccessList::~AccessList()
{
	if (!this->saveList())
	{
		this->log.error().raw("Unable to save INI-file");
	}
}


void AccessList::setDefaultAccess(AccessType access)
{
	if (access == AccessType::UNDEFINED)
	{
		return;
	}

	this->defaultAccess = access;
}

AccessType AccessList::getDefaultAccess() const
{
	return this->defaultAccess;
}


AccessType AccessList::getAccessType(IPaddress ipaddr, std::string userName)
{
	AccessType ipType = this->defaultAccess;
	AccessType unType = this->defaultAccess;

	if (this->filterType & AccessFilter::FILTER_IP)
	{
		if (this->ipAccess.count(ipaddr.host))
		{
			AccessRules rules = this->ipAccess[ipaddr.host];
			ipType = this->getAccessTypeForRules(rules);
			if (ipType == AccessType::UNDEFINED)
			{
				this->ipAccess.erase(ipaddr.host);
				ipType = this->defaultAccess;
			}
		}
	}
	else if (this->filterType & AccessFilter::FILTER_UNAME)
	{
		if (this->unameAccess.count(userName))
		{
			AccessRules rules = this->unameAccess[userName];
			unType = this->getAccessTypeForRules(rules);
			if (unType == AccessType::UNDEFINED)
			{
				this->unameAccess.erase(userName);
				unType = this->defaultAccess;
			}
		}
	}

	if (this->filterType == AccessFilter::FILTER_IP)
	{
		return ipType;
	}
	else if (this->filterType == AccessFilter::FILTER_UNAME)
	{
		return unType;
	}

	// Return the strictest (highest) value of the two
	int iIp = (int)ipType;
	int iUn = (int)unType;

	return static_cast<AccessType>(std::max(iIp, iUn));
}


void AccessList::setAccess(IPaddress ipaddr, std::string userName, AccessType access)
{
	this->setAccess(ipaddr, access);
	this->setAccess(userName, access);
}

void AccessList::setAccess(std::string userName, AccessType access)
{
	this->unameAccess[userName].accessType = access;

	if (access == AccessType::ALWAYS_ALLOW)
	{
		this->unameAccess[userName].banLift = 1;
	}
	else
	{
		this->unameAccess[userName].banLift = 0;
	}
}

void AccessList::setAccess(IPaddress ipaddr, AccessType access)
{
	this->ipAccess[ipaddr.host].accessType = access;

	if (access == AccessType::ALWAYS_ALLOW)
	{
		this->ipAccess[ipaddr.host].banLift = 1;
	}
	else
	{
		this->ipAccess[ipaddr.host].banLift = 0;
	}
}


void AccessList::banWithDuration(IPaddress ipaddr, std::string userName, const Duration &duration)
{
	this->banWithDuration(ipaddr, duration);
	this->banWithDuration(userName, duration);
}

void AccessList::banWithDuration(std::string userName, const Duration &duration)
{
	std::time_t now = std::time(nullptr);
	std::chrono::seconds secs = std::chrono::duration_cast<std::chrono::seconds>(duration);
	std::time_t liftTime = now + secs.count();

	this->unameAccess[userName].accessType = AccessType::ALWAYS_DENY;
	this->unameAccess[userName].banLift = (unsigned long)liftTime;
}

void AccessList::banWithDuration(IPaddress ipaddr, const Duration &duration)
{
	std::time_t now = std::time(nullptr);
	std::chrono::seconds secs = std::chrono::duration_cast<std::chrono::seconds>(duration);
	std::time_t liftTime = now + secs.count();

	this->ipAccess[ipaddr.host].accessType = AccessType::ALWAYS_DENY;
	this->ipAccess[ipaddr.host].banLift = (unsigned long)liftTime;
}


bool AccessList::loadList()
{
	this->ipAccess.clear();
	this->unameAccess.clear();

	if (!this->iniFile.replaceParse(this->fileName))
	{
		this->log.error().raw("Unable to load INI-file");

		while (this->iniFile.hasError())
		{
			this->log.error().raw("INI Error: " + this->iniFile.getError());
		}

		this->log.error().raw("Run-time modifications will still apply.");
		return false;
	}

	this->parseSection(AccessFilter::FILTER_IP);
	this->parseSection(AccessFilter::FILTER_UNAME);

	return true;
}

bool AccessList::saveList()
{
	// this->iniFile may be out of date, in case something has changed
	// since loading it initially. Create a new IniFile instance and save
	// it over the existing file.
	util::IniFile ini;

	for (auto &pair : this->ipAccess)
	{
		IPaddress ipaddr { pair.first, 0U };
		std::string octal = octalIpAddress(ipaddr);

		unsigned long val;
		if (pair.second.accessType == AccessType::ALWAYS_ALLOW)
			val = 1;
		else
			val = pair.second.banLift;

		ini.getSection("IP")->setLong(octal, (long)val);
	}

	for (auto &pair : this->unameAccess)
	{
		unsigned long val;
		if (pair.second.accessType == AccessType::ALWAYS_ALLOW)
			val = 1;
		else
			val = pair.second.banLift;

		ini.getSection("IP")->setLong(pair.first, (long)val);
	}

	return ini.writeToFile(this->fileName);
}


void AccessList::parseSection(AccessFilter filter)
{
	std::string secName;
	if (filter == AccessFilter::FILTER_IP)
	{
		secName = "IP";
	}
	else if (filter == AccessFilter::FILTER_UNAME)
	{
		secName = "UserName";
	}
	else
	{
		return;
	}

	const util::IniFile::Section *ipSection = this->iniFile.getSection(secName);
	auto &values = ipSection->getValues();

	for (auto &pair : values)
	{
		Uint32 ipaddr = 0;

		if (filter == AccessFilter::FILTER_IP)
		{
			// Convert to Network Byte Order
			ipaddr = octalToIp(pair.first, true);
			if (ipaddr == 0)
			{
				continue;
			}
		}
		else
		{
			if (pair.first.empty())
			{
				continue;
			}
		}

		AccessRules access;
		unsigned long value = strtoul(pair.second.c_str(), nullptr, 0);

		if (value == 1)
		{
			access.accessType = AccessType::ALWAYS_ALLOW;
			access.banLift = 0;
		}
		else
		{
			access.accessType = AccessType::ALWAYS_DENY;
			access.banLift = value;
		}

		if (filter == AccessFilter::FILTER_IP)
		{
			this->ipAccess[ipaddr] = access;
		}
		else
		{
			this->unameAccess[pair.first] = access;
		}
	}
}

AccessType AccessList::getAccessTypeForRules(AccessRules &rules)
{
	if (rules.accessType == AccessType::ALWAYS_DENY)
	{
		if (rules.banLift > 1)
		{
			std::time_t now = std::time(nullptr);
			if (now >= (time_t)rules.banLift)
			{
				// Lift the ban
				return AccessType::UNDEFINED;
			}
		}
	}

	return rules.accessType;
}


} } }
