/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/UserData.h>
#include <nox/common/platform.h>

#include <sstream>
#include <iomanip>

#ifdef NOX_OS_LINUX
	#include <sys/types.h>
	#include <unistd.h>
#elif defined(NOX_OS_APPLE)
	#include <unistd.h>
#elif defined(NOX_OS_WINDOWS)
	#include <process.h>

	int getpid(void)
	{
		return _getpid();
	}
#else
	#error "No OS is defined - unable to determine getpid routine."
#endif


namespace nox { namespace logic { namespace net {


UserData::UserData():
	userName("Unkown Player")
{
}

UserData::UserData(const UserData& other)
{
	this->userName = other.userName;
	this->userHash = other.userHash;
	this->clientId = other.clientId;
}


void UserData::setUserName(std::string userName)
{
	this->userName = userName;
}

const std::string& UserData::getUserName() const
{
	return this->userName;
}


void UserData::generateUserHash()
{
	int pid = (int)getpid();
	auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
	int64_t timeCount = now.count();

	std::size_t hPid = std::hash<int32_t>()(pid);
	std::size_t hTime = std::hash<int64_t>()(timeCount);
	std::size_t hash = hPid ^ (hTime << 1);

	std::stringstream ss;
	ss << std::hex << std::setfill('0');
	ss << hash;
	this->userHash = ss.str();
}

const std::string& UserData::getUserHash() const
{
	return this->userHash;
}


void UserData::setClientId(ClientId id)
{
	this->clientId = id;
}

ClientId UserData::getClientId() const
{
	return this->clientId;
}


void UserData::serialize(Packet *packet) const
{
	(*packet) << this->userName << this->userHash << this->clientId;
}

bool UserData::deSerialize(const Packet *packet)
{
	try
	{
		(*packet) >> this->userName >> this->userHash >> this->clientId;
	}
	catch (...)
	{
		return false;
	}

	return true;
}



} } }
