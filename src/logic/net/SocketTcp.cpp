/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/SocketTcp.h>
#include <nox/logic/net/Packet.h>

#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>


namespace nox { namespace logic { namespace net {

SocketTcp::SocketTcp(logic::ILogicContext *ctx, Uint32 remHost, Uint16 remPort)
	:   SocketBase(ctx, remHost, remPort),
		socket(NULL)
{

}

SocketTcp::SocketTcp(logic::ILogicContext *ctx, std::string hostname, Uint16 remPort)
	:   SocketBase(ctx, hostname, remPort),
		socket(NULL)
{

}

SocketTcp::SocketTcp(logic::ILogicContext *ctx, TCPsocket sock)
	:   SocketBase(ctx, *SDLNet_TCP_GetPeerAddress(sock)),
		socket(sock)
{

}

SocketTcp::SocketTcp(logic::ILogicContext *ctx, IPaddress ipaddr)
	:   SocketBase(ctx, ipaddr),
		socket(NULL)
{

}

SocketTcp::~SocketTcp()
{
	cleanup();
}

bool SocketTcp::initSocket()
{
	if (isInitialized())
	{
		return true;
	}

	if (!validIPAddress())
	{
		return false;
	}

	if (!this->socket)
	{
		// The connection has NOT been previously established,
		// connect to the remote host (or bind port in case of server)
		IPaddress ipaddr = getIPAddress();
		this->socket = SDLNet_TCP_Open(&ipaddr);
		if (!this->socket)
		{
			this->getLogger().debug().format("Unable to initialize TCP socket (%s:%hu): \"%s\"",
											 getOctalIP().c_str(),
											 getPort(),
											 SDLNet_GetError());
			cleanup();
			return false;
		}
	}

	if (!onInitSuccess())
	{
		cleanup();
		return false;
	}

	return true;
}

TLProtocol SocketTcp::getTransportLayerProtocol() const
{
	return TLProtocol::TCP;
}

SDLNet_GenericSocket SocketTcp::getGenericSocket() const
{
	return (SDLNet_GenericSocket) this->socket;
}

uint8_t* SocketTcp::getSocketData(int *bufferLen, IPaddress &source)
{
	*bufferLen = 0;
	uint8_t* buf = NULL;
	int read = 0;

	// Use the default source
	source.host = 0U;
	source.port = 0U;

	if (isServerSocket())
	{
		this->getLogger().warning().raw("Attempted to get socket data from server-TCP socket");
		return NULL;
	}

	if (SDLNet_SocketReady(this->socket) <= 0)
	{
		return NULL;
	}

	int maxlen = 16;
	int totRead = 0;
	buf = new uint8_t[maxlen];

	read = SDLNet_TCP_Recv(this->socket, buf, maxlen);
	if (read <= 0)
	{
		delete [] buf;
		return NULL;
	}

	totRead = read;
	int readable = maxlen / 2;

	while (read == readable || read == maxlen)
	{
		maxlen *= 2;
		readable *= 2;

		buf = (uint8_t*) realloc(buf, (size_t)maxlen);
		read = SDLNet_TCP_Recv(this->socket, buf + totRead, readable);
		totRead += read;
	}

	*bufferLen = totRead;
	return buf;
}

bool SocketTcp::sendBuffer(const uint8_t* buffer, int bufferLen) const
{
	if (!buffer || !bufferLen)
	{
		return false;
	}

	if (SDLNet_TCP_Send(this->socket, buffer, bufferLen) <= 0)
	{
		this->getLogger().error().format("Failed to send TCP packet: %s", SDLNet_GetError());
		return false;
	}

	return true;
}

void SocketTcp::cleanup()
{
	if (this->socket)
	{
		SDLNet_TCP_Close(this->socket);
		this->socket = NULL;
	}
}

} } }

