/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/Conversation.h>


namespace nox { namespace logic { namespace net {


Conversation::Conversation(SocketBase *sock, logic::ILogicContext *ctx):
	socket(sock),
	context(ctx),
	initLogger(false)
{

}

bool Conversation::start()
{
	this->getLogger().error().raw("Conversation::start not overridden. Are you sure "
							 "you meant to call it in the first place?");

	return false;
}

void Conversation::createLogger(std::string name)
{
	this->logger = this->context->createLogger();
	this->logger.setName(name);
	this->initLogger = true;
}

SocketBase* Conversation::getSocket()
{
	return this->socket;
}


app::log::Logger& Conversation::getLogger()
{
	if (!this->initLogger)
	{
		createLogger("Conversation");
	}

	return this->logger;
}

logic::ILogicContext* Conversation::getContext()
{
	return this->context;
}


} } }
