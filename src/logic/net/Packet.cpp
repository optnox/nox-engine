/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/Packet.h>
#include <nox/logic/net/protocol.h>
#include <nox/common/types.h>

#include <string.h>
#include <sstream>
#include <assert.h>


namespace nox { namespace logic { namespace net {


Packet::Field::Field(FieldType type)
{
	this->type = type;
	memset(&this->value, 0, sizeof(this->value));

	assert(type != FieldType::UNDEFINED);
}

Packet::Field::Field(const Packet::Field &other):
	Packet::Field(other.getType())
{
	this->type = other.type;

	if (this->type == FieldType::STRING)
	{
		set(other.getString());
	}
	else if (this->type == FieldType::ARRAY)
	{
		FieldValue v = other.getValue();
		set(v.array.ptr, v.array.len);
	}
	else if (this->type == FieldType::PACKET)
	{
		FieldValue v = other.getValue();
		set(v.packet.ptr, v.packet.len);
	}
	else
	{
		memcpy(&this->value, &other.value, sizeof(this->value));
	}
}

Packet::Field::~Field()
{
	if (this->type == FieldType::STRING && this->value.str != NULL)
	{
		delete[] this->value.str;
	}
	else if (this->type == FieldType::ARRAY && this->value.array.ptr != NULL)
	{
		delete[] this->value.array.ptr;
	}
	else if (this->type == FieldType::PACKET && this->value.packet.ptr != nullptr)
	{
		delete[] this->value.packet.ptr;
	}
}

bool Packet::Field::set(void *val)
{
	if (!val)
	{
		return false;
	}

	switch (this->type)
	{
		case FieldType::UINT32:
			this->value.u32 = *((uint32_t*)val);
			break;
		case FieldType::INT32:
			this->value.i32 = *((int32_t*)val);
			break;
		case FieldType::UINT64:
			this->value.u64 = *((uint64_t*)val);
			break;
		case FieldType::INT64:
			this->value.i64 = *((int64_t*)val);
			break;
		case FieldType::FLOAT32:
			this->value.f32 = *((float*)val);
			break;
		case FieldType::BYTE:
			this->value.b = *((uint8_t*)val);
			break;
		default:
			//Log::error("Called Packet::Field::set(void*) on Field of "
			//		   "type STRING or ARRAY");
			return false;
	}

	return true;
}

bool Packet::Field::set(glm::vec2 vec)
{
	if (this->type == FieldType::VEC2)
	{
		this->value.vec2.x = vec.x;
		this->value.vec2.y = vec.y;
		return true;
	}

	return false;
}

bool Packet::Field::set(std::string strValue)
{
	if (this->type != FieldType::STRING)
	{
		//Log::error("Called Packet::Field::set(std::string) on Field of "
		//	"non-string type");
		return false;
	}

	if (this->value.str)
	{
		delete[] this->value.str;
	}

	this->value.str = new char[strValue.length() + 1];
	memcpy(this->value.str, strValue.c_str(), strValue.length() + 1);

	return true;
}

bool Packet::Field::set(const uint8_t* buffer, int len)
{
	if (this->type != FieldType::ARRAY && this->type != FieldType::PACKET)
	{
		return false;
	}

	if (len <= 0 || buffer == NULL)
	{
		return false;
	}

	uint8_t*& bufPtr = (this->type == FieldType::ARRAY ? this->value.array.ptr : this->value.packet.ptr);
	int& bufLen = (this->type == FieldType::ARRAY ? this->value.array.len : this->value.packet.len);

	if (bufPtr)
	{
		delete[] bufPtr;
	}

	bufLen = len;
	bufPtr = new uint8_t[len];
	memcpy(bufPtr, buffer, (size_t)len);
	return true;
}

bool Packet::Field::set(const Packet* packet)
{
	if (this->type != FieldType::PACKET)
	{
		return false;
	}

	if (this->value.packet.ptr != nullptr)
	{
		delete[] this->value.packet.ptr;
	}

	this->value.packet.ptr = packet->serialize(this->value.packet.len);
	return true;
}

bool Packet::Field::set(const Packet& packet)
{
	return this->set(&packet);
}

const Packet::Field::FieldValue& Packet::Field::getValue() const
{
	return this->value;
}

FieldType Packet::Field::getType() const
{
	return this->type;
}

std::string Packet::Field::getString() const
{
	if (this->type != FieldType::STRING)
	{
		//Log::warning("Packet::Field::getString() called on non-string field");
		return "";
	}

	if (!this->value.str)
	{
		return "";
	}

	return std::string(this->value.str);
}

std::vector<uint8_t> Packet::Field::getArray() const
{
	std::vector<uint8_t> vec;

	if (this->type != FieldType::ARRAY)
	{
		//Log::warning("Packet::Field::getArray() called on non-array field");
		return vec;
	}

	for (size_t i = 0; i < (size_t)this->value.array.len; i++)
	{
		vec.push_back(this->value.array.ptr[i]);
	}

	return vec;
}

glm::vec2 Packet::Field::getVec2() const
{
	if (this->type == FieldType::VEC2)
	{
		return glm::vec2(this->value.vec2.x, this->value.vec2.y);
	}

	return glm::vec2(0.0f, 0.0f);
}



Packet::Packet():
	outFieldIndex(0),
	creationTime(0)
{
	this->sourceIp.host = 0U;
	this->sourceIp.port = 0;
}

Packet::~Packet()
{

}

uint8_t* Packet::serialize(int &pktlen) const
{
	std::vector<uint8_t> buffer;

	/**
	 * The total length of the packet is defined in the first four bytes of
	 * the packet. To avoid having to calculate the size before the buffer is
	 * filled, allocate the first four bytes and overwrite them once we know
	 * the size. The alternative is to insert the size into the first four
	 * bytes after the buffer is filled, but that will involve a relatively
	 * costly shift of the entire buffer content.
	 */
	for (int i=0; i<4; i++)
		buffer.push_back(0xFF);

	std::vector<Field> allFields;

	Field creationField(FieldType::INT64);
	creationField.set((void*)&this->creationTime);
	allFields.push_back(creationField);

	allFields.insert(allFields.end(), this->fields.begin(), this->fields.end());

	for (auto field : allFields)
	{
		// Add the field header - the type of the following item
		buffer.push_back(static_cast<uint8_t>(field.getType()));

		switch (field.getType())
		{
			case FieldType::UINT32:
				/* FALLTHROUGH */

			case FieldType::INT32:
				/* FALLTHROUGH */

			case FieldType::FLOAT32:
			{

				Packet::Field::FieldValue fval = field.getValue();

				/**
				 * Assert that the union uses the same memory address for
				 * all 32 bit fields, otherwise using a single fallthrough for
				 * all 32 bit fields is impossible
				 */
				assert((void*) &fval.u32 == (void*) &fval.i32
					&& (void*) &fval.u32 == (void*) &fval.f32);

				uint32_t u32 = fval.u32;
				void *vptr = (void*) &u32;
				uint32_t netdw = networkDword(vptr);
				uint8_t *bytes = (uint8_t*) &netdw;

				for (int i = 0; i < 4; i++)
				{
					buffer.push_back(bytes[i]);
				}
				break;
			}

			case FieldType::UINT64:
				/* FALLTHROUGH */

			case FieldType::INT64:
			{
				Packet::Field::FieldValue fval = field.getValue();

				/**
				 * Assert that the union uses the same memory address for
				 * all 64 bit fields.
				 */
				assert((void*) &fval.u64 == (void*) &fval.i64);

				uint64_t netqw = networkQword((void*)&fval.u64);
				uint8_t* bytes = (uint8_t*)&netqw;

				for (int j=0; j<8; j++)
				{
					buffer.push_back(bytes[j]);
				}

				break;
			}

			case FieldType::BYTE:
			{
				buffer.push_back(field.getValue().b);
				break;
			}

			case FieldType::STRING:
			{
				std::string str = field.getString();
				for (unsigned i = 0; i < str.length(); i++)
				{
					buffer.push_back((uint8_t)str[i]);
				}

				buffer.push_back(0);
				break;
			}

			case FieldType::ARRAY:
			{
				/* The first four bytes of a serialized binary array is the
				 * 32 bit unsigned representation of the length in network
				 * byte order.
				 */
				unsigned l = (unsigned)field.getValue().array.len;
				uint8_t* p = field.getValue().array.ptr;

				unsigned netdw = networkDword(&l);
				uint8_t* netdwptr = (uint8_t*) &netdw;
				for (int i = 0; i < 4; i++)
				{
					buffer.push_back(netdwptr[i]);
				}

				for (unsigned i = 0; i < l; i++)
				{
					buffer.push_back(p[i]);
				}
				break;
			}

			case FieldType::VEC2:
			{
				uint8_t buf[8];

				Packet::Field::FieldValue fval = field.getValue();

				// Cast the X and Y addresses to (unsigned*) and dereference them to interpret
				// the byte values as "native unsigned".
				// This avoids casting the float values to unsigned ones (resulting in loss of
				// precision), while also avoiding usage of extensive copies.
				SDLNet_Write32(*(uint32_t*)&fval.vec2.x, buf);
				SDLNet_Write32(*(uint32_t*)&fval.vec2.y, buf + 4);

				for (int i = 0; i < 8; i++)
				{
					buffer.push_back(buf[i]);
				}

				break;
			}

			case FieldType::PACKET:
			{
				uint8_t* ptr = field.getValue().packet.ptr;
				int len = field.getValue().packet.len;

				for (int i=0; i<len; i++)
				{
					buffer.push_back(ptr[i]);
				}
			}

			case FieldType::UNDEFINED:
			{
				break;
			}
		}
	}

	// The buffer will always contain at least 4 bytes. Do not count these when
	// deciding whether or not we filled the packet with something useful.
	pktlen = (int)buffer.size() - 4;
	if (pktlen < 0)
		return NULL;

	// Replace the allocated header with the length of the packet minus the
	// header itself.
	uint32_t nwdw = networkDword((void*)&pktlen);
	uint8_t* vp = (uint8_t*)&nwdw;
	for (unsigned int i=0; i<4; i++)
		buffer[i] = vp[i];

	// The external caller is interested in the total size of the buffer.
	// Re-include the packet header.
	pktlen += 4;

	uint8_t* b = new uint8_t[pktlen];
	for (unsigned i = 0; i < buffer.size(); i++)
		b[i] = buffer[i];

	return b;
}

int Packet::deserialize(const uint8_t* buf, size_t buflen)
{
	// Erase all existing contents.
	if (fields.size())
		fields.clear();

	int read = 0;

	const uint32_t pktlen = readUint(buf) + 4;
	read += (int)sizeof(pktlen);

	if (pktlen > buflen || buflen < 4)
	{
		// TODO
		// Log a warning indicating that the given buffer is smaller than
		// what it claims to be. Also figure out a way to handle this error
		// properly. Can any of the remaining buffer content be trusted?
		// Probably not. How do we notify the caller properly?
		return 0;
	}

	int readFields = 0;
	while ((uint32_t)read < pktlen)
	{
		FieldType fieldType = static_cast<FieldType>(buf[read++]);
		Field field(fieldType);

		switch (fieldType)
		{
			case FieldType::UINT32:
			{
				unsigned val = readUint(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::INT32:
			{
				int val = readInt(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::UINT64:
			{
				uint64_t val = readLUint(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::INT64:
			{
				int64_t val = readLInt(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::FLOAT32:
			{
				float val = readFloat(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::BYTE:
			{
				uint8_t val = buf[read];
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::STRING:
			{
				char *str = (char*) (buf + read);
				int len = volatileStrlen(str, (int)pktlen - read);
				if (len == -1)
				{
					//Log::error("Error in Packet::deserialize(): "
					//	"String field '%s' is not null-terminated!",
					//	field->getName().cthis->str());
				}
				else
				{
					std::string cppstr(str);
					field.set(cppstr);
					read += (len + 1);
				}
				break;
			}

			case FieldType::ARRAY:
			{
				/* The first four bytes is the 32 bit unsigned representation
				 * of the length of the following array.
				 */
				uint32_t len = readUint(buf + read);
				field.set(buf + read + sizeof(len), (int)len);

				read += (int)sizeof(len);
				read += (int)len;
				break;
			}

			case FieldType::VEC2:
			{
				glm::vec2 vec2;

				vec2.x = readFloat(buf + read);
				read += (int)sizeof(vec2.x);

				vec2.y = readFloat(buf + read);
				read += (int)sizeof(vec2.y);

				field.set(vec2);
				break;
			}

			case FieldType::PACKET:
			{
				// The first four bytes is the length of the packet, excluding the length header itself.
				int subPktLen = readInt(buf + read) + 4;
				assert(subPktLen + read <= (int)pktlen);

				uint8_t* ptr = new uint8_t[subPktLen];
				memcpy(ptr, buf+read, (size_t)subPktLen);

				field.set(ptr, subPktLen);
				read += subPktLen;
			}


			case FieldType::UNDEFINED:
				/* FALLTHROUGH */
			default:
			{
				//Log::error("Packet::deserialize(): Unable to deserialize into "
				//	"field with undefined type");
				break;
			}
		}

		if (readFields == 0)
		{
			assert(field.getType() == FieldType::INT64);
			this->creationTime = field.getValue().i64;
		}
		else
		{
			fields.push_back(field);
		}

		readFields++;
	}

	return (int)read;
}


const Packet& Packet::operator>>(uint8_t& val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::BYTE);
	val = field->getValue().b;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(uint32_t& val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::UINT32);
	val = field->getValue().u32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(int &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::INT32);
	val = field->getValue().i32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(uint64_t& val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::UINT64);
	val = field->getValue().u64;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(int64_t &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::INT64);
	val = field->getValue().i64;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(float &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::FLOAT32);
	val = field->getValue().f32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(std::string &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::STRING);
	val = field->getString();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(std::vector<uint8_t> &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::ARRAY);
	val = field->getArray();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(glm::vec2 &val) const
{
	const Packet::Field *field = this->verifyField(this->outFieldIndex, FieldType::VEC2);
	val = field->getVec2();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(Packet &val) const
{
	const Packet::Field *field = this->verifyField(this->outFieldIndex, FieldType::PACKET);

	const uint8_t* ptr = field->getValue().packet.ptr;
	int len = field->getValue().packet.len;

	val.deserialize(ptr, (size_t)len);

	this->outFieldIndex++;
	return *this;
}


Packet& Packet::operator<<(uint8_t val)
{
	Packet::Field field(FieldType::BYTE);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(uint32_t val)
{
	Packet::Field field(FieldType::UINT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(int val)
{
	Packet::Field field(FieldType::INT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(uint64_t val)
{
	Packet::Field field(FieldType::UINT64);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(int64_t val)
{
	Packet::Field field(FieldType::INT64);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(float val)
{
	Packet::Field field(FieldType::FLOAT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(std::string val)
{
	Packet::Field field(FieldType::STRING);
	field.set(val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(std::vector<uint8_t> val)
{
	Packet::Field field(FieldType::ARRAY);
	field.set(&val[0], (int)val.size());
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(glm::vec2 val)
{
	Packet::Field field(FieldType::VEC2);
	field.set(val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(const Packet& val)
{
	Packet::Field field(FieldType::PACKET);
	field.set(val);
	this->fields.push_back(field);
	return *this;
}


void Packet::appendFields(const Packet& packet)
{
   for (unsigned i=0; i<packet.getFieldCount(); i++)
   {
	   const Packet::Field *field = packet.getField(i);
	   this->fields.push_back(*field);
   }
}


unsigned Packet::getFieldCount() const
{
	return (unsigned)this->fields.size();
}

const Packet::Field* Packet::getField(unsigned index) const
{
	return &this->fields[(size_t)index];
}


void Packet::resetStream()
{
	this->outFieldIndex = 0;
}

unsigned Packet::getOutStreamPosition() const
{
	return this->outFieldIndex;
}

bool Packet::setOutStreamPosition(unsigned position) const
{
	if (position >= this->fields.size())
	{
		return false;
	}

	this->outFieldIndex = position;
	return true;
}



IPaddress Packet::getSourceAddress() const
{
	return this->sourceIp;
}

void Packet::setSourceAddress(IPaddress addr)
{
	this->sourceIp = addr;
}


void Packet::logContents() const
{
	std::stringstream ss;
	ss <<"Packet: [creation: " << this->creationTime << "] { ";

	bool first = true;

	for (auto &field : this->fields)
	{
		if (!first)
			ss << ", ";
		first = false;

		ss << "'" << fieldTypeToStr(field.getType()) << "': '";

		switch (field.getType())
		{
			case FieldType::INT32:
				ss << field.getValue().i32;
				break;
			case FieldType::UINT32:
				ss << field.getValue().u32;
				break;
			case FieldType::INT64:
				ss << field.getValue().i64;
				break;
			case FieldType::UINT64:
				ss << field.getValue().u64;
				break;
			case FieldType::FLOAT32:
				ss << field.getValue().f32;
				break;
			case FieldType::BYTE:
				ss << field.getValue().b;
				break;
			case FieldType::STRING:
				ss << field.getString();
				break;
			case FieldType::ARRAY:
				ss << "[array, " << field.getValue().array.len << " bytes]";
				break;
			case FieldType::PACKET:
				ss << "[subpacket, " << field.getValue().packet.len << " bytes]";
				break;
			case FieldType::VEC2:
			{
				Field::WrapVec2 v = field.getValue().vec2;
				ss << "[vec2 (" << v.x << ", " << v.y << ")]";
				break;
			}
			case FieldType::UNDEFINED:
				ss << "[undefined]";
				break;
		}

		ss << "'";
	}

	ss << " }";

	printf("%s\n", ss.str().c_str());
}


void Packet::setCreationTime(int64_t time) const
{
	this->creationTime = time;
}

int64_t Packet::getCreationTime() const
{
	return this->creationTime;
}

void Packet::setEstimatedAge(const Duration& age)
{
	this->estimatedAge = age;
}

const Duration& Packet::getEstimatedAge() const
{
	return this->estimatedAge;
}


const Packet::Field* Packet::verifyField(unsigned fieldIndex,
										 FieldType expectedType) const
{
	if (fieldIndex >= this->fields.size())
	{
		throw std::runtime_error("Packet >> error: Expected to retrieve "
				"value of type " + fieldTypeToStr(expectedType) + ", but the "
				"Packet has no additional fields.");
	}

	const Packet::Field *field = &this->fields[fieldIndex];

	if (field->getType() != expectedType)
	{
		throw std::runtime_error("Packet >> error: Expected to retrieve "
				"value of type " + fieldTypeToStr(expectedType) + ", but the "
				"next value is of type " +
				fieldTypeToStr(field->getType()));
	}

	return field;
}

} } }

