/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/RemoteClient.h>
#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/ServerDelegate.h>
#include <nox/logic/net/ping_convo.h>

#include <nox/logic/actor/Actor.h>


namespace nox { namespace logic { namespace net {

const Duration RemoteClient::HEARTBEAT_MIN_WAIT = Duration(std::chrono::seconds(1));

RemoteClient::RemoteClient(NetworkManager* netMgr, nox::logic::ILogicContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp):
	CommunicationController(netMgr, context, user, tcp, udp),
	heartbeatSuccessiveFails(0)
{
	this->getLogger().setName("RemoteClient");

	this->addPacketHandler(protocol::PacketId::EVENT_PACKET,
		[&](const Packet& packet)
		{
			this->onNetworkEvent(packet);
		});
}


bool RemoteClient::update(const Duration& deltaTime)
{
	if (!CommunicationController::update(deltaTime))
	{
		return false;
	}

	if (this->heartbeatTimer.getElapsedTime() > RemoteClient::HEARTBEAT_MIN_WAIT)
	{
		protocol::PingInitateConversation *convo;
		convo = new protocol::PingInitateConversation(
				this->getSocketUdp(), this->getContext(),
				this->getNetworkManager(), this->getUserData().getClientId());

		if (!convo->start())
		{
			this->getLogger().debug().raw("Failed to start heartbeat");
			this->onConversationFailed(convo);
			delete convo;
			return false;
		}
		else
		{
			this->getLogger().debug().raw("Started heartbeat");
			this->addConversation(convo);
			this->heartbeatRunning = true;
			this->heartbeatTimer.start();
		}
	}

	return true;
}

void RemoteClient::setServerDelegate(ServerDelegate* serverDelegate)
{
	this->serverDelegate = serverDelegate;
}


void RemoteClient::onActorCreated(logic::actor::Actor* actor)
{
	/**
	 * As RemoteClients - unlike CommunicationController - is only responsible
	 * for dealing with Actors owned by the remote client, do nothing if  the
	 * Actor is owned by someone else.
	 */
	if (actor->getOwnerClientId() != this->getUserData().getClientId())
	{
		return;
	}

	/**
	 * RemoteClients are ONLY used on the server. Remotely owned synchronized actors
	 * only have a SyncIn-component attached to them by default. However, in order to
	 * synchronize the states to the non-owning clients, the actors must be given a
	 * SyncOut component as well.
	 */
	if (actor->isSynchronized() && !actor->isOwnedLocally() && actor->findComponent<SyncOut>() == nullptr)
	{
		std::unique_ptr<logic::actor::Component> syncOut = std::make_unique<SyncOut>();
		assert(actor->addComponent(syncOut) == true);
	}

	CommunicationController::onActorCreated(actor);
}

void RemoteClient::onNetworkEvent(const Packet& packet)
{
	bool defaultTranslated = false;
	auto event = this->translatePacket(packet, defaultTranslated);

	if (event != nullptr)
	{
		if (this->serverDelegate == nullptr)
		{
			this->getLogger().error().raw("Unable to handle incoming packets: no ServerDelegate");
			return;
		}

		this->getLogger().debug().format("Received network event '%s' from client '%u'",
				event->getType().c_str(), this->getUserData().getClientId());

		if (defaultTranslated)
		{
			// The default-translated events are not supposed to be sent from the
			// clients - ever.
			this->getLogger().error().format("Received system event '%s' from client '%s'.",
					event->getType().c_str(), this->getUserData().getUserName().c_str());
		}
		else
		{
			this->serverDelegate->onClientNetworkEvent(this->getUserData().getClientId(), event);
		}
	}
	else
	{
		this->getLogger().error().raw("Unable to handle incoming packet: empty packet.");
	}
}


void RemoteClient::onConversationFailed(Conversation *convo)
{
	if (convo->getName() == protocol::PingInitateConversation::NAME)
	{
		this->heartbeatSuccessiveFails++;
		this->heartbeatRunning = false;

		if (this->heartbeatSuccessiveFails >= RemoteClient::MAX_SUCCESSIVE_HEARTBEAT_FAILS)
		{
			// Forcefully disconnect the connections. The sockets will be put in a bad state, and the
			// ServerDelegate will dispatch of this connection appropriately.
			this->getLogger().warning().format("Disconnecting client %u %s due to %i successive heartbeat failures",
					this->heartbeatSuccessiveFails, this->getUserData().getUserName().c_str(), this->getUserData().getClientId());
			this->getSocketUdp()->disconnect();
			this->getSocketTcp()->disconnect();
		}
	}
}

void RemoteClient::onConversationCompleted(Conversation *convo)
{
	if (convo->getName() == protocol::PingInitateConversation::NAME)
	{
		protocol::PingInitateConversation *pingConvo = (protocol::PingInitateConversation*)convo;
		if (pingConvo->wasSuccessful())
		{
			this->heartbeatSuccessiveFails = 0;
			this->heartbeatRunning = false;
		}
		else
		{
			this->onConversationFailed(convo);
		}
	}
}

} } }
