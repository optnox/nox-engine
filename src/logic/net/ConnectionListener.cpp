/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/ConnectionListener.h>

#include <assert.h>
#include <chrono>


namespace nox { namespace logic { namespace net {


/**
 * The listening thread is spawned in this function. Pardon all the parameters.
 * The method pointers are required, as the callback methods are private and
 * should not be exposed to the public.
 */
static void tcpLoop(bool *abort, 
					Uint16 listenPort,
					ConnectionListener *listener,
					ConnectionListener::NewConnectionCallback connCb,
					ConnectionListener::ErrorCallback errCb) {
	// Set up a listening socket on port TCP_SERVER_PORT
	IPaddress ip;
	TCPsocket server_socket;

	if (SDLNet_ResolveHost(&ip, NULL, listenPort) == -1) {
		(*listener.*errCb)(-1);
		return;
	}

	server_socket = SDLNet_TCP_Open(&ip);
	if (!server_socket) {
		(*listener.*errCb)(-2);
		return;
	}

	while (!(*abort)) {
		TCPsocket new_socket = SDLNet_TCP_Accept(server_socket);

		if (new_socket) {
			(*listener.*connCb)(new_socket);
		} else {
			SDLNet_TCP_Close(new_socket);
		}
		
		// Sleep, no need to fry the CPU for this 
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	SDLNet_TCP_Close(server_socket);
}



ConnectionListener::ConnectionListener(Uint16 listPort):   
	listenPort(listPort),
	abortThreads(false),
	threadError(0)
{
	this->thread = new std::thread(tcpLoop, 
				  &this->abortThreads, 
				  this->listenPort,
				  this, 
				  &ConnectionListener::addNewSocket,
				  &ConnectionListener::setError);
}

ConnectionListener::~ConnectionListener() {
	this->abortThreads = true;
	this->thread->join();
	delete this->thread;

	for (TCPsocket sock : this->newConnections) {
		SDLNet_TCP_Close(sock);
	}
}

bool ConnectionListener::isGood() {
	bool good;

	this->mutex.lock();
	good = (this->threadError == 0);
	this->mutex.unlock();

	return good;
}

bool ConnectionListener::hasNewConnection() {
	this->mutex.lock();
	size_t count = this->newConnections.size();
	this->mutex.unlock();

	return (count > 0);
}

SocketTcp* ConnectionListener::popNewConnection(logic::ILogicContext *context) {
	TCPsocket socket = 0;

	this->mutex.lock();

	if (this->newConnections.size() > 0) {
		socket = this->newConnections[0];
		this->newConnections.erase(this->newConnections.begin());
	}

	this->mutex.unlock();

	if (socket == 0) {
		return nullptr;
	}

	SocketTcp *tcp = new SocketTcp(context, socket);
	if (!tcp->initSocket() || !tcp->isGood()) {
		delete tcp;
		return nullptr;
	}

	return tcp;
}

Uint16 ConnectionListener::getListenPort() const
{
	return this->listenPort;
}


void ConnectionListener::addNewSocket(TCPsocket socket) {
	if (socket != 0) {
		this->mutex.lock();
		this->newConnections.push_back(socket);
		this->mutex.unlock();
	}
}

void ConnectionListener::setError(int e) {
	this->mutex.lock();
	this->threadError = e;
	this->mutex.unlock();
}


} } }
