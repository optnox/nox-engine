/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/LocalClient.h>
#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/component/SyncIn.h>
#include <nox/logic/net/ping_convo.h>
#include <nox/logic/net/event/NetworkOutEvent.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/event/IEventBroadcaster.h>

#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>

#include <algorithm>


namespace nox { namespace logic { namespace net {

LocalClient::LocalClient(NetworkManager* netMgr, nox::logic::ILogicContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp):
	CommunicationController(netMgr, context, user, tcp, udp)
{
	this->getLogger().setName("LocalClient");

	this->addPacketHandler(protocol::PacketId::EVENT_PACKET,
		[&](const Packet& packet)
		{
			this->onNetworkEvent(packet);
		});

	this->addPacketHandler(protocol::PacketId::HEARTBEAT_SYN,
		[&](const Packet& /*packet*/)
		{
			/* Create a new PIngResponseConversation. We perform no uniqueness validation on the client side, nor
			 * do we care if the conversation fails. If the server, based on the heartbeat, decides that the connection
			 * is insufficient, he will disconnect us.
			 */
			protocol::PingResponseConversation *convo = new protocol::PingResponseConversation(
				this->getSocketUdp(), this->getContext(), this->getNetworkManager());
			this->addConversation(convo);
		});
}


bool LocalClient::update(const Duration& deltaTime)
{
	return CommunicationController::update(deltaTime);
}

void LocalClient::onNetworkEvent(const Packet& packet)
{
	bool defaultTranslated = false;
	auto event = this->translatePacket(packet, defaultTranslated);

	if (event != nullptr)
	{
		//What if Jimmy packs a NetworkOutEvent into another NetworkOutEvent?
		if (event->isType(NetworkOutEvent::ID))
		{
			this->getLogger().error().raw("PacketTranslator returned a NetworkOutEvent");
			return;
		}

		this->getLogger().verbose().format("Received event '%s'", event->getType().c_str());

		this->getContext()->getEventBroadcaster()->queueEvent(event);
	}
	else
	{
		this->getLogger().error().raw("Unable to handle incoming packet: empty packet.");
	}

}

} } }
