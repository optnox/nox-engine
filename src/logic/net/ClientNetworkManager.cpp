/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/ClientNetworkManager.h>
#include <nox/logic/net/BroadcastReceiver.h>
#include <nox/logic/net/join_convo.h>

#include <nox/logic/net/event/ConnectionStarted.h>
#include <nox/logic/net/event/ConnectionSuccess.h>
#include <nox/logic/net/event/ConnectionFailed.h>
#include <nox/logic/net/event/ClientConnected.h>
#include <nox/logic/net/event/ClientDisconnected.h>
#include <nox/logic/net/event/NetworkOutEvent.h>

namespace nox { namespace logic { namespace net {


ClientNetworkManager::ClientNetworkManager(IPacketTranslator *translator):
	NetworkManager(translator),
	joinConvo(nullptr),
	client(nullptr),
	broadcastRecv(nullptr),
	listenToBroadcasts(false),
	eventBroadcaster(nullptr),
	eventListener("ClientNetworkManager"),
	loopbackConnected(false)
{

}

ClientNetworkManager::~ClientNetworkManager()
{
	if (this->broadcastRecv)
	{
		delete this->broadcastRecv;
	}

	this->eventListener.stopListening();
}


bool ClientNetworkManager::connectToServer(std::string addr, Uint16 tcpPort,
										UserData &ud)
{
	this->serverHostname = addr;

	IPaddress ipaddr;
	SocketBase::resolveHost(addr, tcpPort, ipaddr);
	return connectToServer(ipaddr, ud);
}

bool ClientNetworkManager::connectToServer(IPaddress ipaddr, UserData &ud)
{
	this->serverHostname = octalIpAddress(ipaddr);

	ud.generateUserHash();

	if (!this->getContext())
	{
		// If we don't have a context yet, we also don't have a Logger.
		std::string msg = "ERROR: ClientNetworkManager::connectToServer called "
						  "before initialize() has been called. Hand the manager "
						  "over to the Logic-instance before attempting "
						  "to connect.";
		printf("%s\n", msg.c_str());
		throw std::runtime_error(msg);

		return false;
	}

	if (this->joinConvo || this->client)
	{
		this->getLogger().error().raw("Attempting to connect with an already "
									  "connected ClientNetworkManager.");
		return false;
	}

	this->loopbackConnected = false;
	this->onConnectionStarted();

	SocketTcp *tcp = new SocketTcp(this->getContext(), ipaddr);
	if (!tcp->initSocket() || !tcp->isGood())
	{
		this->onConnectionFailed(protocol::DcReason::UNKNOWN);

		this->getLogger().error().raw("Unable to connect to the server");
		delete tcp;
		return false;
	}

	this->joinConvo = new protocol::JoinServerConversation(this, getContext(), tcp, ud);

	if (!this->joinConvo->start())
	{
		this->onConnectionFailed(protocol::DcReason::UNKNOWN);

		this->getLogger().error().raw("Unable to start the join-conversation");
		delete this->joinConvo;
		this->joinConvo = nullptr;
		return false;
	}

	return true;
}

bool ClientNetworkManager::isConnected() const
{
	return (this->client != nullptr && this->serverHostname.length() > 0);
}

void ClientNetworkManager::setBroadcastListening(bool listen)
{
	this->listenToBroadcasts = listen;

	if (listen)
	{
		if (!this->broadcastRecv && this->getContext())
		{
			this->broadcastRecv = new BroadcastReceiver(this->getContext());
		}
	}
	else
	{
		if (this->broadcastRecv)
		{
			delete this->broadcastRecv;
			this->broadcastRecv = nullptr;
		}
	}
}


bool ClientNetworkManager::initialize(nox::logic::ILogicContext *context)
{
	if (!NetworkManager::initialize(context))
	{
		return false;
	}

	this->getLogger().setName("ClientNetworkManager");

	// Delayed initialization of BC listening, a context is needed
	if (this->listenToBroadcasts)
	{
		this->setBroadcastListening(true);
	}

	this->eventBroadcaster = context->getEventBroadcaster();

	this->eventListener.setup(this, this->eventBroadcaster);
	this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientConnected::ID);
	this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientDisconnected::ID);
	this->eventListener.addEventTypeToListenFor(nox::logic::net::NetworkOutEvent::ID);
	this->eventListener.startListening();

	return true;
}

void ClientNetworkManager::destroy()
{
	NetworkManager::destroy();
}

void ClientNetworkManager::update(const Duration& deltaTime)
{
	NetworkManager::update(deltaTime);

	handleJoinConvo();

	if (this->broadcastRecv)
	{
		this->broadcastRecv->update(deltaTime);
	}

	if (this->client)
	{
		this->client->update(deltaTime);

		if (!this->client->isGood())
		{
			this->onConnectionFailed(protocol::DcReason::CONNECTION_LOST);
		}
	}
}


void ClientNetworkManager::estimatePacketAge(Packet& packet, ClientId /*sender*/)
{
	// Ignore the sender entirely - the packet's creation time is already in server-time, so
	// we need only subtract the server's estimate of our system clock difference.
	const long int creationInServerTime = packet.getCreationTime();

	const ClientId localClientId = this->client->getUserData().getClientId();
	const long int serverClockDifference = this->getClientStats(localClientId)->getClockDifference();

	// The server's estimation of our clock difference is from the server's perspective, so the
	// difference must be added to our time.
	const long int creationInLocalTime = creationInServerTime + serverClockDifference;

	long int msAge = this->getNetworkTimeMilliSeconds() - creationInLocalTime;
	if (msAge < 0)
	{
		msAge = 0;
	}

	Duration age = Duration(std::chrono::milliseconds(msAge));

	packet.setEstimatedAge(age);
}


LocalClient* ClientNetworkManager::getClient()
{
	return client;
}


void ClientNetworkManager::onEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(ClientConnected::ID))
	{
		ClientConnected *con = (ClientConnected*)event.get();
		this->onClientConnected(con->getUserData());
	}
	else if (event->isType(ClientDisconnected::ID))
	{
		ClientDisconnected *dc = (ClientDisconnected*)event.get();
		this->onClientDisconnected(dc->getClientId());
	}
	else if (event->isType(NetworkOutEvent::ID))
	{
		if (this->client == nullptr)
		{
			return;
		}

		Packet pckt;
		pckt << (uint32_t)protocol::EVENT_PACKET;

		NetworkOutEvent *netEvent = ((NetworkOutEvent*)event.get());
		event::Event *contained = netEvent->getContainedEvent();
		TLProtocol protocol = netEvent->getProtocol();
		contained->serialize(&pckt);
		//send data
		this->client->sendPacket(&pckt, protocol);
	}
}


void ClientNetworkManager::sendSyncOutStateUpdates()
{
	if (this->loopbackConnected || this->client == nullptr)
	{
		return;
	}

	Packet packet;
	packet.setCreationTime(this->getNetworkTimeMilliSeconds());
	packet << static_cast<unsigned>(protocol::PacketId::SYNC_PACKET);

	Packet subPacket;
	subPacket.setCreationTime(this->getNetworkTimeMilliSeconds());
	this->client->packSyncOutUpdates(subPacket);

	packet << subPacket;

	this->client->sendPacket(&packet, TLProtocol::UDP);
}

void ClientNetworkManager::handleJoinConvo()
{
	if (this->joinConvo)
	{
		if (!this->joinConvo->update())
		{
			delete this->joinConvo;
			this->joinConvo = nullptr;
		}
		else if (this->joinConvo->isDone())
		{
			this->getLogger().info().raw("JoinConvo done");

			if (this->joinConvo->wasAccepted())
			{
				this->client = this->joinConvo->getLocalClient();
				this->client->setCustomPacketTranslator(this->packetTranslator);
				this->setLobbySize(this->joinConvo->getLobbySize());
				this->onClientConnected(this->client->getUserData());
				this->onConnectionSuccess();

				// Check if we are connected over loopback by examining the RemoteClient's of the
				// ServerNetworkManager (if it exists). This access of the ServerNetworkManager
				// should *never* occur unless the benefits reaped greatly outweigh the savagery.
				ServerNetworkManager *serverNetMgr = this->getContext()->getServerNetworkManager();
				if (serverNetMgr != nullptr)
				{
					std::string localHash = this->client->getUserData().getUserHash();
					auto remoteClients = serverNetMgr->getClients();
					for (auto rc : remoteClients)
					{
						if (rc->getUserData().getUserHash() == localHash)
						{
							// The connected server is running in this process.
							this->loopbackConnected = true;
						}
					}
				}

				this->getLogger().info().format("Connected to server (%s)", (this->loopbackConnected ? "LOOPBACK" : "REMOTE"));
			}
			else
			{
				this->getLogger().info().raw("Server rejected the join request");
				this->onConnectionFailed(protocol::DcReason::UNKNOWN);
			}

			delete this->joinConvo;
			this->joinConvo = nullptr;
		}
	}
}

void ClientNetworkManager::onConnectionStarted()
{
	auto evt = std::make_shared<ConnectionStarted>(this->serverHostname);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);
}

void ClientNetworkManager::onConnectionSuccess()
{
	UserData ud = this->client->getUserData();
	auto evt = std::make_shared<ConnectionSuccess>(this->serverHostname, ud);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);

	assert(this->isConnected());
}

void ClientNetworkManager::onConnectionFailed(protocol::DcReason dc,
										   std::string reason,
										   unsigned dur)
{
	if (this->client)
	{
		delete this->client;
		this->client = nullptr;
	}

	auto evt = std::make_shared<ConnectionFailed>(
			this->serverHostname, dc, reason, dur);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);

	this->serverHostname = "";

	assert(!this->isConnected());
}


} } }
