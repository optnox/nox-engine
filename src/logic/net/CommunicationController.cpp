/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/CommunicationController.h>
#include <nox/logic/net/Conversation.h>
#include <nox/logic/net/NetworkManager.h>
#include <nox/logic/net/component/SyncOut.h>
#include <nox/logic/net/component/SyncIn.h>

#include <nox/logic/event/IEventBroadcaster.h>

#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>

#include <nox/logic/net/event/NetworkOutEvent.h>

#include <string>


namespace nox { namespace logic { namespace net {

CommunicationController::CommunicationController(NetworkManager* netMgr, nox::logic::ILogicContext *context,
												 UserData user, SocketTcp *tcp, SocketUdp *udp):
	networkManager(netMgr),
	context(context),
	eventListener("CommunicationController"),
	udpSocket(udp),
	tcpSocket(tcp),
	userData(user),
	logger(context->createLogger()),
	customPacketTranslator(nullptr),
	lastSyncInPacketTimestamp(0)
{
	this->logger.setName("CommunicationController");

	this->eventListener.setup(this, this->context->getEventBroadcaster());
	this->eventListener.addEventTypeToListenFor(logic::world::ActorCreated::ID);
	this->eventListener.addEventTypeToListenFor(logic::world::ActorRemoved::ID);
	this->eventListener.startListening();

	this->packetHandlers[protocol::PacketId::SYNC_PACKET] = [&](const Packet& pkt) -> void
	{
		pkt.setOutStreamPosition(1);
		while (pkt.getOutStreamPosition() < pkt.getFieldCount())
		{
			Packet subpkt;
			pkt >> subpkt;
			this->onSyncInUpdate(subpkt);
		}
	};
}

CommunicationController::~CommunicationController()
{
	if (this->tcpSocket)
		delete this->tcpSocket;

	if (this->udpSocket)
		delete this->udpSocket;

	this->eventListener.stopListening();
}


bool CommunicationController::update(const Duration& /*deltaTime*/)
{
	/**
	 * Network data is parsed into Packet-objects by SocketBase::parseTrafficData(). The
	 * active Conversation instances must then be updated, as they have dibs on any
	 * packet they may desire. After that, we can assume that any packets left in the
	 * socket input buffers are free for arbitrary use.
	 */
	SocketBase *sockets[2] = { this->tcpSocket, this->udpSocket };
	bool good = true;

	for (int i=0; i<2; i++)
	{
		if (sockets[i] != nullptr)
		{
			if (!sockets[i]->isGood())
			{
				this->logger.error().format("%s socket disconnected",
						(sockets[i]->getTransportLayerProtocol() == TLProtocol::TCP ? "TCP" : "UDP"));
				good = false;
			}
			else if (!sockets[i]->parseTrafficData())
			{
				this->logger.error().format("Unable to parse %s data",
						(sockets[i]->getTransportLayerProtocol() == TLProtocol::TCP ? "TCP" : "UDP"));
			}
		}
	}

	if (good)
	{
		this->updateConversations();
		this->handleIncomingPackets();

	}

	return good;
}

bool CommunicationController::isGood()
{
	if (!this->udpSocket || !this->tcpSocket)
	{
		return false;
	}

	return (this->udpSocket && this->tcpSocket) &&
		   (this->tcpSocket->isGood() && this->udpSocket->isGood());
}


bool CommunicationController::sendPacket(const Packet *packet, TLProtocol protocol)
{
	if (!this->isGood())
	{
		return false;
	}

	// If the packet already has a creation time, an omniscient object (i.e., a NetworkManager) has already
	// administered the proper creation time adjusted for system time differences. However, if no creation time has been
	// assigned, we must include our current network time (i.e., system clock).
	if (packet->getCreationTime() == 0)
	{
		packet->setCreationTime(this->networkManager->getNetworkTimeMilliSeconds());
	}

	SocketBase *socket = this->getSocket(protocol);
	if (socket != nullptr)
	{
		return socket->sendPacket(packet);
	}

	return false;
}


const UserData& CommunicationController::getUserData() const
{
	return this->userData;
}

IPaddress CommunicationController::getIpAddress(TLProtocol protocol)
{
	SocketBase *socket = this->getSocket(protocol);

	if (socket != nullptr)
	{
		return socket->getIPAddress();
	}

	return IPaddress{0U, 0U};
}

app::log::Logger& CommunicationController::getLogger()
{
	return this->logger;
}

void CommunicationController::onEvent(const std::shared_ptr<logic::event::Event>& event)
{
	if (event->isType(logic::world::ActorCreated::ID))
	{
		auto created = (logic::world::ActorCreated*)event.get();
		logic::actor::Actor* actor = created->getActor();

		if (actor->isSynchronized())
		{
			this->onActorCreated(actor);
		}
	}
	else if (event->isType(logic::world::ActorRemoved::ID))
	{
		auto removed = (logic::world::ActorRemoved*)event.get();
		logic::actor::Actor* actor = removed->getActor();

		if (actor->isSynchronized())
		{
			this->onActorRemoved(actor);
		}
	}
}

void CommunicationController::setCustomPacketTranslator(IPacketTranslator* translator)
{
	this->customPacketTranslator = translator;
}

std::shared_ptr<logic::event::Event> CommunicationController::translatePacket(const Packet& packet, bool &defaultTranslated)
{
	defaultTranslated = false;

	if (packet.getFieldCount() < 2 || packet.getField(1)->getType() != FieldType::STRING)
	{
		return nullptr;
	}

	std::shared_ptr<logic::event::Event> event;

	std::string eventId;

	packet.setOutStreamPosition(1);
	packet >> eventId;
	packet.setOutStreamPosition(1);

	event = this->defaultPacketTranslator.translatePacket(eventId, &packet);
	if (event != nullptr)
	{
		defaultTranslated = true;
		return event;
	}

	packet.setOutStreamPosition(1);
	event = this->customPacketTranslator->translatePacket(eventId, &packet);
	return event;
}


long int CommunicationController::getLastSyncInPacketTimestamp()
{
	return this->lastSyncInPacketTimestamp;
}


void CommunicationController::packSyncOutUpdates(Packet& packet)
{
	packet << this->userData.getClientId();

	for (auto pair : this->syncOutComps)
	{
		SyncOut* out = pair.second;
		out->packNetworkData(packet);
	}
}

void CommunicationController::onSyncInUpdate(Packet& packet)
{
	this->lastSyncInPacketTimestamp = this->networkManager->getNetworkTimeMilliSeconds();

	try
	{
		packet.setOutStreamPosition(0);
		ClientId clientId = 0;

		while (packet.getOutStreamPosition() < packet.getFieldCount())
		{
			unsigned outStreamIdx = packet.getOutStreamPosition();
			if (packet.getField(outStreamIdx)->getType() == FieldType::UINT32)
			{
				packet >> clientId;
				this->networkManager->estimatePacketAge(packet, clientId);
			}
			else if (packet.seekToNext<std::string>(FieldType::STRING, SyncOut::PACKET_HEADER))
			{
				std::string header;
				unsigned packetIndex;
				SyncId syncId;

				packet >> header;
				packet >> packetIndex;
				packet >> syncId;

				if (this->syncInComps.count(syncId) != 0)
				{
					this->syncInComps[syncId]->unpackNetworkData(packet, packetIndex);
				}
				else
				{
					packet.seekToNext<std::string>(FieldType::STRING, SyncOut::PACKET_FOOTER);
					packet.setOutStreamPosition(packet.getOutStreamPosition() + 1);
				}
			}
			else
			{
				break;
			}
		}
	}
	catch (std::runtime_error ex)
	{
		this->logger.error().format("Unable to handle incoming SyncIn update states: %s", ex.what());
	}
}


bool CommunicationController::disconnect(TLProtocol protocol)
{
	SocketBase *socket = getSocket(protocol);
	if (socket == nullptr)
	{
		return false;
	}

	delete socket;

	if (protocol == TLProtocol::TCP)
	{
		this->tcpSocket = nullptr;
	}
	else
	{
		this->udpSocket = nullptr;
	}

	return true;
}


void CommunicationController::handleIncomingPackets()
{
	std::vector<Packet*> packets;
	Packet *packet = nullptr;

	while ((packet = this->tcpSocket->popPacket()) != nullptr)
	{
		packets.push_back(packet);
	}

	while ((packet = this->udpSocket->popPacket()) != nullptr)
	{
		packets.push_back(packet);
	}

	for (Packet *packet : packets)
	{
		try
		{
			protocol::PacketId pktId;
			unsigned int uId = 0;
			*packet >> uId;
			pktId = static_cast<protocol::PacketId>(uId);

			if (this->packetHandlers.count(pktId))
			{
				this->packetHandlers[pktId](*packet);
			}
		}
		catch (std::exception ex)
		{
			this->logger.error().format("handleIncomingPackets: %s", ex.what());
		}

		delete packet;
	}
}


void CommunicationController::onActorCreated(logic::actor::Actor* actor)
{
	SyncOut* out = actor->findComponent<SyncOut>();
	SyncIn* in = actor->findComponent<SyncIn>();

	if (!out && !in)
	{
		this->logger.error().raw("Synchronized actor created without SyncOut or SyncIn");
		return;
	}

	if (out)
	{
		this->syncOutComps[actor->getSyncId()] = out;
	}

	if (in)
	{
		this->syncInComps[actor->getSyncId()] = in;
	}
}

void CommunicationController::onActorRemoved(logic::actor::Actor* actor)
{
	this->syncOutComps.erase(actor->getSyncId());
	this->syncInComps.erase(actor->getSyncId());
}

void CommunicationController::onConversationCompleted(Conversation* /*convo*/)
{
}

void CommunicationController::onConversationFailed(Conversation* /*convo*/)
{
}


void CommunicationController::addConversation(Conversation* convo)
{
	this->conversations.push_back(convo);
}

void CommunicationController::updateConversations()
{
	auto it = this->conversations.begin();
	while (it != this->conversations.end())
	{
		bool discard = false;

		if (!(*it)->update())
		{
			this->logger.error().raw("Conversation failed");

			discard = true;
			this->onConversationFailed(*it);
		}
		else if ((*it)->isDone())
		{
			discard = true;
			this->onConversationCompleted(*it);
		}

		if (discard)
		{
			delete *it;
			it = this->conversations.erase(it);
		}
		else
		{
			it++;
		}
	}
}


nox::logic::ILogicContext* CommunicationController::getContext()
{
	return this->context;
}

NetworkManager* CommunicationController::getNetworkManager()
{
	return this->networkManager;
}


SocketBase* CommunicationController::getSocket(TLProtocol protocol)
{
	if (protocol == TLProtocol::TCP)
	{
		return this->tcpSocket;
	}
	else
	{
		return this->udpSocket;
	}

	return nullptr;
}

SocketUdp* CommunicationController::getSocketUdp()
{
	return this->udpSocket;
}

SocketTcp* CommunicationController::getSocketTcp()
{
	return this->tcpSocket;
}


void CommunicationController::addPacketHandler(protocol::PacketId id, PacketHandler handler)
{
	this->packetHandlers[id] = handler;
}


} } }
