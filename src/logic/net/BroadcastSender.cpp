/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/net/BroadcastSender.h>
#include <nox/logic/net/Packet.h>
#include <nox/logic/net/protocol.h>
#include <nox/logic/net/NetworkManager.h>


namespace nox { namespace logic { namespace net {


BroadcastSender::BroadcastSender(Uint16 port,
								 logic::ILogicContext *ctx,
								 NetworkManager *mgr):
	context(ctx),
	interval(SERVER_BROADCAST_INTERV),
	accumulatedTime(interval),
	tcpPort(port),
	socket(nullptr),
	netMgr(mgr),
	logger(context->createLogger())
{
	this->logger.setName("BroadcastSender");

	SocketUdp *udp = new SocketUdp(this->context, "255.255.255.255",
								   SERVER_BROADCAST_PORT_NO, 0);

	if (!udp->initSocket() || !udp->isGood())
	{
		this->logger.error().raw("Unable to create broadcast socket");
		delete udp;
	}
	else
	{
		this->socket = udp;
	}
}

BroadcastSender::~BroadcastSender()
{
	if (this->socket)
	{
		delete this->socket;
	}
}


void BroadcastSender::setBroadcastInterval(const Duration &interval)
{
	this->interval = interval;
}

void BroadcastSender::update(const Duration &deltaTime)
{
	if (!this->socket->isGood())
	{
		this->logger.error().raw("Socket turned sour");
		delete this->socket;
		this->socket = nullptr;
	}
	else
	{
		this->accumulatedTime += deltaTime;
		if (this->accumulatedTime >= this->interval)
		{
			// Do not keep overflowed time
			this->accumulatedTime = Duration(0);
			this->sendBroadcast();
		}
	}
}


void BroadcastSender::sendBroadcast()
{
	Packet pkt;

	pkt << (unsigned)protocol::SERVER_BROADCAST;
	pkt << this->context->getApplicationId();
	pkt << this->context->getApplicationVersionId();
	pkt << "A Game Server";		 // Server name
	pkt << this->netMgr->getClientsInLobbyCount();
	pkt << this->netMgr->getLobbySize();
	pkt << (unsigned)this->tcpPort; // Server TCP connection port

	if (!this->socket->sendPacket(&pkt))
	{
		this->logger.error().raw("Unable to send broadcast packet");
	}
}


} } }
