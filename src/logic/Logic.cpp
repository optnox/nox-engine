/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/Logic.h>
#include <nox/app/IApplicationContext.h>
#include <nox/logic/net/ServerNetworkManager.h>
#include <nox/logic/net/ClientNetworkManager.h>
#include <nox/logic/View.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/ActorIdentifier.h>
#include <nox/logic/event/EventManager.h>
#include <nox/logic/physics/PhysicsSimulation.h>
#include <nox/logic/world/WorldManager.h>
#include <cassert>

namespace nox { namespace logic
{

Logic::Logic():
	paused(true),
	eventManager(std::make_unique<event::EventManager>())
{
	// Load the settings.ini file asap. Other subsystems may depend on
	// it's contents before Logic::onInit() is called.
	// As we don't have a logger yet, onInit() will call
	// Logic::printSettingsIniErrors() to inform the user of any errors.
	this->settingsFile.replaceParse("settings.ini");
}

Logic::~Logic() = default;

app::IApplicationContext* Logic::getApplicationContext() const NOX_NOEXCEPT
{
	return this->getContext();
}

void Logic::addView(std::unique_ptr<View> view)
{
	view->initialize(this);
	this->viewContainer.push_back(std::move(view));
}

void Logic::setClientNetworkManager(std::unique_ptr<logic::net::ClientNetworkManager> clientManager)
{
	if (clientManager->initialize(this) == true)
	{
		this->clientManager = std::move(clientManager);
	}
}

void Logic::setServerNetworkManager(std::unique_ptr<logic::net::ServerNetworkManager> serverManager)
{
	if (serverManager->initialize(this) == true)
	{
		this->serverManager = std::move(serverManager);
	}
}

logic::net::ClientNetworkManager* Logic::getClientNetworkManager()
{
	return this->clientManager.get();
}

logic::net::ServerNetworkManager* Logic::getServerNetworkManager()
{
	return this->serverManager.get();
}

void Logic::pause(bool pause)
{
	if (this->paused != pause)
	{
		this->paused = pause;

		if (this->paused == true)
		{
			this->log.verbose().raw("Paused.");
		}
		else
		{
			this->log.verbose().raw("Unpaused.");
		}
	}
}

bool Logic::isPaused() const
{
	return this->paused;
}

event::IEventBroadcaster* Logic::getEventBroadcaster()
{
	return this->eventManager.get();
}

physics::PhysicsSimulation* Logic::getPhysics()
{
	return this->physics.get();
}

world::WorldManager* Logic::getWorldManager()
{
	return this->world.get();
}

app::resource::IResourceAccess* Logic::getResourceAccess()
{
	return this->getApplicationContext()->getResourceAccess();
}

app::storage::IDataStorage* Logic::getDataStorage()
{
	return this->getApplicationContext()->getDataStorage();
}

app::log::Logger Logic::createLogger()
{
	auto appContext = this->getApplicationContext();
	if (appContext != nullptr)
	{
		return appContext->createLogger();
	}
	else
	{
		return app::log::Logger();
	}
}

const View* Logic::findControllingView(const actor::ActorIdentifier& actorId) const
{
	auto viewIt = this->viewContainer.cbegin();
	const View* view = nullptr;

	while (view == nullptr && viewIt != this->viewContainer.cend())
	{
		const auto controlledActor = (*viewIt)->getControlledActor();

		if (controlledActor && controlledActor->getId() != actorId)
		{
			view = viewIt->get();
		}

		++viewIt;
	}

	return view;
}

void Logic::setPhysics(std::unique_ptr<physics::PhysicsSimulation> physics)
{
	this->physics = std::move(physics);
}

void Logic::setWorldManager(std::unique_ptr<world::WorldManager> worldHandler)
{
	this->world = std::move(worldHandler);
}

void Logic::destroy()
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->destroy();
	}

	this->viewContainer.clear();

	this->log.verbose().raw("Destroyed.");
}

void Logic::onInit()
{
	this->log = this->getApplicationContext()->createLogger();
	this->log.setName("NoxLogic");

	// The iniFile has already been initialized, but its errors persists.
	this->printSettingsIniErrors();

	this->log.verbose().format("AppId: %s", this->getApplicationId().c_str());
	this->log.verbose().format("Version: %s", this->getApplicationVersionId().c_str());
	this->log.verbose().raw("Initialized.");
}

void Logic::printSettingsIniErrors()
{
	if (this->settingsFile.hasError())
	{
		this->log.warning().raw("Errors when parsing file 'settings.ini'");

		while (this->settingsFile.hasError())
		{
			std::string error = this->settingsFile.getError();
			this->log.error().raw("Settings.ini: " + error);
		}
	}
}

nox::util::IniFile& Logic::getSettingsFile()
{
	return this->settingsFile;
}

std::string Logic::getApplicationId() const
{
	return this->settingsFile.getSection("NOX")->getString("AppId", "NOX Application");
}

std::string Logic::getApplicationVersionId() const
{
	return this->settingsFile.getSection("NOX")->getString("AppVersion", "Undefined Version");
}



void Logic::onUpdate(const Duration& deltaTime)
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->update(deltaTime);
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}

	if (this->isPaused() == false)
	{
		if (this->physics != nullptr)
		{
			this->physics->onUpdate(deltaTime);
			this->physics->updateLighting();
		}

		if (this->world != nullptr)
		{
			this->world->update(deltaTime);
		}
	}

	if (this->serverManager != nullptr)
	{
		this->serverManager->update(deltaTime);
	}

	if (this->clientManager != nullptr)
	{
		this->clientManager->update(deltaTime);
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}
}

void Logic::onUpdateFinished(const Duration& alpha)
{
	if (this->physics != nullptr)
	{
		this->physics->onSyncState(alpha);
	}
}

void Logic::onSuccess()
{
	this->destroy();
}

void Logic::onFail()
{
	this->destroy();
}

void Logic::onAbort()
{
	this->destroy();
}

} }
