/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/event/IEventListener.h>
#include <nox/logic/event/EventManager.h>
#include <nox/util/algorithm.h>
#include <algorithm>
#include <cassert>

namespace nox { namespace logic { namespace event
{

const Event::IdType EventManager::BROADCAST_COMPLETE_EVENT = "nox.event.broadcast_complete";

EventManager::EventManager():
	activeQueueIndex(0)
{
}

void EventManager::queueEvent(std::shared_ptr<Event> event)
{
	std::lock_guard<std::mutex> lock(this->eventQueueMutex);
	this->eventQueues[this->activeQueueIndex].push(std::move(event));
}

void EventManager::queueEvent(Event::IdType eventId)
{
	this->queueEvent(std::make_shared<Event>(std::move(eventId)));
}

void EventManager::triggerEvent(const std::shared_ptr<Event>& event)
{
	this->broadcastEvent(event);
}

void EventManager::triggerEvent(Event::IdType eventId)
{
	this->triggerEvent(std::make_shared<Event>(std::move(eventId)));
}

void EventManager::broadcastEvent(const std::shared_ptr<Event>& event)
{
	const Event::IdType eventType = event->getType();

	ListenerList& listenerList = this->eventListeners[eventType];

	std::lock_guard<std::recursive_mutex> listLock(listenerList.listMutex);

	listenerList.occupied = true;

	for (ListenerHandle& listenerHandle : listenerList.listeners)
	{
		if (listenerHandle.isValid())
		{
			listenerHandle.broadcastEvent(event);
		}
	}

	listenerList.occupied = false;

	while (listenerList.additionQueue.empty() == false)
	{
		listenerList.listeners.push_back(ListenerHandle(listenerList.additionQueue.front()));
		listenerList.additionQueue.pop();
	}

	if (listenerList.listenerRemoved == true)
	{
		auto removalPredicate =
			[](const ListenerHandle& listenerData)
			{
				return listenerData.isValid() == false;
			};

		util::removeFastIf(listenerList.listeners, removalPredicate);

		listenerList.listenerRemoved = false;
	}
}

void EventManager::broadcastEvents()
{
	std::unique_lock<std::mutex> queueLock(this->eventQueueMutex);

	const unsigned int broadcastQueueIndex = this->activeQueueIndex;
	this->activeQueueIndex = (this->activeQueueIndex + 1) % NUM_QUEUES;

	EventQueue& broadcastQueue = this->eventQueues[broadcastQueueIndex];

	queueLock.unlock();

	while (broadcastQueue.empty() == false)
	{
		const std::shared_ptr<Event> event = broadcastQueue.front();
		broadcastQueue.pop();

		this->broadcastEvent(event);
	}
	
	this->triggerEvent(std::make_shared<Event>(BROADCAST_COMPLETE_EVENT));
}

void EventManager::addListener(IEventListener* listener, const Event::IdType& type, const std::string& name)
{
	assert(listener != nullptr);

	ListenerList& listenerList = this->eventListeners[type];

	std::unique_lock<std::recursive_mutex> listLock(listenerList.listMutex);

	auto listenerIt = std::find_if(
		listenerList.listeners.begin(),
		listenerList.listeners.end(),
		[listener](const ListenerHandle& listenerHandle)
		{
			return listenerHandle.handlesListener(listener);
		}
	);

	if (listenerIt == listenerList.listeners.end())
	{
		if (listenerList.occupied == true)
		{
			listenerList.additionQueue.push(ListenerData(listener, name));
		}
		else
		{
			listenerList.listeners.push_back(ListenerHandle(listener, name));
		}
	}
}

void EventManager::removeListener(IEventListener* listener, const Event::IdType& type)
{
	assert(listener != nullptr);

	ListenerList& listenerList = this->eventListeners[type];

	std::unique_lock<std::recursive_mutex> listLock(listenerList.listMutex);

	const auto listenerHandleCompare =
		[listener](const ListenerHandle& listenerHandle)
		{
			return listenerHandle.handlesListener(listener);
		};


	if (listenerList.occupied == true)
	{
		auto removeIt = std::find_if(listenerList.listeners.begin(), listenerList.listeners.end(), listenerHandleCompare);

		if (removeIt != listenerList.listeners.end())
		{
			removeIt->invalidate();
			listenerList.listenerRemoved = true;
		}
	}
	else
	{
		util::removeFastIf(listenerList.listeners, listenerHandleCompare);
	}
}

void EventManager::clearEventQueue()
{
	std::lock_guard<std::mutex> queueLock(this->eventQueueMutex);

	for (EventQueue& queue : this->eventQueues)
	{
		while (queue.empty() == false)
		{
			queue.pop();
		}
	}
}

bool EventManager::hasQueuedEvents() const
{
	std::lock_guard<std::mutex> queueLock(this->eventQueueMutex);

	const auto queuedEventsPredicate =
		[](const EventQueue& queue)
		{
			return queue.empty() == false;
		};

	return std::any_of(this->eventQueues.begin(), this->eventQueues.end(), queuedEventsPredicate);
}

EventManager::ListenerHandle::ListenerHandle(IEventListener* listener, const std::string& name):
	listener(listener),
	name(name)
{}

EventManager::ListenerHandle::ListenerHandle(const ListenerData& data):
	listener(data.listener),
	name(data.name)
{
}

void EventManager::ListenerHandle::broadcastEvent(const std::shared_ptr<Event>& event)
{
	this->listener->onEvent(event);
}

void EventManager::ListenerHandle::invalidate()
{
	this->listener = nullptr;
}

bool EventManager::ListenerHandle::handlesListener(const IEventListener* listener) const
{
	return this->listener == listener;
}

bool EventManager::ListenerHandle::isValid() const
{
	return this->listener != nullptr;
}

}
}
}
