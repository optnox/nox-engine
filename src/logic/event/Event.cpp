/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/event/Event.h>
#include <typeinfo>

namespace nox { namespace logic
{
namespace event
{

Event::Event(IdType type):
	type(std::move(type)),
	serIpacket(nullptr),
	serOpacket(nullptr),
	serIstream(nullptr),
	serOstream(nullptr)
{
}

Event::~Event() = default;

Event::Event(Event&& other) NOX_NOEXCEPT:
	type(std::move(other.type))
{
}

Event& Event::operator=(Event&& other)
{
	this->type = std::move(other.type);
	return *this;
}

bool Event::serialize(nox::logic::net::Packet *packet) const
{
	bool success = true;
	this->serOpacket = packet;

	try
	{
		this->doSerialize();
	}
	catch (std::exception ex)
	{
		success = false;
	}

	this->serOpacket = nullptr;
	return success;
}

bool Event::deSerialize(const nox::logic::net::Packet *packet)
{
	bool success = true;
	this->serIpacket = packet;

	try
	{
		this->doDeSerialize();
	}
	catch (std::runtime_error ex)
	{
		success = false;
		printf("exception caught: %s\n", ex.what());
	}

	this->serIpacket = nullptr;
	return success;
}



bool Event::serialize(std::ostream& eventData) const
{
	bool success = true;
	this->serOstream = &eventData;

	try
	{
		this->doSerialize();
	}
	catch (std::exception ex)
	{
		success = false;
	}
	this->serOstream = nullptr;
	return success;
}

bool Event::deSerialize(std::istream& eventData)
{
	bool success = true;
	this->serIstream = &eventData;

	try
	{
		this->doDeSerialize();
	}
	catch (std::exception ex)
	{
		success = false;
	}

	this->serIstream = nullptr;
	return success;
}


void Event::doSerialize() const
{
	(*this) << this->type;
}

void Event::doDeSerialize()
{
	(*this) >> this->type;
}



static const std::string INV_STRM_MSG =
					"Invalid number of valid stream objects in "
					"event (de)serialization";
#define ASSERT_STREAM_VALIDITY()					\
	int _evt_strm_cnt = 0;						  \
	_evt_strm_cnt += (bool)(this->serIpacket);	  \
	_evt_strm_cnt += (bool)(this->serOpacket);	  \
	_evt_strm_cnt += (bool)(this->serIstream);	  \
	_evt_strm_cnt += (bool)(this->serOstream);	  \
	if (_evt_strm_cnt != 1)						 \
		throw std::runtime_error(INV_STRM_MSG);	 \

#define STD_SERIALIZE_STREAM(_stdstrm, _pkt, _OP)   \
	ASSERT_STREAM_VALIDITY();					   \
													\
	if (this->_pkt != nullptr)					  \
	{											   \
		*this->_pkt _OP val;						\
	}											   \
	else if (this->_stdstrm != nullptr)			 \
	{											   \
		*this->_stdstrm _OP val;					\
	}											   \
	return *this;

const Event& Event::operator>>(uint8_t& val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(bool& val) const
{
	uint8_t tmp = 0;
	*this >> tmp;
	val = (bool)tmp;
	return *this;
}

const Event& Event::operator>>(uint32_t &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(int32_t &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(uint64_t &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(int64_t &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(float &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(std::string &val) const
{
	STD_SERIALIZE_STREAM(serIstream, serIpacket, >>);
}

const Event& Event::operator>>(std::vector<uint8_t> &val) const
{
	ASSERT_STREAM_VALIDITY();

	if (this->serIpacket != nullptr)
	{
		*this->serIpacket >> val;
	}
	else if (this->serIstream != nullptr)
	{
		unsigned length = 0;

		*this->serIstream >> length;
		val.clear();
		val.resize(length);

		for (unsigned i=0; i<length; i++)
		{
			uint8_t b = 0;
			*this->serIstream >> b;
			val.push_back(b);
		}
	}

	return *this;
}

const Event& Event::operator>>(logic::net::UserData &val) const
{
	ASSERT_STREAM_VALIDITY();

	if (this->serIpacket != nullptr)
	{
		if (!val.deSerialize(this->serIpacket))
		{
			throw std::runtime_error("Malformed serialized UserData contents: unable to deserialize");
		}
	}
	else
	{
		throw std::runtime_error("Unable to write nox::logic::net::UserData to std::istream");
	}

	return *this;
}


const Event& Event::operator<<(uint8_t val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(bool val) const
{
	uint8_t tmp = (uint8_t)val;
	*this << tmp;
	return *this;
}

const Event& Event::operator<<(uint32_t val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(int32_t val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(uint64_t val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(int64_t val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(float val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(std::string val) const
{
	STD_SERIALIZE_STREAM(serOstream, serOpacket, <<);
}

const Event& Event::operator<<(std::vector<uint8_t> val) const
{
	ASSERT_STREAM_VALIDITY();

	if (this->serOpacket != nullptr)
	{
		*this->serOpacket << val;
	}
	else if (this->serOstream != nullptr)
	{
		unsigned length = (unsigned)val.size();

		*this->serOstream << length;

		for (uint8_t u8 : val)
		{
			*this->serOstream << u8;
		}
	}

	return *this;
}

const Event& Event::operator<<(logic::net::UserData val) const
{
	ASSERT_STREAM_VALIDITY();

	if (this->serOpacket != nullptr)
	{
		val.serialize(this->serOpacket);
	}
	else
	{
		throw std::runtime_error("Unable to write nox::logic::net::UserData to std::ostream");
	}

	return *this;
}

#undef ASSERT_STREAM_VALIDITY
#undef STD_SERIALIZE_STREAM


}
} }
