/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/graphics/actor/ActorSprite.h>

#include <nox/app/graphics/2d/SpriteRenderNode.h>
#include <nox/util/json_utils.h>

namespace nox { namespace logic { namespace graphics
{

const ActorSprite::IdType ActorSprite::NAME = "Sprite";

const ActorSprite::IdType& ActorSprite::getName() const
{
	return ActorSprite::NAME;
}

std::vector<ActorSprite::IdType> ActorSprite::findInheritedNames() const
{
	std::vector<IdType> inheritedNames({ ActorGraphics::NAME });

	const std::vector<IdType> deeperInheritedNames = this->ActorGraphics::findInheritedNames();
	inheritedNames.insert(inheritedNames.end(), deeperInheritedNames.begin(), deeperInheritedNames.end());

	return inheritedNames;
}


void ActorSprite::initialize(const Json::Value& componentJsonObject)
{
	this->spriteRenderNode = std::make_shared<app::graphics::SpriteRenderNode>();

	ActorGraphics::initialize(componentJsonObject);

	this->spriteName = componentJsonObject.get("spriteName", "").asString();
	this->setActiveSprite(this->spriteName);

	this->renderLevel = componentJsonObject.get("renderLevel", 0).asUInt();
	this->center = util::parseJsonVec(componentJsonObject["center"], glm::vec2(0.5f, 0.5f));
	this->spriteRenderNode->setRenderLevel(this->renderLevel);
	this->spriteRenderNode->setCenter(this->center);
}

std::shared_ptr<app::graphics::SceneGraphNode> ActorSprite::createSceneNode()
{
	return this->spriteRenderNode;
}

void ActorSprite::serialize(Json::Value& componentObject)
{
	ActorGraphics::serialize(componentObject);

	componentObject["spriteName"] = this->spriteName;
	componentObject["renderLevel"] = this->renderLevel;

	componentObject["center"] = Json::objectValue;
	componentObject["center"]["x"] = this->center.x;
	componentObject["center"]["y"] = this->center.y;
}

void ActorSprite::setRenderLevel(unsigned int renderLevel)
{
	if (this->renderLevel != renderLevel)
	{
		this->renderLevel = renderLevel;
		this->spriteRenderNode->setRenderLevel(renderLevel);
	}
}

void ActorSprite::addSprite(const std::string& spriteName)
{
	this->spriteRenderNode->addSprite(spriteName);
}

void ActorSprite::setActiveSprite(const std::string& spriteName)
{
	if (spriteName.empty() == false)
	{
		this->addSprite(spriteName);
		this->spriteRenderNode->setActiveSprite(spriteName);
	}
}


void ActorSprite::setCenter(const glm::vec2& center)
{
	this->center = center;
	this->spriteRenderNode->setCenter(this->center);
}

void ActorSprite::onColorChange(const glm::vec4& color)
{
	this->spriteRenderNode->setColor(color);
}

void ActorSprite::onLightMultiplierChange(const float lightMultiplier)
{
	this->spriteRenderNode->setLightMultiplier(lightMultiplier);
}

void ActorSprite::onEmissiveLightChange(const float emissiveLight)
{
	this->spriteRenderNode->setEmissiveLight(emissiveLight);
}

} } }
