/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/graphics/actor/ActorGraphics.h>

#include <nox/logic/ILogicContext.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/util/json_utils.h>

#include <glm/gtc/matrix_transform.hpp>
#include <cassert>

namespace nox { namespace logic { namespace graphics
{

const ActorGraphics::IdType ActorGraphics::NAME = "Graphics";

ActorGraphics::~ActorGraphics() = default;

const ActorGraphics::IdType& ActorGraphics::getName() const
{
	return NAME;
}

void ActorGraphics::initialize(const Json::Value& componentJsonObject)
{
	Json::Value scaleValue = componentJsonObject.get("scale", Json::nullValue);
	this->scale = util::parseJsonVec(scaleValue, glm::vec2(1.0f));
	this->rotation = componentJsonObject.get("rotation", 0.0f).asFloat();
	this->flipScale = util::parseJsonVec(componentJsonObject["flipScale"], glm::vec2(1.0f));
	this->renderingEnabled = true;

	this->actorTransformNode = std::make_shared<app::graphics::TransformationNode>();
	this->renderTransformNode = std::make_shared<app::graphics::TransformationNode>();
	this->actorTransformNode->addChild(this->renderTransformNode);

	Json::Value colorValue = componentJsonObject["color"];
	this->color.r = colorValue.get("r", 1.0f).asFloat();
	this->color.g = colorValue.get("g", 1.0f).asFloat();
	this->color.b = colorValue.get("b", 1.0f).asFloat();
	this->color.a = colorValue.get("a", 1.0f).asFloat();

	this->lightMultiplier = componentJsonObject.get("lightMultiplier", 1.0f).asFloat();
	this->emissiveLight = componentJsonObject.get("emissiveLight", 0.0f).asFloat();

	this->onColorChange(this->color);
	this->onLightMultiplierChange(this->lightMultiplier);
	this->onEmissiveLightChange(this->emissiveLight);

	this->transformComponent = nullptr;
}

void ActorGraphics::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(actor::TransformChange::ID))
	{
		this->updateActorTransform();
	}
}

void ActorGraphics::setColor(const glm::vec4& color)
{
	this->color = color;
	this->onColorChange(this->color);
}

void ActorGraphics::onCreate()
{
	this->transformComponent = this->getOwner()->findComponent<actor::Transform>();
	assert(this->transformComponent != nullptr);

	std::shared_ptr<app::graphics::SceneGraphNode> renderNode = this->createSceneNode();
	assert(renderNode);

	this->renderTransformNode->addChild(renderNode);

	this->updateRenderTransform();
}

void ActorGraphics::onDestroy()
{
}

void ActorGraphics::setRotation(const float rotation)
{
	if (this->rotation != rotation)
	{
		this->rotation = rotation;
		this->updateRenderTransform();
	}
}

void ActorGraphics::setScale(const glm::vec2 &scale)
{
	if (this->scale != scale)
	{
		this->scale = scale;
		this->updateRenderTransform();
	}
}

void ActorGraphics::setPosition(const glm::vec2& position)
{
	if (this->position != position)
	{
		this->position = position;
		this->updateRenderTransform();
	}
}

const glm::vec2& ActorGraphics::getScale() const
{
	return this->scale;
}

void ActorGraphics::enableRendering(const bool enable)
{
	if (enable == true && this->renderingEnabled == false)
	{
		if (this->isActive() == true)
		{
			this->broadcastSceneNodeCreation();
		}

		this->renderingEnabled = true;
	}

	if (enable == false && this->renderingEnabled == true)
	{
		if (this->isActive() == true)
		{
			this->broadcastSceneNodeRemoval();
		}

		this->renderingEnabled = false;
	}
}

void ActorGraphics::setFlippedHorizontal(const bool flipped)
{
	if (flipped == true)
	{
		this->flipScale.x = -1.0f;
	}
	else
	{
		this->flipScale.x = 1.0f;
	}

	this->updateRenderTransform();
}

void ActorGraphics::setFlippedVertical(const bool flipped)
{
	if (flipped == true)
	{
		this->flipScale.y = -1.0f;
	}
	else
	{
		this->flipScale.y = 1.0f;
	}

	this->updateRenderTransform();
}

void ActorGraphics::onDeactivate()
{
	if (this->renderingEnabled == true)
	{
		this->broadcastSceneNodeRemoval();
	}
}

void ActorGraphics::onActivate()
{
	if (this->renderingEnabled == true)
	{
		this->broadcastSceneNodeCreation();
	}
}

void ActorGraphics::serialize(Json::Value &componentObject)
{
	componentObject["scale"] = util::writeJsonVec(this->scale);
	componentObject["rotation"] = this->rotation;
	componentObject["flipScale"] = util::writeJsonVec(this->flipScale);

	componentObject["color"] = Json::objectValue;
	componentObject["color"] = util::writeJsonVec(this->color, util::JsonVecCoordType::COLOR);
}

void ActorGraphics::updateRenderTransform()
{
	glm::mat4 transformation;
	transformation = glm::translate(transformation, glm::vec3(this->position, 0.0f));
	transformation = glm::rotate(transformation, this->rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	transformation = glm::scale(transformation, glm::vec3(this->flipScale, 1.0f));
	transformation = glm::scale(transformation, glm::vec3(this->scale, 1.0f));
	this->renderTransformNode->setTransform(transformation);
}

void ActorGraphics::onColorChange(const glm::vec4& /*color*/)
{
}

void ActorGraphics::onLightMultiplierChange(const float /*lightMultiplier*/)
{
}

void ActorGraphics::onEmissiveLightChange(const float /*emissiveLight*/)
{
}

void ActorGraphics::setActorPositionOffset(const glm::vec2& offset)
{
	this->actorPositionOffset = offset;

	this->updateActorTransform();
}

void ActorGraphics::updateActorTransform()
{
	const glm::vec2& position = this->transformComponent->getPosition();
	const glm::vec2& scale = this->transformComponent->getScale();
	const float rotation = this->transformComponent->getRotation();

	glm::mat4 transformMatrix;
	transformMatrix = glm::translate(transformMatrix, glm::vec3(this->actorPositionOffset, 0.0f));
	transformMatrix = glm::translate(transformMatrix, glm::vec3(position, 0.0f));
	transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));

	this->actorTransformNode->setTransform(transformMatrix);
}

void ActorGraphics::broadcastSceneNodeCreation()
{
	auto sceneNodeAddEvent = std::make_shared<SceneNodeEdited>(this->actorTransformNode, SceneNodeEdited::Action::CREATE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeAddEvent);
}

void ActorGraphics::broadcastSceneNodeRemoval()
{
	auto sceneNodeRemoveEvent = std::make_shared<SceneNodeEdited>(this->actorTransformNode, SceneNodeEdited::Action::REMOVE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeRemoveEvent);
}

} } }
