/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/graphics/actor/ActorLight.h>

#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/event/IEventBroadcaster.h>
#include <nox/logic/ILogicContext.h>
#include <nox/logic/physics/PhysicsSimulation.h>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/constants.hpp>

namespace nox { namespace logic { namespace graphics
{

const ActorLight::IdType ActorLight::NAME = "Light";

ActorLight::~ActorLight() = default;

void ActorLight::initialize(const Json::Value& componentJsonObject)
{
	const auto& lightListJson = componentJsonObject["lights"];
	const auto lightNameList = lightListJson.getMemberNames();

	for (const auto& lightName : lightNameList)
	{
		const auto& lightJson = lightListJson[lightName];
		auto newLight = std::make_shared<app::graphics::Light>();

		Json::Value colorJson = lightJson.get("color", Json::nullValue);
		glm::vec4 lightColor;

		lightColor.r = colorJson.get("r", 1.0f).asFloat();
		lightColor.g = colorJson.get("g", 0.0f).asFloat();
		lightColor.b = colorJson.get("b", 0.0f).asFloat();
		lightColor.a = colorJson.get("a", 1.0f).asFloat();

		newLight->setColor(lightColor);

		Json::Value positionOffsetJson = lightJson.get("positionOffset", Json::nullValue);
		glm::vec2 positionOffset;

		positionOffset.x = positionOffsetJson.get("x", 0.0f).asFloat();
		positionOffset.y = positionOffsetJson.get("y", 0.0f).asFloat();

		newLight->setPositionOffset(positionOffset);

		newLight->setName(lightName);
		newLight->setRange(lightJson.get("range", 10.0f).asFloat());
		newLight->setAlphaFallOff(lightJson.get("alphaFallOff", 0.0f).asFloat());
		newLight->setConeAngleRadian(lightJson.get("coneSizeRadian", 0.0f).asFloat());
		newLight->setRadialLight(lightJson.get("isRadial", true).asBool());
		newLight->setCastShadows(lightJson.get("castShadows", false).asBool());
		newLight->setZHeight(lightJson.get("zheight", newLight->getZHeight()).asFloat());
		newLight->shouldRotateWithPlayer = lightJson.get("shouldRotateWithPlayer", false).asBool();
		newLight->rotationOffset = lightJson.get("rotationOffset", 0.f).asFloat();

		// Read the light attenuation - graphics::Light's default value as default
		glm::vec3 defaultAtten = newLight->getAttenuation();
		glm::vec3 atten;
		Json::Value attenJson = lightJson.get("attenuation", Json::nullValue);
		atten.y = attenJson.get("constant", defaultAtten.x).asFloat();
		atten.x = attenJson.get("linear", defaultAtten.y).asFloat();
		atten.z = attenJson.get("quadratic", defaultAtten.z).asFloat();
		newLight->setAttenuation(atten);

		const auto defaultRenderHint = static_cast<unsigned int>(app::graphics::Light::RenderHint::DYNAMIC);
		newLight->renderHint = static_cast<app::graphics::Light::RenderHint>(lightJson.get("renderHint", defaultRenderHint).asUInt());

		bool isEffect = lightJson.get("isEffect", false).asBool();
		newLight->setEffect(isEffect);

		auto lightNode = std::make_shared<app::graphics::LightRenderNode>();
		lightNode->setLight(newLight);
		this->lightNodes.push_back(std::move(lightNode));
	}
}

void ActorLight::onCreate()
{
	this->physics = this->getLogicContext()->getPhysics();
	assert(this->physics != nullptr);

	this->transformComponent = this->getOwner()->findComponent<actor::Transform>();
	assert(this->transformComponent != nullptr);

	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		float rotation = transformComponent->getRotation();
		glm::vec2 position = glm::rotate(light->getPositionOffset(), rotation);
		light->setPosition(transformComponent->getPosition() + position);
		light->setCastDirection(rotation);
	}
}

const ActorLight::IdType& ActorLight::getName() const
{
	return NAME;
}

void ActorLight::serialize(Json::Value& componentObject)
{
	componentObject["lights"] = Json::arrayValue;
	Json::Value& lightArray = componentObject["lights"];

	for (const auto& lightNode : this->lightNodes)
	{
		const auto& light = lightNode->getLight();
		const auto& name = light->getName();

		lightArray[name]["color"]["r"] = light->getColor().r;
		lightArray[name]["color"]["g"] = light->getColor().g;
		lightArray[name]["color"]["b"] = light->getColor().b;
		lightArray[name]["color"]["a"] = light->getColor().a;
		lightArray[name]["positionOffset"]["x"] = light->getPositionOffset().x;
		lightArray[name]["positionOffset"]["y"] = light->getPositionOffset().y;
		lightArray[name]["range"] = light->getRange();
		lightArray[name]["alphaFallOff"] = light->getAlphaFallOff();
		lightArray[name]["coneSizeRadian"] = light->getConeAngleRadian();
		lightArray[name]["isRadial"] = light->isRadialLight();
		lightArray[name]["shouldRotateWithPlayer"] = light->shouldRotateWithPlayer;
		lightArray[name]["renderHint"] = (unsigned int)light->renderHint;
	}
}

void ActorLight::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(actor::TransformChange::ID))
	{
		auto transformEvent = static_cast<actor::TransformChange*>(event.get());

		for (auto& lightNode : this->lightNodes)
		{
			auto& light = lightNode->getLight();

			float rotation = transformEvent->getRotation();
			glm::vec2 position = glm::rotate(light->getPositionOffset() * transformEvent->getScale(), rotation);
			light->setPosition(transformEvent->getPosition() + position);
			light->setCastDirection(rotation);
		}
	}
}

void ActorLight::setCastDirection(float direction)
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		light->setCastDirection(direction);

		if (light->shouldRotateWithPlayer == true)
		{
			const glm::vec2 offset = light->getPositionOffset();
			const glm::vec2 rotatedOffset = glm::rotate(offset, direction);
			light->setPosition(this->transformComponent->getPosition() + rotatedOffset);
		}
	}
}

void ActorLight::setLightRange(const std::string& lightName, float range)
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		if (light->getName() == lightName)
		{
			light->setRange(range);
		}
	}
}

float ActorLight::getLightRange(const std::string& lightName) const
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		if (light->getName() == lightName)
		{
			return light->getRange();
		}
	}

	return 0.0f;
}

void ActorLight::disableLights()
{
	for (auto& lightNode : this->lightNodes)
	{
		this->disableLight(lightNode);
	}
}

void ActorLight::disableLight(const std::string& lightName)
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		if (light->getName() == lightName)
		{
			this->disableLight(lightNode);
		}
	}
}

void ActorLight::enableLights()
{
	for (auto& lightNode : this->lightNodes)
	{
		this->enableLight(lightNode);
	}
}

void ActorLight::enableLight(const std::string &lightName)
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		if (light->getName() == lightName)
		{
			this->enableLight(lightNode);
		}
	}
}

app::graphics::Light* ActorLight::getLightByName(const std::string& lightName)
{
	for (auto& lightNode : this->lightNodes)
	{
		auto& light = lightNode->getLight();

		if (light->getName() == lightName)
		{
			return light.get();
		}
	}

	return nullptr;
}

app::graphics::Light* ActorLight::getLightByIndex(int index)
{
	if (index >= 0 && index < (int)this->lightNodes.size())
	{
		return this->lightNodes[(size_t)index]->getLight().get();
	}

	return nullptr;
}

int ActorLight::getNumberOfLights() const
{
	return (int)this->lightNodes.size();
}

void ActorLight::onActivate()
{
	this->enableLights();
}

void ActorLight::onDeactivate()
{
	this->disableLights();
}

void ActorLight::broadcastLightCreation(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode)
{
	auto sceneNodeAddEvent = std::make_shared<SceneNodeEdited>(lightNode, SceneNodeEdited::Action::CREATE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeAddEvent);
}

void ActorLight::broadcastLightRemoval(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode)
{
	auto sceneNodeRemoveEvent = std::make_shared<SceneNodeEdited>(lightNode, SceneNodeEdited::Action::REMOVE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeRemoveEvent);
}

void ActorLight::enableLight(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode)
{
	auto& light = lightNode->getLight();

	if (light->isEnabled() == false)
	{
		light->setEnabled(true);

		this->physics->addLight(light);
		this->broadcastLightCreation(lightNode);
	}
}

void ActorLight::disableLight(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode)
{
	auto& light = lightNode->getLight();

	if (light->isEnabled() == true)
	{
		light->setEnabled(false);

		this->physics->removeLight(light);
		this->broadcastLightRemoval(lightNode);
	}
}

} } }
