/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_PROCESS_PROCESS_H_
#define NOX_PROCESS_PROCESS_H_

#include <nox/util/process/Implementation.h>
#include <memory>

namespace nox
{

namespace process
{

/**
 * Representation of a process running in a ProcessManager.
 */
class Process
{
public:
	Process(std::unique_ptr<Implementation> implementation);
	virtual ~Process();

	void init();
	void update(const Duration& deltaTime);
	void pause();
	void resume();
	void failExecution();
	void succeedExecution();
	void abortExecution();

	/**
	 * Set a successor process.
	 * The successor process will be started if this process succeeds.
	 * @param successor Successor process.
	 */
	void setSuccessorProcess(std::unique_ptr<Process> successor);

	/**
	 * Get the successor process.
	 * @return The successor process, or empty unique_ptr if it has no successor.
	 */
	std::unique_ptr<Process> getSuccessor();

	void onSucceed();
	void onFail();
	void onAbort();
	void onUpdateFinished(const Duration& alpha);

	bool isInitialized() const;
	bool isRunning() const;
	bool isAlive() const;
	bool didSucceed() const;
	bool didFail() const;
	bool wasAborted() const;
	bool hasSuccessor() const;

private:
	/**
	 * State of the process.
	 */
	enum class State
	{
		UNINITIALIZED,//!< The process is not initialized yet.
		RUNNING,	  //!< The process is executing.
		PAUSED,	   //!< The process's execution is paused.
		SUCCEEDED,	//!< The process's execution succeeded.
		FAILED,	   //!< The process's execution failed.
		ABORTED	   //!< The process was aborted by user.
	};

	State state;
	std::unique_ptr<Implementation> implementation;
	std::unique_ptr<Process> successorProcess;
};

}
}

#endif
