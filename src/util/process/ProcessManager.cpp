/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/util/process/ProcessManager.h>
#include <queue>
#include <algorithm>
#include "Process.h"

namespace nox
{

namespace process
{

ProcessManager::ProcessManager():
	updateMode(UpdateMode::ONCE_PER_UPDATE)
{
}

ProcessManager::ProcessManager(const UpdateMode updateMode, const Duration& deltaTime):
	updateMode(updateMode),
	fixedDeltaTime(deltaTime),
	accumulatedTime(0)
{
}

ProcessManager::ProcessManager(ProcessManager&& other):
	processes(std::move(other.processes)),
	updateMode(other.updateMode),
	fixedDeltaTime(other.fixedDeltaTime),
	accumulatedTime(other.accumulatedTime)
{
}

ProcessManager& ProcessManager::operator=(ProcessManager&& other)
{
	this->processes = std::move(other.processes);
	this->updateMode = other.updateMode;
	this->fixedDeltaTime = other.fixedDeltaTime;
	this->accumulatedTime = other.accumulatedTime;

	return *this;
}

ProcessManager::~ProcessManager() = default;

void ProcessManager::updateProcesses(const Duration& deltaTime)
{
	bool updateHasRun = false;
	auto alpha = Duration(std::chrono::seconds(0));

	if (this->updateMode == UpdateMode::ONCE_PER_UPDATE)
	{
		this->runUpdate(deltaTime);

		updateHasRun = true;
	}
	else if (this->updateMode == UpdateMode::FIXED_DELTATIME)
	{
		this->accumulatedTime += deltaTime;

		if (this->accumulatedTime >= fixedDeltaTime)
		{
			updateHasRun = true;
		}

		while (this->accumulatedTime >= this->fixedDeltaTime)
		{
			this->runUpdate(this->fixedDeltaTime);

			this->accumulatedTime -= this->fixedDeltaTime;
		}

		alpha = Duration{this->accumulatedTime.count() / this->fixedDeltaTime.count()};
	}

	if (updateHasRun == true)
	{
		for (auto& process : this->processes)
		{
			if (process->isRunning() == true)
			{
				process->onUpdateFinished(alpha);
			}
		}
	}

	std::queue<std::unique_ptr<Process>> successorProcesses;

	auto processIt = this->processes.begin();

	while (processIt != this->processes.end())
	{
		std::unique_ptr<Process>& process = *processIt;

		if (process->isInitialized() == false)
		{
			process->init();
		}

		if (process->isAlive() == false)
		{
			if (process->didSucceed() == true)
			{
				process->onSucceed();
				if (process->hasSuccessor() == true)
				{
					successorProcesses.push(process->getSuccessor());
				}
			}
			else if (process->didFail() == true)
			{
				process->onFail();
			}
			else if (process->wasAborted() == true)
			{
				process->onAbort();
			}

			processIt = this->processes.erase(processIt);
		}
		else
		{
			processIt++;
		}
	}

	while (successorProcesses.empty() == false)
	{
		this->processes.push_back(std::move(successorProcesses.front()));
		successorProcesses.pop();
	}
}

void ProcessManager::runUpdate(const Duration& deltaTime)
{
	for (auto& process : this->processes)
	{
		if (process->isRunning() == true)
		{
			process->update(deltaTime);
		}
	}
}

ProcessManager::ProcessHandle ProcessManager::startProcess(std::unique_ptr<Implementation> implementation)
{
	auto process = std::unique_ptr<Process>(new Process(std::move(implementation)));
	ProcessHandle handle(process.get(), this);
	this->processes.push_back(std::move(process));

	return handle;
}

bool ProcessManager::abortProcess(const ProcessHandle& handle)
{
	bool foundProcess = false;
	auto processIt = this->processes.begin();

	while (foundProcess == false && processIt != this->processes.end())
	{
		Process* process = (*processIt).get();
		if (handle.isHandleToProcess(process) == true)
		{
			foundProcess = true;
			process->abortExecution();
		}
	}

	return foundProcess;
}

void ProcessManager::abortAllProcesses()
{
	for (const std::unique_ptr<Process>& process : this->processes)
	{
		if (process->isInitialized() == true)
		{
			if (process->didSucceed() == true)
			{
				process->onSucceed();
			}
			else if (process->didFail() == true)
			{
				process->onFail();
			}
			else if (process->wasAborted() == true)
			{
				process->onAbort();
			}
			else
			{
				process->abortExecution();
				process->onAbort();
			}
		}
	}

	this->processes.clear();
}

ProcessManager::ProcessHandle::ProcessHandle():
	process(nullptr),
	manager(nullptr)
{
}

ProcessManager::ProcessHandle::ProcessHandle(Process* process, ProcessManager* manager):
	process(process),
	manager(manager)
{
}

bool ProcessManager::ProcessHandle::isHandleToProcess(const Process* process) const
{
	return this->process == process;
}

bool ProcessManager::ProcessHandle::isValid() const
{
	if (this->process == nullptr)
	{
		return false;
	}
	else if (this->manager == nullptr)
	{
		return false;
	}
	else if (this->manager->isRunning(this->process) == false)
	{
		return false;
	}
	else
	{
		return true;
	}
}

ProcessManager::ProcessHandle ProcessManager::chainProcess(const ProcessHandle& chainToHandle, std::unique_ptr<Implementation> implementation)
{
	if (chainToHandle.isValid())
	{
		auto process = std::unique_ptr<Process>(new Process(std::move(implementation)));
		ProcessHandle handle(process.get(), this);
		chainToHandle.process->setSuccessorProcess(std::move(process));

		return handle;
	}
	else
	{
		return ProcessHandle();
	}
}

bool ProcessManager::hasProcess(const Process* process) const
{
	auto processIt = std::find_if(
			this->processes.begin(),
			this->processes.end(),
			[process] (const std::unique_ptr<Process>& containedProcess)
			{
				return (process == containedProcess.get());
			}
	);

	return (processIt != this->processes.end());
}

bool ProcessManager::isRunning(const Process* process) const
{
	if (this->hasProcess(process) && process->isRunning())
	{
		return true;
	}
	else
	{
		return false;
	}
}

}
}
