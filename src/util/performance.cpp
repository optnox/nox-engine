/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/util/performance.h>
#include <boost/filesystem.hpp>
#include <fstream>
#include <algorithm>
#include <iomanip>


namespace nox { namespace util { namespace perf
{

/*
==================
DataSet
==================
*/
DataSet::DataSet(std::string nm, DataType dtype, AccumulationType atype, int curFrame):
	name(nm),
	accType(atype),
	dataType(dtype),
	startFrame(curFrame),
	curEntry(0.0),
	curEntryCount(0),
	clockRunning(false)
{

}

std::string DataSet::getName() const
{
	return this->name;
}

AccumulationType DataSet::getAccumulationType() const
{
	return this->accType;
}

DataType DataSet::getDataType() const
{
	return this->dataType;
}

void DataSet::onNewFrame()
{
	if (this->accType == AccumulationType::FRAME)
	{
		bool restartTimer = false;
		if (this->dataType == DataType::TIME && this->clockRunning)
		{
			this->stopTimer();
			restartTimer = true;
		}

		if (this->accType == AccumulationType::AVG_FRAME && this->curEntryCount > 0)
		{
			this->curEntry /= (long double)this->curEntryCount;
		}

		// Stash away the current entry
		this->entries.push_back(this->curEntry);
		this->curEntry = 0.0;
		this->curEntryCount = 0;

		// Restart the timer if it was already running.
		if (restartTimer)
		{
			this->startTimer();
		}
	}
}

bool DataSet::startTimer()
{
	if (this->dataType != DataType::TIME)
	{
		// Not allowed to take time in non-TIME DataSet.
		return false;
	}

	if (!this->clockRunning)
	{
		this->clock.start();
		this->clockRunning = true;
		return true;
	}

	return false;
}

bool DataSet::stopTimer()
{
	if (this->dataType != DataType::TIME)
	{
		return false;
	}

	if (this->clockRunning)
	{
		this->clockRunning = false;
		Duration dur = this->clock.getElapsedTime();

		auto microSecDuration = std::chrono::duration_cast<std::chrono::microseconds>(dur);
		const long double microSec = (long double)microSecDuration.count();
		const long double seconds = microSec / (1000.0 * 1000.0);

		this->curEntry += seconds;
		return true;
	}

	return false;
}

bool DataSet::addCountValue(long double value)
{
	if (this->dataType != DataType::COUNT)
	{
		return false;
	}

	this->curEntryCount++;
	this->curEntry += value;
	return true;
}

void DataSet::writeToFile(std::ofstream& file)
{
	// "myDataSet" data.time acc.frame
	// 0 14.3
	// 1 20.9
	// 3 5.4
	// ...
	file << "\"" << this->name << "\"";
	file << " ";
	file << (this->dataType == DataType::TIME ? "data.time" : "data.count");
	file << " ";
	file << (this->accType == AccumulationType::GLOBAL ? "acc.global" : "acc.frame");
	file << std::endl;

	if (this->accType == AccumulationType::FRAME)
	{
		int frame = this->startFrame;
		for (auto entry : this->entries)
		{
			file << frame << " " << entry << std::endl;
			frame++;
		}

		file << frame << " " << this->curEntry << std::endl;
	}
	else
	{
		file << this->curEntry << std::endl;
	}

	file << std::endl;
}


/*
==================
PerformanceMonitor
==================
*/
PerformanceMonitor::PerformanceMonitor(std::string name):
	name(name)
{
}

PerformanceMonitor::~PerformanceMonitor()
{
	for (auto it = this->dataSets.begin(); it != this->dataSets.end(); it++)
	{
		delete it->second;
	}
}

bool PerformanceMonitor::addDataSet(std::string setName, DataType dtype, AccumulationType atype)
{
	if (this->dataSets.count(setName) != 0)
	{
		return false;
	}

	/**
	 * TIME can not be averaged out in any way.
	 */
	if (dtype == DataType::TIME && atype == AccumulationType::AVG_FRAME)
	{
		return false;
	}

	DataSet *set = new DataSet(setName, dtype, atype, PerformanceManager::frameNo);
	this->dataSets[setName] = set;
	return true;
}

bool PerformanceMonitor::add(std::string setName, long double value)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->addCountValue(value);
	}

	return false;
}

bool PerformanceMonitor::startTimer(std::string setName)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->startTimer();
	}

	return false;
}

bool PerformanceMonitor::stopTimer(std::string setName)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->stopTimer();
	}

	return false;
}

void PerformanceMonitor::recordEvent(std::string name)
{
	std::replace(name.begin(), name.end(), '\n', ' ');
	std::replace(name.begin(), name.end(), '"', '\'');

	this->events.push_back(std::pair<std::string,int>(name, PerformanceManager::frameNo));
}

bool PerformanceMonitor::dumpDataToFile(std::string filePath)
{
	std::ofstream file(filePath);
	file << std::fixed << std::setprecision(24);

	if (!file.is_open() || !file.good())
	{
		return false;
	}

	file << this->name << std::endl << std::endl;

	if (this->dataSets.size())
	{
		file << "DATA" << std::endl;
		for (auto pair : this->dataSets)
		{
			pair.second->writeToFile(file);
		}
	}

	if (this->events.size())
	{
		file << "EVENT" << std::endl;
		for (auto pair : this->events)
		{
			file << pair.second << " " << pair.first << std::endl;
		}
	}

	return true;
}

void PerformanceMonitor::onNewFrame()
{
	for (auto pair : this->dataSets)
	{
		pair.second->onNewFrame();
	}
}


/*
==================
PerformanceManager
==================
*/
int PerformanceManager::frameNo = 0;
std::list<std::shared_ptr<PerformanceMonitor>> PerformanceManager::monitors;


std::shared_ptr<PerformanceMonitor> PerformanceManager::createMonitor(std::string name)
{
	std::shared_ptr<PerformanceMonitor> monitor(new PerformanceMonitor(name));
	monitors.push_back(monitor);

	return monitor;
}

void PerformanceManager::onFrameBegin()
{
	PerformanceManager::frameNo++;

	for (auto perfMon : monitors)
	{
		perfMon->onNewFrame();
	}
}

void PerformanceManager::dumpPerformanceData(std::string directory)
{
	boost::filesystem::path path(directory);
	boost::filesystem::create_directories(path);

	for (auto pm : monitors)
	{
		boost::filesystem::path pmFile(path);

		// Someone should be mutilated for thinking this overload is sensible.
		// Either way, it appends the path.
		pmFile /= (pm->name + ".perfmon");

		pm->dumpDataToFile(std::string(pmFile.string().c_str()));
	}
}

}
} }
