/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Content in this file is a derived work of the original work at http://www.codeproject.com/Articles/188256/A-Simple-Wildcard-Matching-Function
 * under the Code Project Open License (CPOL) 1.02 (http://www.codeproject.com/info/cpol10.aspx)
 *
 * Prettiness improvements and bug fixes have been made in 2013.
 */

#include <nox/util/string_utils.h>

namespace nox { namespace util
{

bool matchWildcard(const char *pattern, const char *str)
{

	// We have a special case where string is empty ("") and the mask is "*".
	// We need to handle this too. So we can't test on !*pattern here.
	// The loop breaks when the match string is exhausted.
	while (*pattern)
	{
		// Single wildcard character
		if (*pattern == '?')
		{
			// Matches any character except empty string
			if (!*str)
			{
				return false;
			}
			// OK next
			++pattern;
			++str;
		}
		else if (*pattern == '*')
		{
			// Need to do some tricks.

			// 1. The wildcard * is ignored.
			//	So just an empty string matches. This is done by recursion.
			//	  Because we eat one character from the match string, the
			//	  recursion will stop.
			if (matchWildcard(pattern + 1, str))
			{
				// we have a match and the * replaces no other character
				return true;
			}
			// 2. Chance we eat the next character and try it again, with a
			//	wildcard * match. This is done by recursion. Because we eat
			//	  one character from the string, the recursion will stop.
			if (*str && matchWildcard(pattern, str +1))
			{
				return true;
			}
			// Nothing worked with this wildcard.
			return false;
		}
		else
		{
			// Standard compare of 2 chars. Note that *pattern might be 0
			// here, but then we never get a match on *str that has always
			// a value while inside this loop.
			if (*(pattern++) != *(str++))
			{
				return false;
			}
		}
	}

	// Have a match? Only if both are at the end...
	return !(*pattern) && !(*str);
}

} }
