//? required 130

in vec4 fragColor;

out vec4 outputFragColor;

void main()
{
	outputFragColor = fragColor;
}
