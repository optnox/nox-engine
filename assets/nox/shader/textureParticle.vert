//? required 130

in vec2 vertex;
in float rotation;
in float size;
in vec4 color;
in float scale;

uniform mat4 modelViewProjectionMatrix;
uniform vec2 textureCoord;
uniform vec2 textureSize;

out vec2 fragTexCoord;
out vec2 fragTexSize;
out vec4 fragColor;

void main()
{
	fragColor = color;
	fragTexCoord = textureCoord;
	fragTexSize = textureSize;

	gl_PointSize = size * scale /50;
	gl_Position = modelViewProjectionMatrix * vec4(vertex, 0.0, 1.0);
}
