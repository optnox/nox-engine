//? required 130

in vec2 vertex;
in float rotation;
in float size;
in vec4 color;

uniform mat4 modelViewProjectionMatrix;

const vec4 debugColor = vec4(0.9, 0.7, 0.0, 0.003);
out vec4 fragColor;

void main()
{
	fragColor = debugColor;

	gl_Position = modelViewProjectionMatrix * vec4(vertex, 0.0, 1.0);
}
