//? required 130

in vec2 fragTexCoord;
in vec2 fragTexSize;
in vec4 fragColor;

uniform sampler2D tex;

out vec4 outputFragColor;

void main()
{
	vec2 realTexCoord = fragTexCoord + (gl_PointCoord * fragTexSize);
	outputFragColor = texture(tex, realTexCoord) * fragColor;
}
