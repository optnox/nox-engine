//? required 130

in vec2 vertex;
in float rotation;
in float size;
in vec4 color;

uniform mat4 modelViewProjectionMatrix;

out vec4 fragColor;

void main()
{
	fragColor = vec4(1.0, 0.0, 1.0, 1.0);

	gl_PointSize = size;
	gl_Position = modelViewProjectionMatrix * vec4(vertex, 0.0, 1.0);
}
